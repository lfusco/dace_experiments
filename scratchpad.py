import dace

N = dace.symbol('N', dace.int64)
K = dace.symbol('K', dace.int64)

# @dace.program
# def program(what: dace.bool, then: dace.bool, arr: dace.float64[N*K]):
#     if what:
#         for i in dace.map[0:N]:
#             if then:
#                 M = K
#             else:
#                 M = K/2
#             for j in dace.map[0:M]:
#                 arr[i*M+j] *= 2
#     else:
#         for i in dace.map[0:N]:
#             if then:
#                 M = K
#             else:
#                 M = K/2
#             for j in dace.map[0:M]:
#                 arr[i*M+j] *= 2

@dace.program
def program(flag: dace.bool, arr: dace.float64[N]):
    arr += 2
    if flag:
        arr *= 2
    arr += 2

sdfg = program.to_sdfg()
sdfg.save('program.sdfg')