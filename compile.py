import dace

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('map_split.sdfg')

# sdfg.apply_gpu_transformations()

sdfg.compile()