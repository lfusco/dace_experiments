import dace
from dace.sdfg.utils import fuse_states

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('optimized_fused.sdfgz')
from dace.transformation.auto import auto_optimize as aopt

sdfg = aopt.auto_optimize(sdfg, dace.DeviceType.CPU)
sdfg.simplify()
fused = fuse_states(sdfg)
print(f'fused {fused} states')
sdfg.simplify()
sdfg.save('optimized_fused2.sdfgz', compress=True)