
#include <cuda_runtime.h>
#include <dace/dace.h>

struct t_nh_metrics {
    int __f2dace_SA_bdy_halo_c_blk_d_0_s_5344;
    int __f2dace_SA_bdy_halo_c_idx_d_0_s_5343;
    int __f2dace_SA_bdy_mflx_e_blk_d_0_s_5351;
    int __f2dace_SA_bdy_mflx_e_idx_d_0_s_5350;
    int __f2dace_SA_coeff1_dwdz_d_0_s_5272;
    int __f2dace_SA_coeff1_dwdz_d_1_s_5273;
    int __f2dace_SA_coeff1_dwdz_d_2_s_5274;
    int __f2dace_SA_coeff2_dwdz_d_0_s_5275;
    int __f2dace_SA_coeff2_dwdz_d_1_s_5276;
    int __f2dace_SA_coeff2_dwdz_d_2_s_5277;
    int __f2dace_SA_coeff_gradekin_d_0_s_5269;
    int __f2dace_SA_coeff_gradekin_d_1_s_5270;
    int __f2dace_SA_coeff_gradekin_d_2_s_5271;
    int __f2dace_SA_coeff_gradp_d_0_s_5282;
    int __f2dace_SA_coeff_gradp_d_1_s_5283;
    int __f2dace_SA_coeff_gradp_d_2_s_5284;
    int __f2dace_SA_coeff_gradp_d_3_s_5285;
    int __f2dace_SA_d2dexdz2_fac1_mc_d_0_s_5312;
    int __f2dace_SA_d2dexdz2_fac1_mc_d_1_s_5313;
    int __f2dace_SA_d2dexdz2_fac1_mc_d_2_s_5314;
    int __f2dace_SA_d2dexdz2_fac2_mc_d_0_s_5315;
    int __f2dace_SA_d2dexdz2_fac2_mc_d_1_s_5316;
    int __f2dace_SA_d2dexdz2_fac2_mc_d_2_s_5317;
    int __f2dace_SA_d_exner_dz_ref_ic_d_0_s_5309;
    int __f2dace_SA_d_exner_dz_ref_ic_d_1_s_5310;
    int __f2dace_SA_d_exner_dz_ref_ic_d_2_s_5311;
    int __f2dace_SA_ddqz_z_full_d_0_s_5147;
    int __f2dace_SA_ddqz_z_full_d_1_s_5148;
    int __f2dace_SA_ddqz_z_full_d_2_s_5149;
    int __f2dace_SA_ddqz_z_full_e_d_0_s_5242;
    int __f2dace_SA_ddqz_z_full_e_d_1_s_5243;
    int __f2dace_SA_ddqz_z_full_e_d_2_s_5244;
    int __f2dace_SA_ddqz_z_half_d_0_s_5245;
    int __f2dace_SA_ddqz_z_half_d_1_s_5246;
    int __f2dace_SA_ddqz_z_half_d_2_s_5247;
    int __f2dace_SA_ddxn_z_full_c_d_0_s_5212;
    int __f2dace_SA_ddxn_z_full_c_d_1_s_5213;
    int __f2dace_SA_ddxn_z_full_c_d_2_s_5214;
    int __f2dace_SA_ddxn_z_full_d_0_s_5209;
    int __f2dace_SA_ddxn_z_full_d_1_s_5210;
    int __f2dace_SA_ddxn_z_full_d_2_s_5211;
    int __f2dace_SA_ddxn_z_full_v_d_0_s_5215;
    int __f2dace_SA_ddxn_z_full_v_d_1_s_5216;
    int __f2dace_SA_ddxn_z_full_v_d_2_s_5217;
    int __f2dace_SA_ddxn_z_half_c_d_0_s_5221;
    int __f2dace_SA_ddxn_z_half_c_d_1_s_5222;
    int __f2dace_SA_ddxn_z_half_c_d_2_s_5223;
    int __f2dace_SA_ddxn_z_half_e_d_0_s_5218;
    int __f2dace_SA_ddxn_z_half_e_d_1_s_5219;
    int __f2dace_SA_ddxn_z_half_e_d_2_s_5220;
    int __f2dace_SA_ddxt_z_full_c_d_0_s_5227;
    int __f2dace_SA_ddxt_z_full_c_d_1_s_5228;
    int __f2dace_SA_ddxt_z_full_c_d_2_s_5229;
    int __f2dace_SA_ddxt_z_full_d_0_s_5224;
    int __f2dace_SA_ddxt_z_full_d_1_s_5225;
    int __f2dace_SA_ddxt_z_full_d_2_s_5226;
    int __f2dace_SA_ddxt_z_full_v_d_0_s_5230;
    int __f2dace_SA_ddxt_z_full_v_d_1_s_5231;
    int __f2dace_SA_ddxt_z_full_v_d_2_s_5232;
    int __f2dace_SA_ddxt_z_half_c_d_0_s_5236;
    int __f2dace_SA_ddxt_z_half_c_d_1_s_5237;
    int __f2dace_SA_ddxt_z_half_c_d_2_s_5238;
    int __f2dace_SA_ddxt_z_half_e_d_0_s_5233;
    int __f2dace_SA_ddxt_z_half_e_d_1_s_5234;
    int __f2dace_SA_ddxt_z_half_e_d_2_s_5235;
    int __f2dace_SA_ddxt_z_half_v_d_0_s_5239;
    int __f2dace_SA_ddxt_z_half_v_d_1_s_5240;
    int __f2dace_SA_ddxt_z_half_v_d_2_s_5241;
    int __f2dace_SA_deepatmo_t1ifc_d_0_s_5364;
    int __f2dace_SA_deepatmo_t1ifc_d_1_s_5365;
    int __f2dace_SA_deepatmo_t1mc_d_0_s_5362;
    int __f2dace_SA_deepatmo_t1mc_d_1_s_5363;
    int __f2dace_SA_deepatmo_t2mc_d_0_s_5366;
    int __f2dace_SA_deepatmo_t2mc_d_1_s_5367;
    int __f2dace_SA_dgeopot_mc_d_0_s_5159;
    int __f2dace_SA_dgeopot_mc_d_1_s_5160;
    int __f2dace_SA_dgeopot_mc_d_2_s_5161;
    int __f2dace_SA_dzgpot_mc_d_0_s_5359;
    int __f2dace_SA_dzgpot_mc_d_1_s_5360;
    int __f2dace_SA_dzgpot_mc_d_2_s_5361;
    int __f2dace_SA_enhfac_diffu_d_0_s_5164;
    int __f2dace_SA_exner_exfac_d_0_s_5286;
    int __f2dace_SA_exner_exfac_d_1_s_5287;
    int __f2dace_SA_exner_exfac_d_2_s_5288;
    int __f2dace_SA_exner_ref_mc_d_0_s_5300;
    int __f2dace_SA_exner_ref_mc_d_1_s_5301;
    int __f2dace_SA_exner_ref_mc_d_2_s_5302;
    int __f2dace_SA_fbk_dom_volume_d_0_s_5208;
    int __f2dace_SA_geopot_agl_d_0_s_5153;
    int __f2dace_SA_geopot_agl_d_1_s_5154;
    int __f2dace_SA_geopot_agl_d_2_s_5155;
    int __f2dace_SA_geopot_agl_ifc_d_0_s_5156;
    int __f2dace_SA_geopot_agl_ifc_d_1_s_5157;
    int __f2dace_SA_geopot_agl_ifc_d_2_s_5158;
    int __f2dace_SA_geopot_d_0_s_5150;
    int __f2dace_SA_geopot_d_1_s_5151;
    int __f2dace_SA_geopot_d_2_s_5152;
    int __f2dace_SA_hmask_dd3d_d_0_s_5166;
    int __f2dace_SA_hmask_dd3d_d_1_s_5167;
    int __f2dace_SA_inv_ddqz_z_full_d_0_s_5248;
    int __f2dace_SA_inv_ddqz_z_full_d_1_s_5249;
    int __f2dace_SA_inv_ddqz_z_full_d_2_s_5250;
    int __f2dace_SA_inv_ddqz_z_full_e_d_0_s_5179;
    int __f2dace_SA_inv_ddqz_z_full_e_d_1_s_5180;
    int __f2dace_SA_inv_ddqz_z_full_e_d_2_s_5181;
    int __f2dace_SA_inv_ddqz_z_full_v_d_0_s_5182;
    int __f2dace_SA_inv_ddqz_z_full_v_d_1_s_5183;
    int __f2dace_SA_inv_ddqz_z_full_v_d_2_s_5184;
    int __f2dace_SA_inv_ddqz_z_half_d_0_s_5185;
    int __f2dace_SA_inv_ddqz_z_half_d_1_s_5186;
    int __f2dace_SA_inv_ddqz_z_half_d_2_s_5187;
    int __f2dace_SA_inv_ddqz_z_half_e_d_0_s_5188;
    int __f2dace_SA_inv_ddqz_z_half_e_d_1_s_5189;
    int __f2dace_SA_inv_ddqz_z_half_e_d_2_s_5190;
    int __f2dace_SA_inv_ddqz_z_half_v_d_0_s_5191;
    int __f2dace_SA_inv_ddqz_z_half_v_d_1_s_5192;
    int __f2dace_SA_inv_ddqz_z_half_v_d_2_s_5193;
    int __f2dace_SA_mask_mtnpoints_d_0_s_5200;
    int __f2dace_SA_mask_mtnpoints_d_1_s_5201;
    int __f2dace_SA_mask_mtnpoints_g_d_0_s_5202;
    int __f2dace_SA_mask_mtnpoints_g_d_1_s_5203;
    int __f2dace_SA_mask_prog_halo_c_d_0_s_5368;
    int __f2dace_SA_mask_prog_halo_c_d_1_s_5369;
    int __f2dace_SA_mixing_length_sq_d_0_s_5197;
    int __f2dace_SA_mixing_length_sq_d_1_s_5198;
    int __f2dace_SA_mixing_length_sq_d_2_s_5199;
    int __f2dace_SA_nudge_c_blk_d_0_s_5341;
    int __f2dace_SA_nudge_c_idx_d_0_s_5339;
    int __f2dace_SA_nudge_e_blk_d_0_s_5342;
    int __f2dace_SA_nudge_e_idx_d_0_s_5340;
    int __f2dace_SA_nudgecoeff_vert_d_0_s_5352;
    int __f2dace_SA_ovlp_halo_c_blk_d_0_s_5348;
    int __f2dace_SA_ovlp_halo_c_blk_d_1_s_5349;
    int __f2dace_SA_ovlp_halo_c_dim_d_0_s_5345;
    int __f2dace_SA_ovlp_halo_c_idx_d_0_s_5346;
    int __f2dace_SA_ovlp_halo_c_idx_d_1_s_5347;
    int __f2dace_SA_pg_edgeblk_d_0_s_5337;
    int __f2dace_SA_pg_edgeidx_d_0_s_5336;
    int __f2dace_SA_pg_exdist_d_0_s_5321;
    int __f2dace_SA_pg_vertidx_d_0_s_5338;
    int __f2dace_SA_rayleigh_vn_d_0_s_5163;
    int __f2dace_SA_rayleigh_w_d_0_s_5162;
    int __f2dace_SA_rho_ref_corr_d_0_s_5318;
    int __f2dace_SA_rho_ref_corr_d_1_s_5319;
    int __f2dace_SA_rho_ref_corr_d_2_s_5320;
    int __f2dace_SA_rho_ref_mc_d_0_s_5303;
    int __f2dace_SA_rho_ref_mc_d_1_s_5304;
    int __f2dace_SA_rho_ref_mc_d_2_s_5305;
    int __f2dace_SA_rho_ref_me_d_0_s_5306;
    int __f2dace_SA_rho_ref_me_d_1_s_5307;
    int __f2dace_SA_rho_ref_me_d_2_s_5308;
    int __f2dace_SA_scalfac_dd3d_d_0_s_5165;
    int __f2dace_SA_slope_angle_d_0_s_5204;
    int __f2dace_SA_slope_angle_d_1_s_5205;
    int __f2dace_SA_slope_azimuth_d_0_s_5206;
    int __f2dace_SA_slope_azimuth_d_1_s_5207;
    int __f2dace_SA_theta_ref_ic_d_0_s_5295;
    int __f2dace_SA_theta_ref_ic_d_1_s_5296;
    int __f2dace_SA_theta_ref_ic_d_2_s_5297;
    int __f2dace_SA_theta_ref_mc_d_0_s_5289;
    int __f2dace_SA_theta_ref_mc_d_1_s_5290;
    int __f2dace_SA_theta_ref_mc_d_2_s_5291;
    int __f2dace_SA_theta_ref_me_d_0_s_5292;
    int __f2dace_SA_theta_ref_me_d_1_s_5293;
    int __f2dace_SA_theta_ref_me_d_2_s_5294;
    int __f2dace_SA_tsfc_ref_d_0_s_5298;
    int __f2dace_SA_tsfc_ref_d_1_s_5299;
    int __f2dace_SA_vertidx_gradp_d_0_s_5322;
    int __f2dace_SA_vertidx_gradp_d_1_s_5323;
    int __f2dace_SA_vertidx_gradp_d_2_s_5324;
    int __f2dace_SA_vertidx_gradp_d_3_s_5325;
    int __f2dace_SA_vwind_expl_wgt_d_0_s_5168;
    int __f2dace_SA_vwind_expl_wgt_d_1_s_5169;
    int __f2dace_SA_vwind_impl_wgt_d_0_s_5170;
    int __f2dace_SA_vwind_impl_wgt_d_1_s_5171;
    int __f2dace_SA_wgtfac_c_d_0_s_5251;
    int __f2dace_SA_wgtfac_c_d_1_s_5252;
    int __f2dace_SA_wgtfac_c_d_2_s_5253;
    int __f2dace_SA_wgtfac_e_d_0_s_5254;
    int __f2dace_SA_wgtfac_e_d_1_s_5255;
    int __f2dace_SA_wgtfac_e_d_2_s_5256;
    int __f2dace_SA_wgtfac_v_d_0_s_5194;
    int __f2dace_SA_wgtfac_v_d_1_s_5195;
    int __f2dace_SA_wgtfac_v_d_2_s_5196;
    int __f2dace_SA_wgtfacq1_c_d_0_s_5263;
    int __f2dace_SA_wgtfacq1_c_d_1_s_5264;
    int __f2dace_SA_wgtfacq1_c_d_2_s_5265;
    int __f2dace_SA_wgtfacq1_e_d_0_s_5266;
    int __f2dace_SA_wgtfacq1_e_d_1_s_5267;
    int __f2dace_SA_wgtfacq1_e_d_2_s_5268;
    int __f2dace_SA_wgtfacq_c_d_0_s_5257;
    int __f2dace_SA_wgtfacq_c_d_1_s_5258;
    int __f2dace_SA_wgtfacq_c_d_2_s_5259;
    int __f2dace_SA_wgtfacq_e_d_0_s_5260;
    int __f2dace_SA_wgtfacq_e_d_1_s_5261;
    int __f2dace_SA_wgtfacq_e_d_2_s_5262;
    int __f2dace_SA_z_ifc_d_0_s_5141;
    int __f2dace_SA_z_ifc_d_1_s_5142;
    int __f2dace_SA_z_ifc_d_2_s_5143;
    int __f2dace_SA_z_mc_d_0_s_5144;
    int __f2dace_SA_z_mc_d_1_s_5145;
    int __f2dace_SA_z_mc_d_2_s_5146;
    int __f2dace_SA_zd_blklist_d_0_s_5328;
    int __f2dace_SA_zd_blklist_d_1_s_5329;
    int __f2dace_SA_zd_diffcoef_d_0_s_5178;
    int __f2dace_SA_zd_e2cell_d_0_s_5176;
    int __f2dace_SA_zd_e2cell_d_1_s_5177;
    int __f2dace_SA_zd_edgeblk_d_0_s_5332;
    int __f2dace_SA_zd_edgeblk_d_1_s_5333;
    int __f2dace_SA_zd_edgeidx_d_0_s_5330;
    int __f2dace_SA_zd_edgeidx_d_1_s_5331;
    int __f2dace_SA_zd_geofac_d_0_s_5174;
    int __f2dace_SA_zd_geofac_d_1_s_5175;
    int __f2dace_SA_zd_indlist_d_0_s_5326;
    int __f2dace_SA_zd_indlist_d_1_s_5327;
    int __f2dace_SA_zd_intcoef_d_0_s_5172;
    int __f2dace_SA_zd_intcoef_d_1_s_5173;
    int __f2dace_SA_zd_vertidx_d_0_s_5334;
    int __f2dace_SA_zd_vertidx_d_1_s_5335;
    int __f2dace_SA_zdiff_gradp_d_0_s_5278;
    int __f2dace_SA_zdiff_gradp_d_1_s_5279;
    int __f2dace_SA_zdiff_gradp_d_2_s_5280;
    int __f2dace_SA_zdiff_gradp_d_3_s_5281;
    int __f2dace_SA_zgpot_ifc_d_0_s_5353;
    int __f2dace_SA_zgpot_ifc_d_1_s_5354;
    int __f2dace_SA_zgpot_ifc_d_2_s_5355;
    int __f2dace_SA_zgpot_mc_d_0_s_5356;
    int __f2dace_SA_zgpot_mc_d_1_s_5357;
    int __f2dace_SA_zgpot_mc_d_2_s_5358;
    int __f2dace_SOA_bdy_halo_c_blk_d_0_s_5344;
    int __f2dace_SOA_bdy_halo_c_idx_d_0_s_5343;
    int __f2dace_SOA_bdy_mflx_e_blk_d_0_s_5351;
    int __f2dace_SOA_bdy_mflx_e_idx_d_0_s_5350;
    int __f2dace_SOA_coeff1_dwdz_d_0_s_5272;
    int __f2dace_SOA_coeff1_dwdz_d_1_s_5273;
    int __f2dace_SOA_coeff1_dwdz_d_2_s_5274;
    int __f2dace_SOA_coeff2_dwdz_d_0_s_5275;
    int __f2dace_SOA_coeff2_dwdz_d_1_s_5276;
    int __f2dace_SOA_coeff2_dwdz_d_2_s_5277;
    int __f2dace_SOA_coeff_gradekin_d_0_s_5269;
    int __f2dace_SOA_coeff_gradekin_d_1_s_5270;
    int __f2dace_SOA_coeff_gradekin_d_2_s_5271;
    int __f2dace_SOA_coeff_gradp_d_0_s_5282;
    int __f2dace_SOA_coeff_gradp_d_1_s_5283;
    int __f2dace_SOA_coeff_gradp_d_2_s_5284;
    int __f2dace_SOA_coeff_gradp_d_3_s_5285;
    int __f2dace_SOA_d2dexdz2_fac1_mc_d_0_s_5312;
    int __f2dace_SOA_d2dexdz2_fac1_mc_d_1_s_5313;
    int __f2dace_SOA_d2dexdz2_fac1_mc_d_2_s_5314;
    int __f2dace_SOA_d2dexdz2_fac2_mc_d_0_s_5315;
    int __f2dace_SOA_d2dexdz2_fac2_mc_d_1_s_5316;
    int __f2dace_SOA_d2dexdz2_fac2_mc_d_2_s_5317;
    int __f2dace_SOA_d_exner_dz_ref_ic_d_0_s_5309;
    int __f2dace_SOA_d_exner_dz_ref_ic_d_1_s_5310;
    int __f2dace_SOA_d_exner_dz_ref_ic_d_2_s_5311;
    int __f2dace_SOA_ddqz_z_full_d_0_s_5147;
    int __f2dace_SOA_ddqz_z_full_d_1_s_5148;
    int __f2dace_SOA_ddqz_z_full_d_2_s_5149;
    int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242;
    int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243;
    int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244;
    int __f2dace_SOA_ddqz_z_half_d_0_s_5245;
    int __f2dace_SOA_ddqz_z_half_d_1_s_5246;
    int __f2dace_SOA_ddqz_z_half_d_2_s_5247;
    int __f2dace_SOA_ddxn_z_full_c_d_0_s_5212;
    int __f2dace_SOA_ddxn_z_full_c_d_1_s_5213;
    int __f2dace_SOA_ddxn_z_full_c_d_2_s_5214;
    int __f2dace_SOA_ddxn_z_full_d_0_s_5209;
    int __f2dace_SOA_ddxn_z_full_d_1_s_5210;
    int __f2dace_SOA_ddxn_z_full_d_2_s_5211;
    int __f2dace_SOA_ddxn_z_full_v_d_0_s_5215;
    int __f2dace_SOA_ddxn_z_full_v_d_1_s_5216;
    int __f2dace_SOA_ddxn_z_full_v_d_2_s_5217;
    int __f2dace_SOA_ddxn_z_half_c_d_0_s_5221;
    int __f2dace_SOA_ddxn_z_half_c_d_1_s_5222;
    int __f2dace_SOA_ddxn_z_half_c_d_2_s_5223;
    int __f2dace_SOA_ddxn_z_half_e_d_0_s_5218;
    int __f2dace_SOA_ddxn_z_half_e_d_1_s_5219;
    int __f2dace_SOA_ddxn_z_half_e_d_2_s_5220;
    int __f2dace_SOA_ddxt_z_full_c_d_0_s_5227;
    int __f2dace_SOA_ddxt_z_full_c_d_1_s_5228;
    int __f2dace_SOA_ddxt_z_full_c_d_2_s_5229;
    int __f2dace_SOA_ddxt_z_full_d_0_s_5224;
    int __f2dace_SOA_ddxt_z_full_d_1_s_5225;
    int __f2dace_SOA_ddxt_z_full_d_2_s_5226;
    int __f2dace_SOA_ddxt_z_full_v_d_0_s_5230;
    int __f2dace_SOA_ddxt_z_full_v_d_1_s_5231;
    int __f2dace_SOA_ddxt_z_full_v_d_2_s_5232;
    int __f2dace_SOA_ddxt_z_half_c_d_0_s_5236;
    int __f2dace_SOA_ddxt_z_half_c_d_1_s_5237;
    int __f2dace_SOA_ddxt_z_half_c_d_2_s_5238;
    int __f2dace_SOA_ddxt_z_half_e_d_0_s_5233;
    int __f2dace_SOA_ddxt_z_half_e_d_1_s_5234;
    int __f2dace_SOA_ddxt_z_half_e_d_2_s_5235;
    int __f2dace_SOA_ddxt_z_half_v_d_0_s_5239;
    int __f2dace_SOA_ddxt_z_half_v_d_1_s_5240;
    int __f2dace_SOA_ddxt_z_half_v_d_2_s_5241;
    int __f2dace_SOA_deepatmo_t1ifc_d_0_s_5364;
    int __f2dace_SOA_deepatmo_t1ifc_d_1_s_5365;
    int __f2dace_SOA_deepatmo_t1mc_d_0_s_5362;
    int __f2dace_SOA_deepatmo_t1mc_d_1_s_5363;
    int __f2dace_SOA_deepatmo_t2mc_d_0_s_5366;
    int __f2dace_SOA_deepatmo_t2mc_d_1_s_5367;
    int __f2dace_SOA_dgeopot_mc_d_0_s_5159;
    int __f2dace_SOA_dgeopot_mc_d_1_s_5160;
    int __f2dace_SOA_dgeopot_mc_d_2_s_5161;
    int __f2dace_SOA_dzgpot_mc_d_0_s_5359;
    int __f2dace_SOA_dzgpot_mc_d_1_s_5360;
    int __f2dace_SOA_dzgpot_mc_d_2_s_5361;
    int __f2dace_SOA_enhfac_diffu_d_0_s_5164;
    int __f2dace_SOA_exner_exfac_d_0_s_5286;
    int __f2dace_SOA_exner_exfac_d_1_s_5287;
    int __f2dace_SOA_exner_exfac_d_2_s_5288;
    int __f2dace_SOA_exner_ref_mc_d_0_s_5300;
    int __f2dace_SOA_exner_ref_mc_d_1_s_5301;
    int __f2dace_SOA_exner_ref_mc_d_2_s_5302;
    int __f2dace_SOA_fbk_dom_volume_d_0_s_5208;
    int __f2dace_SOA_geopot_agl_d_0_s_5153;
    int __f2dace_SOA_geopot_agl_d_1_s_5154;
    int __f2dace_SOA_geopot_agl_d_2_s_5155;
    int __f2dace_SOA_geopot_agl_ifc_d_0_s_5156;
    int __f2dace_SOA_geopot_agl_ifc_d_1_s_5157;
    int __f2dace_SOA_geopot_agl_ifc_d_2_s_5158;
    int __f2dace_SOA_geopot_d_0_s_5150;
    int __f2dace_SOA_geopot_d_1_s_5151;
    int __f2dace_SOA_geopot_d_2_s_5152;
    int __f2dace_SOA_hmask_dd3d_d_0_s_5166;
    int __f2dace_SOA_hmask_dd3d_d_1_s_5167;
    int __f2dace_SOA_inv_ddqz_z_full_d_0_s_5248;
    int __f2dace_SOA_inv_ddqz_z_full_d_1_s_5249;
    int __f2dace_SOA_inv_ddqz_z_full_d_2_s_5250;
    int __f2dace_SOA_inv_ddqz_z_full_e_d_0_s_5179;
    int __f2dace_SOA_inv_ddqz_z_full_e_d_1_s_5180;
    int __f2dace_SOA_inv_ddqz_z_full_e_d_2_s_5181;
    int __f2dace_SOA_inv_ddqz_z_full_v_d_0_s_5182;
    int __f2dace_SOA_inv_ddqz_z_full_v_d_1_s_5183;
    int __f2dace_SOA_inv_ddqz_z_full_v_d_2_s_5184;
    int __f2dace_SOA_inv_ddqz_z_half_d_0_s_5185;
    int __f2dace_SOA_inv_ddqz_z_half_d_1_s_5186;
    int __f2dace_SOA_inv_ddqz_z_half_d_2_s_5187;
    int __f2dace_SOA_inv_ddqz_z_half_e_d_0_s_5188;
    int __f2dace_SOA_inv_ddqz_z_half_e_d_1_s_5189;
    int __f2dace_SOA_inv_ddqz_z_half_e_d_2_s_5190;
    int __f2dace_SOA_inv_ddqz_z_half_v_d_0_s_5191;
    int __f2dace_SOA_inv_ddqz_z_half_v_d_1_s_5192;
    int __f2dace_SOA_inv_ddqz_z_half_v_d_2_s_5193;
    int __f2dace_SOA_mask_mtnpoints_d_0_s_5200;
    int __f2dace_SOA_mask_mtnpoints_d_1_s_5201;
    int __f2dace_SOA_mask_mtnpoints_g_d_0_s_5202;
    int __f2dace_SOA_mask_mtnpoints_g_d_1_s_5203;
    int __f2dace_SOA_mask_prog_halo_c_d_0_s_5368;
    int __f2dace_SOA_mask_prog_halo_c_d_1_s_5369;
    int __f2dace_SOA_mixing_length_sq_d_0_s_5197;
    int __f2dace_SOA_mixing_length_sq_d_1_s_5198;
    int __f2dace_SOA_mixing_length_sq_d_2_s_5199;
    int __f2dace_SOA_nudge_c_blk_d_0_s_5341;
    int __f2dace_SOA_nudge_c_idx_d_0_s_5339;
    int __f2dace_SOA_nudge_e_blk_d_0_s_5342;
    int __f2dace_SOA_nudge_e_idx_d_0_s_5340;
    int __f2dace_SOA_nudgecoeff_vert_d_0_s_5352;
    int __f2dace_SOA_ovlp_halo_c_blk_d_0_s_5348;
    int __f2dace_SOA_ovlp_halo_c_blk_d_1_s_5349;
    int __f2dace_SOA_ovlp_halo_c_dim_d_0_s_5345;
    int __f2dace_SOA_ovlp_halo_c_idx_d_0_s_5346;
    int __f2dace_SOA_ovlp_halo_c_idx_d_1_s_5347;
    int __f2dace_SOA_pg_edgeblk_d_0_s_5337;
    int __f2dace_SOA_pg_edgeidx_d_0_s_5336;
    int __f2dace_SOA_pg_exdist_d_0_s_5321;
    int __f2dace_SOA_pg_vertidx_d_0_s_5338;
    int __f2dace_SOA_rayleigh_vn_d_0_s_5163;
    int __f2dace_SOA_rayleigh_w_d_0_s_5162;
    int __f2dace_SOA_rho_ref_corr_d_0_s_5318;
    int __f2dace_SOA_rho_ref_corr_d_1_s_5319;
    int __f2dace_SOA_rho_ref_corr_d_2_s_5320;
    int __f2dace_SOA_rho_ref_mc_d_0_s_5303;
    int __f2dace_SOA_rho_ref_mc_d_1_s_5304;
    int __f2dace_SOA_rho_ref_mc_d_2_s_5305;
    int __f2dace_SOA_rho_ref_me_d_0_s_5306;
    int __f2dace_SOA_rho_ref_me_d_1_s_5307;
    int __f2dace_SOA_rho_ref_me_d_2_s_5308;
    int __f2dace_SOA_scalfac_dd3d_d_0_s_5165;
    int __f2dace_SOA_slope_angle_d_0_s_5204;
    int __f2dace_SOA_slope_angle_d_1_s_5205;
    int __f2dace_SOA_slope_azimuth_d_0_s_5206;
    int __f2dace_SOA_slope_azimuth_d_1_s_5207;
    int __f2dace_SOA_theta_ref_ic_d_0_s_5295;
    int __f2dace_SOA_theta_ref_ic_d_1_s_5296;
    int __f2dace_SOA_theta_ref_ic_d_2_s_5297;
    int __f2dace_SOA_theta_ref_mc_d_0_s_5289;
    int __f2dace_SOA_theta_ref_mc_d_1_s_5290;
    int __f2dace_SOA_theta_ref_mc_d_2_s_5291;
    int __f2dace_SOA_theta_ref_me_d_0_s_5292;
    int __f2dace_SOA_theta_ref_me_d_1_s_5293;
    int __f2dace_SOA_theta_ref_me_d_2_s_5294;
    int __f2dace_SOA_tsfc_ref_d_0_s_5298;
    int __f2dace_SOA_tsfc_ref_d_1_s_5299;
    int __f2dace_SOA_vertidx_gradp_d_0_s_5322;
    int __f2dace_SOA_vertidx_gradp_d_1_s_5323;
    int __f2dace_SOA_vertidx_gradp_d_2_s_5324;
    int __f2dace_SOA_vertidx_gradp_d_3_s_5325;
    int __f2dace_SOA_vwind_expl_wgt_d_0_s_5168;
    int __f2dace_SOA_vwind_expl_wgt_d_1_s_5169;
    int __f2dace_SOA_vwind_impl_wgt_d_0_s_5170;
    int __f2dace_SOA_vwind_impl_wgt_d_1_s_5171;
    int __f2dace_SOA_wgtfac_c_d_0_s_5251;
    int __f2dace_SOA_wgtfac_c_d_1_s_5252;
    int __f2dace_SOA_wgtfac_c_d_2_s_5253;
    int __f2dace_SOA_wgtfac_e_d_0_s_5254;
    int __f2dace_SOA_wgtfac_e_d_1_s_5255;
    int __f2dace_SOA_wgtfac_e_d_2_s_5256;
    int __f2dace_SOA_wgtfac_v_d_0_s_5194;
    int __f2dace_SOA_wgtfac_v_d_1_s_5195;
    int __f2dace_SOA_wgtfac_v_d_2_s_5196;
    int __f2dace_SOA_wgtfacq1_c_d_0_s_5263;
    int __f2dace_SOA_wgtfacq1_c_d_1_s_5264;
    int __f2dace_SOA_wgtfacq1_c_d_2_s_5265;
    int __f2dace_SOA_wgtfacq1_e_d_0_s_5266;
    int __f2dace_SOA_wgtfacq1_e_d_1_s_5267;
    int __f2dace_SOA_wgtfacq1_e_d_2_s_5268;
    int __f2dace_SOA_wgtfacq_c_d_0_s_5257;
    int __f2dace_SOA_wgtfacq_c_d_1_s_5258;
    int __f2dace_SOA_wgtfacq_c_d_2_s_5259;
    int __f2dace_SOA_wgtfacq_e_d_0_s_5260;
    int __f2dace_SOA_wgtfacq_e_d_1_s_5261;
    int __f2dace_SOA_wgtfacq_e_d_2_s_5262;
    int __f2dace_SOA_z_ifc_d_0_s_5141;
    int __f2dace_SOA_z_ifc_d_1_s_5142;
    int __f2dace_SOA_z_ifc_d_2_s_5143;
    int __f2dace_SOA_z_mc_d_0_s_5144;
    int __f2dace_SOA_z_mc_d_1_s_5145;
    int __f2dace_SOA_z_mc_d_2_s_5146;
    int __f2dace_SOA_zd_blklist_d_0_s_5328;
    int __f2dace_SOA_zd_blklist_d_1_s_5329;
    int __f2dace_SOA_zd_diffcoef_d_0_s_5178;
    int __f2dace_SOA_zd_e2cell_d_0_s_5176;
    int __f2dace_SOA_zd_e2cell_d_1_s_5177;
    int __f2dace_SOA_zd_edgeblk_d_0_s_5332;
    int __f2dace_SOA_zd_edgeblk_d_1_s_5333;
    int __f2dace_SOA_zd_edgeidx_d_0_s_5330;
    int __f2dace_SOA_zd_edgeidx_d_1_s_5331;
    int __f2dace_SOA_zd_geofac_d_0_s_5174;
    int __f2dace_SOA_zd_geofac_d_1_s_5175;
    int __f2dace_SOA_zd_indlist_d_0_s_5326;
    int __f2dace_SOA_zd_indlist_d_1_s_5327;
    int __f2dace_SOA_zd_intcoef_d_0_s_5172;
    int __f2dace_SOA_zd_intcoef_d_1_s_5173;
    int __f2dace_SOA_zd_vertidx_d_0_s_5334;
    int __f2dace_SOA_zd_vertidx_d_1_s_5335;
    int __f2dace_SOA_zdiff_gradp_d_0_s_5278;
    int __f2dace_SOA_zdiff_gradp_d_1_s_5279;
    int __f2dace_SOA_zdiff_gradp_d_2_s_5280;
    int __f2dace_SOA_zdiff_gradp_d_3_s_5281;
    int __f2dace_SOA_zgpot_ifc_d_0_s_5353;
    int __f2dace_SOA_zgpot_ifc_d_1_s_5354;
    int __f2dace_SOA_zgpot_ifc_d_2_s_5355;
    int __f2dace_SOA_zgpot_mc_d_0_s_5356;
    int __f2dace_SOA_zgpot_mc_d_1_s_5357;
    int __f2dace_SOA_zgpot_mc_d_2_s_5358;
    int* bdy_halo_c_blk;
    int bdy_halo_c_dim;
    int* bdy_halo_c_idx;
    int* bdy_mflx_e_blk;
    int bdy_mflx_e_dim;
    int* bdy_mflx_e_idx;
    double* coeff1_dwdz;
    double* coeff2_dwdz;
    double* coeff_gradekin;
    double* coeff_gradp;
    double* d2dexdz2_fac1_mc;
    double* d2dexdz2_fac2_mc;
    double* d_exner_dz_ref_ic;
    double* ddqz_z_full;
    double* ddqz_z_full_e;
    double* ddqz_z_half;
    double* ddxn_z_full;
    double* ddxn_z_full_c;
    double* ddxn_z_full_v;
    double* ddxn_z_half_c;
    double* ddxn_z_half_e;
    double* ddxt_z_full;
    double* ddxt_z_full_c;
    double* ddxt_z_full_v;
    double* ddxt_z_half_c;
    double* ddxt_z_half_e;
    double* ddxt_z_half_v;
    double* deepatmo_t1ifc;
    double* deepatmo_t1mc;
    double* deepatmo_t2mc;
    double* dgeopot_mc;
    double* dzgpot_mc;
    double* enhfac_diffu;
    double* exner_exfac;
    double* exner_ref_mc;
    double* fbk_dom_volume;
    double* geopot;
    double* geopot_agl;
    double* geopot_agl_ifc;
    double* hmask_dd3d;
    double* inv_ddqz_z_full;
    double* inv_ddqz_z_full_e;
    double* inv_ddqz_z_full_v;
    double* inv_ddqz_z_half;
    double* inv_ddqz_z_half_e;
    double* inv_ddqz_z_half_v;
    double* mask_mtnpoints;
    double* mask_mtnpoints_g;
    double* mixing_length_sq;
    int* nudge_c_blk;
    int nudge_c_dim;
    int* nudge_c_idx;
    int* nudge_e_blk;
    int nudge_e_dim;
    int* nudge_e_idx;
    double* nudgecoeff_vert;
    int* ovlp_halo_c_blk;
    int* ovlp_halo_c_dim;
    int* ovlp_halo_c_idx;
    int* pg_edgeblk;
    int* pg_edgeidx;
    double* pg_exdist;
    int pg_listdim;
    int* pg_vertidx;
    double* rayleigh_vn;
    double* rayleigh_w;
    double* rho_ref_corr;
    double* rho_ref_mc;
    double* rho_ref_me;
    double* scalfac_dd3d;
    double* slope_angle;
    double* slope_azimuth;
    double* theta_ref_ic;
    double* theta_ref_mc;
    double* theta_ref_me;
    double* tsfc_ref;
    int* vertidx_gradp;
    double* vwind_expl_wgt;
    double* vwind_impl_wgt;
    double* wgtfac_c;
    double* wgtfac_e;
    double* wgtfac_v;
    double* wgtfacq1_c;
    double* wgtfacq1_e;
    double* wgtfacq_c;
    double* wgtfacq_e;
    double* z_ifc;
    double* z_mc;
    int* zd_blklist;
    double* zd_diffcoef;
    double* zd_e2cell;
    int* zd_edgeblk;
    int* zd_edgeidx;
    double* zd_geofac;
    int* zd_indlist;
    double* zd_intcoef;
    int zd_listdim;
    int* zd_vertidx;
    double* zdiff_gradp;
    double* zgpot_ifc;
    double* zgpot_mc;
};
struct t_glb2loc_index_lookup {
    int __f2dace_SA_inner_glb_index_d_0_s_3682;
    int __f2dace_SA_inner_glb_index_to_loc_d_0_s_3683;
    int __f2dace_SA_outer_glb_index_d_0_s_3684;
    int __f2dace_SA_outer_glb_index_to_loc_d_0_s_3685;
    int __f2dace_SOA_inner_glb_index_d_0_s_3682;
    int __f2dace_SOA_inner_glb_index_to_loc_d_0_s_3683;
    int __f2dace_SOA_outer_glb_index_d_0_s_3684;
    int __f2dace_SOA_outer_glb_index_to_loc_d_0_s_3685;
    int global_size;
};
struct t_dist_dir {
    int __f2dace_SA_owner_d_0_s_3695;
    int __f2dace_SOA_owner_d_0_s_3695;
    int comm;
    int comm_rank;
    int comm_size;
    int global_size;
    int local_start_index;
};
struct t_grid_domain_decomp_info {
    int __f2dace_SA_decomp_domain_d_0_s_3690;
    int __f2dace_SA_decomp_domain_d_1_s_3691;
    int __f2dace_SA_glb_index_d_0_s_3689;
    int __f2dace_SA_halo_level_d_0_s_3692;
    int __f2dace_SA_halo_level_d_1_s_3693;
    int __f2dace_SA_owner_local_d_0_s_3688;
    int __f2dace_SA_owner_mask_d_0_s_3686;
    int __f2dace_SA_owner_mask_d_1_s_3687;
    int __f2dace_SOA_decomp_domain_d_0_s_3690;
    int __f2dace_SOA_decomp_domain_d_1_s_3691;
    int __f2dace_SOA_glb_index_d_0_s_3689;
    int __f2dace_SOA_halo_level_d_0_s_3692;
    int __f2dace_SOA_halo_level_d_1_s_3693;
    int __f2dace_SOA_owner_local_d_0_s_3688;
    int __f2dace_SOA_owner_mask_d_0_s_3686;
    int __f2dace_SOA_owner_mask_d_1_s_3687;
    t_glb2loc_index_lookup* glb2loc_index;
    t_dist_dir* owner_dist_dir;
    int* owner_mask;
};
struct t_subset_range {
    int __f2dace_SA_vertical_levels_d_0_s_3084;
    int __f2dace_SA_vertical_levels_d_1_s_3085;
    int __f2dace_SOA_vertical_levels_d_0_s_3084;
    int __f2dace_SOA_vertical_levels_d_1_s_3085;
    int block_size;
    int end_block;
    int end_index;
    int entity_location;
    int is_in_domain;
    int max_vertical_levels;
    char name;
    int no_of_holes;
    int size;
    int start_block;
    int start_index;
};
struct t_grid_vertices {
    int __f2dace_SA_cartesian_d_0_s_3306;
    int __f2dace_SA_cartesian_d_1_s_3307;
    int __f2dace_SA_cell_blk_d_0_s_3286;
    int __f2dace_SA_cell_blk_d_1_s_3287;
    int __f2dace_SA_cell_blk_d_2_s_3288;
    int __f2dace_SA_cell_idx_d_0_s_3283;
    int __f2dace_SA_cell_idx_d_1_s_3284;
    int __f2dace_SA_cell_idx_d_2_s_3285;
    int __f2dace_SA_dual_area_d_0_s_3302;
    int __f2dace_SA_dual_area_d_1_s_3303;
    int __f2dace_SA_edge_blk_d_0_s_3292;
    int __f2dace_SA_edge_blk_d_1_s_3293;
    int __f2dace_SA_edge_blk_d_2_s_3294;
    int __f2dace_SA_edge_idx_d_0_s_3289;
    int __f2dace_SA_edge_idx_d_1_s_3290;
    int __f2dace_SA_edge_idx_d_2_s_3291;
    int __f2dace_SA_edge_orientation_d_0_s_3295;
    int __f2dace_SA_edge_orientation_d_1_s_3296;
    int __f2dace_SA_edge_orientation_d_2_s_3297;
    int __f2dace_SA_end_blk_d_0_s_3319;
    int __f2dace_SA_end_blk_d_1_s_3320;
    int __f2dace_SA_end_block_d_0_s_3321;
    int __f2dace_SA_end_idx_d_0_s_3313;
    int __f2dace_SA_end_idx_d_1_s_3314;
    int __f2dace_SA_end_index_d_0_s_3315;
    int __f2dace_SA_f_v_d_0_s_3304;
    int __f2dace_SA_f_v_d_1_s_3305;
    int __f2dace_SA_neighbor_blk_d_0_s_3280;
    int __f2dace_SA_neighbor_blk_d_1_s_3281;
    int __f2dace_SA_neighbor_blk_d_2_s_3282;
    int __f2dace_SA_neighbor_idx_d_0_s_3277;
    int __f2dace_SA_neighbor_idx_d_1_s_3278;
    int __f2dace_SA_neighbor_idx_d_2_s_3279;
    int __f2dace_SA_num_edges_d_0_s_3298;
    int __f2dace_SA_num_edges_d_1_s_3299;
    int __f2dace_SA_phys_id_d_0_s_3275;
    int __f2dace_SA_phys_id_d_1_s_3276;
    int __f2dace_SA_refin_ctrl_d_0_s_3308;
    int __f2dace_SA_refin_ctrl_d_1_s_3309;
    int __f2dace_SA_start_blk_d_0_s_3316;
    int __f2dace_SA_start_blk_d_1_s_3317;
    int __f2dace_SA_start_block_d_0_s_3318;
    int __f2dace_SA_start_idx_d_0_s_3310;
    int __f2dace_SA_start_idx_d_1_s_3311;
    int __f2dace_SA_start_index_d_0_s_3312;
    int __f2dace_SA_vertex_d_0_s_3300;
    int __f2dace_SA_vertex_d_1_s_3301;
    int __f2dace_SOA_cartesian_d_0_s_3306;
    int __f2dace_SOA_cartesian_d_1_s_3307;
    int __f2dace_SOA_cell_blk_d_0_s_3286;
    int __f2dace_SOA_cell_blk_d_1_s_3287;
    int __f2dace_SOA_cell_blk_d_2_s_3288;
    int __f2dace_SOA_cell_idx_d_0_s_3283;
    int __f2dace_SOA_cell_idx_d_1_s_3284;
    int __f2dace_SOA_cell_idx_d_2_s_3285;
    int __f2dace_SOA_dual_area_d_0_s_3302;
    int __f2dace_SOA_dual_area_d_1_s_3303;
    int __f2dace_SOA_edge_blk_d_0_s_3292;
    int __f2dace_SOA_edge_blk_d_1_s_3293;
    int __f2dace_SOA_edge_blk_d_2_s_3294;
    int __f2dace_SOA_edge_idx_d_0_s_3289;
    int __f2dace_SOA_edge_idx_d_1_s_3290;
    int __f2dace_SOA_edge_idx_d_2_s_3291;
    int __f2dace_SOA_edge_orientation_d_0_s_3295;
    int __f2dace_SOA_edge_orientation_d_1_s_3296;
    int __f2dace_SOA_edge_orientation_d_2_s_3297;
    int __f2dace_SOA_end_blk_d_0_s_3319;
    int __f2dace_SOA_end_blk_d_1_s_3320;
    int __f2dace_SOA_end_block_d_0_s_3321;
    int __f2dace_SOA_end_idx_d_0_s_3313;
    int __f2dace_SOA_end_idx_d_1_s_3314;
    int __f2dace_SOA_end_index_d_0_s_3315;
    int __f2dace_SOA_f_v_d_0_s_3304;
    int __f2dace_SOA_f_v_d_1_s_3305;
    int __f2dace_SOA_neighbor_blk_d_0_s_3280;
    int __f2dace_SOA_neighbor_blk_d_1_s_3281;
    int __f2dace_SOA_neighbor_blk_d_2_s_3282;
    int __f2dace_SOA_neighbor_idx_d_0_s_3277;
    int __f2dace_SOA_neighbor_idx_d_1_s_3278;
    int __f2dace_SOA_neighbor_idx_d_2_s_3279;
    int __f2dace_SOA_num_edges_d_0_s_3298;
    int __f2dace_SOA_num_edges_d_1_s_3299;
    int __f2dace_SOA_phys_id_d_0_s_3275;
    int __f2dace_SOA_phys_id_d_1_s_3276;
    int __f2dace_SOA_refin_ctrl_d_0_s_3308;
    int __f2dace_SOA_refin_ctrl_d_1_s_3309;
    int __f2dace_SOA_start_blk_d_0_s_3316;
    int __f2dace_SOA_start_blk_d_1_s_3317;
    int __f2dace_SOA_start_block_d_0_s_3318;
    int __f2dace_SOA_start_idx_d_0_s_3310;
    int __f2dace_SOA_start_idx_d_1_s_3311;
    int __f2dace_SOA_start_index_d_0_s_3312;
    int __f2dace_SOA_vertex_d_0_s_3300;
    int __f2dace_SOA_vertex_d_1_s_3301;
    t_subset_range* all;
    int* cell_blk;
    int* cell_idx;
    t_grid_domain_decomp_info* decomp_info;
    int* edge_blk;
    int* edge_idx;
    int* end_blk;
    int* end_block;
    int* end_index;
    t_subset_range* in_domain;
    int max_connectivity;
    int* neighbor_blk;
    int* neighbor_idx;
    t_subset_range* not_in_domain;
    t_subset_range* not_owned;
    t_subset_range* owned;
    int* start_blk;
    int* start_block;
    int* start_index;
};
struct t_lsq {
    int __f2dace_SA_lsq_blk_c_d_0_s_3908;
    int __f2dace_SA_lsq_blk_c_d_1_s_3909;
    int __f2dace_SA_lsq_blk_c_d_2_s_3910;
    int __f2dace_SA_lsq_dim_stencil_d_0_s_3903;
    int __f2dace_SA_lsq_dim_stencil_d_1_s_3904;
    int __f2dace_SA_lsq_idx_c_d_0_s_3905;
    int __f2dace_SA_lsq_idx_c_d_1_s_3906;
    int __f2dace_SA_lsq_idx_c_d_2_s_3907;
    int __f2dace_SA_lsq_moments_d_0_s_3928;
    int __f2dace_SA_lsq_moments_d_1_s_3929;
    int __f2dace_SA_lsq_moments_d_2_s_3930;
    int __f2dace_SA_lsq_moments_hat_d_0_s_3931;
    int __f2dace_SA_lsq_moments_hat_d_1_s_3932;
    int __f2dace_SA_lsq_moments_hat_d_2_s_3933;
    int __f2dace_SA_lsq_moments_hat_d_3_s_3934;
    int __f2dace_SA_lsq_pseudoinv_d_0_s_3924;
    int __f2dace_SA_lsq_pseudoinv_d_1_s_3925;
    int __f2dace_SA_lsq_pseudoinv_d_2_s_3926;
    int __f2dace_SA_lsq_pseudoinv_d_3_s_3927;
    int __f2dace_SA_lsq_qtmat_c_d_0_s_3914;
    int __f2dace_SA_lsq_qtmat_c_d_1_s_3915;
    int __f2dace_SA_lsq_qtmat_c_d_2_s_3916;
    int __f2dace_SA_lsq_qtmat_c_d_3_s_3917;
    int __f2dace_SA_lsq_rmat_rdiag_c_d_0_s_3918;
    int __f2dace_SA_lsq_rmat_rdiag_c_d_1_s_3919;
    int __f2dace_SA_lsq_rmat_rdiag_c_d_2_s_3920;
    int __f2dace_SA_lsq_rmat_utri_c_d_0_s_3921;
    int __f2dace_SA_lsq_rmat_utri_c_d_1_s_3922;
    int __f2dace_SA_lsq_rmat_utri_c_d_2_s_3923;
    int __f2dace_SA_lsq_weights_c_d_0_s_3911;
    int __f2dace_SA_lsq_weights_c_d_1_s_3912;
    int __f2dace_SA_lsq_weights_c_d_2_s_3913;
    int __f2dace_SOA_lsq_blk_c_d_0_s_3908;
    int __f2dace_SOA_lsq_blk_c_d_1_s_3909;
    int __f2dace_SOA_lsq_blk_c_d_2_s_3910;
    int __f2dace_SOA_lsq_dim_stencil_d_0_s_3903;
    int __f2dace_SOA_lsq_dim_stencil_d_1_s_3904;
    int __f2dace_SOA_lsq_idx_c_d_0_s_3905;
    int __f2dace_SOA_lsq_idx_c_d_1_s_3906;
    int __f2dace_SOA_lsq_idx_c_d_2_s_3907;
    int __f2dace_SOA_lsq_moments_d_0_s_3928;
    int __f2dace_SOA_lsq_moments_d_1_s_3929;
    int __f2dace_SOA_lsq_moments_d_2_s_3930;
    int __f2dace_SOA_lsq_moments_hat_d_0_s_3931;
    int __f2dace_SOA_lsq_moments_hat_d_1_s_3932;
    int __f2dace_SOA_lsq_moments_hat_d_2_s_3933;
    int __f2dace_SOA_lsq_moments_hat_d_3_s_3934;
    int __f2dace_SOA_lsq_pseudoinv_d_0_s_3924;
    int __f2dace_SOA_lsq_pseudoinv_d_1_s_3925;
    int __f2dace_SOA_lsq_pseudoinv_d_2_s_3926;
    int __f2dace_SOA_lsq_pseudoinv_d_3_s_3927;
    int __f2dace_SOA_lsq_qtmat_c_d_0_s_3914;
    int __f2dace_SOA_lsq_qtmat_c_d_1_s_3915;
    int __f2dace_SOA_lsq_qtmat_c_d_2_s_3916;
    int __f2dace_SOA_lsq_qtmat_c_d_3_s_3917;
    int __f2dace_SOA_lsq_rmat_rdiag_c_d_0_s_3918;
    int __f2dace_SOA_lsq_rmat_rdiag_c_d_1_s_3919;
    int __f2dace_SOA_lsq_rmat_rdiag_c_d_2_s_3920;
    int __f2dace_SOA_lsq_rmat_utri_c_d_0_s_3921;
    int __f2dace_SOA_lsq_rmat_utri_c_d_1_s_3922;
    int __f2dace_SOA_lsq_rmat_utri_c_d_2_s_3923;
    int __f2dace_SOA_lsq_weights_c_d_0_s_3911;
    int __f2dace_SOA_lsq_weights_c_d_1_s_3912;
    int __f2dace_SOA_lsq_weights_c_d_2_s_3913;
};
struct t_gauss_quad {
    int __f2dace_SA_qpts_tri_c_d_0_s_3940;
    int __f2dace_SA_qpts_tri_c_d_1_s_3941;
    int __f2dace_SA_qpts_tri_c_d_2_s_3942;
    int __f2dace_SA_qpts_tri_l_d_0_s_3935;
    int __f2dace_SA_qpts_tri_l_d_1_s_3936;
    int __f2dace_SA_qpts_tri_q_d_0_s_3937;
    int __f2dace_SA_qpts_tri_q_d_1_s_3938;
    int __f2dace_SA_qpts_tri_q_d_2_s_3939;
    int __f2dace_SA_weights_tri_c_d_0_s_3944;
    int __f2dace_SA_weights_tri_q_d_0_s_3943;
    int __f2dace_SOA_qpts_tri_c_d_0_s_3940;
    int __f2dace_SOA_qpts_tri_c_d_1_s_3941;
    int __f2dace_SOA_qpts_tri_c_d_2_s_3942;
    int __f2dace_SOA_qpts_tri_l_d_0_s_3935;
    int __f2dace_SOA_qpts_tri_l_d_1_s_3936;
    int __f2dace_SOA_qpts_tri_q_d_0_s_3937;
    int __f2dace_SOA_qpts_tri_q_d_1_s_3938;
    int __f2dace_SOA_qpts_tri_q_d_2_s_3939;
    int __f2dace_SOA_weights_tri_c_d_0_s_3944;
    int __f2dace_SOA_weights_tri_q_d_0_s_3943;
};
struct t_cell_environ {
    int __f2dace_SA_area_norm_d_0_s_3953;
    int __f2dace_SA_area_norm_d_1_s_3954;
    int __f2dace_SA_area_norm_d_2_s_3955;
    int __f2dace_SA_blk_d_0_s_3950;
    int __f2dace_SA_blk_d_1_s_3951;
    int __f2dace_SA_blk_d_2_s_3952;
    int __f2dace_SA_idx_d_0_s_3947;
    int __f2dace_SA_idx_d_1_s_3948;
    int __f2dace_SA_idx_d_2_s_3949;
    int __f2dace_SA_nmbr_nghbr_cells_d_0_s_3945;
    int __f2dace_SA_nmbr_nghbr_cells_d_1_s_3946;
    int __f2dace_SOA_area_norm_d_0_s_3953;
    int __f2dace_SOA_area_norm_d_1_s_3954;
    int __f2dace_SOA_area_norm_d_2_s_3955;
    int __f2dace_SOA_blk_d_0_s_3950;
    int __f2dace_SOA_blk_d_1_s_3951;
    int __f2dace_SOA_blk_d_2_s_3952;
    int __f2dace_SOA_idx_d_0_s_3947;
    int __f2dace_SOA_idx_d_1_s_3948;
    int __f2dace_SOA_idx_d_2_s_3949;
    int __f2dace_SOA_nmbr_nghbr_cells_d_0_s_3945;
    int __f2dace_SOA_nmbr_nghbr_cells_d_1_s_3946;
    int is_used;
    int max_nmbr_iter;
    int max_nmbr_nghbr_cells;
    int nmbr_nghbr_cells_alloc;
    double radius;
};
struct t_int_state {
    int __f2dace_SA_c_bln_avg_d_0_s_3968;
    int __f2dace_SA_c_bln_avg_d_1_s_3969;
    int __f2dace_SA_c_bln_avg_d_2_s_3970;
    int __f2dace_SA_c_lin_e_d_0_s_3956;
    int __f2dace_SA_c_lin_e_d_1_s_3957;
    int __f2dace_SA_c_lin_e_d_2_s_3958;
    int __f2dace_SA_cell_vert_dist_d_0_s_4064;
    int __f2dace_SA_cell_vert_dist_d_1_s_4065;
    int __f2dace_SA_cell_vert_dist_d_2_s_4066;
    int __f2dace_SA_cell_vert_dist_d_3_s_4067;
    int __f2dace_SA_cells_aw_verts_d_0_s_3990;
    int __f2dace_SA_cells_aw_verts_d_1_s_3991;
    int __f2dace_SA_cells_aw_verts_d_2_s_3992;
    int __f2dace_SA_cells_plwa_verts_d_0_s_3977;
    int __f2dace_SA_cells_plwa_verts_d_1_s_3978;
    int __f2dace_SA_cells_plwa_verts_d_2_s_3979;
    int __f2dace_SA_dist_cell2edge_d_0_s_4107;
    int __f2dace_SA_dist_cell2edge_d_1_s_4108;
    int __f2dace_SA_dist_cell2edge_d_2_s_4109;
    int __f2dace_SA_e_bln_c_s_d_0_s_3959;
    int __f2dace_SA_e_bln_c_s_d_1_s_3960;
    int __f2dace_SA_e_bln_c_s_d_2_s_3961;
    int __f2dace_SA_e_bln_c_u_d_0_s_3962;
    int __f2dace_SA_e_bln_c_u_d_1_s_3963;
    int __f2dace_SA_e_bln_c_u_d_2_s_3964;
    int __f2dace_SA_e_bln_c_v_d_0_s_3965;
    int __f2dace_SA_e_bln_c_v_d_1_s_3966;
    int __f2dace_SA_e_bln_c_v_d_2_s_3967;
    int __f2dace_SA_e_flx_avg_d_0_s_3971;
    int __f2dace_SA_e_flx_avg_d_1_s_3972;
    int __f2dace_SA_e_flx_avg_d_2_s_3973;
    int __f2dace_SA_e_inn_c_d_0_s_3984;
    int __f2dace_SA_e_inn_c_d_1_s_3985;
    int __f2dace_SA_e_inn_c_d_2_s_3986;
    int __f2dace_SA_edge2cell_coeff_cc_d_0_s_4084;
    int __f2dace_SA_edge2cell_coeff_cc_d_1_s_4085;
    int __f2dace_SA_edge2cell_coeff_cc_d_2_s_4086;
    int __f2dace_SA_edge2cell_coeff_cc_t_d_0_s_4087;
    int __f2dace_SA_edge2cell_coeff_cc_t_d_1_s_4088;
    int __f2dace_SA_edge2cell_coeff_cc_t_d_2_s_4089;
    int __f2dace_SA_edge2vert_coeff_cc_d_0_s_4090;
    int __f2dace_SA_edge2vert_coeff_cc_d_1_s_4091;
    int __f2dace_SA_edge2vert_coeff_cc_d_2_s_4092;
    int __f2dace_SA_edge2vert_coeff_cc_t_d_0_s_4093;
    int __f2dace_SA_edge2vert_coeff_cc_t_d_1_s_4094;
    int __f2dace_SA_edge2vert_coeff_cc_t_d_2_s_4095;
    int __f2dace_SA_edge2vert_vector_cc_d_0_s_4096;
    int __f2dace_SA_edge2vert_vector_cc_d_1_s_4097;
    int __f2dace_SA_edge2vert_vector_cc_d_2_s_4098;
    int __f2dace_SA_edge_cell_length_d_0_s_4061;
    int __f2dace_SA_edge_cell_length_d_1_s_4062;
    int __f2dace_SA_edge_cell_length_d_2_s_4063;
    int __f2dace_SA_fixed_vol_norm_d_0_s_4099;
    int __f2dace_SA_fixed_vol_norm_d_1_s_4100;
    int __f2dace_SA_geofac_div_d_0_s_4038;
    int __f2dace_SA_geofac_div_d_1_s_4039;
    int __f2dace_SA_geofac_div_d_2_s_4040;
    int __f2dace_SA_geofac_grdiv_d_0_s_4044;
    int __f2dace_SA_geofac_grdiv_d_1_s_4045;
    int __f2dace_SA_geofac_grdiv_d_2_s_4046;
    int __f2dace_SA_geofac_grg_d_0_s_4053;
    int __f2dace_SA_geofac_grg_d_1_s_4054;
    int __f2dace_SA_geofac_grg_d_2_s_4055;
    int __f2dace_SA_geofac_grg_d_3_s_4056;
    int __f2dace_SA_geofac_n2s_d_0_s_4050;
    int __f2dace_SA_geofac_n2s_d_1_s_4051;
    int __f2dace_SA_geofac_n2s_d_2_s_4052;
    int __f2dace_SA_geofac_qdiv_d_0_s_4041;
    int __f2dace_SA_geofac_qdiv_d_1_s_4042;
    int __f2dace_SA_geofac_qdiv_d_2_s_4043;
    int __f2dace_SA_geofac_rot_d_0_s_4047;
    int __f2dace_SA_geofac_rot_d_1_s_4048;
    int __f2dace_SA_geofac_rot_d_2_s_4049;
    int __f2dace_SA_gradc_bmat_d_0_s_3980;
    int __f2dace_SA_gradc_bmat_d_1_s_3981;
    int __f2dace_SA_gradc_bmat_d_2_s_3982;
    int __f2dace_SA_gradc_bmat_d_3_s_3983;
    int __f2dace_SA_nudgecoeff_c_d_0_s_4080;
    int __f2dace_SA_nudgecoeff_c_d_1_s_4081;
    int __f2dace_SA_nudgecoeff_e_d_0_s_4082;
    int __f2dace_SA_nudgecoeff_e_d_1_s_4083;
    int __f2dace_SA_pos_on_tplane_c_edge_d_0_s_4076;
    int __f2dace_SA_pos_on_tplane_c_edge_d_1_s_4077;
    int __f2dace_SA_pos_on_tplane_c_edge_d_2_s_4078;
    int __f2dace_SA_pos_on_tplane_c_edge_d_3_s_4079;
    int __f2dace_SA_pos_on_tplane_e_d_0_s_4068;
    int __f2dace_SA_pos_on_tplane_e_d_1_s_4069;
    int __f2dace_SA_pos_on_tplane_e_d_2_s_4070;
    int __f2dace_SA_pos_on_tplane_e_d_3_s_4071;
    int __f2dace_SA_primal_normal_ec_d_0_s_4057;
    int __f2dace_SA_primal_normal_ec_d_1_s_4058;
    int __f2dace_SA_primal_normal_ec_d_2_s_4059;
    int __f2dace_SA_primal_normal_ec_d_3_s_4060;
    int __f2dace_SA_rbf_c2grad_blk_d_0_s_4008;
    int __f2dace_SA_rbf_c2grad_blk_d_1_s_4009;
    int __f2dace_SA_rbf_c2grad_blk_d_2_s_4010;
    int __f2dace_SA_rbf_c2grad_coeff_d_0_s_4011;
    int __f2dace_SA_rbf_c2grad_coeff_d_1_s_4012;
    int __f2dace_SA_rbf_c2grad_coeff_d_2_s_4013;
    int __f2dace_SA_rbf_c2grad_coeff_d_3_s_4014;
    int __f2dace_SA_rbf_c2grad_idx_d_0_s_4005;
    int __f2dace_SA_rbf_c2grad_idx_d_1_s_4006;
    int __f2dace_SA_rbf_c2grad_idx_d_2_s_4007;
    int __f2dace_SA_rbf_vec_blk_c_d_0_s_3996;
    int __f2dace_SA_rbf_vec_blk_c_d_1_s_3997;
    int __f2dace_SA_rbf_vec_blk_c_d_2_s_3998;
    int __f2dace_SA_rbf_vec_blk_e_d_0_s_4030;
    int __f2dace_SA_rbf_vec_blk_e_d_1_s_4031;
    int __f2dace_SA_rbf_vec_blk_e_d_2_s_4032;
    int __f2dace_SA_rbf_vec_blk_v_d_0_s_4018;
    int __f2dace_SA_rbf_vec_blk_v_d_1_s_4019;
    int __f2dace_SA_rbf_vec_blk_v_d_2_s_4020;
    int __f2dace_SA_rbf_vec_coeff_c_d_0_s_4001;
    int __f2dace_SA_rbf_vec_coeff_c_d_1_s_4002;
    int __f2dace_SA_rbf_vec_coeff_c_d_2_s_4003;
    int __f2dace_SA_rbf_vec_coeff_c_d_3_s_4004;
    int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035;
    int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036;
    int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037;
    int __f2dace_SA_rbf_vec_coeff_v_d_0_s_4023;
    int __f2dace_SA_rbf_vec_coeff_v_d_1_s_4024;
    int __f2dace_SA_rbf_vec_coeff_v_d_2_s_4025;
    int __f2dace_SA_rbf_vec_coeff_v_d_3_s_4026;
    int __f2dace_SA_rbf_vec_idx_c_d_0_s_3993;
    int __f2dace_SA_rbf_vec_idx_c_d_1_s_3994;
    int __f2dace_SA_rbf_vec_idx_c_d_2_s_3995;
    int __f2dace_SA_rbf_vec_idx_e_d_0_s_4027;
    int __f2dace_SA_rbf_vec_idx_e_d_1_s_4028;
    int __f2dace_SA_rbf_vec_idx_e_d_2_s_4029;
    int __f2dace_SA_rbf_vec_idx_v_d_0_s_4015;
    int __f2dace_SA_rbf_vec_idx_v_d_1_s_4016;
    int __f2dace_SA_rbf_vec_idx_v_d_2_s_4017;
    int __f2dace_SA_rbf_vec_stencil_c_d_0_s_3999;
    int __f2dace_SA_rbf_vec_stencil_c_d_1_s_4000;
    int __f2dace_SA_rbf_vec_stencil_e_d_0_s_4033;
    int __f2dace_SA_rbf_vec_stencil_e_d_1_s_4034;
    int __f2dace_SA_rbf_vec_stencil_v_d_0_s_4021;
    int __f2dace_SA_rbf_vec_stencil_v_d_1_s_4022;
    int __f2dace_SA_tplane_e_dotprod_d_0_s_4072;
    int __f2dace_SA_tplane_e_dotprod_d_1_s_4073;
    int __f2dace_SA_tplane_e_dotprod_d_2_s_4074;
    int __f2dace_SA_tplane_e_dotprod_d_3_s_4075;
    int __f2dace_SA_v_1o2_e_d_0_s_3974;
    int __f2dace_SA_v_1o2_e_d_1_s_3975;
    int __f2dace_SA_v_1o2_e_d_2_s_3976;
    int __f2dace_SA_variable_dual_vol_norm_d_0_s_4104;
    int __f2dace_SA_variable_dual_vol_norm_d_1_s_4105;
    int __f2dace_SA_variable_dual_vol_norm_d_2_s_4106;
    int __f2dace_SA_variable_vol_norm_d_0_s_4101;
    int __f2dace_SA_variable_vol_norm_d_1_s_4102;
    int __f2dace_SA_variable_vol_norm_d_2_s_4103;
    int __f2dace_SA_verts_aw_cells_d_0_s_3987;
    int __f2dace_SA_verts_aw_cells_d_1_s_3988;
    int __f2dace_SA_verts_aw_cells_d_2_s_3989;
    int __f2dace_SOA_c_bln_avg_d_0_s_3968;
    int __f2dace_SOA_c_bln_avg_d_1_s_3969;
    int __f2dace_SOA_c_bln_avg_d_2_s_3970;
    int __f2dace_SOA_c_lin_e_d_0_s_3956;
    int __f2dace_SOA_c_lin_e_d_1_s_3957;
    int __f2dace_SOA_c_lin_e_d_2_s_3958;
    int __f2dace_SOA_cell_vert_dist_d_0_s_4064;
    int __f2dace_SOA_cell_vert_dist_d_1_s_4065;
    int __f2dace_SOA_cell_vert_dist_d_2_s_4066;
    int __f2dace_SOA_cell_vert_dist_d_3_s_4067;
    int __f2dace_SOA_cells_aw_verts_d_0_s_3990;
    int __f2dace_SOA_cells_aw_verts_d_1_s_3991;
    int __f2dace_SOA_cells_aw_verts_d_2_s_3992;
    int __f2dace_SOA_cells_plwa_verts_d_0_s_3977;
    int __f2dace_SOA_cells_plwa_verts_d_1_s_3978;
    int __f2dace_SOA_cells_plwa_verts_d_2_s_3979;
    int __f2dace_SOA_dist_cell2edge_d_0_s_4107;
    int __f2dace_SOA_dist_cell2edge_d_1_s_4108;
    int __f2dace_SOA_dist_cell2edge_d_2_s_4109;
    int __f2dace_SOA_e_bln_c_s_d_0_s_3959;
    int __f2dace_SOA_e_bln_c_s_d_1_s_3960;
    int __f2dace_SOA_e_bln_c_s_d_2_s_3961;
    int __f2dace_SOA_e_bln_c_u_d_0_s_3962;
    int __f2dace_SOA_e_bln_c_u_d_1_s_3963;
    int __f2dace_SOA_e_bln_c_u_d_2_s_3964;
    int __f2dace_SOA_e_bln_c_v_d_0_s_3965;
    int __f2dace_SOA_e_bln_c_v_d_1_s_3966;
    int __f2dace_SOA_e_bln_c_v_d_2_s_3967;
    int __f2dace_SOA_e_flx_avg_d_0_s_3971;
    int __f2dace_SOA_e_flx_avg_d_1_s_3972;
    int __f2dace_SOA_e_flx_avg_d_2_s_3973;
    int __f2dace_SOA_e_inn_c_d_0_s_3984;
    int __f2dace_SOA_e_inn_c_d_1_s_3985;
    int __f2dace_SOA_e_inn_c_d_2_s_3986;
    int __f2dace_SOA_edge2cell_coeff_cc_d_0_s_4084;
    int __f2dace_SOA_edge2cell_coeff_cc_d_1_s_4085;
    int __f2dace_SOA_edge2cell_coeff_cc_d_2_s_4086;
    int __f2dace_SOA_edge2cell_coeff_cc_t_d_0_s_4087;
    int __f2dace_SOA_edge2cell_coeff_cc_t_d_1_s_4088;
    int __f2dace_SOA_edge2cell_coeff_cc_t_d_2_s_4089;
    int __f2dace_SOA_edge2vert_coeff_cc_d_0_s_4090;
    int __f2dace_SOA_edge2vert_coeff_cc_d_1_s_4091;
    int __f2dace_SOA_edge2vert_coeff_cc_d_2_s_4092;
    int __f2dace_SOA_edge2vert_coeff_cc_t_d_0_s_4093;
    int __f2dace_SOA_edge2vert_coeff_cc_t_d_1_s_4094;
    int __f2dace_SOA_edge2vert_coeff_cc_t_d_2_s_4095;
    int __f2dace_SOA_edge2vert_vector_cc_d_0_s_4096;
    int __f2dace_SOA_edge2vert_vector_cc_d_1_s_4097;
    int __f2dace_SOA_edge2vert_vector_cc_d_2_s_4098;
    int __f2dace_SOA_edge_cell_length_d_0_s_4061;
    int __f2dace_SOA_edge_cell_length_d_1_s_4062;
    int __f2dace_SOA_edge_cell_length_d_2_s_4063;
    int __f2dace_SOA_fixed_vol_norm_d_0_s_4099;
    int __f2dace_SOA_fixed_vol_norm_d_1_s_4100;
    int __f2dace_SOA_geofac_div_d_0_s_4038;
    int __f2dace_SOA_geofac_div_d_1_s_4039;
    int __f2dace_SOA_geofac_div_d_2_s_4040;
    int __f2dace_SOA_geofac_grdiv_d_0_s_4044;
    int __f2dace_SOA_geofac_grdiv_d_1_s_4045;
    int __f2dace_SOA_geofac_grdiv_d_2_s_4046;
    int __f2dace_SOA_geofac_grg_d_0_s_4053;
    int __f2dace_SOA_geofac_grg_d_1_s_4054;
    int __f2dace_SOA_geofac_grg_d_2_s_4055;
    int __f2dace_SOA_geofac_grg_d_3_s_4056;
    int __f2dace_SOA_geofac_n2s_d_0_s_4050;
    int __f2dace_SOA_geofac_n2s_d_1_s_4051;
    int __f2dace_SOA_geofac_n2s_d_2_s_4052;
    int __f2dace_SOA_geofac_qdiv_d_0_s_4041;
    int __f2dace_SOA_geofac_qdiv_d_1_s_4042;
    int __f2dace_SOA_geofac_qdiv_d_2_s_4043;
    int __f2dace_SOA_geofac_rot_d_0_s_4047;
    int __f2dace_SOA_geofac_rot_d_1_s_4048;
    int __f2dace_SOA_geofac_rot_d_2_s_4049;
    int __f2dace_SOA_gradc_bmat_d_0_s_3980;
    int __f2dace_SOA_gradc_bmat_d_1_s_3981;
    int __f2dace_SOA_gradc_bmat_d_2_s_3982;
    int __f2dace_SOA_gradc_bmat_d_3_s_3983;
    int __f2dace_SOA_nudgecoeff_c_d_0_s_4080;
    int __f2dace_SOA_nudgecoeff_c_d_1_s_4081;
    int __f2dace_SOA_nudgecoeff_e_d_0_s_4082;
    int __f2dace_SOA_nudgecoeff_e_d_1_s_4083;
    int __f2dace_SOA_pos_on_tplane_c_edge_d_0_s_4076;
    int __f2dace_SOA_pos_on_tplane_c_edge_d_1_s_4077;
    int __f2dace_SOA_pos_on_tplane_c_edge_d_2_s_4078;
    int __f2dace_SOA_pos_on_tplane_c_edge_d_3_s_4079;
    int __f2dace_SOA_pos_on_tplane_e_d_0_s_4068;
    int __f2dace_SOA_pos_on_tplane_e_d_1_s_4069;
    int __f2dace_SOA_pos_on_tplane_e_d_2_s_4070;
    int __f2dace_SOA_pos_on_tplane_e_d_3_s_4071;
    int __f2dace_SOA_primal_normal_ec_d_0_s_4057;
    int __f2dace_SOA_primal_normal_ec_d_1_s_4058;
    int __f2dace_SOA_primal_normal_ec_d_2_s_4059;
    int __f2dace_SOA_primal_normal_ec_d_3_s_4060;
    int __f2dace_SOA_rbf_c2grad_blk_d_0_s_4008;
    int __f2dace_SOA_rbf_c2grad_blk_d_1_s_4009;
    int __f2dace_SOA_rbf_c2grad_blk_d_2_s_4010;
    int __f2dace_SOA_rbf_c2grad_coeff_d_0_s_4011;
    int __f2dace_SOA_rbf_c2grad_coeff_d_1_s_4012;
    int __f2dace_SOA_rbf_c2grad_coeff_d_2_s_4013;
    int __f2dace_SOA_rbf_c2grad_coeff_d_3_s_4014;
    int __f2dace_SOA_rbf_c2grad_idx_d_0_s_4005;
    int __f2dace_SOA_rbf_c2grad_idx_d_1_s_4006;
    int __f2dace_SOA_rbf_c2grad_idx_d_2_s_4007;
    int __f2dace_SOA_rbf_vec_blk_c_d_0_s_3996;
    int __f2dace_SOA_rbf_vec_blk_c_d_1_s_3997;
    int __f2dace_SOA_rbf_vec_blk_c_d_2_s_3998;
    int __f2dace_SOA_rbf_vec_blk_e_d_0_s_4030;
    int __f2dace_SOA_rbf_vec_blk_e_d_1_s_4031;
    int __f2dace_SOA_rbf_vec_blk_e_d_2_s_4032;
    int __f2dace_SOA_rbf_vec_blk_v_d_0_s_4018;
    int __f2dace_SOA_rbf_vec_blk_v_d_1_s_4019;
    int __f2dace_SOA_rbf_vec_blk_v_d_2_s_4020;
    int __f2dace_SOA_rbf_vec_coeff_c_d_0_s_4001;
    int __f2dace_SOA_rbf_vec_coeff_c_d_1_s_4002;
    int __f2dace_SOA_rbf_vec_coeff_c_d_2_s_4003;
    int __f2dace_SOA_rbf_vec_coeff_c_d_3_s_4004;
    int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035;
    int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036;
    int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037;
    int __f2dace_SOA_rbf_vec_coeff_v_d_0_s_4023;
    int __f2dace_SOA_rbf_vec_coeff_v_d_1_s_4024;
    int __f2dace_SOA_rbf_vec_coeff_v_d_2_s_4025;
    int __f2dace_SOA_rbf_vec_coeff_v_d_3_s_4026;
    int __f2dace_SOA_rbf_vec_idx_c_d_0_s_3993;
    int __f2dace_SOA_rbf_vec_idx_c_d_1_s_3994;
    int __f2dace_SOA_rbf_vec_idx_c_d_2_s_3995;
    int __f2dace_SOA_rbf_vec_idx_e_d_0_s_4027;
    int __f2dace_SOA_rbf_vec_idx_e_d_1_s_4028;
    int __f2dace_SOA_rbf_vec_idx_e_d_2_s_4029;
    int __f2dace_SOA_rbf_vec_idx_v_d_0_s_4015;
    int __f2dace_SOA_rbf_vec_idx_v_d_1_s_4016;
    int __f2dace_SOA_rbf_vec_idx_v_d_2_s_4017;
    int __f2dace_SOA_rbf_vec_stencil_c_d_0_s_3999;
    int __f2dace_SOA_rbf_vec_stencil_c_d_1_s_4000;
    int __f2dace_SOA_rbf_vec_stencil_e_d_0_s_4033;
    int __f2dace_SOA_rbf_vec_stencil_e_d_1_s_4034;
    int __f2dace_SOA_rbf_vec_stencil_v_d_0_s_4021;
    int __f2dace_SOA_rbf_vec_stencil_v_d_1_s_4022;
    int __f2dace_SOA_tplane_e_dotprod_d_0_s_4072;
    int __f2dace_SOA_tplane_e_dotprod_d_1_s_4073;
    int __f2dace_SOA_tplane_e_dotprod_d_2_s_4074;
    int __f2dace_SOA_tplane_e_dotprod_d_3_s_4075;
    int __f2dace_SOA_v_1o2_e_d_0_s_3974;
    int __f2dace_SOA_v_1o2_e_d_1_s_3975;
    int __f2dace_SOA_v_1o2_e_d_2_s_3976;
    int __f2dace_SOA_variable_dual_vol_norm_d_0_s_4104;
    int __f2dace_SOA_variable_dual_vol_norm_d_1_s_4105;
    int __f2dace_SOA_variable_dual_vol_norm_d_2_s_4106;
    int __f2dace_SOA_variable_vol_norm_d_0_s_4101;
    int __f2dace_SOA_variable_vol_norm_d_1_s_4102;
    int __f2dace_SOA_variable_vol_norm_d_2_s_4103;
    int __f2dace_SOA_verts_aw_cells_d_0_s_3987;
    int __f2dace_SOA_verts_aw_cells_d_1_s_3988;
    int __f2dace_SOA_verts_aw_cells_d_2_s_3989;
    double* c_lin_e;
    t_cell_environ* cell_environ;
    double* cells_aw_verts;
    double* e_bln_c_s;
    double* geofac_grdiv;
    double* geofac_n2s;
    double* geofac_rot;
    t_gauss_quad* gquad;
    t_lsq* lsq_high;
    t_lsq* lsq_lin;
    double* rbf_vec_coeff_e;
};
struct t_nh_prog {
    int __f2dace_SA_conv_tracer_d_0_s_4778;
    int __f2dace_SA_conv_tracer_d_1_s_4779;
    int __f2dace_SA_exner_d_0_s_4764;
    int __f2dace_SA_exner_d_1_s_4765;
    int __f2dace_SA_exner_d_2_s_4766;
    int __f2dace_SA_rho_d_0_s_4761;
    int __f2dace_SA_rho_d_1_s_4762;
    int __f2dace_SA_rho_d_2_s_4763;
    int __f2dace_SA_theta_v_d_0_s_4767;
    int __f2dace_SA_theta_v_d_1_s_4768;
    int __f2dace_SA_theta_v_d_2_s_4769;
    int __f2dace_SA_tke_d_0_s_4774;
    int __f2dace_SA_tke_d_1_s_4775;
    int __f2dace_SA_tke_d_2_s_4776;
    int __f2dace_SA_tracer_d_0_s_4770;
    int __f2dace_SA_tracer_d_1_s_4771;
    int __f2dace_SA_tracer_d_2_s_4772;
    int __f2dace_SA_tracer_d_3_s_4773;
    int __f2dace_SA_tracer_ptr_d_0_s_4777;
    int __f2dace_SA_turb_tracer_d_0_s_4780;
    int __f2dace_SA_turb_tracer_d_1_s_4781;
    int __f2dace_SA_vn_d_0_s_4758;
    int __f2dace_SA_vn_d_1_s_4759;
    int __f2dace_SA_vn_d_2_s_4760;
    int __f2dace_SA_w_d_0_s_4755;
    int __f2dace_SA_w_d_1_s_4756;
    int __f2dace_SA_w_d_2_s_4757;
    int __f2dace_SOA_conv_tracer_d_0_s_4778;
    int __f2dace_SOA_conv_tracer_d_1_s_4779;
    int __f2dace_SOA_exner_d_0_s_4764;
    int __f2dace_SOA_exner_d_1_s_4765;
    int __f2dace_SOA_exner_d_2_s_4766;
    int __f2dace_SOA_rho_d_0_s_4761;
    int __f2dace_SOA_rho_d_1_s_4762;
    int __f2dace_SOA_rho_d_2_s_4763;
    int __f2dace_SOA_theta_v_d_0_s_4767;
    int __f2dace_SOA_theta_v_d_1_s_4768;
    int __f2dace_SOA_theta_v_d_2_s_4769;
    int __f2dace_SOA_tke_d_0_s_4774;
    int __f2dace_SOA_tke_d_1_s_4775;
    int __f2dace_SOA_tke_d_2_s_4776;
    int __f2dace_SOA_tracer_d_0_s_4770;
    int __f2dace_SOA_tracer_d_1_s_4771;
    int __f2dace_SOA_tracer_d_2_s_4772;
    int __f2dace_SOA_tracer_d_3_s_4773;
    int __f2dace_SOA_tracer_ptr_d_0_s_4777;
    int __f2dace_SOA_turb_tracer_d_0_s_4780;
    int __f2dace_SOA_turb_tracer_d_1_s_4781;
    int __f2dace_SOA_vn_d_0_s_4758;
    int __f2dace_SOA_vn_d_1_s_4759;
    int __f2dace_SOA_vn_d_2_s_4760;
    int __f2dace_SOA_w_d_0_s_4755;
    int __f2dace_SOA_w_d_1_s_4756;
    int __f2dace_SOA_w_d_2_s_4757;
    double* exner;
    double* rho;
    double* theta_v;
    double* tke;
    double* tracer;
    double* vn;
    double* w;
};
struct t_uuid {
    int* data;
};
struct t_cartesian_coordinates {
    double* x;
};
struct t_grid_geometry_info {
    int cell_type;
    t_cartesian_coordinates* center;
    double domain_height;
    double domain_length;
    int geometry_type;
    int grid_creation_process;
    int grid_optimization_process;
    double mean_cell_area;
    double mean_characteristic_length;
    double mean_dual_cell_area;
    double mean_dual_edge_length;
    double mean_edge_length;
    double sphere_radius;
};
struct t_grid_cells {
    int __f2dace_SA_area_d_0_s_3131;
    int __f2dace_SA_area_d_1_s_3132;
    int __f2dace_SA_cartesian_center_d_0_s_3137;
    int __f2dace_SA_cartesian_center_d_1_s_3138;
    int __f2dace_SA_center_d_0_s_3129;
    int __f2dace_SA_center_d_1_s_3130;
    int __f2dace_SA_child_blk_d_0_s_3101;
    int __f2dace_SA_child_blk_d_1_s_3102;
    int __f2dace_SA_child_blk_d_2_s_3103;
    int __f2dace_SA_child_id_d_0_s_3104;
    int __f2dace_SA_child_id_d_1_s_3105;
    int __f2dace_SA_child_idx_d_0_s_3098;
    int __f2dace_SA_child_idx_d_1_s_3099;
    int __f2dace_SA_child_idx_d_2_s_3100;
    int __f2dace_SA_cz_c_d_0_s_3135;
    int __f2dace_SA_cz_c_d_1_s_3136;
    int __f2dace_SA_ddqz_z_full_d_0_s_3153;
    int __f2dace_SA_ddqz_z_full_d_1_s_3154;
    int __f2dace_SA_ddqz_z_full_d_2_s_3155;
    int __f2dace_SA_edge_blk_d_0_s_3117;
    int __f2dace_SA_edge_blk_d_1_s_3118;
    int __f2dace_SA_edge_blk_d_2_s_3119;
    int __f2dace_SA_edge_idx_d_0_s_3114;
    int __f2dace_SA_edge_idx_d_1_s_3115;
    int __f2dace_SA_edge_idx_d_2_s_3116;
    int __f2dace_SA_edge_orientation_d_0_s_3126;
    int __f2dace_SA_edge_orientation_d_1_s_3127;
    int __f2dace_SA_edge_orientation_d_2_s_3128;
    int __f2dace_SA_end_blk_d_0_s_3150;
    int __f2dace_SA_end_blk_d_1_s_3151;
    int __f2dace_SA_end_block_d_0_s_3152;
    int __f2dace_SA_end_idx_d_0_s_3144;
    int __f2dace_SA_end_idx_d_1_s_3145;
    int __f2dace_SA_end_index_d_0_s_3146;
    int __f2dace_SA_f_c_d_0_s_3133;
    int __f2dace_SA_f_c_d_1_s_3134;
    int __f2dace_SA_neighbor_blk_d_0_s_3111;
    int __f2dace_SA_neighbor_blk_d_1_s_3112;
    int __f2dace_SA_neighbor_blk_d_2_s_3113;
    int __f2dace_SA_neighbor_idx_d_0_s_3108;
    int __f2dace_SA_neighbor_idx_d_1_s_3109;
    int __f2dace_SA_neighbor_idx_d_2_s_3110;
    int __f2dace_SA_num_edges_d_0_s_3086;
    int __f2dace_SA_num_edges_d_1_s_3087;
    int __f2dace_SA_parent_glb_blk_d_0_s_3094;
    int __f2dace_SA_parent_glb_blk_d_1_s_3095;
    int __f2dace_SA_parent_glb_idx_d_0_s_3090;
    int __f2dace_SA_parent_glb_idx_d_1_s_3091;
    int __f2dace_SA_parent_loc_blk_d_0_s_3092;
    int __f2dace_SA_parent_loc_blk_d_1_s_3093;
    int __f2dace_SA_parent_loc_idx_d_0_s_3088;
    int __f2dace_SA_parent_loc_idx_d_1_s_3089;
    int __f2dace_SA_pc_idx_d_0_s_3096;
    int __f2dace_SA_pc_idx_d_1_s_3097;
    int __f2dace_SA_phys_id_d_0_s_3106;
    int __f2dace_SA_phys_id_d_1_s_3107;
    int __f2dace_SA_refin_ctrl_d_0_s_3139;
    int __f2dace_SA_refin_ctrl_d_1_s_3140;
    int __f2dace_SA_start_blk_d_0_s_3147;
    int __f2dace_SA_start_blk_d_1_s_3148;
    int __f2dace_SA_start_block_d_0_s_3149;
    int __f2dace_SA_start_idx_d_0_s_3141;
    int __f2dace_SA_start_idx_d_1_s_3142;
    int __f2dace_SA_start_index_d_0_s_3143;
    int __f2dace_SA_vertex_blk_d_0_s_3123;
    int __f2dace_SA_vertex_blk_d_1_s_3124;
    int __f2dace_SA_vertex_blk_d_2_s_3125;
    int __f2dace_SA_vertex_idx_d_0_s_3120;
    int __f2dace_SA_vertex_idx_d_1_s_3121;
    int __f2dace_SA_vertex_idx_d_2_s_3122;
    int __f2dace_SOA_area_d_0_s_3131;
    int __f2dace_SOA_area_d_1_s_3132;
    int __f2dace_SOA_cartesian_center_d_0_s_3137;
    int __f2dace_SOA_cartesian_center_d_1_s_3138;
    int __f2dace_SOA_center_d_0_s_3129;
    int __f2dace_SOA_center_d_1_s_3130;
    int __f2dace_SOA_child_blk_d_0_s_3101;
    int __f2dace_SOA_child_blk_d_1_s_3102;
    int __f2dace_SOA_child_blk_d_2_s_3103;
    int __f2dace_SOA_child_id_d_0_s_3104;
    int __f2dace_SOA_child_id_d_1_s_3105;
    int __f2dace_SOA_child_idx_d_0_s_3098;
    int __f2dace_SOA_child_idx_d_1_s_3099;
    int __f2dace_SOA_child_idx_d_2_s_3100;
    int __f2dace_SOA_cz_c_d_0_s_3135;
    int __f2dace_SOA_cz_c_d_1_s_3136;
    int __f2dace_SOA_ddqz_z_full_d_0_s_3153;
    int __f2dace_SOA_ddqz_z_full_d_1_s_3154;
    int __f2dace_SOA_ddqz_z_full_d_2_s_3155;
    int __f2dace_SOA_edge_blk_d_0_s_3117;
    int __f2dace_SOA_edge_blk_d_1_s_3118;
    int __f2dace_SOA_edge_blk_d_2_s_3119;
    int __f2dace_SOA_edge_idx_d_0_s_3114;
    int __f2dace_SOA_edge_idx_d_1_s_3115;
    int __f2dace_SOA_edge_idx_d_2_s_3116;
    int __f2dace_SOA_edge_orientation_d_0_s_3126;
    int __f2dace_SOA_edge_orientation_d_1_s_3127;
    int __f2dace_SOA_edge_orientation_d_2_s_3128;
    int __f2dace_SOA_end_blk_d_0_s_3150;
    int __f2dace_SOA_end_blk_d_1_s_3151;
    int __f2dace_SOA_end_block_d_0_s_3152;
    int __f2dace_SOA_end_idx_d_0_s_3144;
    int __f2dace_SOA_end_idx_d_1_s_3145;
    int __f2dace_SOA_end_index_d_0_s_3146;
    int __f2dace_SOA_f_c_d_0_s_3133;
    int __f2dace_SOA_f_c_d_1_s_3134;
    int __f2dace_SOA_neighbor_blk_d_0_s_3111;
    int __f2dace_SOA_neighbor_blk_d_1_s_3112;
    int __f2dace_SOA_neighbor_blk_d_2_s_3113;
    int __f2dace_SOA_neighbor_idx_d_0_s_3108;
    int __f2dace_SOA_neighbor_idx_d_1_s_3109;
    int __f2dace_SOA_neighbor_idx_d_2_s_3110;
    int __f2dace_SOA_num_edges_d_0_s_3086;
    int __f2dace_SOA_num_edges_d_1_s_3087;
    int __f2dace_SOA_parent_glb_blk_d_0_s_3094;
    int __f2dace_SOA_parent_glb_blk_d_1_s_3095;
    int __f2dace_SOA_parent_glb_idx_d_0_s_3090;
    int __f2dace_SOA_parent_glb_idx_d_1_s_3091;
    int __f2dace_SOA_parent_loc_blk_d_0_s_3092;
    int __f2dace_SOA_parent_loc_blk_d_1_s_3093;
    int __f2dace_SOA_parent_loc_idx_d_0_s_3088;
    int __f2dace_SOA_parent_loc_idx_d_1_s_3089;
    int __f2dace_SOA_pc_idx_d_0_s_3096;
    int __f2dace_SOA_pc_idx_d_1_s_3097;
    int __f2dace_SOA_phys_id_d_0_s_3106;
    int __f2dace_SOA_phys_id_d_1_s_3107;
    int __f2dace_SOA_refin_ctrl_d_0_s_3139;
    int __f2dace_SOA_refin_ctrl_d_1_s_3140;
    int __f2dace_SOA_start_blk_d_0_s_3147;
    int __f2dace_SOA_start_blk_d_1_s_3148;
    int __f2dace_SOA_start_block_d_0_s_3149;
    int __f2dace_SOA_start_idx_d_0_s_3141;
    int __f2dace_SOA_start_idx_d_1_s_3142;
    int __f2dace_SOA_start_index_d_0_s_3143;
    int __f2dace_SOA_vertex_blk_d_0_s_3123;
    int __f2dace_SOA_vertex_blk_d_1_s_3124;
    int __f2dace_SOA_vertex_blk_d_2_s_3125;
    int __f2dace_SOA_vertex_idx_d_0_s_3120;
    int __f2dace_SOA_vertex_idx_d_1_s_3121;
    int __f2dace_SOA_vertex_idx_d_2_s_3122;
    t_subset_range* all;
    double* area;
    t_grid_domain_decomp_info* decomp_info;
    int dummy_cell_block;
    int dummy_cell_index;
    int* edge_blk;
    int* edge_idx;
    int* end_blk;
    int* end_block;
    int* end_index;
    t_subset_range* in_domain;
    int max_connectivity;
    int* neighbor_blk;
    int* neighbor_idx;
    t_subset_range* not_in_domain;
    t_subset_range* not_owned;
    t_subset_range* one_edge_in_domain;
    t_subset_range* owned;
    t_subset_range* owned_no_boundary;
    int* start_blk;
    int* start_block;
    int* start_index;
    int* vertex_blk;
    int* vertex_idx;
};
struct t_grid_edges {
    int __f2dace_SA_area_edge_d_0_s_3245;
    int __f2dace_SA_area_edge_d_1_s_3246;
    int __f2dace_SA_butterfly_blk_d_0_s_3203;
    int __f2dace_SA_butterfly_blk_d_1_s_3204;
    int __f2dace_SA_butterfly_blk_d_2_s_3205;
    int __f2dace_SA_butterfly_blk_d_3_s_3206;
    int __f2dace_SA_butterfly_idx_d_0_s_3199;
    int __f2dace_SA_butterfly_idx_d_1_s_3200;
    int __f2dace_SA_butterfly_idx_d_2_s_3201;
    int __f2dace_SA_butterfly_idx_d_3_s_3202;
    int __f2dace_SA_cartesian_center_d_0_s_3249;
    int __f2dace_SA_cartesian_center_d_1_s_3250;
    int __f2dace_SA_cartesian_dual_middle_d_0_s_3251;
    int __f2dace_SA_cartesian_dual_middle_d_1_s_3252;
    int __f2dace_SA_cell_blk_d_0_s_3179;
    int __f2dace_SA_cell_blk_d_1_s_3180;
    int __f2dace_SA_cell_blk_d_2_s_3181;
    int __f2dace_SA_cell_idx_d_0_s_3176;
    int __f2dace_SA_cell_idx_d_1_s_3177;
    int __f2dace_SA_cell_idx_d_2_s_3178;
    int __f2dace_SA_center_d_0_s_3207;
    int __f2dace_SA_center_d_1_s_3208;
    int __f2dace_SA_child_blk_d_0_s_3169;
    int __f2dace_SA_child_blk_d_1_s_3170;
    int __f2dace_SA_child_blk_d_2_s_3171;
    int __f2dace_SA_child_id_d_0_s_3172;
    int __f2dace_SA_child_id_d_1_s_3173;
    int __f2dace_SA_child_idx_d_0_s_3166;
    int __f2dace_SA_child_idx_d_1_s_3167;
    int __f2dace_SA_child_idx_d_2_s_3168;
    int __f2dace_SA_cn_e_d_0_s_3259;
    int __f2dace_SA_cn_e_d_1_s_3260;
    int __f2dace_SA_dual_cart_normal_d_0_s_3213;
    int __f2dace_SA_dual_cart_normal_d_1_s_3214;
    int __f2dace_SA_dual_edge_length_d_0_s_3233;
    int __f2dace_SA_dual_edge_length_d_1_s_3234;
    int __f2dace_SA_dual_normal_cell_d_0_s_3220;
    int __f2dace_SA_dual_normal_cell_d_1_s_3221;
    int __f2dace_SA_dual_normal_cell_d_2_s_3222;
    int __f2dace_SA_dual_normal_d_0_s_3215;
    int __f2dace_SA_dual_normal_d_1_s_3216;
    int __f2dace_SA_dual_normal_vert_d_0_s_3226;
    int __f2dace_SA_dual_normal_vert_d_1_s_3227;
    int __f2dace_SA_dual_normal_vert_d_2_s_3228;
    int __f2dace_SA_edge_cell_length_d_0_s_3240;
    int __f2dace_SA_edge_cell_length_d_1_s_3241;
    int __f2dace_SA_edge_cell_length_d_2_s_3242;
    int __f2dace_SA_edge_vert_length_d_0_s_3237;
    int __f2dace_SA_edge_vert_length_d_1_s_3238;
    int __f2dace_SA_edge_vert_length_d_2_s_3239;
    int __f2dace_SA_end_blk_d_0_s_3272;
    int __f2dace_SA_end_blk_d_1_s_3273;
    int __f2dace_SA_end_block_d_0_s_3274;
    int __f2dace_SA_end_idx_d_0_s_3266;
    int __f2dace_SA_end_idx_d_1_s_3267;
    int __f2dace_SA_end_index_d_0_s_3268;
    int __f2dace_SA_f_e_d_0_s_3253;
    int __f2dace_SA_f_e_d_1_s_3254;
    int __f2dace_SA_fn_e_d_0_s_3255;
    int __f2dace_SA_fn_e_d_1_s_3256;
    int __f2dace_SA_ft_e_d_0_s_3257;
    int __f2dace_SA_ft_e_d_1_s_3258;
    int __f2dace_SA_inv_dual_edge_length_d_0_s_3235;
    int __f2dace_SA_inv_dual_edge_length_d_1_s_3236;
    int __f2dace_SA_inv_primal_edge_length_d_0_s_3231;
    int __f2dace_SA_inv_primal_edge_length_d_1_s_3232;
    int __f2dace_SA_inv_vert_vert_length_d_0_s_3243;
    int __f2dace_SA_inv_vert_vert_length_d_1_s_3244;
    int __f2dace_SA_parent_glb_blk_d_0_s_3162;
    int __f2dace_SA_parent_glb_blk_d_1_s_3163;
    int __f2dace_SA_parent_glb_idx_d_0_s_3158;
    int __f2dace_SA_parent_glb_idx_d_1_s_3159;
    int __f2dace_SA_parent_loc_blk_d_0_s_3160;
    int __f2dace_SA_parent_loc_blk_d_1_s_3161;
    int __f2dace_SA_parent_loc_idx_d_0_s_3156;
    int __f2dace_SA_parent_loc_idx_d_1_s_3157;
    int __f2dace_SA_pc_idx_d_0_s_3164;
    int __f2dace_SA_pc_idx_d_1_s_3165;
    int __f2dace_SA_phys_id_d_0_s_3174;
    int __f2dace_SA_phys_id_d_1_s_3175;
    int __f2dace_SA_primal_cart_normal_d_0_s_3211;
    int __f2dace_SA_primal_cart_normal_d_1_s_3212;
    int __f2dace_SA_primal_edge_length_d_0_s_3229;
    int __f2dace_SA_primal_edge_length_d_1_s_3230;
    int __f2dace_SA_primal_normal_cell_d_0_s_3217;
    int __f2dace_SA_primal_normal_cell_d_1_s_3218;
    int __f2dace_SA_primal_normal_cell_d_2_s_3219;
    int __f2dace_SA_primal_normal_d_0_s_3209;
    int __f2dace_SA_primal_normal_d_1_s_3210;
    int __f2dace_SA_primal_normal_vert_d_0_s_3223;
    int __f2dace_SA_primal_normal_vert_d_1_s_3224;
    int __f2dace_SA_primal_normal_vert_d_2_s_3225;
    int __f2dace_SA_quad_area_d_0_s_3247;
    int __f2dace_SA_quad_area_d_1_s_3248;
    int __f2dace_SA_quad_blk_d_0_s_3193;
    int __f2dace_SA_quad_blk_d_1_s_3194;
    int __f2dace_SA_quad_blk_d_2_s_3195;
    int __f2dace_SA_quad_idx_d_0_s_3190;
    int __f2dace_SA_quad_idx_d_1_s_3191;
    int __f2dace_SA_quad_idx_d_2_s_3192;
    int __f2dace_SA_quad_orientation_d_0_s_3196;
    int __f2dace_SA_quad_orientation_d_1_s_3197;
    int __f2dace_SA_quad_orientation_d_2_s_3198;
    int __f2dace_SA_refin_ctrl_d_0_s_3261;
    int __f2dace_SA_refin_ctrl_d_1_s_3262;
    int __f2dace_SA_start_blk_d_0_s_3269;
    int __f2dace_SA_start_blk_d_1_s_3270;
    int __f2dace_SA_start_block_d_0_s_3271;
    int __f2dace_SA_start_idx_d_0_s_3263;
    int __f2dace_SA_start_idx_d_1_s_3264;
    int __f2dace_SA_start_index_d_0_s_3265;
    int __f2dace_SA_tangent_orientation_d_0_s_3188;
    int __f2dace_SA_tangent_orientation_d_1_s_3189;
    int __f2dace_SA_vertex_blk_d_0_s_3185;
    int __f2dace_SA_vertex_blk_d_1_s_3186;
    int __f2dace_SA_vertex_blk_d_2_s_3187;
    int __f2dace_SA_vertex_idx_d_0_s_3182;
    int __f2dace_SA_vertex_idx_d_1_s_3183;
    int __f2dace_SA_vertex_idx_d_2_s_3184;
    int __f2dace_SOA_area_edge_d_0_s_3245;
    int __f2dace_SOA_area_edge_d_1_s_3246;
    int __f2dace_SOA_butterfly_blk_d_0_s_3203;
    int __f2dace_SOA_butterfly_blk_d_1_s_3204;
    int __f2dace_SOA_butterfly_blk_d_2_s_3205;
    int __f2dace_SOA_butterfly_blk_d_3_s_3206;
    int __f2dace_SOA_butterfly_idx_d_0_s_3199;
    int __f2dace_SOA_butterfly_idx_d_1_s_3200;
    int __f2dace_SOA_butterfly_idx_d_2_s_3201;
    int __f2dace_SOA_butterfly_idx_d_3_s_3202;
    int __f2dace_SOA_cartesian_center_d_0_s_3249;
    int __f2dace_SOA_cartesian_center_d_1_s_3250;
    int __f2dace_SOA_cartesian_dual_middle_d_0_s_3251;
    int __f2dace_SOA_cartesian_dual_middle_d_1_s_3252;
    int __f2dace_SOA_cell_blk_d_0_s_3179;
    int __f2dace_SOA_cell_blk_d_1_s_3180;
    int __f2dace_SOA_cell_blk_d_2_s_3181;
    int __f2dace_SOA_cell_idx_d_0_s_3176;
    int __f2dace_SOA_cell_idx_d_1_s_3177;
    int __f2dace_SOA_cell_idx_d_2_s_3178;
    int __f2dace_SOA_center_d_0_s_3207;
    int __f2dace_SOA_center_d_1_s_3208;
    int __f2dace_SOA_child_blk_d_0_s_3169;
    int __f2dace_SOA_child_blk_d_1_s_3170;
    int __f2dace_SOA_child_blk_d_2_s_3171;
    int __f2dace_SOA_child_id_d_0_s_3172;
    int __f2dace_SOA_child_id_d_1_s_3173;
    int __f2dace_SOA_child_idx_d_0_s_3166;
    int __f2dace_SOA_child_idx_d_1_s_3167;
    int __f2dace_SOA_child_idx_d_2_s_3168;
    int __f2dace_SOA_cn_e_d_0_s_3259;
    int __f2dace_SOA_cn_e_d_1_s_3260;
    int __f2dace_SOA_dual_cart_normal_d_0_s_3213;
    int __f2dace_SOA_dual_cart_normal_d_1_s_3214;
    int __f2dace_SOA_dual_edge_length_d_0_s_3233;
    int __f2dace_SOA_dual_edge_length_d_1_s_3234;
    int __f2dace_SOA_dual_normal_cell_d_0_s_3220;
    int __f2dace_SOA_dual_normal_cell_d_1_s_3221;
    int __f2dace_SOA_dual_normal_cell_d_2_s_3222;
    int __f2dace_SOA_dual_normal_d_0_s_3215;
    int __f2dace_SOA_dual_normal_d_1_s_3216;
    int __f2dace_SOA_dual_normal_vert_d_0_s_3226;
    int __f2dace_SOA_dual_normal_vert_d_1_s_3227;
    int __f2dace_SOA_dual_normal_vert_d_2_s_3228;
    int __f2dace_SOA_edge_cell_length_d_0_s_3240;
    int __f2dace_SOA_edge_cell_length_d_1_s_3241;
    int __f2dace_SOA_edge_cell_length_d_2_s_3242;
    int __f2dace_SOA_edge_vert_length_d_0_s_3237;
    int __f2dace_SOA_edge_vert_length_d_1_s_3238;
    int __f2dace_SOA_edge_vert_length_d_2_s_3239;
    int __f2dace_SOA_end_blk_d_0_s_3272;
    int __f2dace_SOA_end_blk_d_1_s_3273;
    int __f2dace_SOA_end_block_d_0_s_3274;
    int __f2dace_SOA_end_idx_d_0_s_3266;
    int __f2dace_SOA_end_idx_d_1_s_3267;
    int __f2dace_SOA_end_index_d_0_s_3268;
    int __f2dace_SOA_f_e_d_0_s_3253;
    int __f2dace_SOA_f_e_d_1_s_3254;
    int __f2dace_SOA_fn_e_d_0_s_3255;
    int __f2dace_SOA_fn_e_d_1_s_3256;
    int __f2dace_SOA_ft_e_d_0_s_3257;
    int __f2dace_SOA_ft_e_d_1_s_3258;
    int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235;
    int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236;
    int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231;
    int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232;
    int __f2dace_SOA_inv_vert_vert_length_d_0_s_3243;
    int __f2dace_SOA_inv_vert_vert_length_d_1_s_3244;
    int __f2dace_SOA_parent_glb_blk_d_0_s_3162;
    int __f2dace_SOA_parent_glb_blk_d_1_s_3163;
    int __f2dace_SOA_parent_glb_idx_d_0_s_3158;
    int __f2dace_SOA_parent_glb_idx_d_1_s_3159;
    int __f2dace_SOA_parent_loc_blk_d_0_s_3160;
    int __f2dace_SOA_parent_loc_blk_d_1_s_3161;
    int __f2dace_SOA_parent_loc_idx_d_0_s_3156;
    int __f2dace_SOA_parent_loc_idx_d_1_s_3157;
    int __f2dace_SOA_pc_idx_d_0_s_3164;
    int __f2dace_SOA_pc_idx_d_1_s_3165;
    int __f2dace_SOA_phys_id_d_0_s_3174;
    int __f2dace_SOA_phys_id_d_1_s_3175;
    int __f2dace_SOA_primal_cart_normal_d_0_s_3211;
    int __f2dace_SOA_primal_cart_normal_d_1_s_3212;
    int __f2dace_SOA_primal_edge_length_d_0_s_3229;
    int __f2dace_SOA_primal_edge_length_d_1_s_3230;
    int __f2dace_SOA_primal_normal_cell_d_0_s_3217;
    int __f2dace_SOA_primal_normal_cell_d_1_s_3218;
    int __f2dace_SOA_primal_normal_cell_d_2_s_3219;
    int __f2dace_SOA_primal_normal_d_0_s_3209;
    int __f2dace_SOA_primal_normal_d_1_s_3210;
    int __f2dace_SOA_primal_normal_vert_d_0_s_3223;
    int __f2dace_SOA_primal_normal_vert_d_1_s_3224;
    int __f2dace_SOA_primal_normal_vert_d_2_s_3225;
    int __f2dace_SOA_quad_area_d_0_s_3247;
    int __f2dace_SOA_quad_area_d_1_s_3248;
    int __f2dace_SOA_quad_blk_d_0_s_3193;
    int __f2dace_SOA_quad_blk_d_1_s_3194;
    int __f2dace_SOA_quad_blk_d_2_s_3195;
    int __f2dace_SOA_quad_idx_d_0_s_3190;
    int __f2dace_SOA_quad_idx_d_1_s_3191;
    int __f2dace_SOA_quad_idx_d_2_s_3192;
    int __f2dace_SOA_quad_orientation_d_0_s_3196;
    int __f2dace_SOA_quad_orientation_d_1_s_3197;
    int __f2dace_SOA_quad_orientation_d_2_s_3198;
    int __f2dace_SOA_refin_ctrl_d_0_s_3261;
    int __f2dace_SOA_refin_ctrl_d_1_s_3262;
    int __f2dace_SOA_start_blk_d_0_s_3269;
    int __f2dace_SOA_start_blk_d_1_s_3270;
    int __f2dace_SOA_start_block_d_0_s_3271;
    int __f2dace_SOA_start_idx_d_0_s_3263;
    int __f2dace_SOA_start_idx_d_1_s_3264;
    int __f2dace_SOA_start_index_d_0_s_3265;
    int __f2dace_SOA_tangent_orientation_d_0_s_3188;
    int __f2dace_SOA_tangent_orientation_d_1_s_3189;
    int __f2dace_SOA_vertex_blk_d_0_s_3185;
    int __f2dace_SOA_vertex_blk_d_1_s_3186;
    int __f2dace_SOA_vertex_blk_d_2_s_3187;
    int __f2dace_SOA_vertex_idx_d_0_s_3182;
    int __f2dace_SOA_vertex_idx_d_1_s_3183;
    int __f2dace_SOA_vertex_idx_d_2_s_3184;
    t_subset_range* all;
    double* area_edge;
    int* cell_blk;
    int* cell_idx;
    t_grid_domain_decomp_info* decomp_info;
    int* end_blk;
    int* end_block;
    int* end_index;
    double* f_e;
    t_subset_range* gradiscalculable;
    t_subset_range* in_domain;
    double* inv_dual_edge_length;
    double* inv_primal_edge_length;
    t_subset_range* not_in_domain;
    t_subset_range* not_owned;
    t_subset_range* owned;
    int* quad_blk;
    int* quad_idx;
    int* start_blk;
    int* start_block;
    int* start_index;
    double* tangent_orientation;
    int* vertex_blk;
    int* vertex_idx;
};
struct t_comm_gather_pattern {
    int __f2dace_SA_collector_pes_d_0_s_3330;
    int __f2dace_SA_collector_send_size_d_0_s_3332;
    int __f2dace_SA_collector_size_d_0_s_3331;
    int __f2dace_SA_loc_index_d_0_s_3333;
    int __f2dace_SA_recv_buffer_reorder_d_0_s_3334;
    int __f2dace_SA_recv_buffer_reorder_fill_d_0_s_3335;
    int __f2dace_SA_recv_pes_d_0_s_3336;
    int __f2dace_SA_recv_size_d_0_s_3337;
    int __f2dace_SOA_collector_pes_d_0_s_3330;
    int __f2dace_SOA_collector_send_size_d_0_s_3332;
    int __f2dace_SOA_collector_size_d_0_s_3331;
    int __f2dace_SOA_loc_index_d_0_s_3333;
    int __f2dace_SOA_recv_buffer_reorder_d_0_s_3334;
    int __f2dace_SOA_recv_buffer_reorder_fill_d_0_s_3335;
    int __f2dace_SOA_recv_pes_d_0_s_3336;
    int __f2dace_SOA_recv_size_d_0_s_3337;
    int global_size;
};
struct t_p_comm_pattern {

};
struct t_patch {
    int alloc_cell_blocks;
    int boundary_depth_index;
    t_grid_cells* cells;
    int* child_id;
    int* child_id_list;
    int comm;
    t_comm_gather_pattern* comm_pat_gather_c;
    t_comm_gather_pattern* comm_pat_gather_e;
    t_comm_gather_pattern* comm_pat_gather_v;
    t_p_comm_pattern** comm_pat_work2test;
    int compute_is_parallel;
    int domain_is_owned;
    t_grid_edges* edges;
    t_grid_geometry_info* geometry_info;
    char grid_filename;
    char grid_filename_grfinfo;
    t_uuid* grid_uuid;
    int id;
    int is_in_parallel_test;
    int ldom_active;
    int level;
    int max_childdom;
    int n_chd_total;
    int n_childdom;
    int n_patch_cells;
    int n_patch_cells_g;
    int n_patch_edges;
    int n_patch_edges_g;
    int n_patch_verts;
    int n_patch_verts_g;
    int n_proc;
    int nblks_c;
    int nblks_e;
    int nblks_v;
    int nest_level;
    int nlev;
    int nlevp1;
    int npromz_c;
    int npromz_e;
    int npromz_v;
    int nshift;
    int nshift_child;
    int nshift_total;
    int parallel_test_communicator;
    int parent_child_index;
    int parent_id;
    int proc0;
    int rank;
    int sync_cells_not_in_domain;
    int sync_cells_not_owned;
    int sync_cells_one_edge_in_domain;
    int sync_edges_not_in_domain;
    int sync_edges_not_owned;
    int sync_verts_not_in_domain;
    int sync_verts_not_owned;
    t_grid_vertices* verts;
    int work_communicator;
};
struct t_nh_diag {
    int __f2dace_SA_airmass_new_d_0_s_4851;
    int __f2dace_SA_airmass_new_d_1_s_4852;
    int __f2dace_SA_airmass_new_d_2_s_4853;
    int __f2dace_SA_airmass_now_d_0_s_4848;
    int __f2dace_SA_airmass_now_d_1_s_4849;
    int __f2dace_SA_airmass_now_d_2_s_4850;
    int __f2dace_SA_ddt_exner_phy_d_0_s_4980;
    int __f2dace_SA_ddt_exner_phy_d_1_s_4981;
    int __f2dace_SA_ddt_exner_phy_d_2_s_4982;
    int __f2dace_SA_ddt_grf_trc_ptr_d_0_s_5131;
    int __f2dace_SA_ddt_pres_sfc_d_0_s_4823;
    int __f2dace_SA_ddt_pres_sfc_d_1_s_4824;
    int __f2dace_SA_ddt_temp_dyn_d_0_s_5121;
    int __f2dace_SA_ddt_temp_dyn_d_1_s_5122;
    int __f2dace_SA_ddt_temp_dyn_d_2_s_5123;
    int __f2dace_SA_ddt_tracer_adv_d_0_s_4794;
    int __f2dace_SA_ddt_tracer_adv_d_1_s_4795;
    int __f2dace_SA_ddt_tracer_adv_d_2_s_4796;
    int __f2dace_SA_ddt_tracer_adv_d_3_s_4797;
    int __f2dace_SA_ddt_trc_adv_ptr_d_0_s_5134;
    int __f2dace_SA_ddt_ua_adv_d_0_s_5052;
    int __f2dace_SA_ddt_ua_adv_d_1_s_5053;
    int __f2dace_SA_ddt_ua_adv_d_2_s_5054;
    int __f2dace_SA_ddt_ua_cen_d_0_s_5088;
    int __f2dace_SA_ddt_ua_cen_d_1_s_5089;
    int __f2dace_SA_ddt_ua_cen_d_2_s_5090;
    int __f2dace_SA_ddt_ua_cor_d_0_s_5061;
    int __f2dace_SA_ddt_ua_cor_d_1_s_5062;
    int __f2dace_SA_ddt_ua_cor_d_2_s_5063;
    int __f2dace_SA_ddt_ua_dmp_d_0_s_5034;
    int __f2dace_SA_ddt_ua_dmp_d_1_s_5035;
    int __f2dace_SA_ddt_ua_dmp_d_2_s_5036;
    int __f2dace_SA_ddt_ua_dyn_d_0_s_5025;
    int __f2dace_SA_ddt_ua_dyn_d_1_s_5026;
    int __f2dace_SA_ddt_ua_dyn_d_2_s_5027;
    int __f2dace_SA_ddt_ua_grf_d_0_s_5115;
    int __f2dace_SA_ddt_ua_grf_d_1_s_5116;
    int __f2dace_SA_ddt_ua_grf_d_2_s_5117;
    int __f2dace_SA_ddt_ua_hdf_d_0_s_5043;
    int __f2dace_SA_ddt_ua_hdf_d_1_s_5044;
    int __f2dace_SA_ddt_ua_hdf_d_2_s_5045;
    int __f2dace_SA_ddt_ua_iau_d_0_s_5097;
    int __f2dace_SA_ddt_ua_iau_d_1_s_5098;
    int __f2dace_SA_ddt_ua_iau_d_2_s_5099;
    int __f2dace_SA_ddt_ua_pgr_d_0_s_5070;
    int __f2dace_SA_ddt_ua_pgr_d_1_s_5071;
    int __f2dace_SA_ddt_ua_pgr_d_2_s_5072;
    int __f2dace_SA_ddt_ua_phd_d_0_s_5079;
    int __f2dace_SA_ddt_ua_phd_d_1_s_5080;
    int __f2dace_SA_ddt_ua_phd_d_2_s_5081;
    int __f2dace_SA_ddt_ua_ray_d_0_s_5106;
    int __f2dace_SA_ddt_ua_ray_d_1_s_5107;
    int __f2dace_SA_ddt_ua_ray_d_2_s_5108;
    int __f2dace_SA_ddt_va_adv_d_0_s_5055;
    int __f2dace_SA_ddt_va_adv_d_1_s_5056;
    int __f2dace_SA_ddt_va_adv_d_2_s_5057;
    int __f2dace_SA_ddt_va_cen_d_0_s_5091;
    int __f2dace_SA_ddt_va_cen_d_1_s_5092;
    int __f2dace_SA_ddt_va_cen_d_2_s_5093;
    int __f2dace_SA_ddt_va_cor_d_0_s_5064;
    int __f2dace_SA_ddt_va_cor_d_1_s_5065;
    int __f2dace_SA_ddt_va_cor_d_2_s_5066;
    int __f2dace_SA_ddt_va_dmp_d_0_s_5037;
    int __f2dace_SA_ddt_va_dmp_d_1_s_5038;
    int __f2dace_SA_ddt_va_dmp_d_2_s_5039;
    int __f2dace_SA_ddt_va_dyn_d_0_s_5028;
    int __f2dace_SA_ddt_va_dyn_d_1_s_5029;
    int __f2dace_SA_ddt_va_dyn_d_2_s_5030;
    int __f2dace_SA_ddt_va_grf_d_0_s_5118;
    int __f2dace_SA_ddt_va_grf_d_1_s_5119;
    int __f2dace_SA_ddt_va_grf_d_2_s_5120;
    int __f2dace_SA_ddt_va_hdf_d_0_s_5046;
    int __f2dace_SA_ddt_va_hdf_d_1_s_5047;
    int __f2dace_SA_ddt_va_hdf_d_2_s_5048;
    int __f2dace_SA_ddt_va_iau_d_0_s_5100;
    int __f2dace_SA_ddt_va_iau_d_1_s_5101;
    int __f2dace_SA_ddt_va_iau_d_2_s_5102;
    int __f2dace_SA_ddt_va_pgr_d_0_s_5073;
    int __f2dace_SA_ddt_va_pgr_d_1_s_5074;
    int __f2dace_SA_ddt_va_pgr_d_2_s_5075;
    int __f2dace_SA_ddt_va_phd_d_0_s_5082;
    int __f2dace_SA_ddt_va_phd_d_1_s_5083;
    int __f2dace_SA_ddt_va_phd_d_2_s_5084;
    int __f2dace_SA_ddt_va_ray_d_0_s_5109;
    int __f2dace_SA_ddt_va_ray_d_1_s_5110;
    int __f2dace_SA_ddt_va_ray_d_2_s_5111;
    int __f2dace_SA_ddt_vn_adv_d_0_s_5049;
    int __f2dace_SA_ddt_vn_adv_d_1_s_5050;
    int __f2dace_SA_ddt_vn_adv_d_2_s_5051;
    int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998;
    int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999;
    int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000;
    int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001;
    int __f2dace_SA_ddt_vn_apc_pc_ptr_d_0_s_5138;
    int __f2dace_SA_ddt_vn_cen_d_0_s_5085;
    int __f2dace_SA_ddt_vn_cen_d_1_s_5086;
    int __f2dace_SA_ddt_vn_cen_d_2_s_5087;
    int __f2dace_SA_ddt_vn_cor_d_0_s_5058;
    int __f2dace_SA_ddt_vn_cor_d_1_s_5059;
    int __f2dace_SA_ddt_vn_cor_d_2_s_5060;
    int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002;
    int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003;
    int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004;
    int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005;
    int __f2dace_SA_ddt_vn_cor_pc_ptr_d_0_s_5139;
    int __f2dace_SA_ddt_vn_dmp_d_0_s_5031;
    int __f2dace_SA_ddt_vn_dmp_d_1_s_5032;
    int __f2dace_SA_ddt_vn_dmp_d_2_s_5033;
    int __f2dace_SA_ddt_vn_dyn_d_0_s_5022;
    int __f2dace_SA_ddt_vn_dyn_d_1_s_5023;
    int __f2dace_SA_ddt_vn_dyn_d_2_s_5024;
    int __f2dace_SA_ddt_vn_grf_d_0_s_5112;
    int __f2dace_SA_ddt_vn_grf_d_1_s_5113;
    int __f2dace_SA_ddt_vn_grf_d_2_s_5114;
    int __f2dace_SA_ddt_vn_hdf_d_0_s_5040;
    int __f2dace_SA_ddt_vn_hdf_d_1_s_5041;
    int __f2dace_SA_ddt_vn_hdf_d_2_s_5042;
    int __f2dace_SA_ddt_vn_iau_d_0_s_5094;
    int __f2dace_SA_ddt_vn_iau_d_1_s_5095;
    int __f2dace_SA_ddt_vn_iau_d_2_s_5096;
    int __f2dace_SA_ddt_vn_pgr_d_0_s_5067;
    int __f2dace_SA_ddt_vn_pgr_d_1_s_5068;
    int __f2dace_SA_ddt_vn_pgr_d_2_s_5069;
    int __f2dace_SA_ddt_vn_phd_d_0_s_5076;
    int __f2dace_SA_ddt_vn_phd_d_1_s_5077;
    int __f2dace_SA_ddt_vn_phd_d_2_s_5078;
    int __f2dace_SA_ddt_vn_phy_d_0_s_4983;
    int __f2dace_SA_ddt_vn_phy_d_1_s_4984;
    int __f2dace_SA_ddt_vn_phy_d_2_s_4985;
    int __f2dace_SA_ddt_vn_ray_d_0_s_5103;
    int __f2dace_SA_ddt_vn_ray_d_1_s_5104;
    int __f2dace_SA_ddt_vn_ray_d_2_s_5105;
    int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006;
    int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007;
    int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008;
    int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009;
    int __f2dace_SA_ddt_w_adv_pc_ptr_d_0_s_5140;
    int __f2dace_SA_div_d_0_s_4836;
    int __f2dace_SA_div_d_1_s_4837;
    int __f2dace_SA_div_d_2_s_4838;
    int __f2dace_SA_div_ic_d_0_s_5010;
    int __f2dace_SA_div_ic_d_1_s_5011;
    int __f2dace_SA_div_ic_d_2_s_5012;
    int __f2dace_SA_dpres_mc_d_0_s_4825;
    int __f2dace_SA_dpres_mc_d_1_s_4826;
    int __f2dace_SA_dpres_mc_d_2_s_4827;
    int __f2dace_SA_dwdx_d_0_s_5016;
    int __f2dace_SA_dwdx_d_1_s_5017;
    int __f2dace_SA_dwdx_d_2_s_5018;
    int __f2dace_SA_dwdy_d_0_s_5019;
    int __f2dace_SA_dwdy_d_1_s_5020;
    int __f2dace_SA_dwdy_d_2_s_5021;
    int __f2dace_SA_exner_dyn_incr_d_0_s_4986;
    int __f2dace_SA_exner_dyn_incr_d_1_s_4987;
    int __f2dace_SA_exner_dyn_incr_d_2_s_4988;
    int __f2dace_SA_exner_incr_d_0_s_4932;
    int __f2dace_SA_exner_incr_d_1_s_4933;
    int __f2dace_SA_exner_incr_d_2_s_4934;
    int __f2dace_SA_exner_pr_d_0_s_4801;
    int __f2dace_SA_exner_pr_d_1_s_4802;
    int __f2dace_SA_exner_pr_d_2_s_4803;
    int __f2dace_SA_extra_2d_d_0_s_5124;
    int __f2dace_SA_extra_2d_d_1_s_5125;
    int __f2dace_SA_extra_2d_d_2_s_5126;
    int __f2dace_SA_extra_2d_ptr_d_0_s_5136;
    int __f2dace_SA_extra_3d_d_0_s_5127;
    int __f2dace_SA_extra_3d_d_1_s_5128;
    int __f2dace_SA_extra_3d_d_2_s_5129;
    int __f2dace_SA_extra_3d_d_3_s_5130;
    int __f2dace_SA_extra_3d_ptr_d_0_s_5137;
    int __f2dace_SA_grf_bdy_mflx_d_0_s_4866;
    int __f2dace_SA_grf_bdy_mflx_d_1_s_4867;
    int __f2dace_SA_grf_bdy_mflx_d_2_s_4868;
    int __f2dace_SA_grf_tend_mflx_d_0_s_4863;
    int __f2dace_SA_grf_tend_mflx_d_1_s_4864;
    int __f2dace_SA_grf_tend_mflx_d_2_s_4865;
    int __f2dace_SA_grf_tend_rho_d_0_s_4860;
    int __f2dace_SA_grf_tend_rho_d_1_s_4861;
    int __f2dace_SA_grf_tend_rho_d_2_s_4862;
    int __f2dace_SA_grf_tend_thv_d_0_s_4869;
    int __f2dace_SA_grf_tend_thv_d_1_s_4870;
    int __f2dace_SA_grf_tend_thv_d_2_s_4871;
    int __f2dace_SA_grf_tend_tracer_d_0_s_4872;
    int __f2dace_SA_grf_tend_tracer_d_1_s_4873;
    int __f2dace_SA_grf_tend_tracer_d_2_s_4874;
    int __f2dace_SA_grf_tend_tracer_d_3_s_4875;
    int __f2dace_SA_grf_tend_vn_d_0_s_4854;
    int __f2dace_SA_grf_tend_vn_d_1_s_4855;
    int __f2dace_SA_grf_tend_vn_d_2_s_4856;
    int __f2dace_SA_grf_tend_w_d_0_s_4857;
    int __f2dace_SA_grf_tend_w_d_1_s_4858;
    int __f2dace_SA_grf_tend_w_d_2_s_4859;
    int __f2dace_SA_hdef_ic_d_0_s_5013;
    int __f2dace_SA_hdef_ic_d_1_s_5014;
    int __f2dace_SA_hdef_ic_d_2_s_5015;
    int __f2dace_SA_hfl_tracer_d_0_s_4828;
    int __f2dace_SA_hfl_tracer_d_1_s_4829;
    int __f2dace_SA_hfl_tracer_d_2_s_4830;
    int __f2dace_SA_hfl_tracer_d_3_s_4831;
    int __f2dace_SA_hfl_trc_ptr_d_0_s_5132;
    int __f2dace_SA_mass_fl_e_d_0_s_4839;
    int __f2dace_SA_mass_fl_e_d_1_s_4840;
    int __f2dace_SA_mass_fl_e_d_2_s_4841;
    int __f2dace_SA_mass_fl_e_sv_d_0_s_4995;
    int __f2dace_SA_mass_fl_e_sv_d_1_s_4996;
    int __f2dace_SA_mass_fl_e_sv_d_2_s_4997;
    int __f2dace_SA_mflx_ic_int_d_0_s_4900;
    int __f2dace_SA_mflx_ic_int_d_1_s_4901;
    int __f2dace_SA_mflx_ic_int_d_2_s_4902;
    int __f2dace_SA_mflx_ic_ubc_d_0_s_4903;
    int __f2dace_SA_mflx_ic_ubc_d_1_s_4904;
    int __f2dace_SA_mflx_ic_ubc_d_2_s_4905;
    int __f2dace_SA_omega_d_0_s_4920;
    int __f2dace_SA_omega_d_1_s_4921;
    int __f2dace_SA_omega_d_2_s_4922;
    int __f2dace_SA_omega_z_d_0_s_4788;
    int __f2dace_SA_omega_z_d_1_s_4789;
    int __f2dace_SA_omega_z_d_2_s_4790;
    int __f2dace_SA_p_avginc_d_0_s_4914;
    int __f2dace_SA_p_avginc_d_1_s_4915;
    int __f2dace_SA_pres_d_0_s_4813;
    int __f2dace_SA_pres_d_1_s_4814;
    int __f2dace_SA_pres_d_2_s_4815;
    int __f2dace_SA_pres_ifc_d_0_s_4816;
    int __f2dace_SA_pres_ifc_d_1_s_4817;
    int __f2dace_SA_pres_ifc_d_2_s_4818;
    int __f2dace_SA_pres_msl_d_0_s_4918;
    int __f2dace_SA_pres_msl_d_1_s_4919;
    int __f2dace_SA_pres_sfc_d_0_s_4819;
    int __f2dace_SA_pres_sfc_d_1_s_4820;
    int __f2dace_SA_pres_sfc_old_d_0_s_4821;
    int __f2dace_SA_pres_sfc_old_d_1_s_4822;
    int __f2dace_SA_rh_avginc_d_0_s_4908;
    int __f2dace_SA_rh_avginc_d_1_s_4909;
    int __f2dace_SA_rho_ic_d_0_s_4842;
    int __f2dace_SA_rho_ic_d_1_s_4843;
    int __f2dace_SA_rho_ic_d_2_s_4844;
    int __f2dace_SA_rho_ic_int_d_0_s_4894;
    int __f2dace_SA_rho_ic_int_d_1_s_4895;
    int __f2dace_SA_rho_ic_int_d_2_s_4896;
    int __f2dace_SA_rho_ic_ubc_d_0_s_4897;
    int __f2dace_SA_rho_ic_ubc_d_1_s_4898;
    int __f2dace_SA_rho_ic_ubc_d_2_s_4899;
    int __f2dace_SA_rho_incr_d_0_s_4935;
    int __f2dace_SA_rho_incr_d_1_s_4936;
    int __f2dace_SA_rho_incr_d_2_s_4937;
    int __f2dace_SA_rhoc_incr_d_0_s_4941;
    int __f2dace_SA_rhoc_incr_d_1_s_4942;
    int __f2dace_SA_rhoc_incr_d_2_s_4943;
    int __f2dace_SA_rhog_incr_d_0_s_4953;
    int __f2dace_SA_rhog_incr_d_1_s_4954;
    int __f2dace_SA_rhog_incr_d_2_s_4955;
    int __f2dace_SA_rhoh_incr_d_0_s_4956;
    int __f2dace_SA_rhoh_incr_d_1_s_4957;
    int __f2dace_SA_rhoh_incr_d_2_s_4958;
    int __f2dace_SA_rhoi_incr_d_0_s_4944;
    int __f2dace_SA_rhoi_incr_d_1_s_4945;
    int __f2dace_SA_rhoi_incr_d_2_s_4946;
    int __f2dace_SA_rhonc_incr_d_0_s_4959;
    int __f2dace_SA_rhonc_incr_d_1_s_4960;
    int __f2dace_SA_rhonc_incr_d_2_s_4961;
    int __f2dace_SA_rhong_incr_d_0_s_4971;
    int __f2dace_SA_rhong_incr_d_1_s_4972;
    int __f2dace_SA_rhong_incr_d_2_s_4973;
    int __f2dace_SA_rhonh_incr_d_0_s_4974;
    int __f2dace_SA_rhonh_incr_d_1_s_4975;
    int __f2dace_SA_rhonh_incr_d_2_s_4976;
    int __f2dace_SA_rhoni_incr_d_0_s_4962;
    int __f2dace_SA_rhoni_incr_d_1_s_4963;
    int __f2dace_SA_rhoni_incr_d_2_s_4964;
    int __f2dace_SA_rhonr_incr_d_0_s_4965;
    int __f2dace_SA_rhonr_incr_d_1_s_4966;
    int __f2dace_SA_rhonr_incr_d_2_s_4967;
    int __f2dace_SA_rhons_incr_d_0_s_4968;
    int __f2dace_SA_rhons_incr_d_1_s_4969;
    int __f2dace_SA_rhons_incr_d_2_s_4970;
    int __f2dace_SA_rhor_incr_d_0_s_4947;
    int __f2dace_SA_rhor_incr_d_1_s_4948;
    int __f2dace_SA_rhor_incr_d_2_s_4949;
    int __f2dace_SA_rhos_incr_d_0_s_4950;
    int __f2dace_SA_rhos_incr_d_1_s_4951;
    int __f2dace_SA_rhos_incr_d_2_s_4952;
    int __f2dace_SA_rhov_incr_d_0_s_4938;
    int __f2dace_SA_rhov_incr_d_1_s_4939;
    int __f2dace_SA_rhov_incr_d_2_s_4940;
    int __f2dace_SA_t2m_bias_d_0_s_4906;
    int __f2dace_SA_t2m_bias_d_1_s_4907;
    int __f2dace_SA_t_avginc_d_0_s_4910;
    int __f2dace_SA_t_avginc_d_1_s_4911;
    int __f2dace_SA_t_wgt_avginc_d_0_s_4912;
    int __f2dace_SA_t_wgt_avginc_d_1_s_4913;
    int __f2dace_SA_temp_d_0_s_4804;
    int __f2dace_SA_temp_d_1_s_4805;
    int __f2dace_SA_temp_d_2_s_4806;
    int __f2dace_SA_temp_ifc_d_0_s_4810;
    int __f2dace_SA_temp_ifc_d_1_s_4811;
    int __f2dace_SA_temp_ifc_d_2_s_4812;
    int __f2dace_SA_tempv_d_0_s_4807;
    int __f2dace_SA_tempv_d_1_s_4808;
    int __f2dace_SA_tempv_d_2_s_4809;
    int __f2dace_SA_theta_v_ic_d_0_s_4845;
    int __f2dace_SA_theta_v_ic_d_1_s_4846;
    int __f2dace_SA_theta_v_ic_d_2_s_4847;
    int __f2dace_SA_theta_v_ic_int_d_0_s_4888;
    int __f2dace_SA_theta_v_ic_int_d_1_s_4889;
    int __f2dace_SA_theta_v_ic_int_d_2_s_4890;
    int __f2dace_SA_theta_v_ic_ubc_d_0_s_4891;
    int __f2dace_SA_theta_v_ic_ubc_d_1_s_4892;
    int __f2dace_SA_theta_v_ic_ubc_d_2_s_4893;
    int __f2dace_SA_tracer_vi_d_0_s_4798;
    int __f2dace_SA_tracer_vi_d_1_s_4799;
    int __f2dace_SA_tracer_vi_d_2_s_4800;
    int __f2dace_SA_tracer_vi_ptr_d_0_s_5135;
    int __f2dace_SA_u_d_0_s_4782;
    int __f2dace_SA_u_d_1_s_4783;
    int __f2dace_SA_u_d_2_s_4784;
    int __f2dace_SA_v_d_0_s_4785;
    int __f2dace_SA_v_d_1_s_4786;
    int __f2dace_SA_v_d_2_s_4787;
    int __f2dace_SA_vabs_avginc_d_0_s_4916;
    int __f2dace_SA_vabs_avginc_d_1_s_4917;
    int __f2dace_SA_vfl_tracer_d_0_s_4832;
    int __f2dace_SA_vfl_tracer_d_1_s_4833;
    int __f2dace_SA_vfl_tracer_d_2_s_4834;
    int __f2dace_SA_vfl_tracer_d_3_s_4835;
    int __f2dace_SA_vfl_trc_ptr_d_0_s_5133;
    int __f2dace_SA_vn_ie_d_0_s_4989;
    int __f2dace_SA_vn_ie_d_1_s_4990;
    int __f2dace_SA_vn_ie_d_2_s_4991;
    int __f2dace_SA_vn_ie_int_d_0_s_4876;
    int __f2dace_SA_vn_ie_int_d_1_s_4877;
    int __f2dace_SA_vn_ie_int_d_2_s_4878;
    int __f2dace_SA_vn_ie_ubc_d_0_s_4879;
    int __f2dace_SA_vn_ie_ubc_d_1_s_4880;
    int __f2dace_SA_vn_ie_ubc_d_2_s_4881;
    int __f2dace_SA_vn_incr_d_0_s_4929;
    int __f2dace_SA_vn_incr_d_1_s_4930;
    int __f2dace_SA_vn_incr_d_2_s_4931;
    int __f2dace_SA_vor_d_0_s_4791;
    int __f2dace_SA_vor_d_1_s_4792;
    int __f2dace_SA_vor_d_2_s_4793;
    int __f2dace_SA_vor_u_d_0_s_4923;
    int __f2dace_SA_vor_u_d_1_s_4924;
    int __f2dace_SA_vor_u_d_2_s_4925;
    int __f2dace_SA_vor_v_d_0_s_4926;
    int __f2dace_SA_vor_v_d_1_s_4927;
    int __f2dace_SA_vor_v_d_2_s_4928;
    int __f2dace_SA_vt_d_0_s_4977;
    int __f2dace_SA_vt_d_1_s_4978;
    int __f2dace_SA_vt_d_2_s_4979;
    int __f2dace_SA_w_concorr_c_d_0_s_4992;
    int __f2dace_SA_w_concorr_c_d_1_s_4993;
    int __f2dace_SA_w_concorr_c_d_2_s_4994;
    int __f2dace_SA_w_int_d_0_s_4882;
    int __f2dace_SA_w_int_d_1_s_4883;
    int __f2dace_SA_w_int_d_2_s_4884;
    int __f2dace_SA_w_ubc_d_0_s_4885;
    int __f2dace_SA_w_ubc_d_1_s_4886;
    int __f2dace_SA_w_ubc_d_2_s_4887;
    int __f2dace_SOA_airmass_new_d_0_s_4851;
    int __f2dace_SOA_airmass_new_d_1_s_4852;
    int __f2dace_SOA_airmass_new_d_2_s_4853;
    int __f2dace_SOA_airmass_now_d_0_s_4848;
    int __f2dace_SOA_airmass_now_d_1_s_4849;
    int __f2dace_SOA_airmass_now_d_2_s_4850;
    int __f2dace_SOA_ddt_exner_phy_d_0_s_4980;
    int __f2dace_SOA_ddt_exner_phy_d_1_s_4981;
    int __f2dace_SOA_ddt_exner_phy_d_2_s_4982;
    int __f2dace_SOA_ddt_grf_trc_ptr_d_0_s_5131;
    int __f2dace_SOA_ddt_pres_sfc_d_0_s_4823;
    int __f2dace_SOA_ddt_pres_sfc_d_1_s_4824;
    int __f2dace_SOA_ddt_temp_dyn_d_0_s_5121;
    int __f2dace_SOA_ddt_temp_dyn_d_1_s_5122;
    int __f2dace_SOA_ddt_temp_dyn_d_2_s_5123;
    int __f2dace_SOA_ddt_tracer_adv_d_0_s_4794;
    int __f2dace_SOA_ddt_tracer_adv_d_1_s_4795;
    int __f2dace_SOA_ddt_tracer_adv_d_2_s_4796;
    int __f2dace_SOA_ddt_tracer_adv_d_3_s_4797;
    int __f2dace_SOA_ddt_trc_adv_ptr_d_0_s_5134;
    int __f2dace_SOA_ddt_ua_adv_d_0_s_5052;
    int __f2dace_SOA_ddt_ua_adv_d_1_s_5053;
    int __f2dace_SOA_ddt_ua_adv_d_2_s_5054;
    int __f2dace_SOA_ddt_ua_cen_d_0_s_5088;
    int __f2dace_SOA_ddt_ua_cen_d_1_s_5089;
    int __f2dace_SOA_ddt_ua_cen_d_2_s_5090;
    int __f2dace_SOA_ddt_ua_cor_d_0_s_5061;
    int __f2dace_SOA_ddt_ua_cor_d_1_s_5062;
    int __f2dace_SOA_ddt_ua_cor_d_2_s_5063;
    int __f2dace_SOA_ddt_ua_dmp_d_0_s_5034;
    int __f2dace_SOA_ddt_ua_dmp_d_1_s_5035;
    int __f2dace_SOA_ddt_ua_dmp_d_2_s_5036;
    int __f2dace_SOA_ddt_ua_dyn_d_0_s_5025;
    int __f2dace_SOA_ddt_ua_dyn_d_1_s_5026;
    int __f2dace_SOA_ddt_ua_dyn_d_2_s_5027;
    int __f2dace_SOA_ddt_ua_grf_d_0_s_5115;
    int __f2dace_SOA_ddt_ua_grf_d_1_s_5116;
    int __f2dace_SOA_ddt_ua_grf_d_2_s_5117;
    int __f2dace_SOA_ddt_ua_hdf_d_0_s_5043;
    int __f2dace_SOA_ddt_ua_hdf_d_1_s_5044;
    int __f2dace_SOA_ddt_ua_hdf_d_2_s_5045;
    int __f2dace_SOA_ddt_ua_iau_d_0_s_5097;
    int __f2dace_SOA_ddt_ua_iau_d_1_s_5098;
    int __f2dace_SOA_ddt_ua_iau_d_2_s_5099;
    int __f2dace_SOA_ddt_ua_pgr_d_0_s_5070;
    int __f2dace_SOA_ddt_ua_pgr_d_1_s_5071;
    int __f2dace_SOA_ddt_ua_pgr_d_2_s_5072;
    int __f2dace_SOA_ddt_ua_phd_d_0_s_5079;
    int __f2dace_SOA_ddt_ua_phd_d_1_s_5080;
    int __f2dace_SOA_ddt_ua_phd_d_2_s_5081;
    int __f2dace_SOA_ddt_ua_ray_d_0_s_5106;
    int __f2dace_SOA_ddt_ua_ray_d_1_s_5107;
    int __f2dace_SOA_ddt_ua_ray_d_2_s_5108;
    int __f2dace_SOA_ddt_va_adv_d_0_s_5055;
    int __f2dace_SOA_ddt_va_adv_d_1_s_5056;
    int __f2dace_SOA_ddt_va_adv_d_2_s_5057;
    int __f2dace_SOA_ddt_va_cen_d_0_s_5091;
    int __f2dace_SOA_ddt_va_cen_d_1_s_5092;
    int __f2dace_SOA_ddt_va_cen_d_2_s_5093;
    int __f2dace_SOA_ddt_va_cor_d_0_s_5064;
    int __f2dace_SOA_ddt_va_cor_d_1_s_5065;
    int __f2dace_SOA_ddt_va_cor_d_2_s_5066;
    int __f2dace_SOA_ddt_va_dmp_d_0_s_5037;
    int __f2dace_SOA_ddt_va_dmp_d_1_s_5038;
    int __f2dace_SOA_ddt_va_dmp_d_2_s_5039;
    int __f2dace_SOA_ddt_va_dyn_d_0_s_5028;
    int __f2dace_SOA_ddt_va_dyn_d_1_s_5029;
    int __f2dace_SOA_ddt_va_dyn_d_2_s_5030;
    int __f2dace_SOA_ddt_va_grf_d_0_s_5118;
    int __f2dace_SOA_ddt_va_grf_d_1_s_5119;
    int __f2dace_SOA_ddt_va_grf_d_2_s_5120;
    int __f2dace_SOA_ddt_va_hdf_d_0_s_5046;
    int __f2dace_SOA_ddt_va_hdf_d_1_s_5047;
    int __f2dace_SOA_ddt_va_hdf_d_2_s_5048;
    int __f2dace_SOA_ddt_va_iau_d_0_s_5100;
    int __f2dace_SOA_ddt_va_iau_d_1_s_5101;
    int __f2dace_SOA_ddt_va_iau_d_2_s_5102;
    int __f2dace_SOA_ddt_va_pgr_d_0_s_5073;
    int __f2dace_SOA_ddt_va_pgr_d_1_s_5074;
    int __f2dace_SOA_ddt_va_pgr_d_2_s_5075;
    int __f2dace_SOA_ddt_va_phd_d_0_s_5082;
    int __f2dace_SOA_ddt_va_phd_d_1_s_5083;
    int __f2dace_SOA_ddt_va_phd_d_2_s_5084;
    int __f2dace_SOA_ddt_va_ray_d_0_s_5109;
    int __f2dace_SOA_ddt_va_ray_d_1_s_5110;
    int __f2dace_SOA_ddt_va_ray_d_2_s_5111;
    int __f2dace_SOA_ddt_vn_adv_d_0_s_5049;
    int __f2dace_SOA_ddt_vn_adv_d_1_s_5050;
    int __f2dace_SOA_ddt_vn_adv_d_2_s_5051;
    int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998;
    int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999;
    int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000;
    int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001;
    int __f2dace_SOA_ddt_vn_apc_pc_ptr_d_0_s_5138;
    int __f2dace_SOA_ddt_vn_cen_d_0_s_5085;
    int __f2dace_SOA_ddt_vn_cen_d_1_s_5086;
    int __f2dace_SOA_ddt_vn_cen_d_2_s_5087;
    int __f2dace_SOA_ddt_vn_cor_d_0_s_5058;
    int __f2dace_SOA_ddt_vn_cor_d_1_s_5059;
    int __f2dace_SOA_ddt_vn_cor_d_2_s_5060;
    int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002;
    int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003;
    int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004;
    int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005;
    int __f2dace_SOA_ddt_vn_cor_pc_ptr_d_0_s_5139;
    int __f2dace_SOA_ddt_vn_dmp_d_0_s_5031;
    int __f2dace_SOA_ddt_vn_dmp_d_1_s_5032;
    int __f2dace_SOA_ddt_vn_dmp_d_2_s_5033;
    int __f2dace_SOA_ddt_vn_dyn_d_0_s_5022;
    int __f2dace_SOA_ddt_vn_dyn_d_1_s_5023;
    int __f2dace_SOA_ddt_vn_dyn_d_2_s_5024;
    int __f2dace_SOA_ddt_vn_grf_d_0_s_5112;
    int __f2dace_SOA_ddt_vn_grf_d_1_s_5113;
    int __f2dace_SOA_ddt_vn_grf_d_2_s_5114;
    int __f2dace_SOA_ddt_vn_hdf_d_0_s_5040;
    int __f2dace_SOA_ddt_vn_hdf_d_1_s_5041;
    int __f2dace_SOA_ddt_vn_hdf_d_2_s_5042;
    int __f2dace_SOA_ddt_vn_iau_d_0_s_5094;
    int __f2dace_SOA_ddt_vn_iau_d_1_s_5095;
    int __f2dace_SOA_ddt_vn_iau_d_2_s_5096;
    int __f2dace_SOA_ddt_vn_pgr_d_0_s_5067;
    int __f2dace_SOA_ddt_vn_pgr_d_1_s_5068;
    int __f2dace_SOA_ddt_vn_pgr_d_2_s_5069;
    int __f2dace_SOA_ddt_vn_phd_d_0_s_5076;
    int __f2dace_SOA_ddt_vn_phd_d_1_s_5077;
    int __f2dace_SOA_ddt_vn_phd_d_2_s_5078;
    int __f2dace_SOA_ddt_vn_phy_d_0_s_4983;
    int __f2dace_SOA_ddt_vn_phy_d_1_s_4984;
    int __f2dace_SOA_ddt_vn_phy_d_2_s_4985;
    int __f2dace_SOA_ddt_vn_ray_d_0_s_5103;
    int __f2dace_SOA_ddt_vn_ray_d_1_s_5104;
    int __f2dace_SOA_ddt_vn_ray_d_2_s_5105;
    int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006;
    int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007;
    int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008;
    int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009;
    int __f2dace_SOA_ddt_w_adv_pc_ptr_d_0_s_5140;
    int __f2dace_SOA_div_d_0_s_4836;
    int __f2dace_SOA_div_d_1_s_4837;
    int __f2dace_SOA_div_d_2_s_4838;
    int __f2dace_SOA_div_ic_d_0_s_5010;
    int __f2dace_SOA_div_ic_d_1_s_5011;
    int __f2dace_SOA_div_ic_d_2_s_5012;
    int __f2dace_SOA_dpres_mc_d_0_s_4825;
    int __f2dace_SOA_dpres_mc_d_1_s_4826;
    int __f2dace_SOA_dpres_mc_d_2_s_4827;
    int __f2dace_SOA_dwdx_d_0_s_5016;
    int __f2dace_SOA_dwdx_d_1_s_5017;
    int __f2dace_SOA_dwdx_d_2_s_5018;
    int __f2dace_SOA_dwdy_d_0_s_5019;
    int __f2dace_SOA_dwdy_d_1_s_5020;
    int __f2dace_SOA_dwdy_d_2_s_5021;
    int __f2dace_SOA_exner_dyn_incr_d_0_s_4986;
    int __f2dace_SOA_exner_dyn_incr_d_1_s_4987;
    int __f2dace_SOA_exner_dyn_incr_d_2_s_4988;
    int __f2dace_SOA_exner_incr_d_0_s_4932;
    int __f2dace_SOA_exner_incr_d_1_s_4933;
    int __f2dace_SOA_exner_incr_d_2_s_4934;
    int __f2dace_SOA_exner_pr_d_0_s_4801;
    int __f2dace_SOA_exner_pr_d_1_s_4802;
    int __f2dace_SOA_exner_pr_d_2_s_4803;
    int __f2dace_SOA_extra_2d_d_0_s_5124;
    int __f2dace_SOA_extra_2d_d_1_s_5125;
    int __f2dace_SOA_extra_2d_d_2_s_5126;
    int __f2dace_SOA_extra_2d_ptr_d_0_s_5136;
    int __f2dace_SOA_extra_3d_d_0_s_5127;
    int __f2dace_SOA_extra_3d_d_1_s_5128;
    int __f2dace_SOA_extra_3d_d_2_s_5129;
    int __f2dace_SOA_extra_3d_d_3_s_5130;
    int __f2dace_SOA_extra_3d_ptr_d_0_s_5137;
    int __f2dace_SOA_grf_bdy_mflx_d_0_s_4866;
    int __f2dace_SOA_grf_bdy_mflx_d_1_s_4867;
    int __f2dace_SOA_grf_bdy_mflx_d_2_s_4868;
    int __f2dace_SOA_grf_tend_mflx_d_0_s_4863;
    int __f2dace_SOA_grf_tend_mflx_d_1_s_4864;
    int __f2dace_SOA_grf_tend_mflx_d_2_s_4865;
    int __f2dace_SOA_grf_tend_rho_d_0_s_4860;
    int __f2dace_SOA_grf_tend_rho_d_1_s_4861;
    int __f2dace_SOA_grf_tend_rho_d_2_s_4862;
    int __f2dace_SOA_grf_tend_thv_d_0_s_4869;
    int __f2dace_SOA_grf_tend_thv_d_1_s_4870;
    int __f2dace_SOA_grf_tend_thv_d_2_s_4871;
    int __f2dace_SOA_grf_tend_tracer_d_0_s_4872;
    int __f2dace_SOA_grf_tend_tracer_d_1_s_4873;
    int __f2dace_SOA_grf_tend_tracer_d_2_s_4874;
    int __f2dace_SOA_grf_tend_tracer_d_3_s_4875;
    int __f2dace_SOA_grf_tend_vn_d_0_s_4854;
    int __f2dace_SOA_grf_tend_vn_d_1_s_4855;
    int __f2dace_SOA_grf_tend_vn_d_2_s_4856;
    int __f2dace_SOA_grf_tend_w_d_0_s_4857;
    int __f2dace_SOA_grf_tend_w_d_1_s_4858;
    int __f2dace_SOA_grf_tend_w_d_2_s_4859;
    int __f2dace_SOA_hdef_ic_d_0_s_5013;
    int __f2dace_SOA_hdef_ic_d_1_s_5014;
    int __f2dace_SOA_hdef_ic_d_2_s_5015;
    int __f2dace_SOA_hfl_tracer_d_0_s_4828;
    int __f2dace_SOA_hfl_tracer_d_1_s_4829;
    int __f2dace_SOA_hfl_tracer_d_2_s_4830;
    int __f2dace_SOA_hfl_tracer_d_3_s_4831;
    int __f2dace_SOA_hfl_trc_ptr_d_0_s_5132;
    int __f2dace_SOA_mass_fl_e_d_0_s_4839;
    int __f2dace_SOA_mass_fl_e_d_1_s_4840;
    int __f2dace_SOA_mass_fl_e_d_2_s_4841;
    int __f2dace_SOA_mass_fl_e_sv_d_0_s_4995;
    int __f2dace_SOA_mass_fl_e_sv_d_1_s_4996;
    int __f2dace_SOA_mass_fl_e_sv_d_2_s_4997;
    int __f2dace_SOA_mflx_ic_int_d_0_s_4900;
    int __f2dace_SOA_mflx_ic_int_d_1_s_4901;
    int __f2dace_SOA_mflx_ic_int_d_2_s_4902;
    int __f2dace_SOA_mflx_ic_ubc_d_0_s_4903;
    int __f2dace_SOA_mflx_ic_ubc_d_1_s_4904;
    int __f2dace_SOA_mflx_ic_ubc_d_2_s_4905;
    int __f2dace_SOA_omega_d_0_s_4920;
    int __f2dace_SOA_omega_d_1_s_4921;
    int __f2dace_SOA_omega_d_2_s_4922;
    int __f2dace_SOA_omega_z_d_0_s_4788;
    int __f2dace_SOA_omega_z_d_1_s_4789;
    int __f2dace_SOA_omega_z_d_2_s_4790;
    int __f2dace_SOA_p_avginc_d_0_s_4914;
    int __f2dace_SOA_p_avginc_d_1_s_4915;
    int __f2dace_SOA_pres_d_0_s_4813;
    int __f2dace_SOA_pres_d_1_s_4814;
    int __f2dace_SOA_pres_d_2_s_4815;
    int __f2dace_SOA_pres_ifc_d_0_s_4816;
    int __f2dace_SOA_pres_ifc_d_1_s_4817;
    int __f2dace_SOA_pres_ifc_d_2_s_4818;
    int __f2dace_SOA_pres_msl_d_0_s_4918;
    int __f2dace_SOA_pres_msl_d_1_s_4919;
    int __f2dace_SOA_pres_sfc_d_0_s_4819;
    int __f2dace_SOA_pres_sfc_d_1_s_4820;
    int __f2dace_SOA_pres_sfc_old_d_0_s_4821;
    int __f2dace_SOA_pres_sfc_old_d_1_s_4822;
    int __f2dace_SOA_rh_avginc_d_0_s_4908;
    int __f2dace_SOA_rh_avginc_d_1_s_4909;
    int __f2dace_SOA_rho_ic_d_0_s_4842;
    int __f2dace_SOA_rho_ic_d_1_s_4843;
    int __f2dace_SOA_rho_ic_d_2_s_4844;
    int __f2dace_SOA_rho_ic_int_d_0_s_4894;
    int __f2dace_SOA_rho_ic_int_d_1_s_4895;
    int __f2dace_SOA_rho_ic_int_d_2_s_4896;
    int __f2dace_SOA_rho_ic_ubc_d_0_s_4897;
    int __f2dace_SOA_rho_ic_ubc_d_1_s_4898;
    int __f2dace_SOA_rho_ic_ubc_d_2_s_4899;
    int __f2dace_SOA_rho_incr_d_0_s_4935;
    int __f2dace_SOA_rho_incr_d_1_s_4936;
    int __f2dace_SOA_rho_incr_d_2_s_4937;
    int __f2dace_SOA_rhoc_incr_d_0_s_4941;
    int __f2dace_SOA_rhoc_incr_d_1_s_4942;
    int __f2dace_SOA_rhoc_incr_d_2_s_4943;
    int __f2dace_SOA_rhog_incr_d_0_s_4953;
    int __f2dace_SOA_rhog_incr_d_1_s_4954;
    int __f2dace_SOA_rhog_incr_d_2_s_4955;
    int __f2dace_SOA_rhoh_incr_d_0_s_4956;
    int __f2dace_SOA_rhoh_incr_d_1_s_4957;
    int __f2dace_SOA_rhoh_incr_d_2_s_4958;
    int __f2dace_SOA_rhoi_incr_d_0_s_4944;
    int __f2dace_SOA_rhoi_incr_d_1_s_4945;
    int __f2dace_SOA_rhoi_incr_d_2_s_4946;
    int __f2dace_SOA_rhonc_incr_d_0_s_4959;
    int __f2dace_SOA_rhonc_incr_d_1_s_4960;
    int __f2dace_SOA_rhonc_incr_d_2_s_4961;
    int __f2dace_SOA_rhong_incr_d_0_s_4971;
    int __f2dace_SOA_rhong_incr_d_1_s_4972;
    int __f2dace_SOA_rhong_incr_d_2_s_4973;
    int __f2dace_SOA_rhonh_incr_d_0_s_4974;
    int __f2dace_SOA_rhonh_incr_d_1_s_4975;
    int __f2dace_SOA_rhonh_incr_d_2_s_4976;
    int __f2dace_SOA_rhoni_incr_d_0_s_4962;
    int __f2dace_SOA_rhoni_incr_d_1_s_4963;
    int __f2dace_SOA_rhoni_incr_d_2_s_4964;
    int __f2dace_SOA_rhonr_incr_d_0_s_4965;
    int __f2dace_SOA_rhonr_incr_d_1_s_4966;
    int __f2dace_SOA_rhonr_incr_d_2_s_4967;
    int __f2dace_SOA_rhons_incr_d_0_s_4968;
    int __f2dace_SOA_rhons_incr_d_1_s_4969;
    int __f2dace_SOA_rhons_incr_d_2_s_4970;
    int __f2dace_SOA_rhor_incr_d_0_s_4947;
    int __f2dace_SOA_rhor_incr_d_1_s_4948;
    int __f2dace_SOA_rhor_incr_d_2_s_4949;
    int __f2dace_SOA_rhos_incr_d_0_s_4950;
    int __f2dace_SOA_rhos_incr_d_1_s_4951;
    int __f2dace_SOA_rhos_incr_d_2_s_4952;
    int __f2dace_SOA_rhov_incr_d_0_s_4938;
    int __f2dace_SOA_rhov_incr_d_1_s_4939;
    int __f2dace_SOA_rhov_incr_d_2_s_4940;
    int __f2dace_SOA_t2m_bias_d_0_s_4906;
    int __f2dace_SOA_t2m_bias_d_1_s_4907;
    int __f2dace_SOA_t_avginc_d_0_s_4910;
    int __f2dace_SOA_t_avginc_d_1_s_4911;
    int __f2dace_SOA_t_wgt_avginc_d_0_s_4912;
    int __f2dace_SOA_t_wgt_avginc_d_1_s_4913;
    int __f2dace_SOA_temp_d_0_s_4804;
    int __f2dace_SOA_temp_d_1_s_4805;
    int __f2dace_SOA_temp_d_2_s_4806;
    int __f2dace_SOA_temp_ifc_d_0_s_4810;
    int __f2dace_SOA_temp_ifc_d_1_s_4811;
    int __f2dace_SOA_temp_ifc_d_2_s_4812;
    int __f2dace_SOA_tempv_d_0_s_4807;
    int __f2dace_SOA_tempv_d_1_s_4808;
    int __f2dace_SOA_tempv_d_2_s_4809;
    int __f2dace_SOA_theta_v_ic_d_0_s_4845;
    int __f2dace_SOA_theta_v_ic_d_1_s_4846;
    int __f2dace_SOA_theta_v_ic_d_2_s_4847;
    int __f2dace_SOA_theta_v_ic_int_d_0_s_4888;
    int __f2dace_SOA_theta_v_ic_int_d_1_s_4889;
    int __f2dace_SOA_theta_v_ic_int_d_2_s_4890;
    int __f2dace_SOA_theta_v_ic_ubc_d_0_s_4891;
    int __f2dace_SOA_theta_v_ic_ubc_d_1_s_4892;
    int __f2dace_SOA_theta_v_ic_ubc_d_2_s_4893;
    int __f2dace_SOA_tracer_vi_d_0_s_4798;
    int __f2dace_SOA_tracer_vi_d_1_s_4799;
    int __f2dace_SOA_tracer_vi_d_2_s_4800;
    int __f2dace_SOA_tracer_vi_ptr_d_0_s_5135;
    int __f2dace_SOA_u_d_0_s_4782;
    int __f2dace_SOA_u_d_1_s_4783;
    int __f2dace_SOA_u_d_2_s_4784;
    int __f2dace_SOA_v_d_0_s_4785;
    int __f2dace_SOA_v_d_1_s_4786;
    int __f2dace_SOA_v_d_2_s_4787;
    int __f2dace_SOA_vabs_avginc_d_0_s_4916;
    int __f2dace_SOA_vabs_avginc_d_1_s_4917;
    int __f2dace_SOA_vfl_tracer_d_0_s_4832;
    int __f2dace_SOA_vfl_tracer_d_1_s_4833;
    int __f2dace_SOA_vfl_tracer_d_2_s_4834;
    int __f2dace_SOA_vfl_tracer_d_3_s_4835;
    int __f2dace_SOA_vfl_trc_ptr_d_0_s_5133;
    int __f2dace_SOA_vn_ie_d_0_s_4989;
    int __f2dace_SOA_vn_ie_d_1_s_4990;
    int __f2dace_SOA_vn_ie_d_2_s_4991;
    int __f2dace_SOA_vn_ie_int_d_0_s_4876;
    int __f2dace_SOA_vn_ie_int_d_1_s_4877;
    int __f2dace_SOA_vn_ie_int_d_2_s_4878;
    int __f2dace_SOA_vn_ie_ubc_d_0_s_4879;
    int __f2dace_SOA_vn_ie_ubc_d_1_s_4880;
    int __f2dace_SOA_vn_ie_ubc_d_2_s_4881;
    int __f2dace_SOA_vn_incr_d_0_s_4929;
    int __f2dace_SOA_vn_incr_d_1_s_4930;
    int __f2dace_SOA_vn_incr_d_2_s_4931;
    int __f2dace_SOA_vor_d_0_s_4791;
    int __f2dace_SOA_vor_d_1_s_4792;
    int __f2dace_SOA_vor_d_2_s_4793;
    int __f2dace_SOA_vor_u_d_0_s_4923;
    int __f2dace_SOA_vor_u_d_1_s_4924;
    int __f2dace_SOA_vor_u_d_2_s_4925;
    int __f2dace_SOA_vor_v_d_0_s_4926;
    int __f2dace_SOA_vor_v_d_1_s_4927;
    int __f2dace_SOA_vor_v_d_2_s_4928;
    int __f2dace_SOA_vt_d_0_s_4977;
    int __f2dace_SOA_vt_d_1_s_4978;
    int __f2dace_SOA_vt_d_2_s_4979;
    int __f2dace_SOA_w_concorr_c_d_0_s_4992;
    int __f2dace_SOA_w_concorr_c_d_1_s_4993;
    int __f2dace_SOA_w_concorr_c_d_2_s_4994;
    int __f2dace_SOA_w_int_d_0_s_4882;
    int __f2dace_SOA_w_int_d_1_s_4883;
    int __f2dace_SOA_w_int_d_2_s_4884;
    int __f2dace_SOA_w_ubc_d_0_s_4885;
    int __f2dace_SOA_w_ubc_d_1_s_4886;
    int __f2dace_SOA_w_ubc_d_2_s_4887;
    double* airmass_new;
    double* airmass_now;
    double* ddt_exner_phy;
    double* ddt_pres_sfc;
    double* ddt_temp_dyn;
    double* ddt_tracer_adv;
    double* ddt_ua_adv;
    int ddt_ua_adv_is_associated;
    double* ddt_ua_cen;
    int ddt_ua_cen_is_associated;
    double* ddt_ua_cor;
    int ddt_ua_cor_is_associated;
    double* ddt_ua_dmp;
    int ddt_ua_dmp_is_associated;
    double* ddt_ua_dyn;
    int ddt_ua_dyn_is_associated;
    double* ddt_ua_grf;
    int ddt_ua_grf_is_associated;
    double* ddt_ua_hdf;
    int ddt_ua_hdf_is_associated;
    double* ddt_ua_iau;
    int ddt_ua_iau_is_associated;
    double* ddt_ua_pgr;
    int ddt_ua_pgr_is_associated;
    double* ddt_ua_phd;
    int ddt_ua_phd_is_associated;
    double* ddt_ua_ray;
    int ddt_ua_ray_is_associated;
    double* ddt_va_adv;
    int ddt_va_adv_is_associated;
    double* ddt_va_cen;
    int ddt_va_cen_is_associated;
    double* ddt_va_cor;
    int ddt_va_cor_is_associated;
    double* ddt_va_dmp;
    int ddt_va_dmp_is_associated;
    double* ddt_va_dyn;
    int ddt_va_dyn_is_associated;
    double* ddt_va_grf;
    int ddt_va_grf_is_associated;
    double* ddt_va_hdf;
    int ddt_va_hdf_is_associated;
    double* ddt_va_iau;
    int ddt_va_iau_is_associated;
    double* ddt_va_pgr;
    int ddt_va_pgr_is_associated;
    double* ddt_va_phd;
    int ddt_va_phd_is_associated;
    double* ddt_va_ray;
    int ddt_va_ray_is_associated;
    double* ddt_vn_adv;
    int ddt_vn_adv_is_associated;
    double* ddt_vn_apc_pc;
    double* ddt_vn_cen;
    int ddt_vn_cen_is_associated;
    double* ddt_vn_cor;
    int ddt_vn_cor_is_associated;
    double* ddt_vn_cor_pc;
    double* ddt_vn_dmp;
    int ddt_vn_dmp_is_associated;
    double* ddt_vn_dyn;
    int ddt_vn_dyn_is_associated;
    double* ddt_vn_grf;
    int ddt_vn_grf_is_associated;
    double* ddt_vn_hdf;
    int ddt_vn_hdf_is_associated;
    double* ddt_vn_iau;
    int ddt_vn_iau_is_associated;
    double* ddt_vn_pgr;
    int ddt_vn_pgr_is_associated;
    double* ddt_vn_phd;
    int ddt_vn_phd_is_associated;
    double* ddt_vn_phy;
    double* ddt_vn_ray;
    int ddt_vn_ray_is_associated;
    double* ddt_w_adv_pc;
    double* div;
    double* div_ic;
    double* dpres_mc;
    double* dwdx;
    double* dwdy;
    double* exner_dyn_incr;
    double* exner_incr;
    double* exner_pr;
    double* grf_bdy_mflx;
    double* grf_tend_mflx;
    double* grf_tend_rho;
    double* grf_tend_thv;
    double* grf_tend_tracer;
    double* grf_tend_vn;
    double* grf_tend_w;
    double* hdef_ic;
    double* hfl_tracer;
    double* mass_fl_e;
    double* mass_fl_e_sv;
    double max_vcfl_dyn;
    double* mflx_ic_int;
    double* mflx_ic_ubc;
    double* omega;
    double* omega_z;
    double* p_avginc;
    double* pres;
    double* pres_ifc;
    double* pres_msl;
    double* pres_sfc;
    double* pres_sfc_old;
    double* rh_avginc;
    double* rho_ic;
    double* rho_ic_int;
    double* rho_ic_ubc;
    double* rho_incr;
    double* rhoc_incr;
    double* rhog_incr;
    double* rhoh_incr;
    double* rhoi_incr;
    double* rhonc_incr;
    double* rhong_incr;
    double* rhonh_incr;
    double* rhoni_incr;
    double* rhonr_incr;
    double* rhons_incr;
    double* rhor_incr;
    double* rhos_incr;
    double* rhov_incr;
    double* t2m_bias;
    double* t_avginc;
    double* t_wgt_avginc;
    double* temp;
    double* temp_ifc;
    double* tempv;
    double* theta_v_ic;
    double* theta_v_ic_int;
    double* theta_v_ic_ubc;
    double* tracer_vi;
    double* u;
    double* v;
    double* vabs_avginc;
    double* vfl_tracer;
    double* vn_ie;
    double* vn_ie_int;
    double* vn_ie_ubc;
    double* vn_incr;
    double* vor;
    double* vor_u;
    double* vor_v;
    double* vt;
    double* w_concorr_c;
    double* w_int;
    double* w_ubc;
};

struct velocity_tendencies_state_t {
    dace::cuda::Context *gpu_context;
    int * __restrict__ __0_gpu_levmask;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_edges_quad_blk;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_edges_quad_idx;
    double * __restrict__ __0_gpu_v_p_int_rbf_vec_coeff_e;
    double * __restrict__ __0_gpu_v_p_prog_vn;
    double * __restrict__ __0_gpu_v_p_diag_vt;
    double * __restrict__ __0_gpu_v_p_metrics_wgtfac_e;
    double * __restrict__ __0_gpu_v_p_diag_vn_ie_ubc;
    double * __restrict__ __0_gpu_v_p_metrics_wgtfacq_e;
    double * __restrict__ __0_gpu_v_p_metrics_ddxn_z_full;
    double * __restrict__ __0_gpu_v_p_metrics_ddxt_z_full;
    double * __restrict__ __0_gpu_z_w_v;
    double * __restrict__ __0_gpu_z_vt_ie;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_cell_blk;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_cell_idx;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_vertex_blk;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_vertex_idx;
    double * __restrict__ __0_gpu_v_p_diag_vn_ie;
    double * __restrict__ __0_gpu_v_p_prog_w;
    double * __restrict__ __0_gpu_v_v_ptr_patch_edges_inv_dual_edge_length;
    double * __restrict__ __0_gpu_v_v_ptr_patch_edges_inv_primal_edge_length;
    double * __restrict__ __0_gpu_v_v_ptr_patch_edges_tangent_orientation;
    double * __restrict__ __0_z_w_con_c;
    double * __restrict__ __0_gpu_z_w_con_c;
    double * __restrict__ __0_gpu_v_p_diag_w_concorr_c;
    double * __restrict__ __0_gpu_v_p_metrics_coeff1_dwdz;
    double * __restrict__ __0_gpu_v_p_metrics_coeff2_dwdz;
    double * __restrict__ __0_gpu_z_v_grad_w;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_cells_edge_blk;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_cells_edge_idx;
    double * __restrict__ __0_gpu_v_p_diag_ddt_w_adv_pc;
    double * __restrict__ __0_gpu_v_p_int_e_bln_c_s;
    double * __restrict__ __0_gpu_z_w_concorr_me;
    double * __restrict__ __0_z_w_concorr_mc;
    double * __restrict__ __0_gpu_z_w_concorr_mc;
    double * __restrict__ __0_gpu_v_p_metrics_wgtfac_c;
    int * __restrict__ __0_cfl_clipping;
    int * __restrict__ __0_gpu_cfl_clipping;
    double * __restrict__ __0_gpu_v_p_metrics_ddqz_z_half;
    double * __restrict__ __0_gpu_z_kin_hor_e;
    int * __restrict__ __0_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_cells_neighbor_blk;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_cells_neighbor_idx;
    double * __restrict__ __0_gpu_v_p_int_geofac_n2s;
    double * __restrict__ __0_gpu_v_v_ptr_patch_cells_area;
    double * __restrict__ __0_gpu_v_v_ptr_patch_edges_f_e;
    double * __restrict__ __0_gpu_z_w_con_c_full;
    double * __restrict__ __0_gpu_z_ekinh;
    double * __restrict__ __0_gpu_zeta;
    double * __restrict__ __0_gpu_v_p_int_c_lin_e;
    double * __restrict__ __0_gpu_v_p_metrics_coeff_gradekin;
    double * __restrict__ __0_gpu_v_p_metrics_ddqz_z_full_e;
    int * __restrict__ __0_gpu_levelmask;
    double * __restrict__ __0_gpu_flat_p_int_c_lin_e;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_quad_blk;
    int * __restrict__ __0_gpu_flat_v_ptr_patch_edges_quad_idx;
    double * __restrict__ __0_gpu_v_p_diag_ddt_vn_apc_pc;
    double * __restrict__ __0_gpu_v_p_int_geofac_grdiv;
    double * __restrict__ __0_gpu_v_v_ptr_patch_edges_area_edge;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_verts_edge_blk;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_verts_edge_idx;
    double * __restrict__ __0_gpu_p_prog_vn_2;
    double * __restrict__ __0_gpu_v_ptr_int_0_geofac_rot;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_verts_cell_blk;
    int * __restrict__ __0_gpu_flat_flat_ptr_patch_verts_cell_idx;
    double * __restrict__ __0_gpu_p_int_cells_aw_verts_1;
    double * __restrict__ __0_gpu_p_prog_w_0;
    double * __restrict__ __0_gpu_v_p_diag_ddt_vn_cor_pc;
    double * __restrict__ __1_gpu_z_w_con_c;
    double * __restrict__ __1_gpu_v_p_diag_w_concorr_c;
    int * __restrict__ __1_gpu_levmask;
    double * __restrict__ __1_gpu_v_p_metrics_coeff1_dwdz;
    double * __restrict__ __1_gpu_v_p_metrics_coeff2_dwdz;
    double * __restrict__ __1_gpu_v_p_prog_w;
    double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc;
    double * __restrict__ __1_gpu_z_v_grad_w;
    int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk;
    int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx;
    double * __restrict__ __1_gpu_v_p_int_e_bln_c_s;
    double * __restrict__ __1_gpu_z_w_concorr_me;
    double * __restrict__ __1_gpu_z_w_concorr_mc;
    double * __restrict__ __1_gpu_v_p_metrics_wgtfac_c;
    int * __restrict__ __1_gpu_cfl_clipping;
    double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half;
    double * __restrict__ __1_gpu_z_w_con_c_full;
    double * __restrict__ __1_gpu_z_kin_hor_e;
    double * __restrict__ __1_gpu_z_ekinh;
    int * __restrict__ __1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask;
    int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_blk;
    int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_idx;
    double * __restrict__ __1_gpu_v_p_int_geofac_n2s;
    double * __restrict__ __1_gpu_v_v_ptr_patch_cells_area;
    int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_blk;
    int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_idx;
    double * __restrict__ __24_gpu_p_prog_vn_2;
    double * __restrict__ __24_gpu_v_ptr_int_0_geofac_rot;
    double * __restrict__ __24_gpu_zeta;
    int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_blk;
    int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_idx;
    double * __restrict__ __27_gpu_p_int_cells_aw_verts_1;
    double * __restrict__ __27_gpu_p_prog_w_0;
    double * __restrict__ __27_gpu_z_w_v;
    double * __restrict__ __30_gpu_v_p_diag_vt;
    double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e;
    double * __restrict__ __30_gpu_v_p_diag_ddt_vn_cor_pc;
    double * __restrict__ __30_gpu_z_w_con_c_full;
    double * __restrict__ __30_gpu_z_kin_hor_e;
    double * __restrict__ __30_gpu_z_ekinh;
    double * __restrict__ __30_gpu_zeta;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx;
    double * __restrict__ __30_gpu_v_p_diag_vn_ie;
    double * __restrict__ __30_gpu_v_p_int_c_lin_e;
    double * __restrict__ __30_gpu_v_p_metrics_coeff_gradekin;
    double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e;
    double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc;
    int * __restrict__ __30_gpu_levelmask;
    double * __restrict__ __30_gpu_flat_p_int_c_lin_e;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_blk;
    int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_idx;
    double * __restrict__ __30_gpu_v_p_int_geofac_grdiv;
    double * __restrict__ __30_gpu_v_p_prog_vn;
    double * __restrict__ __30_gpu_v_v_ptr_patch_edges_area_edge;
    double * __restrict__ __30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length;
    double * __restrict__ __30_gpu_v_v_ptr_patch_edges_tangent_orientation;
    double * __restrict__ __37_gpu_z_w_v;
    double * __restrict__ __37_gpu_z_vt_ie;
    int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_blk;
    int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_idx;
    int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_blk;
    int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_idx;
    double * __restrict__ __37_gpu_v_p_diag_vn_ie;
    double * __restrict__ __37_gpu_v_p_prog_w;
    double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length;
    double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length;
    double * __restrict__ __37_gpu_v_v_ptr_patch_edges_tangent_orientation;
    double * __restrict__ __37_gpu_z_v_grad_w;
    int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_blk;
    int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_idx;
    double * __restrict__ __40_gpu_v_p_int_rbf_vec_coeff_e;
    double * __restrict__ __40_gpu_v_p_prog_vn;
    double * __restrict__ __40_gpu_v_p_diag_vt;
    double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e;
    double * __restrict__ __40_gpu_z_kin_hor_e;
    double * __restrict__ __40_gpu_v_p_diag_vn_ie;
    double * __restrict__ __40_gpu_z_vt_ie;
    double * __restrict__ __40_gpu_v_p_diag_vn_ie_ubc;
    double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e;
    double * __restrict__ __40_gpu_v_p_metrics_ddxn_z_full;
    double * __restrict__ __40_gpu_v_p_metrics_ddxt_z_full;
    double * __restrict__ __40_gpu_z_w_concorr_me;
    dace::perf::Report report;
};


#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>

#include <chrono>


DACE_EXPORTED int __dace_init_cuda(velocity_tendencies_state_t *__state, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_airmass_new_d_0_s_4851_p_diag_45, int __f2dace_SA_airmass_new_d_1_s_4852_p_diag_45, int __f2dace_SA_airmass_now_d_0_s_4848_p_diag_45, int __f2dace_SA_airmass_now_d_1_s_4849_p_diag_45, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SA_coeff_gradp_d_0_s_5282_p_metrics_44, int __f2dace_SA_coeff_gradp_d_1_s_5283_p_metrics_44, int __f2dace_SA_coeff_gradp_d_2_s_5284_p_metrics_44, int __f2dace_SA_d2dexdz2_fac1_mc_d_0_s_5312_p_metrics_44, int __f2dace_SA_d2dexdz2_fac1_mc_d_1_s_5313_p_metrics_44, int __f2dace_SA_d2dexdz2_fac2_mc_d_0_s_5315_p_metrics_44, int __f2dace_SA_d2dexdz2_fac2_mc_d_1_s_5316_p_metrics_44, int __f2dace_SA_d_exner_dz_ref_ic_d_0_s_5309_p_metrics_44, int __f2dace_SA_d_exner_dz_ref_ic_d_1_s_5310_p_metrics_44, int __f2dace_SA_ddqz_z_full_d_0_s_5147_p_metrics_44, int __f2dace_SA_ddqz_z_full_d_1_s_5148_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SA_ddt_exner_phy_d_0_s_4980_p_diag_45, int __f2dace_SA_ddt_exner_phy_d_1_s_4981_p_diag_45, int __f2dace_SA_ddt_pres_sfc_d_0_s_4823_p_diag_45, int __f2dace_SA_ddt_temp_dyn_d_0_s_5121_p_diag_45, int __f2dace_SA_ddt_temp_dyn_d_1_s_5122_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_0_s_4794_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_1_s_4795_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_2_s_4796_p_diag_45, int __f2dace_SA_ddt_ua_adv_d_0_s_5052_p_diag_45, int __f2dace_SA_ddt_ua_adv_d_1_s_5053_p_diag_45, int __f2dace_SA_ddt_ua_cen_d_0_s_5088_p_diag_45, int __f2dace_SA_ddt_ua_cen_d_1_s_5089_p_diag_45, int __f2dace_SA_ddt_ua_cor_d_0_s_5061_p_diag_45, int __f2dace_SA_ddt_ua_cor_d_1_s_5062_p_diag_45, int __f2dace_SA_ddt_ua_dmp_d_0_s_5034_p_diag_45, int __f2dace_SA_ddt_ua_dmp_d_1_s_5035_p_diag_45, int __f2dace_SA_ddt_ua_dyn_d_0_s_5025_p_diag_45, int __f2dace_SA_ddt_ua_dyn_d_1_s_5026_p_diag_45, int __f2dace_SA_ddt_ua_grf_d_0_s_5115_p_diag_45, int __f2dace_SA_ddt_ua_grf_d_1_s_5116_p_diag_45, int __f2dace_SA_ddt_ua_hdf_d_0_s_5043_p_diag_45, int __f2dace_SA_ddt_ua_hdf_d_1_s_5044_p_diag_45, int __f2dace_SA_ddt_ua_iau_d_0_s_5097_p_diag_45, int __f2dace_SA_ddt_ua_iau_d_1_s_5098_p_diag_45, int __f2dace_SA_ddt_ua_pgr_d_0_s_5070_p_diag_45, int __f2dace_SA_ddt_ua_pgr_d_1_s_5071_p_diag_45, int __f2dace_SA_ddt_ua_phd_d_0_s_5079_p_diag_45, int __f2dace_SA_ddt_ua_phd_d_1_s_5080_p_diag_45, int __f2dace_SA_ddt_ua_ray_d_0_s_5106_p_diag_45, int __f2dace_SA_ddt_ua_ray_d_1_s_5107_p_diag_45, int __f2dace_SA_ddt_va_adv_d_0_s_5055_p_diag_45, int __f2dace_SA_ddt_va_adv_d_1_s_5056_p_diag_45, int __f2dace_SA_ddt_va_cen_d_0_s_5091_p_diag_45, int __f2dace_SA_ddt_va_cen_d_1_s_5092_p_diag_45, int __f2dace_SA_ddt_va_cor_d_0_s_5064_p_diag_45, int __f2dace_SA_ddt_va_cor_d_1_s_5065_p_diag_45, int __f2dace_SA_ddt_va_dmp_d_0_s_5037_p_diag_45, int __f2dace_SA_ddt_va_dmp_d_1_s_5038_p_diag_45, int __f2dace_SA_ddt_va_dyn_d_0_s_5028_p_diag_45, int __f2dace_SA_ddt_va_dyn_d_1_s_5029_p_diag_45, int __f2dace_SA_ddt_va_grf_d_0_s_5118_p_diag_45, int __f2dace_SA_ddt_va_grf_d_1_s_5119_p_diag_45, int __f2dace_SA_ddt_va_hdf_d_0_s_5046_p_diag_45, int __f2dace_SA_ddt_va_hdf_d_1_s_5047_p_diag_45, int __f2dace_SA_ddt_va_iau_d_0_s_5100_p_diag_45, int __f2dace_SA_ddt_va_iau_d_1_s_5101_p_diag_45, int __f2dace_SA_ddt_va_pgr_d_0_s_5073_p_diag_45, int __f2dace_SA_ddt_va_pgr_d_1_s_5074_p_diag_45, int __f2dace_SA_ddt_va_phd_d_0_s_5082_p_diag_45, int __f2dace_SA_ddt_va_phd_d_1_s_5083_p_diag_45, int __f2dace_SA_ddt_va_ray_d_0_s_5109_p_diag_45, int __f2dace_SA_ddt_va_ray_d_1_s_5110_p_diag_45, int __f2dace_SA_ddt_vn_adv_d_0_s_5049_p_diag_45, int __f2dace_SA_ddt_vn_adv_d_1_s_5050_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_ddt_vn_cen_d_0_s_5085_p_diag_45, int __f2dace_SA_ddt_vn_cen_d_1_s_5086_p_diag_45, int __f2dace_SA_ddt_vn_cor_d_0_s_5058_p_diag_45, int __f2dace_SA_ddt_vn_cor_d_1_s_5059_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SA_ddt_vn_dmp_d_0_s_5031_p_diag_45, int __f2dace_SA_ddt_vn_dmp_d_1_s_5032_p_diag_45, int __f2dace_SA_ddt_vn_dyn_d_0_s_5022_p_diag_45, int __f2dace_SA_ddt_vn_dyn_d_1_s_5023_p_diag_45, int __f2dace_SA_ddt_vn_grf_d_0_s_5112_p_diag_45, int __f2dace_SA_ddt_vn_grf_d_1_s_5113_p_diag_45, int __f2dace_SA_ddt_vn_hdf_d_0_s_5040_p_diag_45, int __f2dace_SA_ddt_vn_hdf_d_1_s_5041_p_diag_45, int __f2dace_SA_ddt_vn_iau_d_0_s_5094_p_diag_45, int __f2dace_SA_ddt_vn_iau_d_1_s_5095_p_diag_45, int __f2dace_SA_ddt_vn_pgr_d_0_s_5067_p_diag_45, int __f2dace_SA_ddt_vn_pgr_d_1_s_5068_p_diag_45, int __f2dace_SA_ddt_vn_phd_d_0_s_5076_p_diag_45, int __f2dace_SA_ddt_vn_phd_d_1_s_5077_p_diag_45, int __f2dace_SA_ddt_vn_phy_d_0_s_4983_p_diag_45, int __f2dace_SA_ddt_vn_phy_d_1_s_4984_p_diag_45, int __f2dace_SA_ddt_vn_ray_d_0_s_5103_p_diag_45, int __f2dace_SA_ddt_vn_ray_d_1_s_5104_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_ddxn_z_full_c_d_0_s_5212_p_metrics_44, int __f2dace_SA_ddxn_z_full_c_d_1_s_5213_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SA_ddxn_z_full_v_d_0_s_5215_p_metrics_44, int __f2dace_SA_ddxn_z_full_v_d_1_s_5216_p_metrics_44, int __f2dace_SA_ddxn_z_half_c_d_0_s_5221_p_metrics_44, int __f2dace_SA_ddxn_z_half_c_d_1_s_5222_p_metrics_44, int __f2dace_SA_ddxn_z_half_e_d_0_s_5218_p_metrics_44, int __f2dace_SA_ddxn_z_half_e_d_1_s_5219_p_metrics_44, int __f2dace_SA_ddxt_z_full_c_d_0_s_5227_p_metrics_44, int __f2dace_SA_ddxt_z_full_c_d_1_s_5228_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SA_ddxt_z_full_v_d_0_s_5230_p_metrics_44, int __f2dace_SA_ddxt_z_full_v_d_1_s_5231_p_metrics_44, int __f2dace_SA_ddxt_z_half_c_d_0_s_5236_p_metrics_44, int __f2dace_SA_ddxt_z_half_c_d_1_s_5237_p_metrics_44, int __f2dace_SA_ddxt_z_half_e_d_0_s_5233_p_metrics_44, int __f2dace_SA_ddxt_z_half_e_d_1_s_5234_p_metrics_44, int __f2dace_SA_ddxt_z_half_v_d_0_s_5239_p_metrics_44, int __f2dace_SA_ddxt_z_half_v_d_1_s_5240_p_metrics_44, int __f2dace_SA_deepatmo_t1ifc_d_0_s_5364_p_metrics_44, int __f2dace_SA_deepatmo_t1mc_d_0_s_5362_p_metrics_44, int __f2dace_SA_deepatmo_t2mc_d_0_s_5366_p_metrics_44, int __f2dace_SA_dgeopot_mc_d_0_s_5159_p_metrics_44, int __f2dace_SA_dgeopot_mc_d_1_s_5160_p_metrics_44, int __f2dace_SA_div_d_0_s_4836_p_diag_45, int __f2dace_SA_div_d_1_s_4837_p_diag_45, int __f2dace_SA_div_ic_d_0_s_5010_p_diag_45, int __f2dace_SA_div_ic_d_1_s_5011_p_diag_45, int __f2dace_SA_dpres_mc_d_0_s_4825_p_diag_45, int __f2dace_SA_dpres_mc_d_1_s_4826_p_diag_45, int __f2dace_SA_dwdx_d_0_s_5016_p_diag_45, int __f2dace_SA_dwdx_d_1_s_5017_p_diag_45, int __f2dace_SA_dwdy_d_0_s_5019_p_diag_45, int __f2dace_SA_dwdy_d_1_s_5020_p_diag_45, int __f2dace_SA_dzgpot_mc_d_0_s_5359_p_metrics_44, int __f2dace_SA_dzgpot_mc_d_1_s_5360_p_metrics_44, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SA_end_blk_d_0_s_3150_cells_ptr_patch_4, int __f2dace_SA_end_blk_d_0_s_3272_edges_ptr_patch_15, int __f2dace_SA_end_blk_d_0_s_3319_verts_ptr_patch_25, int __f2dace_SA_end_blk_d_1_s_3151_cells_ptr_patch_4, int __f2dace_SA_end_blk_d_1_s_3273_edges_ptr_patch_15, int __f2dace_SA_end_blk_d_1_s_3320_verts_ptr_patch_25, int __f2dace_SA_end_block_d_0_s_3152_cells_ptr_patch_4, int __f2dace_SA_end_block_d_0_s_3274_edges_ptr_patch_15, int __f2dace_SA_end_block_d_0_s_3321_verts_ptr_patch_25, int __f2dace_SA_end_index_d_0_s_3146_cells_ptr_patch_4, int __f2dace_SA_end_index_d_0_s_3268_edges_ptr_patch_15, int __f2dace_SA_end_index_d_0_s_3315_verts_ptr_patch_25, int __f2dace_SA_exner_d_0_s_4764_p_prog_43, int __f2dace_SA_exner_d_1_s_4765_p_prog_43, int __f2dace_SA_exner_dyn_incr_d_0_s_4986_p_diag_45, int __f2dace_SA_exner_dyn_incr_d_1_s_4987_p_diag_45, int __f2dace_SA_exner_exfac_d_0_s_5286_p_metrics_44, int __f2dace_SA_exner_exfac_d_1_s_5287_p_metrics_44, int __f2dace_SA_exner_incr_d_0_s_4932_p_diag_45, int __f2dace_SA_exner_incr_d_1_s_4933_p_diag_45, int __f2dace_SA_exner_pr_d_0_s_4801_p_diag_45, int __f2dace_SA_exner_pr_d_1_s_4802_p_diag_45, int __f2dace_SA_exner_ref_mc_d_0_s_5300_p_metrics_44, int __f2dace_SA_exner_ref_mc_d_1_s_5301_p_metrics_44, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SA_geopot_agl_d_0_s_5153_p_metrics_44, int __f2dace_SA_geopot_agl_d_1_s_5154_p_metrics_44, int __f2dace_SA_geopot_agl_ifc_d_0_s_5156_p_metrics_44, int __f2dace_SA_geopot_agl_ifc_d_1_s_5157_p_metrics_44, int __f2dace_SA_geopot_d_0_s_5150_p_metrics_44, int __f2dace_SA_geopot_d_1_s_5151_p_metrics_44, int __f2dace_SA_grf_bdy_mflx_d_0_s_4866_p_diag_45, int __f2dace_SA_grf_bdy_mflx_d_1_s_4867_p_diag_45, int __f2dace_SA_grf_tend_mflx_d_0_s_4863_p_diag_45, int __f2dace_SA_grf_tend_mflx_d_1_s_4864_p_diag_45, int __f2dace_SA_grf_tend_rho_d_0_s_4860_p_diag_45, int __f2dace_SA_grf_tend_rho_d_1_s_4861_p_diag_45, int __f2dace_SA_grf_tend_thv_d_0_s_4869_p_diag_45, int __f2dace_SA_grf_tend_thv_d_1_s_4870_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_0_s_4872_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_1_s_4873_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_2_s_4874_p_diag_45, int __f2dace_SA_grf_tend_vn_d_0_s_4854_p_diag_45, int __f2dace_SA_grf_tend_vn_d_1_s_4855_p_diag_45, int __f2dace_SA_grf_tend_w_d_0_s_4857_p_diag_45, int __f2dace_SA_grf_tend_w_d_1_s_4858_p_diag_45, int __f2dace_SA_hdef_ic_d_0_s_5013_p_diag_45, int __f2dace_SA_hdef_ic_d_1_s_5014_p_diag_45, int __f2dace_SA_hfl_tracer_d_0_s_4828_p_diag_45, int __f2dace_SA_hfl_tracer_d_1_s_4829_p_diag_45, int __f2dace_SA_hfl_tracer_d_2_s_4830_p_diag_45, int __f2dace_SA_hmask_dd3d_d_0_s_5166_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_d_0_s_5248_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_d_1_s_5249_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_e_d_0_s_5179_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_e_d_1_s_5180_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_v_d_0_s_5182_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_v_d_1_s_5183_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_d_0_s_5185_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_d_1_s_5186_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_e_d_0_s_5188_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_e_d_1_s_5189_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_v_d_0_s_5191_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_v_d_1_s_5192_p_metrics_44, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_mask_mtnpoints_d_0_s_5200_p_metrics_44, int __f2dace_SA_mask_mtnpoints_g_d_0_s_5202_p_metrics_44, int __f2dace_SA_mass_fl_e_d_0_s_4839_p_diag_45, int __f2dace_SA_mass_fl_e_d_1_s_4840_p_diag_45, int __f2dace_SA_mass_fl_e_sv_d_0_s_4995_p_diag_45, int __f2dace_SA_mass_fl_e_sv_d_1_s_4996_p_diag_45, int __f2dace_SA_mflx_ic_int_d_0_s_4900_p_diag_45, int __f2dace_SA_mflx_ic_int_d_1_s_4901_p_diag_45, int __f2dace_SA_mflx_ic_ubc_d_0_s_4903_p_diag_45, int __f2dace_SA_mflx_ic_ubc_d_1_s_4904_p_diag_45, int __f2dace_SA_mixing_length_sq_d_0_s_5197_p_metrics_44, int __f2dace_SA_mixing_length_sq_d_1_s_5198_p_metrics_44, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_0_s_3280_verts_ptr_patch_25, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3281_verts_ptr_patch_25, int __f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_2_s_3282_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3277_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3278_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_2_s_3279_verts_ptr_patch_25, int __f2dace_SA_omega_d_0_s_4920_p_diag_45, int __f2dace_SA_omega_d_1_s_4921_p_diag_45, int __f2dace_SA_omega_z_d_0_s_4788_p_diag_45, int __f2dace_SA_omega_z_d_1_s_4789_p_diag_45, int __f2dace_SA_ovlp_halo_c_blk_d_0_s_5348_p_metrics_44, int __f2dace_SA_ovlp_halo_c_idx_d_0_s_5346_p_metrics_44, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SA_p_avginc_d_0_s_4914_p_diag_45, int __f2dace_SA_pres_d_0_s_4813_p_diag_45, int __f2dace_SA_pres_d_1_s_4814_p_diag_45, int __f2dace_SA_pres_ifc_d_0_s_4816_p_diag_45, int __f2dace_SA_pres_ifc_d_1_s_4817_p_diag_45, int __f2dace_SA_pres_msl_d_0_s_4918_p_diag_45, int __f2dace_SA_pres_sfc_d_0_s_4819_p_diag_45, int __f2dace_SA_pres_sfc_old_d_0_s_4821_p_diag_45, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SA_rh_avginc_d_0_s_4908_p_diag_45, int __f2dace_SA_rho_d_0_s_4761_p_prog_43, int __f2dace_SA_rho_d_1_s_4762_p_prog_43, int __f2dace_SA_rho_ic_d_0_s_4842_p_diag_45, int __f2dace_SA_rho_ic_d_1_s_4843_p_diag_45, int __f2dace_SA_rho_ic_int_d_0_s_4894_p_diag_45, int __f2dace_SA_rho_ic_int_d_1_s_4895_p_diag_45, int __f2dace_SA_rho_ic_ubc_d_0_s_4897_p_diag_45, int __f2dace_SA_rho_ic_ubc_d_1_s_4898_p_diag_45, int __f2dace_SA_rho_incr_d_0_s_4935_p_diag_45, int __f2dace_SA_rho_incr_d_1_s_4936_p_diag_45, int __f2dace_SA_rho_ref_corr_d_0_s_5318_p_metrics_44, int __f2dace_SA_rho_ref_corr_d_1_s_5319_p_metrics_44, int __f2dace_SA_rho_ref_mc_d_0_s_5303_p_metrics_44, int __f2dace_SA_rho_ref_mc_d_1_s_5304_p_metrics_44, int __f2dace_SA_rho_ref_me_d_0_s_5306_p_metrics_44, int __f2dace_SA_rho_ref_me_d_1_s_5307_p_metrics_44, int __f2dace_SA_rhoc_incr_d_0_s_4941_p_diag_45, int __f2dace_SA_rhoc_incr_d_1_s_4942_p_diag_45, int __f2dace_SA_rhog_incr_d_0_s_4953_p_diag_45, int __f2dace_SA_rhog_incr_d_1_s_4954_p_diag_45, int __f2dace_SA_rhoh_incr_d_0_s_4956_p_diag_45, int __f2dace_SA_rhoh_incr_d_1_s_4957_p_diag_45, int __f2dace_SA_rhoi_incr_d_0_s_4944_p_diag_45, int __f2dace_SA_rhoi_incr_d_1_s_4945_p_diag_45, int __f2dace_SA_rhonc_incr_d_0_s_4959_p_diag_45, int __f2dace_SA_rhonc_incr_d_1_s_4960_p_diag_45, int __f2dace_SA_rhong_incr_d_0_s_4971_p_diag_45, int __f2dace_SA_rhong_incr_d_1_s_4972_p_diag_45, int __f2dace_SA_rhonh_incr_d_0_s_4974_p_diag_45, int __f2dace_SA_rhonh_incr_d_1_s_4975_p_diag_45, int __f2dace_SA_rhoni_incr_d_0_s_4962_p_diag_45, int __f2dace_SA_rhoni_incr_d_1_s_4963_p_diag_45, int __f2dace_SA_rhonr_incr_d_0_s_4965_p_diag_45, int __f2dace_SA_rhonr_incr_d_1_s_4966_p_diag_45, int __f2dace_SA_rhons_incr_d_0_s_4968_p_diag_45, int __f2dace_SA_rhons_incr_d_1_s_4969_p_diag_45, int __f2dace_SA_rhor_incr_d_0_s_4947_p_diag_45, int __f2dace_SA_rhor_incr_d_1_s_4948_p_diag_45, int __f2dace_SA_rhos_incr_d_0_s_4950_p_diag_45, int __f2dace_SA_rhos_incr_d_1_s_4951_p_diag_45, int __f2dace_SA_rhov_incr_d_0_s_4938_p_diag_45, int __f2dace_SA_rhov_incr_d_1_s_4939_p_diag_45, int __f2dace_SA_slope_angle_d_0_s_5204_p_metrics_44, int __f2dace_SA_slope_azimuth_d_0_s_5206_p_metrics_44, int __f2dace_SA_start_blk_d_0_s_3147_cells_ptr_patch_4, int __f2dace_SA_start_blk_d_0_s_3269_edges_ptr_patch_15, int __f2dace_SA_start_blk_d_0_s_3316_verts_ptr_patch_25, int __f2dace_SA_start_blk_d_1_s_3148_cells_ptr_patch_4, int __f2dace_SA_start_blk_d_1_s_3270_edges_ptr_patch_15, int __f2dace_SA_start_blk_d_1_s_3317_verts_ptr_patch_25, int __f2dace_SA_start_block_d_0_s_3149_cells_ptr_patch_4, int __f2dace_SA_start_block_d_0_s_3271_edges_ptr_patch_15, int __f2dace_SA_start_block_d_0_s_3318_verts_ptr_patch_25, int __f2dace_SA_start_index_d_0_s_3143_cells_ptr_patch_4, int __f2dace_SA_start_index_d_0_s_3265_edges_ptr_patch_15, int __f2dace_SA_start_index_d_0_s_3312_verts_ptr_patch_25, int __f2dace_SA_t2m_bias_d_0_s_4906_p_diag_45, int __f2dace_SA_t_avginc_d_0_s_4910_p_diag_45, int __f2dace_SA_t_wgt_avginc_d_0_s_4912_p_diag_45, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_temp_d_0_s_4804_p_diag_45, int __f2dace_SA_temp_d_1_s_4805_p_diag_45, int __f2dace_SA_temp_ifc_d_0_s_4810_p_diag_45, int __f2dace_SA_temp_ifc_d_1_s_4811_p_diag_45, int __f2dace_SA_tempv_d_0_s_4807_p_diag_45, int __f2dace_SA_tempv_d_1_s_4808_p_diag_45, int __f2dace_SA_theta_ref_ic_d_0_s_5295_p_metrics_44, int __f2dace_SA_theta_ref_ic_d_1_s_5296_p_metrics_44, int __f2dace_SA_theta_ref_mc_d_0_s_5289_p_metrics_44, int __f2dace_SA_theta_ref_mc_d_1_s_5290_p_metrics_44, int __f2dace_SA_theta_ref_me_d_0_s_5292_p_metrics_44, int __f2dace_SA_theta_ref_me_d_1_s_5293_p_metrics_44, int __f2dace_SA_theta_v_d_0_s_4767_p_prog_43, int __f2dace_SA_theta_v_d_1_s_4768_p_prog_43, int __f2dace_SA_theta_v_ic_d_0_s_4845_p_diag_45, int __f2dace_SA_theta_v_ic_d_1_s_4846_p_diag_45, int __f2dace_SA_theta_v_ic_int_d_0_s_4888_p_diag_45, int __f2dace_SA_theta_v_ic_int_d_1_s_4889_p_diag_45, int __f2dace_SA_theta_v_ic_ubc_d_0_s_4891_p_diag_45, int __f2dace_SA_theta_v_ic_ubc_d_1_s_4892_p_diag_45, int __f2dace_SA_tke_d_0_s_4774_p_prog_43, int __f2dace_SA_tke_d_1_s_4775_p_prog_43, int __f2dace_SA_tracer_d_0_s_4770_p_prog_43, int __f2dace_SA_tracer_d_1_s_4771_p_prog_43, int __f2dace_SA_tracer_d_2_s_4772_p_prog_43, int __f2dace_SA_tracer_vi_d_0_s_4798_p_diag_45, int __f2dace_SA_tracer_vi_d_1_s_4799_p_diag_45, int __f2dace_SA_tsfc_ref_d_0_s_5298_p_metrics_44, int __f2dace_SA_u_d_0_s_4782_p_diag_45, int __f2dace_SA_u_d_1_s_4783_p_diag_45, int __f2dace_SA_v_d_0_s_4785_p_diag_45, int __f2dace_SA_v_d_1_s_4786_p_diag_45, int __f2dace_SA_vabs_avginc_d_0_s_4916_p_diag_45, int __f2dace_SA_vertex_blk_d_0_s_3123_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3124_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3125_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3120_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3121_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3122_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vertidx_gradp_d_0_s_5322_p_metrics_44, int __f2dace_SA_vertidx_gradp_d_1_s_5323_p_metrics_44, int __f2dace_SA_vertidx_gradp_d_2_s_5324_p_metrics_44, int __f2dace_SA_vfl_tracer_d_0_s_4832_p_diag_45, int __f2dace_SA_vfl_tracer_d_1_s_4833_p_diag_45, int __f2dace_SA_vfl_tracer_d_2_s_4834_p_diag_45, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vn_ie_int_d_0_s_4876_p_diag_45, int __f2dace_SA_vn_ie_int_d_1_s_4877_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SA_vn_incr_d_0_s_4929_p_diag_45, int __f2dace_SA_vn_incr_d_1_s_4930_p_diag_45, int __f2dace_SA_vor_d_0_s_4791_p_diag_45, int __f2dace_SA_vor_d_1_s_4792_p_diag_45, int __f2dace_SA_vor_u_d_0_s_4923_p_diag_45, int __f2dace_SA_vor_u_d_1_s_4924_p_diag_45, int __f2dace_SA_vor_v_d_0_s_4926_p_diag_45, int __f2dace_SA_vor_v_d_1_s_4927_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_vwind_expl_wgt_d_0_s_5168_p_metrics_44, int __f2dace_SA_vwind_impl_wgt_d_0_s_5170_p_metrics_44, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SA_w_int_d_0_s_4882_p_diag_45, int __f2dace_SA_w_int_d_1_s_4883_p_diag_45, int __f2dace_SA_w_ubc_d_0_s_4885_p_diag_45, int __f2dace_SA_w_ubc_d_1_s_4886_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SA_wgtfac_v_d_0_s_5194_p_metrics_44, int __f2dace_SA_wgtfac_v_d_1_s_5195_p_metrics_44, int __f2dace_SA_wgtfacq1_c_d_0_s_5263_p_metrics_44, int __f2dace_SA_wgtfacq1_c_d_1_s_5264_p_metrics_44, int __f2dace_SA_wgtfacq1_e_d_0_s_5266_p_metrics_44, int __f2dace_SA_wgtfacq1_e_d_1_s_5267_p_metrics_44, int __f2dace_SA_wgtfacq_c_d_0_s_5257_p_metrics_44, int __f2dace_SA_wgtfacq_c_d_1_s_5258_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SA_z_ifc_d_0_s_5141_p_metrics_44, int __f2dace_SA_z_ifc_d_1_s_5142_p_metrics_44, int __f2dace_SA_z_mc_d_0_s_5144_p_metrics_44, int __f2dace_SA_z_mc_d_1_s_5145_p_metrics_44, int __f2dace_SA_zd_blklist_d_0_s_5328_p_metrics_44, int __f2dace_SA_zd_e2cell_d_0_s_5176_p_metrics_44, int __f2dace_SA_zd_edgeblk_d_0_s_5332_p_metrics_44, int __f2dace_SA_zd_edgeidx_d_0_s_5330_p_metrics_44, int __f2dace_SA_zd_geofac_d_0_s_5174_p_metrics_44, int __f2dace_SA_zd_indlist_d_0_s_5326_p_metrics_44, int __f2dace_SA_zd_intcoef_d_0_s_5172_p_metrics_44, int __f2dace_SA_zd_vertidx_d_0_s_5334_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_0_s_5278_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_1_s_5279_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_2_s_5280_p_metrics_44, int __f2dace_SA_zgpot_ifc_d_0_s_5353_p_metrics_44, int __f2dace_SA_zgpot_ifc_d_1_s_5354_p_metrics_44, int __f2dace_SA_zgpot_mc_d_0_s_5356_p_metrics_44, int __f2dace_SA_zgpot_mc_d_1_s_5357_p_metrics_44, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_end_blk_d_0_s_3319_verts_ptr_patch_25, int __f2dace_SOA_end_blk_d_1_s_3320_verts_ptr_patch_25, int __f2dace_SOA_end_block_d_0_s_3152_cells_ptr_patch_4, int __f2dace_SOA_end_block_d_0_s_3274_edges_ptr_patch_15, int __f2dace_SOA_end_block_d_0_s_3321_verts_ptr_patch_25, int __f2dace_SOA_end_index_d_0_s_3146_cells_ptr_patch_4, int __f2dace_SOA_end_index_d_0_s_3268_edges_ptr_patch_15, int __f2dace_SOA_end_index_d_0_s_3315_verts_ptr_patch_25, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_start_blk_d_0_s_3316_verts_ptr_patch_25, int __f2dace_SOA_start_blk_d_1_s_3317_verts_ptr_patch_25, int __f2dace_SOA_start_block_d_0_s_3149_cells_ptr_patch_4, int __f2dace_SOA_start_block_d_0_s_3271_edges_ptr_patch_15, int __f2dace_SOA_start_block_d_0_s_3318_verts_ptr_patch_25, int __f2dace_SOA_start_index_d_0_s_3143_cells_ptr_patch_4, int __f2dace_SOA_start_index_d_0_s_3265_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int lextra_diffu, int lvert_nest, int tmp_struct_symbol_0, int tmp_struct_symbol_1, int tmp_struct_symbol_10, int tmp_struct_symbol_11, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_15, int tmp_struct_symbol_16, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int tmp_struct_symbol_6, int tmp_struct_symbol_7, int tmp_struct_symbol_8, int tmp_struct_symbol_9);
DACE_EXPORTED int __dace_exit_cuda(velocity_tendencies_state_t *__state);

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_28_1_2(const double * __restrict__ p_int_cells_aw_verts_1, const double * __restrict__ p_prog_w_0, double * __restrict__ z_w_v, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int _for_it_46_0, int _for_it_47_0, int _for_it_48_0, int tmp_index_776_0, int tmp_index_778_0, int tmp_index_788_0, int tmp_index_790_0, int tmp_index_800_0, int tmp_index_802_0, int tmp_index_812_0, int tmp_index_814_0, int tmp_index_824_0, int tmp_index_826_0, int tmp_index_836_0, int tmp_index_838_0, int tmp_struct_symbol_6) {

    {

        {
            double c_int_0_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (1 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double c_int_1_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (2 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double c_int_2_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (3 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double c_int_3_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (4 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double c_int_4_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (5 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double c_int_5_in_1 = p_int_cells_aw_verts_1[(((((__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38) * ((- __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38) + _for_it_46_0)) + (__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38 * (6 - __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38))) - __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38) + _for_it_47_0)];
            double p_cell_in_0_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_778_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_776_0)];
            double p_cell_in_1_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_790_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_788_0)];
            double p_cell_in_2_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_802_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_800_0)];
            double p_cell_in_3_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_814_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_812_0)];
            double p_cell_in_4_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_826_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_824_0)];
            double p_cell_in_5_in_1 = p_prog_w_0[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_838_0) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_48_0))) + tmp_index_836_0)];
            double p_vert_out_out_1;

            ///////////////////
            // Tasklet code (T_l1327_c1339)
            p_vert_out_out_1 = ((((((c_int_0_in_1 * p_cell_in_0_in_1) + (c_int_1_in_1 * p_cell_in_1_in_1)) + (c_int_2_in_1 * p_cell_in_2_in_1)) + (c_int_3_in_1 * p_cell_in_3_in_1)) + (c_int_4_in_1 * p_cell_in_4_in_1)) + (c_int_5_in_1 * p_cell_in_5_in_1));
            ///////////////////

            z_w_v[(((_for_it_48_0 + ((20480 * tmp_struct_symbol_6) * (_for_it_46_0 - 1))) + (tmp_struct_symbol_6 * (_for_it_47_0 - 1))) - 1)] = p_vert_out_out_1;
        }

    }
    
}

DACE_DFI void loop_body_27_6_0(const int * __restrict__ flat_flat_ptr_patch_verts_cell_blk, const int * __restrict__ flat_flat_ptr_patch_verts_cell_idx, const double * __restrict__ p_int_cells_aw_verts_1, const double * __restrict__ p_prog_w_0, double * __restrict__ z_w_v, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_46_0, int _for_it_47_0, int tmp_struct_symbol_6) {
    int tmp_index_778_0_0;
    int tmp_index_838_0_0;
    int tmp_index_826_0_0;
    int tmp_index_814_0_0;
    int tmp_index_776_0_0;
    int tmp_index_790_0_0;
    int tmp_index_836_0_0;
    int tmp_index_812_0_0;
    int tmp_index_802_0_0;
    int tmp_index_824_0_0;
    int tmp_index_788_0_0;
    int tmp_index_800_0_0;


    tmp_index_778_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (1 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_838_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (6 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_826_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (5 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_814_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (4 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_776_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (1 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_790_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (2 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_836_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (6 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_812_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (4 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_802_0_0 = (flat_flat_ptr_patch_verts_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25) * (3 - __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25)) + (__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_824_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (5 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_788_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (2 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_800_0_0 = (flat_flat_ptr_patch_verts_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25) * (3 - __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25)) + (__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25 * ((- __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25) + _for_it_46_0))) - __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25) + _for_it_47_0)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    {

        {
            int _for_it_48_0 = (blockIdx.y + 1);
            if (_for_it_48_0 >= 1 && _for_it_48_0 < (__f2dace_SA_w_d_1_s_4756_p_prog_43 + 1)) {
                loop_body_inlined_28_1_2(&p_int_cells_aw_verts_1[0], &p_prog_w_0[0], &z_w_v[0], __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, __f2dace_SOA_w_d_1_s_4756_p_prog_43, _for_it_46_0, _for_it_47_0, _for_it_48_0, tmp_index_776_0_0, tmp_index_778_0_0, tmp_index_788_0_0, tmp_index_790_0_0, tmp_index_800_0_0, tmp_index_802_0_0, tmp_index_812_0_0, tmp_index_814_0_0, tmp_index_824_0_0, tmp_index_826_0_0, tmp_index_836_0_0, tmp_index_838_0_0, tmp_struct_symbol_6);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_25_1_2(const double * __restrict__ p_prog_vn_2, const double * __restrict__ v_ptr_int_0_geofac_rot, double * __restrict__ zeta, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int _for_it_49_0, int _for_it_50_0, int _for_it_51_0, int tmp_index_858_0, int tmp_index_860_0, int tmp_index_870_0, int tmp_index_872_0, int tmp_index_882_0, int tmp_index_884_0, int tmp_index_894_0, int tmp_index_896_0, int tmp_index_906_0, int tmp_index_908_0, int tmp_index_918_0, int tmp_index_920_0, int tmp_struct_symbol_8) {

    {

        {
            double ptr_int_0_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (1 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double ptr_int_1_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (2 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double ptr_int_2_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (3 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double ptr_int_3_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (4 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double ptr_int_4_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (5 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double ptr_int_5_in_geofac_rot_1 = v_ptr_int_0_geofac_rot[(((((__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38) * ((- __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38) + _for_it_49_0)) + (__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38 * (6 - __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38))) - __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38) + _for_it_50_0)];
            double vec_e_0_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_860_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_858_0)];
            double vec_e_1_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_872_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_870_0)];
            double vec_e_2_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_884_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_882_0)];
            double vec_e_3_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_896_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_894_0)];
            double vec_e_4_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_908_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_906_0)];
            double vec_e_5_in_1 = p_prog_vn_2[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_920_0) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_51_0))) + tmp_index_918_0)];
            double rot_vec_out_1;

            ///////////////////
            // Tasklet code (T_l2716_c2728)
            rot_vec_out_1 = ((((((vec_e_0_in_1 * ptr_int_0_in_geofac_rot_1) + (vec_e_1_in_1 * ptr_int_1_in_geofac_rot_1)) + (vec_e_2_in_1 * ptr_int_2_in_geofac_rot_1)) + (vec_e_3_in_1 * ptr_int_3_in_geofac_rot_1)) + (vec_e_4_in_1 * ptr_int_4_in_geofac_rot_1)) + (vec_e_5_in_1 * ptr_int_5_in_geofac_rot_1));
            ///////////////////

            zeta[(((_for_it_51_0 + ((20480 * tmp_struct_symbol_8) * (_for_it_49_0 - 1))) + (tmp_struct_symbol_8 * (_for_it_50_0 - 1))) - 1)] = rot_vec_out_1;
        }

    }
    
}

DACE_DFI void loop_body_24_6_0(const int * __restrict__ flat_flat_ptr_patch_verts_edge_blk, const int * __restrict__ flat_flat_ptr_patch_verts_edge_idx, const double * __restrict__ p_prog_vn_2, const double * __restrict__ v_ptr_int_0_geofac_rot, double * __restrict__ zeta, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_49_0, int _for_it_50_0, int tmp_struct_symbol_8) {
    int tmp_index_860_0_0;
    int tmp_index_896_0_0;
    int tmp_index_906_0_0;
    int tmp_index_870_0_0;
    int tmp_index_872_0_0;
    int tmp_index_920_0_0;
    int tmp_index_918_0_0;
    int tmp_index_882_0_0;
    int tmp_index_858_0_0;
    int tmp_index_884_0_0;
    int tmp_index_894_0_0;
    int tmp_index_908_0_0;


    tmp_index_860_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (1 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_896_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (4 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_906_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (5 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_870_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (2 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_872_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (2 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_920_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (6 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_918_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (6 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_882_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (3 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_858_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (1 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_884_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (3 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_894_0_0 = (flat_flat_ptr_patch_verts_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25) * (4 - __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25)) + (__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_908_0_0 = (flat_flat_ptr_patch_verts_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25) * (5 - __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25)) + (__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25 * ((- __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25) + _for_it_49_0))) - __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25) + _for_it_50_0)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    {

        {
            int _for_it_51_0 = (blockIdx.y + 1);
            if (_for_it_51_0 >= 1 && _for_it_51_0 < 1) {
                loop_body_inlined_25_1_2(&p_prog_vn_2[0], &v_ptr_int_0_geofac_rot[0], &zeta[0], __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, _for_it_49_0, _for_it_50_0, _for_it_51_0, tmp_index_858_0_0, tmp_index_860_0_0, tmp_index_870_0_0, tmp_index_872_0_0, tmp_index_882_0_0, tmp_index_884_0_0, tmp_index_894_0_0, tmp_index_896_0_0, tmp_index_906_0_0, tmp_index_908_0_0, tmp_index_918_0_0, tmp_index_920_0_0, tmp_struct_symbol_8);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_43_0_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfac_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vn_ie, double * __restrict__ z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int _for_it_3, int _for_it_4) {

    {

        {
            double p_metrics_0_in_wgtfac_e_1 = v_p_metrics_wgtfac_e[(((((__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44) * ((- __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * ((- __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44) + _for_it_3))) - __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44) + _for_it_4)];
            double p_metrics_1_in_wgtfac_e_1 = v_p_metrics_wgtfac_e[(((((__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44) * ((- __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * ((- __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44) + _for_it_3))) - __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44) + _for_it_4)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_3))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_4)];
            double p_prog_1_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_3) - 1))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_4)];
            double p_diag_out_vn_ie_1;

            ///////////////////
            // Tasklet code (T_l241_c243)
            p_diag_out_vn_ie_1 = ((p_metrics_0_in_wgtfac_e_1 * p_prog_0_in_vn_1) + ((1.0 - p_metrics_1_in_wgtfac_e_1) * p_prog_1_in_vn_1));
            ///////////////////

            v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * ((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + _for_it_3))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_4)] = p_diag_out_vn_ie_1;
        }
        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_3))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_4)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_3))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_4)];
            double z_kin_hor_e_out_1;

            ///////////////////
            // Tasklet code (T_l244_c244)
            z_kin_hor_e_out_1 = (0.5 * ((dace::math::ipow(p_prog_0_in_vn_1, 2)) + (dace::math::ipow(p_diag_0_in_vt_1, 2))));
            ///////////////////

            z_kin_hor_e[(((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * ((- __f2dace_OA_z_kin_hor_e_d_2_s_2773) + _for_it_0)) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * ((- __f2dace_OA_z_kin_hor_e_d_1_s_2772) + _for_it_3))) - __f2dace_OA_z_kin_hor_e_d_0_s_2771) + _for_it_4)] = z_kin_hor_e_out_1;
        }

    }
    
}

DACE_DFI void loop_body_40_4_3(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfac_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vn_ie, double * __restrict__ z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int _for_it_3, int i_endidx, int i_startidx) {

    {

        {
            int _for_it_4 = (blockIdx.y + i_startidx);
            if (_for_it_4 >= i_startidx && _for_it_4 < (i_endidx + 1)) {
                loop_body_43_0_0(&v_p_diag_vt[0], &v_p_metrics_wgtfac_e[0], &v_p_prog_vn[0], &v_p_diag_vn_ie[0], &z_kin_hor_e[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, _for_it_0, _for_it_3, _for_it_4);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_41_0_2(const double * __restrict__ v_p_int_rbf_vec_coeff_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vt, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int _for_it_1, int _for_it_2, int tmp_index_18, int tmp_index_20, int tmp_index_30, int tmp_index_32, int tmp_index_42, int tmp_index_44, int tmp_index_54, int tmp_index_56) {

    {

        {
            double p_int_0_in_rbf_vec_coeff_e_1 = v_p_int_rbf_vec_coeff_e[(((((__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) * ((- __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38) + _for_it_0)) + (__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * ((- __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) + _for_it_1))) - __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38) + 1)];
            double p_int_1_in_rbf_vec_coeff_e_1 = v_p_int_rbf_vec_coeff_e[(((((__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) * ((- __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38) + _for_it_0)) + (__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * ((- __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) + _for_it_1))) - __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38) + 2)];
            double p_int_2_in_rbf_vec_coeff_e_1 = v_p_int_rbf_vec_coeff_e[(((((__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) * ((- __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38) + _for_it_0)) + (__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * ((- __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) + _for_it_1))) - __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38) + 3)];
            double p_int_3_in_rbf_vec_coeff_e_1 = v_p_int_rbf_vec_coeff_e[(((((__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) * ((- __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38) + _for_it_0)) + (__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38 * ((- __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38) + _for_it_1))) - __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38) + 4)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_20) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_2))) + tmp_index_18)];
            double p_prog_1_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_32) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_2))) + tmp_index_30)];
            double p_prog_2_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_44) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_2))) + tmp_index_42)];
            double p_prog_3_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_56) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_2))) + tmp_index_54)];
            double p_diag_out_vt_1;

            ///////////////////
            // Tasklet code (T_l224_c228)
            p_diag_out_vt_1 = ((((p_int_0_in_rbf_vec_coeff_e_1 * p_prog_0_in_vn_1) + (p_int_1_in_rbf_vec_coeff_e_1 * p_prog_1_in_vn_1)) + (p_int_2_in_rbf_vec_coeff_e_1 * p_prog_2_in_vn_1)) + (p_int_3_in_rbf_vec_coeff_e_1 * p_prog_3_in_vn_1));
            ///////////////////

            v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_2))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_1)] = p_diag_out_vt_1;
        }

    }
    
}

DACE_DFI void loop_body_40_4_0(const int * __restrict__ flat_flat_ptr_patch_edges_quad_blk, const int * __restrict__ flat_flat_ptr_patch_edges_quad_idx, const double * __restrict__ v_p_int_rbf_vec_coeff_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vt, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int _for_it_1, int nlev) {
    int tmp_index_54_0;
    int tmp_index_18_0;
    int tmp_index_56_0;
    int tmp_index_20_0;
    int tmp_index_32_0;
    int tmp_index_30_0;
    int tmp_index_44_0;
    int tmp_index_42_0;


    tmp_index_54_0 = (flat_flat_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (4 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_18_0 = (flat_flat_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (1 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_56_0 = (flat_flat_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (4 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_20_0 = (flat_flat_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (1 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_32_0 = (flat_flat_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (2 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_30_0 = (flat_flat_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (2 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    tmp_index_44_0 = (flat_flat_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (3 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
    tmp_index_42_0 = (flat_flat_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (3 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_0))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_1)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
    {

        {
            int _for_it_2 = (blockIdx.y + 1);
            if (_for_it_2 >= 1 && _for_it_2 < (nlev + 1)) {
                loop_body_inlined_41_0_2(&v_p_int_rbf_vec_coeff_e[0], &v_p_prog_vn[0], &v_p_diag_vt[0], __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_0, _for_it_1, _for_it_2, tmp_index_18_0, tmp_index_20_0, tmp_index_30_0, tmp_index_32_0, tmp_index_42_0, tmp_index_44_0, tmp_index_54_0, tmp_index_56_0);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_45_0_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfac_e, double * __restrict__ z_vt_ie, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int _for_it_5, int _for_it_6) {

    {

        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_5))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_6)];
            double p_diag_1_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * (((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_5) - 1))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_6)];
            double p_metrics_0_in_wgtfac_e_1 = v_p_metrics_wgtfac_e[(((((__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44) * ((- __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * ((- __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44) + _for_it_5))) - __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44) + _for_it_6)];
            double p_metrics_1_in_wgtfac_e_1 = v_p_metrics_wgtfac_e[(((((__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44) * ((- __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44 * ((- __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44) + _for_it_5))) - __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44) + _for_it_6)];
            double z_vt_ie_out_1;

            ///////////////////
            // Tasklet code (T_l262_c264)
            z_vt_ie_out_1 = ((p_metrics_0_in_wgtfac_e_1 * p_diag_0_in_vt_1) + ((1.0 - p_metrics_1_in_wgtfac_e_1) * p_diag_1_in_vt_1));
            ///////////////////

            z_vt_ie[(((((__f2dace_A_z_vt_ie_d_0_s_2774 * __f2dace_A_z_vt_ie_d_1_s_2775) * ((- __f2dace_OA_z_vt_ie_d_2_s_2776) + _for_it_0)) + (__f2dace_A_z_vt_ie_d_0_s_2774 * ((- __f2dace_OA_z_vt_ie_d_1_s_2775) + _for_it_5))) - __f2dace_OA_z_vt_ie_d_0_s_2774) + _for_it_6)] = z_vt_ie_out_1;
        }

    }
    
}

DACE_DFI void loop_body_40_5_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfac_e, double * __restrict__ z_vt_ie, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int _for_it_5, int i_endidx, int i_startidx) {

    {

        {
            int _for_it_6 = (blockIdx.y + i_startidx);
            if (_for_it_6 >= i_startidx && _for_it_6 < (i_endidx + 1)) {
                loop_body_45_0_0(&v_p_diag_vt[0], &v_p_metrics_wgtfac_e[0], &z_vt_ie[0], __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, _for_it_0, _for_it_5, _for_it_6);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_48_0_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_ddxn_z_full, const double * __restrict__ v_p_metrics_ddxt_z_full, const double * __restrict__ v_p_prog_vn, double * __restrict__ z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int _for_it_7, int _for_it_8) {

    {

        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_7))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_8)];
            double p_metrics_0_in_ddxn_z_full_1 = v_p_metrics_ddxn_z_full[(((((__f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44 * __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44) * ((- __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44) + _for_it_0)) + (__f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44 * ((- __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44) + _for_it_7))) - __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44) + _for_it_8)];
            double p_metrics_1_in_ddxt_z_full_1 = v_p_metrics_ddxt_z_full[(((((__f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44 * __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44) * ((- __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44) + _for_it_0)) + (__f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44 * ((- __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44) + _for_it_7))) - __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44) + _for_it_8)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_7))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_8)];
            double z_w_concorr_me_out_1;

            ///////////////////
            // Tasklet code (T_l280_c282)
            z_w_concorr_me_out_1 = ((p_prog_0_in_vn_1 * p_metrics_0_in_ddxn_z_full_1) + (p_diag_0_in_vt_1 * p_metrics_1_in_ddxt_z_full_1));
            ///////////////////

            z_w_concorr_me[(((((__f2dace_A_z_w_concorr_me_d_0_s_2768 * __f2dace_A_z_w_concorr_me_d_1_s_2769) * ((- __f2dace_OA_z_w_concorr_me_d_2_s_2770) + _for_it_0)) + (__f2dace_A_z_w_concorr_me_d_0_s_2768 * ((- __f2dace_OA_z_w_concorr_me_d_1_s_2769) + _for_it_7))) - __f2dace_OA_z_w_concorr_me_d_0_s_2768) + _for_it_8)] = z_w_concorr_me_out_1;
        }

    }
    
}

DACE_DFI void loop_body_40_7_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_ddxn_z_full, const double * __restrict__ v_p_metrics_ddxt_z_full, const double * __restrict__ v_p_prog_vn, double * __restrict__ z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int _for_it_7, int i_endidx, int i_startidx) {

    {

        {
            int _for_it_8 = (blockIdx.y + i_startidx);
            if (_for_it_8 >= i_startidx && _for_it_8 < (i_endidx + 1)) {
                loop_body_48_0_0(&v_p_diag_vt[0], &v_p_metrics_ddxn_z_full[0], &v_p_metrics_ddxt_z_full[0], &v_p_prog_vn[0], &z_w_concorr_me[0], __f2dace_A_z_w_concorr_me_d_0_s_2768, __f2dace_A_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_0_s_2768, __f2dace_OA_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_2_s_2770, __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_0, _for_it_7, _for_it_8);
            }
        }

    }
    
}

#include <chrono>
DACE_DFI void loop_body_40_6_0(const const double&  dt_linintp_ubc, const double * __restrict__ v_p_diag_vn_ie_ubc, const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfacq_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vn_ie, double * __restrict__ z_kin_hor_e, double * __restrict__ z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, int _for_it_10, int nlev, int nlevp1) {

    {

        {
            double dt_linintp_ubc_0_in = dt_linintp_ubc;
            double p_diag_0_in_vn_ie_ubc_1 = v_p_diag_vn_ie_ubc[(((((__f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45 * __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45) * ((- __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45 * (1 - __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45))) - __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45) + _for_it_10)];
            double p_diag_1_in_vn_ie_ubc_1 = v_p_diag_vn_ie_ubc[(((((__f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45 * __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45) * ((- __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45 * (2 - __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45))) - __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45) + _for_it_10)];
            double p_diag_out_vn_ie_1;

            ///////////////////
            // Tasklet code (T_l314_c314)
            p_diag_out_vn_ie_1 = (p_diag_0_in_vn_ie_ubc_1 + (dt_linintp_ubc_0_in * p_diag_1_in_vn_ie_ubc_1));
            ///////////////////

            v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * (1 - __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_10)] = p_diag_out_vn_ie_1;
        }
        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * (1 - __f2dace_SOA_vt_d_1_s_4978_p_diag_45))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_10)];
            double z_vt_ie_out_1;

            ///////////////////
            // Tasklet code (T_l316_c316)
            z_vt_ie_out_1 = p_diag_0_in_vt_1;
            ///////////////////

            z_vt_ie[(((((__f2dace_A_z_vt_ie_d_0_s_2774 * __f2dace_A_z_vt_ie_d_1_s_2775) * ((- __f2dace_OA_z_vt_ie_d_2_s_2776) + _for_it_0)) + (__f2dace_A_z_vt_ie_d_0_s_2774 * (1 - __f2dace_OA_z_vt_ie_d_1_s_2775))) - __f2dace_OA_z_vt_ie_d_0_s_2774) + _for_it_10)] = z_vt_ie_out_1;
        }
        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * (1 - __f2dace_SOA_vt_d_1_s_4978_p_diag_45))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_10)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (1 - __f2dace_SOA_vn_d_1_s_4759_p_prog_43))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_10)];
            double z_kin_hor_e_out_1;

            ///////////////////
            // Tasklet code (T_l318_c318)
            z_kin_hor_e_out_1 = (0.5 * ((dace::math::ipow(p_prog_0_in_vn_1, 2)) + (dace::math::ipow(p_diag_0_in_vt_1, 2))));
            ///////////////////

            z_kin_hor_e[(((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * ((- __f2dace_OA_z_kin_hor_e_d_2_s_2773) + _for_it_0)) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * (1 - __f2dace_OA_z_kin_hor_e_d_1_s_2772))) - __f2dace_OA_z_kin_hor_e_d_0_s_2771) + _for_it_10)] = z_kin_hor_e_out_1;
        }

    }
    {

        {
            double p_metrics_0_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (1 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_10)];
            double p_metrics_1_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (2 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_10)];
            double p_metrics_2_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (3 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_10)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_10)];
            double p_prog_1_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev) - 1))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_10)];
            double p_prog_2_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev) - 2))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_10)];
            double p_diag_out_vn_ie_1;

            ///////////////////
            // Tasklet code (T_l319_c322)
            p_diag_out_vn_ie_1 = (((p_metrics_0_in_wgtfacq_e_1 * p_prog_0_in_vn_1) + (p_metrics_1_in_wgtfacq_e_1 * p_prog_1_in_vn_1)) + (p_metrics_2_in_wgtfacq_e_1 * p_prog_2_in_vn_1));
            ///////////////////

            v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * ((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + nlevp1))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_10)] = p_diag_out_vn_ie_1;
        }

    }
    
}

#include <chrono>
DACE_DFI void loop_body_40_8_0(const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_metrics_wgtfacq_e, const double * __restrict__ v_p_prog_vn, double * __restrict__ v_p_diag_vn_ie, double * __restrict__ z_kin_hor_e, double * __restrict__ z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, int _for_it_9, int nlev, int nlevp1) {

    {

        {
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (1 - __f2dace_SOA_vn_d_1_s_4759_p_prog_43))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_9)];
            double p_diag_out_vn_ie_1;

            ///////////////////
            // Tasklet code (T_l296_c296)
            p_diag_out_vn_ie_1 = p_prog_0_in_vn_1;
            ///////////////////

            v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * (1 - __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_9)] = p_diag_out_vn_ie_1;
        }
        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * (1 - __f2dace_SOA_vt_d_1_s_4978_p_diag_45))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_9)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (1 - __f2dace_SOA_vn_d_1_s_4759_p_prog_43))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_9)];
            double z_kin_hor_e_out_1;

            ///////////////////
            // Tasklet code (T_l300_c300)
            z_kin_hor_e_out_1 = (0.5 * ((dace::math::ipow(p_prog_0_in_vn_1, 2)) + (dace::math::ipow(p_diag_0_in_vt_1, 2))));
            ///////////////////

            z_kin_hor_e[(((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * ((- __f2dace_OA_z_kin_hor_e_d_2_s_2773) + _for_it_0)) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * (1 - __f2dace_OA_z_kin_hor_e_d_1_s_2772))) - __f2dace_OA_z_kin_hor_e_d_0_s_2771) + _for_it_9)] = z_kin_hor_e_out_1;
        }
        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_0)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * (1 - __f2dace_SOA_vt_d_1_s_4978_p_diag_45))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_9)];
            double z_vt_ie_out_1;

            ///////////////////
            // Tasklet code (T_l298_c298)
            z_vt_ie_out_1 = p_diag_0_in_vt_1;
            ///////////////////

            z_vt_ie[(((((__f2dace_A_z_vt_ie_d_0_s_2774 * __f2dace_A_z_vt_ie_d_1_s_2775) * ((- __f2dace_OA_z_vt_ie_d_2_s_2776) + _for_it_0)) + (__f2dace_A_z_vt_ie_d_0_s_2774 * (1 - __f2dace_OA_z_vt_ie_d_1_s_2775))) - __f2dace_OA_z_vt_ie_d_0_s_2774) + _for_it_9)] = z_vt_ie_out_1;
        }

    }
    {

        {
            double p_metrics_0_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (1 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_9)];
            double p_metrics_1_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (2 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_9)];
            double p_metrics_2_in_wgtfacq_e_1 = v_p_metrics_wgtfacq_e[(((((__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44) * ((- __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44) + _for_it_0)) + (__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44 * (3 - __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44))) - __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44) + _for_it_9)];
            double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_9)];
            double p_prog_1_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev) - 1))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_9)];
            double p_prog_2_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_0)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * (((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + nlev) - 2))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_9)];
            double p_diag_out_vn_ie_1;

            ///////////////////
            // Tasklet code (T_l301_c304)
            p_diag_out_vn_ie_1 = (((p_metrics_0_in_wgtfacq_e_1 * p_prog_0_in_vn_1) + (p_metrics_1_in_wgtfacq_e_1 * p_prog_1_in_vn_1)) + (p_metrics_2_in_wgtfacq_e_1 * p_prog_2_in_vn_1));
            ///////////////////

            v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_0)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * ((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + nlevp1))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_9)] = p_diag_out_vn_ie_1;
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_38_0_2(const double * __restrict__ v_p_diag_vn_ie, const double * __restrict__ v_p_prog_w, const double * __restrict__ v_v_ptr_patch_edges_inv_dual_edge_length, const double * __restrict__ v_v_ptr_patch_edges_inv_primal_edge_length, const double * __restrict__ v_v_ptr_patch_edges_tangent_orientation, const double * __restrict__ z_vt_ie, const double * __restrict__ z_w_v, double * __restrict__ z_v_grad_w, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int _for_it_11, int _for_it_12, int _for_it_13, int tmp_index_214, int tmp_index_216, int tmp_index_223, int tmp_index_225, int tmp_index_240, int tmp_index_241, int tmp_index_249, int tmp_index_250, int tmp_struct_symbol_4, int tmp_struct_symbol_6) {

    {

        {
            double p_diag_0_in_vn_ie_1 = v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_11)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * ((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + _for_it_13))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_12)];
            double p_prog_0_in_w_1 = v_p_prog_w[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_216) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_13))) + tmp_index_214)];
            double p_prog_1_in_w_1 = v_p_prog_w[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_225) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_13))) + tmp_index_223)];
            double ptr_patch_0_in_edges_inv_dual_edge_length_1 = v_v_ptr_patch_edges_inv_dual_edge_length[(((__f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15 * ((- __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15) + _for_it_11)) - __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15) + _for_it_12)];
            double ptr_patch_1_in_edges_inv_primal_edge_length_1 = v_v_ptr_patch_edges_inv_primal_edge_length[(((__f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15 * ((- __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15) + _for_it_11)) - __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15) + _for_it_12)];
            double ptr_patch_2_in_edges_tangent_orientation_1 = v_v_ptr_patch_edges_tangent_orientation[(((__f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15 * ((- __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15) + _for_it_11)) - __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15) + _for_it_12)];
            double z_vt_ie_0_in_1 = z_vt_ie[(((((__f2dace_A_z_vt_ie_d_0_s_2774 * __f2dace_A_z_vt_ie_d_1_s_2775) * ((- __f2dace_OA_z_vt_ie_d_2_s_2776) + _for_it_11)) + (__f2dace_A_z_vt_ie_d_0_s_2774 * ((- __f2dace_OA_z_vt_ie_d_1_s_2775) + _for_it_13))) - __f2dace_OA_z_vt_ie_d_0_s_2774) + _for_it_12)];
            double z_w_v_0_in_1 = z_w_v[(((_for_it_13 + (tmp_index_240 * tmp_struct_symbol_6)) + ((20480 * tmp_index_241) * tmp_struct_symbol_6)) - 1)];
            double z_w_v_1_in_1 = z_w_v[(((_for_it_13 + (tmp_index_249 * tmp_struct_symbol_6)) + ((20480 * tmp_index_250) * tmp_struct_symbol_6)) - 1)];
            double z_v_grad_w_out_1;

            ///////////////////
            // Tasklet code (T_l354_c358)
            z_v_grad_w_out_1 = (((p_diag_0_in_vn_ie_1 * ptr_patch_0_in_edges_inv_dual_edge_length_1) * (p_prog_0_in_w_1 - p_prog_1_in_w_1)) + (((z_vt_ie_0_in_1 * ptr_patch_1_in_edges_inv_primal_edge_length_1) * ptr_patch_2_in_edges_tangent_orientation_1) * (z_w_v_0_in_1 - z_w_v_1_in_1)));
            ///////////////////

            z_v_grad_w[(((_for_it_13 + ((20480 * tmp_struct_symbol_4) * (_for_it_11 - 1))) + (tmp_struct_symbol_4 * (_for_it_12 - 1))) - 1)] = z_v_grad_w_out_1;
        }

    }
    
}

DACE_DFI void loop_body_37_3_0(const int * __restrict__ flat_v_ptr_patch_edges_cell_blk, const int * __restrict__ flat_v_ptr_patch_edges_cell_idx, const int * __restrict__ flat_v_ptr_patch_edges_vertex_blk, const int * __restrict__ flat_v_ptr_patch_edges_vertex_idx, const double * __restrict__ v_p_diag_vn_ie, const double * __restrict__ v_p_prog_w, const double * __restrict__ v_v_ptr_patch_edges_inv_dual_edge_length, const double * __restrict__ v_v_ptr_patch_edges_inv_primal_edge_length, const double * __restrict__ v_v_ptr_patch_edges_tangent_orientation, const double * __restrict__ z_vt_ie, const double * __restrict__ z_w_v, double * __restrict__ z_v_grad_w, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_11, int _for_it_12, int nlev, int tmp_struct_symbol_4, int tmp_struct_symbol_6) {
    int tmp_index_241_0;
    int tmp_index_250_0;
    int tmp_index_214_0;
    int tmp_index_223_0;
    int tmp_index_216_0;
    int tmp_index_249_0;
    int tmp_index_225_0;
    int tmp_index_240_0;


    tmp_index_241_0 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_12)] - 1);
    tmp_index_250_0 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_12)] - 1);
    tmp_index_214_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_12)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_223_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_12)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
    tmp_index_216_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_12)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_249_0 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_12)] - 1);
    tmp_index_225_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_12)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
    tmp_index_240_0 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_11))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_12)] - 1);
    {

        {
            int _for_it_13 = (blockIdx.y + 1);
            if (_for_it_13 >= 1 && _for_it_13 < (nlev + 1)) {
                loop_body_inlined_38_0_2(&v_p_diag_vn_ie[0], &v_p_prog_w[0], &v_v_ptr_patch_edges_inv_dual_edge_length[0], &v_v_ptr_patch_edges_inv_primal_edge_length[0], &v_v_ptr_patch_edges_tangent_orientation[0], &z_vt_ie[0], &z_w_v[0], &z_v_grad_w[0], __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_w_d_1_s_4756_p_prog_43, _for_it_11, _for_it_12, _for_it_13, tmp_index_214_0, tmp_index_216_0, tmp_index_223_0, tmp_index_225_0, tmp_index_240_0, tmp_index_241_0, tmp_index_249_0, tmp_index_250_0, tmp_struct_symbol_4, tmp_struct_symbol_6);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_18_0_2(const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_kin_hor_e, double * __restrict__ z_ekinh, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int _for_it_14, int _for_it_15, int _for_it_16, int tmp_index_267, int tmp_index_269, int tmp_index_279, int tmp_index_281, int tmp_index_291, int tmp_index_293, int tmp_struct_symbol_10) {

    {

        {
            double p_int_0_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (1 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_15)];
            double p_int_1_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (2 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_15)];
            double p_int_2_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (3 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_15)];
            double z_kin_hor_e_0_in_1 = z_kin_hor_e[((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * tmp_index_269) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * ((- __f2dace_OA_z_kin_hor_e_d_1_s_2772) + _for_it_16))) + tmp_index_267)];
            double z_kin_hor_e_1_in_1 = z_kin_hor_e[((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * tmp_index_281) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * ((- __f2dace_OA_z_kin_hor_e_d_1_s_2772) + _for_it_16))) + tmp_index_279)];
            double z_kin_hor_e_2_in_1 = z_kin_hor_e[((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * tmp_index_293) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * ((- __f2dace_OA_z_kin_hor_e_d_1_s_2772) + _for_it_16))) + tmp_index_291)];
            double z_ekinh_out_1;

            ///////////////////
            // Tasklet code (T_l395_c404)
            z_ekinh_out_1 = (((p_int_0_in_e_bln_c_s_1 * z_kin_hor_e_0_in_1) + (p_int_1_in_e_bln_c_s_1 * z_kin_hor_e_1_in_1)) + (p_int_2_in_e_bln_c_s_1 * z_kin_hor_e_2_in_1));
            ///////////////////

            z_ekinh[(((_for_it_16 + ((20480 * tmp_struct_symbol_10) * (_for_it_14 - 1))) + (tmp_struct_symbol_10 * (_for_it_15 - 1))) - 1)] = z_ekinh_out_1;
        }

    }
    
}

DACE_DFI void loop_body_1_18_0(const int * __restrict__ flat_flat_ptr_patch_cells_edge_blk, const int * __restrict__ flat_flat_ptr_patch_cells_edge_idx, const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_kin_hor_e, double * __restrict__ z_ekinh, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int _for_it_15, int nlev, int tmp_struct_symbol_10) {
    int tmp_index_281_0;
    int tmp_index_279_0;
    int tmp_index_269_0;
    int tmp_index_293_0;
    int tmp_index_267_0;
    int tmp_index_291_0;


    tmp_index_281_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_2_s_2773);
    tmp_index_279_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_0_s_2771);
    tmp_index_269_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_2_s_2773);
    tmp_index_293_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_2_s_2773);
    tmp_index_267_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_0_s_2771);
    tmp_index_291_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_15)] - __f2dace_OA_z_kin_hor_e_d_0_s_2771);
    {

        {
            int _for_it_16 = (blockIdx.y + 1);
            if (_for_it_16 >= 1 && _for_it_16 < (nlev + 1)) {
                loop_body_inlined_18_0_2(&v_p_int_e_bln_c_s[0], &z_kin_hor_e[0], &z_ekinh[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, _for_it_14, _for_it_15, _for_it_16, tmp_index_267_0, tmp_index_269_0, tmp_index_279_0, tmp_index_281_0, tmp_index_291_0, tmp_index_293_0, tmp_struct_symbol_10);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_10_0_2(const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_w_concorr_me, double * __restrict__ z_w_concorr_mc, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int _for_it_14, int _for_it_17, int _for_it_18, int tmp_index_305, int tmp_index_307, int tmp_index_317, int tmp_index_319, int tmp_index_329, int tmp_index_331) {

    {

        {
            double p_int_0_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (1 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_17)];
            double p_int_1_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (2 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_17)];
            double p_int_2_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (3 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_17)];
            double z_w_concorr_me_0_in_1 = z_w_concorr_me[((((__f2dace_A_z_w_concorr_me_d_0_s_2768 * __f2dace_A_z_w_concorr_me_d_1_s_2769) * tmp_index_307) + (__f2dace_A_z_w_concorr_me_d_0_s_2768 * ((- __f2dace_OA_z_w_concorr_me_d_1_s_2769) + _for_it_18))) + tmp_index_305)];
            double z_w_concorr_me_1_in_1 = z_w_concorr_me[((((__f2dace_A_z_w_concorr_me_d_0_s_2768 * __f2dace_A_z_w_concorr_me_d_1_s_2769) * tmp_index_319) + (__f2dace_A_z_w_concorr_me_d_0_s_2768 * ((- __f2dace_OA_z_w_concorr_me_d_1_s_2769) + _for_it_18))) + tmp_index_317)];
            double z_w_concorr_me_2_in_1 = z_w_concorr_me[((((__f2dace_A_z_w_concorr_me_d_0_s_2768 * __f2dace_A_z_w_concorr_me_d_1_s_2769) * tmp_index_331) + (__f2dace_A_z_w_concorr_me_d_0_s_2768 * ((- __f2dace_OA_z_w_concorr_me_d_1_s_2769) + _for_it_18))) + tmp_index_329)];
            double z_w_concorr_mc_out_1;

            ///////////////////
            // Tasklet code (T_l425_c428)
            z_w_concorr_mc_out_1 = (((p_int_0_in_e_bln_c_s_1 * z_w_concorr_me_0_in_1) + (p_int_1_in_e_bln_c_s_1 * z_w_concorr_me_1_in_1)) + (p_int_2_in_e_bln_c_s_1 * z_w_concorr_me_2_in_1));
            ///////////////////

            z_w_concorr_mc[((_for_it_17 + (20480 * _for_it_18)) - 20481)] = z_w_concorr_mc_out_1;
        }

    }
    
}

DACE_DFI void loop_body_1_16_0(const int * __restrict__ flat_flat_ptr_patch_cells_edge_blk, const int * __restrict__ flat_flat_ptr_patch_cells_edge_idx, const const int&  nflatlev_jg, const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_w_concorr_me, double * __restrict__ z_w_concorr_mc, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int _for_it_17, int nlev) {
    int tmp_index_331_0;
    int tmp_index_305_0;
    int tmp_index_317_0;
    int tmp_index_319_0;
    int tmp_index_307_0;
    int tmp_index_329_0;


    tmp_index_331_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_2_s_2770);
    tmp_index_305_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_0_s_2768);
    tmp_index_317_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_0_s_2768);
    tmp_index_319_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_2_s_2770);
    tmp_index_307_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_2_s_2770);
    tmp_index_329_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_17)] - __f2dace_OA_z_w_concorr_me_d_0_s_2768);
    {

        {
            int _for_it_18 = (blockIdx.y + nflatlev_jg);
            if (_for_it_18 >= nflatlev_jg && _for_it_18 < (nlev + 1)) {
                loop_body_inlined_10_0_2(&v_p_int_e_bln_c_s[0], &z_w_concorr_me[0], &z_w_concorr_mc[0], __f2dace_A_z_w_concorr_me_d_0_s_2768, __f2dace_A_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_1_s_2769, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, _for_it_14, _for_it_17, _for_it_18, tmp_index_305_0, tmp_index_307_0, tmp_index_317_0, tmp_index_319_0, tmp_index_329_0, tmp_index_331_0);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_12_0_0(const double * __restrict__ v_p_metrics_wgtfac_c, const double * __restrict__ z_w_concorr_mc, double * __restrict__ v_p_diag_w_concorr_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int _for_it_14, int _for_it_19, int _for_it_20) {

    {

        {
            double p_metrics_0_in_wgtfac_c_1 = v_p_metrics_wgtfac_c[(((((__f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44 * __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44) * ((- __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44) + _for_it_14)) + (__f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44 * ((- __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44) + _for_it_19))) - __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44) + _for_it_20)];
            double p_metrics_1_in_wgtfac_c_1 = v_p_metrics_wgtfac_c[(((((__f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44 * __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44) * ((- __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44) + _for_it_14)) + (__f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44 * ((- __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44) + _for_it_19))) - __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44) + _for_it_20)];
            double z_w_concorr_mc_0_in_1 = z_w_concorr_mc[(((20480 * _for_it_19) + _for_it_20) - 20481)];
            double z_w_concorr_mc_1_in_1 = z_w_concorr_mc[(((20480 * _for_it_19) + _for_it_20) - 40961)];
            double p_diag_out_w_concorr_c_1;

            ///////////////////
            // Tasklet code (T_l443_c445)
            p_diag_out_w_concorr_c_1 = ((p_metrics_0_in_wgtfac_c_1 * z_w_concorr_mc_0_in_1) + ((1.0 - p_metrics_1_in_wgtfac_c_1) * z_w_concorr_mc_1_in_1));
            ///////////////////

            v_p_diag_w_concorr_c[(((((__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45 * __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45) * ((- __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45) + _for_it_14)) + (__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45 * ((- __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45) + _for_it_19))) - __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45) + _for_it_20)] = p_diag_out_w_concorr_c_1;
        }

    }
    
}

DACE_DFI void loop_body_1_16_6(const double * __restrict__ v_p_metrics_wgtfac_c, const double * __restrict__ z_w_concorr_mc, double * __restrict__ v_p_diag_w_concorr_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int _for_it_14, int _for_it_19, int i_endidx) {

    {

        {
            int _for_it_20 = (blockIdx.y + 1);
            if (_for_it_20 >= 1 && _for_it_20 < (i_endidx + 1)) {
                loop_body_12_0_0(&v_p_metrics_wgtfac_c[0], &z_w_concorr_mc[0], &v_p_diag_w_concorr_c[0], __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, _for_it_14, _for_it_19, _for_it_20);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_20_0_0(const double * __restrict__ v_p_prog_w, double * __restrict__ z_w_con_c, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_21, int _for_it_22) {

    {

        {
            double p_prog_0_in_w_1 = v_p_prog_w[(((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * ((- __f2dace_SOA_w_d_2_s_4757_p_prog_43) + _for_it_14)) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_21))) - __f2dace_SOA_w_d_0_s_4755_p_prog_43) + _for_it_22)];
            double z_w_con_c_out_1;

            ///////////////////
            // Tasklet code (T_l457_c457)
            z_w_con_c_out_1 = p_prog_0_in_w_1;
            ///////////////////

            z_w_con_c[(((20480 * _for_it_21) + _for_it_22) - 20481)] = z_w_con_c_out_1;
        }

    }
    
}

DACE_DFI void loop_body_1_19_0(const double * __restrict__ v_p_prog_w, double * __restrict__ z_w_con_c, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_21, int i_endidx) {

    {

        {
            int _for_it_22 = (blockIdx.y + 1);
            if (_for_it_22 >= 1 && _for_it_22 < (i_endidx + 1)) {
                loop_body_20_0_0(&v_p_prog_w[0], &z_w_con_c[0], __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_21, _for_it_22);
            }
        }

    }
    
}

#include <chrono>
DACE_DFI void loop_body_1_2_0(double * __restrict__ z_w_con_c, int _for_it_23, int nlevp1) {

    {

        {
            double z_w_con_c_out_1;

            ///////////////////
            // Tasklet code (T_l465_c465)
            z_w_con_c_out_1 = 0.0;
            ///////////////////

            z_w_con_c[((_for_it_23 + (20480 * nlevp1)) - 20481)] = z_w_con_c_out_1;
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_3_0_0(const double * __restrict__ v_p_diag_w_concorr_c, double * __restrict__ z_w_con_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int _for_it_14, int _for_it_24, int _for_it_25) {

    {

        {
            double p_diag_0_in_w_concorr_c_1 = v_p_diag_w_concorr_c[(((((__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45 * __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45) * ((- __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45) + _for_it_14)) + (__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45 * ((- __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45) + _for_it_24))) - __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45) + _for_it_25)];
            double z_w_con_c_0_in_1 = z_w_con_c[(((20480 * _for_it_24) + _for_it_25) - 20481)];
            double z_w_con_c_out_1;

            ///////////////////
            // Tasklet code (T_l475_c475)
            z_w_con_c_out_1 = (z_w_con_c_0_in_1 - p_diag_0_in_w_concorr_c_1);
            ///////////////////

            z_w_con_c[(((20480 * _for_it_24) + _for_it_25) - 20481)] = z_w_con_c_out_1;
        }

    }
    
}

DACE_DFI void loop_body_1_2_4(const double * __restrict__ v_p_diag_w_concorr_c, double * __restrict__ z_w_con_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int _for_it_14, int _for_it_24, int i_endidx) {

    {

        {
            int _for_it_25 = (blockIdx.y + 1);
            if (_for_it_25 >= 1 && _for_it_25 < (i_endidx + 1)) {
                loop_body_3_0_0(&v_p_diag_w_concorr_c[0], &z_w_con_c[0], __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, _for_it_14, _for_it_24, _for_it_25);
            }
        }

    }
    
}

#include <chrono>
DACE_DFI void loop_body_1_2_9(int * __restrict__ levmask, int _for_it_14, int _for_it_26, int tmp_struct_symbol_13) {

    {

        {
            int levmask_out_1;

            ///////////////////
            // Tasklet code (T_l487_c487)
            levmask_out_1 = false;
            ///////////////////

            levmask[((_for_it_14 + (tmp_struct_symbol_13 * (_for_it_26 - 1))) - 1)] = levmask_out_1;
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_14_0_0(const double * __restrict__ v_p_metrics_ddqz_z_half, double * __restrict__ z_w_con_c, int * __restrict__ cfl_clipping, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int _for_it_14, int _for_it_27, int _for_it_28, double cfl_w_limit) {

    {
        double tmp_call_2;

        {
            double z_w_con_c_0_in_1 = z_w_con_c[(((20480 * _for_it_27) + _for_it_28) - 20481)];
            double tmp_call_2_out;

            ///////////////////
            // Tasklet code (T_l502_c502)
            tmp_call_2_out = abs(z_w_con_c_0_in_1);
            ///////////////////

            tmp_call_2 = tmp_call_2_out;
        }
        {
            double p_metrics_0_in_ddqz_z_half_1 = v_p_metrics_ddqz_z_half[(((((__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44 * __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44) * ((- __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44) + _for_it_14)) + (__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44 * ((- __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44) + _for_it_27))) - __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44) + _for_it_28)];
            double tmp_call_2_0_in = tmp_call_2;
            int cfl_clipping_out_1;

            ///////////////////
            // Tasklet code (T_l502_c502)
            cfl_clipping_out_1 = (tmp_call_2_0_in > (cfl_w_limit * p_metrics_0_in_ddqz_z_half_1));
            ///////////////////

            cfl_clipping[(((20480 * _for_it_27) + _for_it_28) - 20481)] = cfl_clipping_out_1;
        }

    }
    if (cfl_clipping[(((20480 * _for_it_27) + _for_it_28) - 20481)]) {


    }

    
}

DACE_DFI void loop_body_1_17_0(const const double&  dtime, const double * __restrict__ v_p_metrics_ddqz_z_half, int * __restrict__ cfl_clipping, int * __restrict__ levmask, double&  maxvcfl, double * __restrict__ z_w_con_c, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int _for_it_14, int _for_it_27, double cfl_w_limit, int i_endidx) {

    {

        {
            int _for_it_28 = (blockIdx.y + 1);
            if (_for_it_28 >= 1 && _for_it_28 < (i_endidx + 1)) {
                loop_body_14_0_0(&v_p_metrics_ddqz_z_half[0], &z_w_con_c[0], &cfl_clipping[0], __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, _for_it_14, _for_it_27, _for_it_28, cfl_w_limit);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_16_0_0(const double * __restrict__ z_w_con_c, double * __restrict__ z_w_con_c_full, int _for_it_14, int _for_it_30, int _for_it_31, int tmp_struct_symbol_2) {

    {

        {
            double z_w_con_c_0_in_1 = z_w_con_c[(((20480 * _for_it_30) + _for_it_31) - 20481)];
            double z_w_con_c_1_in_1 = z_w_con_c[(((20480 * _for_it_30) + _for_it_31) - 1)];
            double z_w_con_c_full_out_1;

            ///////////////////
            // Tasklet code (T_l533_c533)
            z_w_con_c_full_out_1 = (0.5 * (z_w_con_c_0_in_1 + z_w_con_c_1_in_1));
            ///////////////////

            z_w_con_c_full[((((20480 * _for_it_30) + _for_it_31) + ((20480 * tmp_struct_symbol_2) * (_for_it_14 - 1))) - 20481)] = z_w_con_c_full_out_1;
        }

    }
    
}

DACE_DFI void loop_body_1_17_11(const double * __restrict__ z_w_con_c, double * __restrict__ z_w_con_c_full, int _for_it_14, int _for_it_30, int i_endidx, int tmp_struct_symbol_2) {

    {

        {
            int _for_it_31 = (blockIdx.y + 1);
            if (_for_it_31 >= 1 && _for_it_31 < (i_endidx + 1)) {
                loop_body_16_0_0(&z_w_con_c[0], &z_w_con_c_full[0], _for_it_14, _for_it_30, _for_it_31, tmp_struct_symbol_2);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_6_0_2(const int  ntnd, const double * __restrict__ v_p_metrics_coeff1_dwdz, const double * __restrict__ v_p_metrics_coeff2_dwdz, const double * __restrict__ v_p_prog_w, const double * __restrict__ z_w_con_c, double * __restrict__ v_p_diag_ddt_w_adv_pc, int * __restrict__   __6_ntnd_0, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_32, int _for_it_33, int tmp_index_400) {

    {

        {
            double p_metrics_0_in_coeff1_dwdz_1 = v_p_metrics_coeff1_dwdz[(((((__f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44 * __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44) * ((- __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44) + _for_it_14)) + (__f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44 * ((- __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44) + _for_it_32))) - __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44) + _for_it_33)];
            double p_metrics_1_in_coeff2_dwdz_1 = v_p_metrics_coeff2_dwdz[(((((__f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44 * __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44) * ((- __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44) + _for_it_14)) + (__f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44 * ((- __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44) + _for_it_32))) - __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44) + _for_it_33)];
            double p_metrics_2_in_coeff2_dwdz_1 = v_p_metrics_coeff2_dwdz[(((((__f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44 * __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44) * ((- __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44) + _for_it_14)) + (__f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44 * ((- __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44) + _for_it_32))) - __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44) + _for_it_33)];
            double p_metrics_3_in_coeff1_dwdz_1 = v_p_metrics_coeff1_dwdz[(((((__f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44 * __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44) * ((- __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44) + _for_it_14)) + (__f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44 * ((- __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44) + _for_it_32))) - __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44) + _for_it_33)];
            double p_prog_0_in_w_1 = v_p_prog_w[(((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * ((- __f2dace_SOA_w_d_2_s_4757_p_prog_43) + _for_it_14)) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * (((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_32) - 1))) - __f2dace_SOA_w_d_0_s_4755_p_prog_43) + _for_it_33)];
            double p_prog_1_in_w_1 = v_p_prog_w[(((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * ((- __f2dace_SOA_w_d_2_s_4757_p_prog_43) + _for_it_14)) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * (((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_32) + 1))) - __f2dace_SOA_w_d_0_s_4755_p_prog_43) + _for_it_33)];
            double p_prog_2_in_w_1 = v_p_prog_w[(((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * ((- __f2dace_SOA_w_d_2_s_4757_p_prog_43) + _for_it_14)) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_32))) - __f2dace_SOA_w_d_0_s_4755_p_prog_43) + _for_it_33)];
            double z_w_con_c_0_in_1 = z_w_con_c[(((20480 * _for_it_32) + _for_it_33) - 20481)];
            double p_diag_out_ddt_w_adv_pc_1;

            ///////////////////
            // Tasklet code (T_l556_c559)
            p_diag_out_ddt_w_adv_pc_1 = (- (z_w_con_c_0_in_1 * (((p_prog_0_in_w_1 * p_metrics_0_in_coeff1_dwdz_1) - (p_prog_1_in_w_1 * p_metrics_1_in_coeff2_dwdz_1)) + (p_prog_2_in_w_1 * (p_metrics_2_in_coeff2_dwdz_1 - p_metrics_3_in_coeff1_dwdz_1)))));
            ///////////////////

            v_p_diag_ddt_w_adv_pc[(((((((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) * tmp_index_400) + ((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * ((- __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) + _for_it_14))) + (__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * ((- __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) + _for_it_32))) - __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45) + _for_it_33)] = p_diag_out_ddt_w_adv_pc_1;
        }

    }
    
}

DACE_DFI void loop_body_1_15_0(const const int&  ntnd, const double * __restrict__ v_p_metrics_coeff1_dwdz, const double * __restrict__ v_p_metrics_coeff2_dwdz, const double * __restrict__ v_p_prog_w, const double * __restrict__ z_w_con_c, double * __restrict__ v_p_diag_ddt_w_adv_pc, int * __restrict__   __6_ntnd_0, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_32, int i_endidx_2) {
    int tmp_index_400_0;


    tmp_index_400_0 = (ntnd - __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45);
    {

        {
            int _for_it_33 = (blockIdx.y + 1);
            if (_for_it_33 >= 1 && _for_it_33 < (i_endidx_2 + 1)) {
                loop_body_inlined_6_0_2(__6_ntnd_0, &v_p_metrics_coeff1_dwdz[0], &v_p_metrics_coeff2_dwdz[0], &v_p_prog_w[0], &z_w_con_c[0], &v_p_diag_ddt_w_adv_pc[0], __6_ntnd_0, __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_32, _for_it_33, tmp_index_400_0);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_8_1_2(const int  ntnd, const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_v_grad_w, double * __restrict__ v_p_diag_ddt_w_adv_pc, int * __restrict__   __8_ntnd_0, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int _for_it_14, int _for_it_34, int _for_it_35, int tmp_index_427, int tmp_index_431, int tmp_index_442, int tmp_index_443, int tmp_index_454, int tmp_index_455, int tmp_index_466, int tmp_index_467, int tmp_struct_symbol_4) {

    {

        {
            double p_diag_0_in_ddt_w_adv_pc_1 = v_p_diag_ddt_w_adv_pc[(((((((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) * tmp_index_431) + ((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * ((- __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) + _for_it_14))) + (__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * ((- __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) + _for_it_35))) - __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45) + _for_it_34)];
            double p_int_0_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (1 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_34)];
            double p_int_1_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (2 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_34)];
            double p_int_2_in_e_bln_c_s_1 = v_p_int_e_bln_c_s[(((((__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38) * ((- __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38) + _for_it_14)) + (__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38 * (3 - __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38))) - __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38) + _for_it_34)];
            double z_v_grad_w_0_in_1 = z_v_grad_w[(((_for_it_35 + (tmp_index_442 * tmp_struct_symbol_4)) + ((20480 * tmp_index_443) * tmp_struct_symbol_4)) - 1)];
            double z_v_grad_w_1_in_1 = z_v_grad_w[(((_for_it_35 + (tmp_index_454 * tmp_struct_symbol_4)) + ((20480 * tmp_index_455) * tmp_struct_symbol_4)) - 1)];
            double z_v_grad_w_2_in_1 = z_v_grad_w[(((_for_it_35 + (tmp_index_466 * tmp_struct_symbol_4)) + ((20480 * tmp_index_467) * tmp_struct_symbol_4)) - 1)];
            double p_diag_out_ddt_w_adv_pc_1;

            ///////////////////
            // Tasklet code (T_l569_c572)
            p_diag_out_ddt_w_adv_pc_1 = (((p_diag_0_in_ddt_w_adv_pc_1 + (p_int_0_in_e_bln_c_s_1 * z_v_grad_w_0_in_1)) + (p_int_1_in_e_bln_c_s_1 * z_v_grad_w_1_in_1)) + (p_int_2_in_e_bln_c_s_1 * z_v_grad_w_2_in_1));
            ///////////////////

            v_p_diag_ddt_w_adv_pc[(((((((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) * tmp_index_427) + ((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * ((- __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) + _for_it_14))) + (__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * ((- __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) + _for_it_35))) - __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45) + _for_it_34)] = p_diag_out_ddt_w_adv_pc_1;
        }

    }
    
}

DACE_DFI void loop_body_1_15_5(const int * __restrict__ flat_flat_ptr_patch_cells_edge_blk, const int * __restrict__ flat_flat_ptr_patch_cells_edge_idx, const const int&  ntnd, const double * __restrict__ v_p_int_e_bln_c_s, const double * __restrict__ z_v_grad_w, double * __restrict__ v_p_diag_ddt_w_adv_pc, int * __restrict__   __8_ntnd_0, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int _for_it_34, int nlev, int tmp_struct_symbol_4) {
    int tmp_index_455_0;
    int tmp_index_427_0;
    int tmp_index_431_0;
    int tmp_index_467_0;
    int tmp_index_443_0;
    int tmp_index_454_0;
    int tmp_index_466_0;
    int tmp_index_442_0;


    tmp_index_455_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_34)] - 1);
    tmp_index_427_0 = (ntnd - __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45);
    tmp_index_431_0 = (ntnd - __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45);
    tmp_index_467_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_34)] - 1);
    tmp_index_443_0 = (flat_flat_ptr_patch_cells_edge_blk[(((((__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4)) + (__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4) + _for_it_34)] - 1);
    tmp_index_454_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (2 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_34)] - 1);
    tmp_index_466_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (3 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_34)] - 1);
    tmp_index_442_0 = (flat_flat_ptr_patch_cells_edge_idx[(((((__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4) * (1 - __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4)) + (__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4 * ((- __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4) + _for_it_34)] - 1);
    {

        {
            int _for_it_35 = (blockIdx.y + 2);
            if (_for_it_35 >= 2 && _for_it_35 < (nlev + 1)) {
                loop_body_inlined_8_1_2(__8_ntnd_0, &v_p_int_e_bln_c_s[0], &z_v_grad_w[0], &v_p_diag_ddt_w_adv_pc[0], __8_ntnd_0, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, _for_it_14, _for_it_34, _for_it_35, tmp_index_427_0, tmp_index_431_0, tmp_index_442_0, tmp_index_443_0, tmp_index_454_0, tmp_index_455_0, tmp_index_466_0, tmp_index_467_0, tmp_struct_symbol_4);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_22_2_0(const int * __restrict__ cfl_clipping, const const double&  dtime, const int * __restrict__ flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, const int * __restrict__ flat_v_ptr_patch_cells_neighbor_blk, const int * __restrict__ flat_v_ptr_patch_cells_neighbor_idx, const const int&  ntnd, const const double&  scalfac_exdiff, const double * __restrict__ v_p_int_geofac_n2s, const double * __restrict__ v_p_metrics_ddqz_z_half, const double * __restrict__ v_p_prog_w, const double * __restrict__ v_v_ptr_patch_cells_area, const double * __restrict__ z_w_con_c, double * __restrict__ v_p_diag_ddt_w_adv_pc, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_36, int _for_it_37, double cfl_w_limit) {
    int tmp_index_482;
    int tmp_index_486;
    int tmp_index_501;
    int tmp_index_503;
    int tmp_index_513;
    int tmp_index_515;
    int tmp_index_525;
    int tmp_index_527;


    if ((cfl_clipping[(((20480 * _for_it_36) + _for_it_37) - 20481)] && flat_flat_v_ptr_patch_cells_decomp_info_owner_mask[(((__f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26 * ((- __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26) + _for_it_14)) - __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26) + _for_it_37)])) {

        tmp_index_482 = (ntnd - __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45);
        tmp_index_486 = (ntnd - __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45);
        tmp_index_501 = (flat_v_ptr_patch_cells_neighbor_idx[(((((__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) * (1 - __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
        tmp_index_503 = (flat_v_ptr_patch_cells_neighbor_blk[(((((__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) * (1 - __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
        tmp_index_513 = (flat_v_ptr_patch_cells_neighbor_idx[(((((__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) * (2 - __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
        tmp_index_515 = (flat_v_ptr_patch_cells_neighbor_blk[(((((__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) * (2 - __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
        tmp_index_525 = (flat_v_ptr_patch_cells_neighbor_idx[(((((__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) * (3 - __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_0_s_4755_p_prog_43);
        tmp_index_527 = (flat_v_ptr_patch_cells_neighbor_blk[(((((__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) * (3 - __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4)) + (__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4 * ((- __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4) + _for_it_14))) - __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4) + _for_it_37)] - __f2dace_SOA_w_d_2_s_4757_p_prog_43);
        {
            double tmp_call_6;
            double tmp_arg_4;
            double tmp_call_7;
            double tmp_arg_3;
            double difcoef_0;

            {
                double z_w_con_c_0_in_1 = z_w_con_c[(((20480 * _for_it_36) + _for_it_37) - 20481)];
                double tmp_call_7_out;

                ///////////////////
                // Tasklet code (T_l588_c589)
                tmp_call_7_out = abs(z_w_con_c_0_in_1);
                ///////////////////

                tmp_call_7 = tmp_call_7_out;
            }
            {
                double dtime_0_in = dtime;
                double tmp_arg_3_out;

                ///////////////////
                // Tasklet code (T_l588_c589)
                tmp_arg_3_out = (0.85 - (cfl_w_limit * dtime_0_in));
                ///////////////////

                tmp_arg_3 = tmp_arg_3_out;
            }
            {
                double dtime_0_in = dtime;
                double dtime_1_in = dtime;
                double p_metrics_0_in_ddqz_z_half_1 = v_p_metrics_ddqz_z_half[(((((__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44 * __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44) * ((- __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44) + _for_it_14)) + (__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44 * ((- __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44) + _for_it_36))) - __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44) + _for_it_37)];
                double tmp_call_7_0_in = tmp_call_7;
                double tmp_arg_4_out;

                ///////////////////
                // Tasklet code (T_l588_c589)
                tmp_arg_4_out = (((tmp_call_7_0_in * dtime_0_in) / p_metrics_0_in_ddqz_z_half_1) - (cfl_w_limit * dtime_1_in));
                ///////////////////

                tmp_arg_4 = tmp_arg_4_out;
            }
            {
                double tmp_arg_3_0_in = tmp_arg_3;
                double tmp_arg_4_0_in = tmp_arg_4;
                double tmp_call_6_out;

                ///////////////////
                // Tasklet code (T_l588_c589)
                tmp_call_6_out = min(tmp_arg_3_0_in, tmp_arg_4_0_in);
                ///////////////////

                tmp_call_6 = tmp_call_6_out;
            }
            {
                double scalfac_exdiff_0_in = scalfac_exdiff;
                double tmp_call_6_0_in = tmp_call_6;
                double difcoef_out;

                ///////////////////
                // Tasklet code (T_l588_c589)
                difcoef_out = (scalfac_exdiff_0_in * tmp_call_6_0_in);
                ///////////////////

                difcoef_0 = difcoef_out;
            }
            {
                double difcoef_0_in = difcoef_0;
                double p_diag_0_in_ddt_w_adv_pc_1 = v_p_diag_ddt_w_adv_pc[(((((((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) * tmp_index_486) + ((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * ((- __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) + _for_it_14))) + (__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * ((- __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) + _for_it_36))) - __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45) + _for_it_37)];
                double p_int_0_in_geofac_n2s_1 = v_p_int_geofac_n2s[(((((__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38) * ((- __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38) + _for_it_14)) + (__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * (1 - __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38))) - __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38) + _for_it_37)];
                double p_int_1_in_geofac_n2s_1 = v_p_int_geofac_n2s[(((((__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38) * ((- __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38) + _for_it_14)) + (__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * (2 - __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38))) - __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38) + _for_it_37)];
                double p_int_2_in_geofac_n2s_1 = v_p_int_geofac_n2s[(((((__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38) * ((- __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38) + _for_it_14)) + (__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * (3 - __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38))) - __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38) + _for_it_37)];
                double p_int_3_in_geofac_n2s_1 = v_p_int_geofac_n2s[(((((__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38) * ((- __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38) + _for_it_14)) + (__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38 * (4 - __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38))) - __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38) + _for_it_37)];
                double p_prog_0_in_w_1 = v_p_prog_w[(((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * ((- __f2dace_SOA_w_d_2_s_4757_p_prog_43) + _for_it_14)) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_36))) - __f2dace_SOA_w_d_0_s_4755_p_prog_43) + _for_it_37)];
                double p_prog_1_in_w_1 = v_p_prog_w[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_503) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_36))) + tmp_index_501)];
                double p_prog_2_in_w_1 = v_p_prog_w[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_515) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_36))) + tmp_index_513)];
                double p_prog_3_in_w_1 = v_p_prog_w[((((__f2dace_SA_w_d_0_s_4755_p_prog_43 * __f2dace_SA_w_d_1_s_4756_p_prog_43) * tmp_index_527) + (__f2dace_SA_w_d_0_s_4755_p_prog_43 * ((- __f2dace_SOA_w_d_1_s_4756_p_prog_43) + _for_it_36))) + tmp_index_525)];
                double ptr_patch_0_in_cells_area_1 = v_v_ptr_patch_cells_area[(((__f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4 * ((- __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4) + _for_it_14)) - __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4) + _for_it_37)];
                double p_diag_out_ddt_w_adv_pc_1;

                ///////////////////
                // Tasklet code (T_l592_c597)
                p_diag_out_ddt_w_adv_pc_1 = (p_diag_0_in_ddt_w_adv_pc_1 + ((difcoef_0_in * ptr_patch_0_in_cells_area_1) * ((((p_prog_0_in_w_1 * p_int_0_in_geofac_n2s_1) + (p_prog_1_in_w_1 * p_int_1_in_geofac_n2s_1)) + (p_prog_2_in_w_1 * p_int_2_in_geofac_n2s_1)) + (p_prog_3_in_w_1 * p_int_3_in_geofac_n2s_1))));
                ///////////////////

                v_p_diag_ddt_w_adv_pc[(((((((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) * tmp_index_482) + ((__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) * ((- __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45) + _for_it_14))) + (__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45 * ((- __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45) + _for_it_36))) - __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45) + _for_it_37)] = p_diag_out_ddt_w_adv_pc_1;
            }

        }

    }

    
}

DACE_DFI void loop_body_1_20_0(const int * __restrict__ cfl_clipping, const const double&  dtime, const int * __restrict__ flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, const int * __restrict__ flat_v_ptr_patch_cells_neighbor_blk, const int * __restrict__ flat_v_ptr_patch_cells_neighbor_idx, const int * __restrict__ levmask, const const int&  ntnd, const const double&  scalfac_exdiff, const double * __restrict__ v_p_int_geofac_n2s, const double * __restrict__ v_p_metrics_ddqz_z_half, const double * __restrict__ v_p_prog_w, const double * __restrict__ v_v_ptr_patch_cells_area, const double * __restrict__ z_w_con_c, double * __restrict__ v_p_diag_ddt_w_adv_pc, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int _for_it_36, double cfl_w_limit, int i_endidx_2, int tmp_struct_symbol_13) {


    if (levmask[((_for_it_14 + (tmp_struct_symbol_13 * (_for_it_36 - 1))) - 1)]) {
        {

            {
                int _for_it_37 = (blockIdx.y + 1);
                if (_for_it_37 >= 1 && _for_it_37 < (i_endidx_2 + 1)) {
                    loop_body_22_2_0(&cfl_clipping[0], dtime, &flat_flat_v_ptr_patch_cells_decomp_info_owner_mask[0], &flat_v_ptr_patch_cells_neighbor_blk[0], &flat_v_ptr_patch_cells_neighbor_idx[0], ntnd, scalfac_exdiff, &v_p_int_geofac_n2s[0], &v_p_metrics_ddqz_z_half[0], &v_p_prog_w[0], &v_v_ptr_patch_cells_area[0], &z_w_con_c[0], &v_p_diag_ddt_w_adv_pc[0], __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_36, _for_it_37, cfl_w_limit);
                }
            }

        }

    }

    
}

#include <chrono>
DACE_DFI void loop_body_0_24_2(const int * __restrict__ levmask, int * __restrict__ levelmask, int _for_it_38) {

    {

        {
            int levelmask_out_1;

            ///////////////////
            // Tasklet code (T_l615_c615)
            levelmask_out_1 = 0;
            ///////////////////

            levelmask[(_for_it_38 - 1)] = levelmask_out_1;
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_33_1_2(const int  ntnd, const double * __restrict__ v_p_diag_vn_ie, const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_int_c_lin_e, const double * __restrict__ v_p_metrics_coeff_gradekin, const double * __restrict__ v_p_metrics_ddqz_z_full_e, const double * __restrict__ v_v_ptr_patch_edges_f_e, const double * __restrict__ z_ekinh, const double * __restrict__ z_kin_hor_e, const double * __restrict__ z_w_con_c_full, const double * __restrict__ zeta, double * __restrict__ v_p_diag_ddt_vn_apc_pc, int * __restrict__   __33_ntnd_0, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int _for_it_40, int _for_it_41, int tmp_index_539, int tmp_index_559, int tmp_index_560, int tmp_index_571, int tmp_index_572, int tmp_index_585, int tmp_index_586, int tmp_index_594, int tmp_index_595, int tmp_index_605, int tmp_index_607, int tmp_index_617, int tmp_index_619, int tmp_struct_symbol_10, int tmp_struct_symbol_2, int tmp_struct_symbol_8) {

    {

        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_39)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_41))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_40)];
            double p_diag_1_in_vn_ie_1 = v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_39)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * ((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + _for_it_41))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_40)];
            double p_diag_2_in_vn_ie_1 = v_p_diag_vn_ie[(((((__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45) * ((- __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45) + _for_it_39)) + (__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45 * (((- __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45) + _for_it_41) + 1))) - __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45) + _for_it_40)];
            double p_int_0_in_c_lin_e_1 = v_p_int_c_lin_e[(((((__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38) * ((- __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38) + _for_it_39)) + (__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * (1 - __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38))) - __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38) + _for_it_40)];
            double p_int_1_in_c_lin_e_1 = v_p_int_c_lin_e[(((((__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38) * ((- __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38) + _for_it_39)) + (__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * (2 - __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38))) - __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38) + _for_it_40)];
            double p_metrics_0_in_coeff_gradekin_1 = v_p_metrics_coeff_gradekin[(((((__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44) * ((- __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44) + _for_it_39)) + (__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * (1 - __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44))) - __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44) + _for_it_40)];
            double p_metrics_1_in_coeff_gradekin_1 = v_p_metrics_coeff_gradekin[(((((__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44) * ((- __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44) + _for_it_39)) + (__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * (2 - __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44))) - __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44) + _for_it_40)];
            double p_metrics_2_in_coeff_gradekin_1 = v_p_metrics_coeff_gradekin[(((((__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44) * ((- __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44) + _for_it_39)) + (__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * (2 - __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44))) - __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44) + _for_it_40)];
            double p_metrics_3_in_coeff_gradekin_1 = v_p_metrics_coeff_gradekin[(((((__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44) * ((- __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44) + _for_it_39)) + (__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44 * (1 - __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44))) - __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44) + _for_it_40)];
            double p_metrics_4_in_ddqz_z_full_e_1 = v_p_metrics_ddqz_z_full_e[(((((__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) * ((- __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44) + _for_it_39)) + (__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * ((- __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) + _for_it_41))) - __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44) + _for_it_40)];
            double ptr_patch_0_in_edges_f_e_1 = v_v_ptr_patch_edges_f_e[(((__f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15 * ((- __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15) + _for_it_39)) - __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15) + _for_it_40)];
            double z_ekinh_0_in_1 = z_ekinh[(((_for_it_41 + (tmp_index_559 * tmp_struct_symbol_10)) + ((20480 * tmp_index_560) * tmp_struct_symbol_10)) - 1)];
            double z_ekinh_1_in_1 = z_ekinh[(((_for_it_41 + (tmp_index_571 * tmp_struct_symbol_10)) + ((20480 * tmp_index_572) * tmp_struct_symbol_10)) - 1)];
            double z_kin_hor_e_0_in_1 = z_kin_hor_e[(((((__f2dace_A_z_kin_hor_e_d_0_s_2771 * __f2dace_A_z_kin_hor_e_d_1_s_2772) * ((- __f2dace_OA_z_kin_hor_e_d_2_s_2773) + _for_it_39)) + (__f2dace_A_z_kin_hor_e_d_0_s_2771 * ((- __f2dace_OA_z_kin_hor_e_d_1_s_2772) + _for_it_41))) - __f2dace_OA_z_kin_hor_e_d_0_s_2771) + _for_it_40)];
            double z_w_con_c_full_0_in_1 = z_w_con_c_full[((((20480 * _for_it_41) + tmp_index_605) + ((20480 * tmp_index_607) * tmp_struct_symbol_2)) - 20480)];
            double z_w_con_c_full_1_in_1 = z_w_con_c_full[((((20480 * _for_it_41) + tmp_index_617) + ((20480 * tmp_index_619) * tmp_struct_symbol_2)) - 20480)];
            double zeta_0_in_1 = zeta[(((_for_it_41 + (tmp_index_585 * tmp_struct_symbol_8)) + ((20480 * tmp_index_586) * tmp_struct_symbol_8)) - 1)];
            double zeta_1_in_1 = zeta[(((_for_it_41 + (tmp_index_594 * tmp_struct_symbol_8)) + ((20480 * tmp_index_595) * tmp_struct_symbol_8)) - 1)];
            double p_diag_out_ddt_vn_apc_pc_1;

            ///////////////////
            // Tasklet code (T_l642_c650)
            p_diag_out_ddt_vn_apc_pc_1 = (- (((((z_kin_hor_e_0_in_1 * (p_metrics_0_in_coeff_gradekin_1 - p_metrics_1_in_coeff_gradekin_1)) + (p_metrics_2_in_coeff_gradekin_1 * z_ekinh_0_in_1)) - (p_metrics_3_in_coeff_gradekin_1 * z_ekinh_1_in_1)) + (p_diag_0_in_vt_1 * (ptr_patch_0_in_edges_f_e_1 + (0.5 * (zeta_0_in_1 + zeta_1_in_1))))) + ((((p_int_0_in_c_lin_e_1 * z_w_con_c_full_0_in_1) + (p_int_1_in_c_lin_e_1 * z_w_con_c_full_1_in_1)) * (p_diag_1_in_vn_ie_1 - p_diag_2_in_vn_ie_1)) / p_metrics_4_in_ddqz_z_full_e_1)));
            ///////////////////

            v_p_diag_ddt_vn_apc_pc[(((((((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) * tmp_index_539) + ((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * ((- __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) + _for_it_39))) + (__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * ((- __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) + _for_it_41))) - __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45) + _for_it_40)] = p_diag_out_ddt_vn_apc_pc_1;
        }

    }
    
}

DACE_DFI void loop_body_30_6_0(const int * __restrict__ flat_v_ptr_patch_edges_cell_blk, const int * __restrict__ flat_v_ptr_patch_edges_cell_idx, const int * __restrict__ flat_v_ptr_patch_edges_vertex_blk, const int * __restrict__ flat_v_ptr_patch_edges_vertex_idx, const const int&  ntnd, const double * __restrict__ v_p_diag_vn_ie, const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_p_int_c_lin_e, const double * __restrict__ v_p_metrics_coeff_gradekin, const double * __restrict__ v_p_metrics_ddqz_z_full_e, const double * __restrict__ v_v_ptr_patch_edges_f_e, const double * __restrict__ z_ekinh, const double * __restrict__ z_kin_hor_e, const double * __restrict__ z_w_con_c_full, const double * __restrict__ zeta, double * __restrict__ v_p_diag_ddt_vn_apc_pc, int * __restrict__   __33_ntnd_0, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int _for_it_40, int nlev, int tmp_struct_symbol_10, int tmp_struct_symbol_2, int tmp_struct_symbol_8) {
    int tmp_index_572_0;
    int tmp_index_571_0;
    int tmp_index_585_0;
    int tmp_index_605_0;
    int tmp_index_607_0;
    int tmp_index_594_0;
    int tmp_index_539_0;
    int tmp_index_559_0;
    int tmp_index_595_0;
    int tmp_index_617_0;
    int tmp_index_586_0;
    int tmp_index_560_0;
    int tmp_index_619_0;


    tmp_index_572_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_571_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_585_0 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_605_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_607_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_594_0 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_539_0 = (ntnd - __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45);
    tmp_index_559_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_595_0 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_617_0 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_586_0 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_560_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_40)] - 1);
    tmp_index_619_0 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_40)] - 1);
    {

        {
            int _for_it_41 = (blockIdx.y + 1);
            if (_for_it_41 >= 1 && _for_it_41 < (nlev + 1)) {
                loop_body_inlined_33_1_2(__33_ntnd_0, &v_p_diag_vn_ie[0], &v_p_diag_vt[0], &v_p_int_c_lin_e[0], &v_p_metrics_coeff_gradekin[0], &v_p_metrics_ddqz_z_full_e[0], &v_v_ptr_patch_edges_f_e[0], &z_ekinh[0], &z_kin_hor_e[0], &z_w_con_c_full[0], &zeta[0], &v_p_diag_ddt_vn_apc_pc[0], __33_ntnd_0, __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_39, _for_it_40, _for_it_41, tmp_index_539_0, tmp_index_559_0, tmp_index_560_0, tmp_index_571_0, tmp_index_572_0, tmp_index_585_0, tmp_index_586_0, tmp_index_594_0, tmp_index_595_0, tmp_index_605_0, tmp_index_607_0, tmp_index_617_0, tmp_index_619_0, tmp_struct_symbol_10, tmp_struct_symbol_2, tmp_struct_symbol_8);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_inlined_31_0_2(const int  ntnd, const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_v_ptr_patch_edges_f_e, double * __restrict__ v_p_diag_ddt_vn_cor_pc, int * __restrict__   __31_ntnd_0, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int _for_it_42, int _for_it_43, int tmp_index_632) {

    {

        {
            double p_diag_0_in_vt_1 = v_p_diag_vt[(((((__f2dace_SA_vt_d_0_s_4977_p_diag_45 * __f2dace_SA_vt_d_1_s_4978_p_diag_45) * ((- __f2dace_SOA_vt_d_2_s_4979_p_diag_45) + _for_it_39)) + (__f2dace_SA_vt_d_0_s_4977_p_diag_45 * ((- __f2dace_SOA_vt_d_1_s_4978_p_diag_45) + _for_it_42))) - __f2dace_SOA_vt_d_0_s_4977_p_diag_45) + _for_it_43)];
            double ptr_patch_0_in_edges_f_e_1 = v_v_ptr_patch_edges_f_e[(((__f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15 * ((- __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15) + _for_it_39)) - __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15) + _for_it_43)];
            double p_diag_out_ddt_vn_cor_pc_1;

            ///////////////////
            // Tasklet code (T_l662_c662)
            p_diag_out_ddt_vn_cor_pc_1 = (- (p_diag_0_in_vt_1 * ptr_patch_0_in_edges_f_e_1));
            ///////////////////

            v_p_diag_ddt_vn_cor_pc[(((((((__f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45 * __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45) * __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45) * tmp_index_632) + ((__f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45 * __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45) * ((- __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45) + _for_it_39))) + (__f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45 * ((- __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45) + _for_it_42))) - __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45) + _for_it_43)] = p_diag_out_ddt_vn_cor_pc_1;
        }

    }
    
}

DACE_DFI void loop_body_30_5_0(const const int&  ntnd, const double * __restrict__ v_p_diag_vt, const double * __restrict__ v_v_ptr_patch_edges_f_e, double * __restrict__ v_p_diag_ddt_vn_cor_pc, int * __restrict__   __31_ntnd_0, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int _for_it_42, int i_endidx, int i_startidx) {
    int tmp_index_632_0;


    tmp_index_632_0 = (ntnd - __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45);
    {

        {
            int _for_it_43 = (blockIdx.y + i_startidx);
            if (_for_it_43 >= i_startidx && _for_it_43 < (i_endidx + 1)) {
                loop_body_inlined_31_0_2(__31_ntnd_0, &v_p_diag_vt[0], &v_v_ptr_patch_edges_f_e[0], &v_p_diag_ddt_vn_cor_pc[0], __31_ntnd_0, __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_39, _for_it_42, _for_it_43, tmp_index_632_0);
            }
        }

    }
    
}

#include <chrono>
#include <chrono>
DACE_DFI void loop_body_35_2_0(const const double&  dtime, const double * __restrict__ flat_p_int_c_lin_e, const int * __restrict__ flat_v_ptr_patch_edges_cell_blk, const int * __restrict__ flat_v_ptr_patch_edges_cell_idx, const int * __restrict__ flat_v_ptr_patch_edges_quad_blk, const int * __restrict__ flat_v_ptr_patch_edges_quad_idx, const int * __restrict__ flat_v_ptr_patch_edges_vertex_blk, const int * __restrict__ flat_v_ptr_patch_edges_vertex_idx, const const int&  ntnd, const const double&  scalfac_exdiff, const double * __restrict__ v_p_int_geofac_grdiv, const double * __restrict__ v_p_metrics_ddqz_z_full_e, const double * __restrict__ v_p_prog_vn, const double * __restrict__ v_v_ptr_patch_edges_area_edge, const double * __restrict__ v_v_ptr_patch_edges_inv_primal_edge_length, const double * __restrict__ v_v_ptr_patch_edges_tangent_orientation, const double * __restrict__ z_w_con_c_full, const double * __restrict__ zeta, double * __restrict__ v_p_diag_ddt_vn_apc_pc, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_39, int _for_it_44, int _for_it_45, double cfl_w_limit, int tmp_struct_symbol_2, int tmp_struct_symbol_8) {
    int tmp_index_649;
    int tmp_index_651;
    int tmp_index_661;
    int tmp_index_663;
    double w_con_e;
    int tmp_index_673;
    int tmp_index_677;
    int tmp_index_695;
    int tmp_index_697;
    int tmp_index_707;
    int tmp_index_709;
    int tmp_index_719;
    int tmp_index_721;
    int tmp_index_731;
    int tmp_index_733;
    int tmp_index_745;
    int tmp_index_746;
    int tmp_index_754;
    int tmp_index_755;


    tmp_index_649 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_45)] - 1);
    tmp_index_651 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (1 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_45)] - 1);
    tmp_index_661 = (flat_v_ptr_patch_edges_cell_idx[(((((__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15)) + (__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15) + _for_it_45)] - 1);
    tmp_index_663 = (flat_v_ptr_patch_edges_cell_blk[(((((__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15) * (2 - __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15)) + (__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15 * ((- __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15) + _for_it_45)] - 1);

    w_con_e = ((flat_p_int_c_lin_e[(((((__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38) * ((- __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38) + _for_it_39)) + (__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * (1 - __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38))) - __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38) + _for_it_45)] * z_w_con_c_full[((((20480 * _for_it_44) + tmp_index_649) + ((20480 * tmp_index_651) * tmp_struct_symbol_2)) - 20480)]) + (flat_p_int_c_lin_e[(((((__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38) * ((- __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38) + _for_it_39)) + (__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38 * (2 - __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38))) - __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38) + _for_it_45)] * z_w_con_c_full[((((20480 * _for_it_44) + tmp_index_661) + ((20480 * tmp_index_663) * tmp_struct_symbol_2)) - 20480)]));

    if ((abs(w_con_e) > (cfl_w_limit * v_p_metrics_ddqz_z_full_e[(((((__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) * ((- __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44) + _for_it_39)) + (__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * ((- __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) + _for_it_44))) - __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44) + _for_it_45)]))) {

        tmp_index_673 = (ntnd - __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45);
        tmp_index_677 = (ntnd - __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45);
        tmp_index_695 = (flat_v_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (1 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
        tmp_index_697 = (flat_v_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (1 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
        tmp_index_707 = (flat_v_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (2 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
        tmp_index_709 = (flat_v_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (2 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
        tmp_index_719 = (flat_v_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (3 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
        tmp_index_721 = (flat_v_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (3 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
        tmp_index_731 = (flat_v_ptr_patch_edges_quad_idx[(((((__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15) * (4 - __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15)) + (__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_0_s_4758_p_prog_43);
        tmp_index_733 = (flat_v_ptr_patch_edges_quad_blk[(((((__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15) * (4 - __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15)) + (__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15 * ((- __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15) + _for_it_45)] - __f2dace_SOA_vn_d_2_s_4760_p_prog_43);
        tmp_index_745 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_45)] - 1);
        tmp_index_746 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (2 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_45)] - 1);
        tmp_index_754 = (flat_v_ptr_patch_edges_vertex_idx[(((((__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15)) + (__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15) + _for_it_45)] - 1);
        tmp_index_755 = (flat_v_ptr_patch_edges_vertex_blk[(((((__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) * (1 - __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15)) + (__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15 * ((- __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15) + _for_it_39))) - __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15) + _for_it_45)] - 1);
        {
            double tmp_arg_8;
            double difcoef_1;
            double tmp_call_12;
            double tmp_call_13;
            double tmp_arg_7;

            {
                double tmp_call_13_out;

                ///////////////////
                // Tasklet code (T_l682_c683)
                tmp_call_13_out = abs(w_con_e);
                ///////////////////

                tmp_call_13 = tmp_call_13_out;
            }
            {
                double dtime_0_in = dtime;
                double tmp_arg_7_out;

                ///////////////////
                // Tasklet code (T_l682_c683)
                tmp_arg_7_out = (0.85 - (cfl_w_limit * dtime_0_in));
                ///////////////////

                tmp_arg_7 = tmp_arg_7_out;
            }
            {
                double dtime_0_in = dtime;
                double dtime_1_in = dtime;
                double p_metrics_0_in_ddqz_z_full_e_1 = v_p_metrics_ddqz_z_full_e[(((((__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) * ((- __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44) + _for_it_39)) + (__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44 * ((- __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44) + _for_it_44))) - __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44) + _for_it_45)];
                double tmp_call_13_0_in = tmp_call_13;
                double tmp_arg_8_out;

                ///////////////////
                // Tasklet code (T_l682_c683)
                tmp_arg_8_out = (((tmp_call_13_0_in * dtime_0_in) / p_metrics_0_in_ddqz_z_full_e_1) - (cfl_w_limit * dtime_1_in));
                ///////////////////

                tmp_arg_8 = tmp_arg_8_out;
            }
            {
                double tmp_arg_7_0_in = tmp_arg_7;
                double tmp_arg_8_0_in = tmp_arg_8;
                double tmp_call_12_out;

                ///////////////////
                // Tasklet code (T_l682_c683)
                tmp_call_12_out = min(tmp_arg_7_0_in, tmp_arg_8_0_in);
                ///////////////////

                tmp_call_12 = tmp_call_12_out;
            }
            {
                double scalfac_exdiff_0_in = scalfac_exdiff;
                double tmp_call_12_0_in = tmp_call_12;
                double difcoef_out;

                ///////////////////
                // Tasklet code (T_l682_c683)
                difcoef_out = (scalfac_exdiff_0_in * tmp_call_12_0_in);
                ///////////////////

                difcoef_1 = difcoef_out;
            }
            {
                double difcoef_0_in = difcoef_1;
                double p_diag_0_in_ddt_vn_apc_pc_1 = v_p_diag_ddt_vn_apc_pc[(((((((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) * tmp_index_677) + ((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * ((- __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) + _for_it_39))) + (__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * ((- __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) + _for_it_44))) - __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45) + _for_it_45)];
                double p_int_0_in_geofac_grdiv_1 = v_p_int_geofac_grdiv[(((((__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38) * ((- __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38) + _for_it_39)) + (__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * (1 - __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38))) - __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38) + _for_it_45)];
                double p_int_1_in_geofac_grdiv_1 = v_p_int_geofac_grdiv[(((((__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38) * ((- __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38) + _for_it_39)) + (__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * (2 - __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38))) - __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38) + _for_it_45)];
                double p_int_2_in_geofac_grdiv_1 = v_p_int_geofac_grdiv[(((((__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38) * ((- __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38) + _for_it_39)) + (__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * (3 - __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38))) - __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38) + _for_it_45)];
                double p_int_3_in_geofac_grdiv_1 = v_p_int_geofac_grdiv[(((((__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38) * ((- __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38) + _for_it_39)) + (__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * (4 - __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38))) - __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38) + _for_it_45)];
                double p_int_4_in_geofac_grdiv_1 = v_p_int_geofac_grdiv[(((((__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38) * ((- __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38) + _for_it_39)) + (__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38 * (5 - __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38))) - __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38) + _for_it_45)];
                double p_prog_0_in_vn_1 = v_p_prog_vn[(((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * ((- __f2dace_SOA_vn_d_2_s_4760_p_prog_43) + _for_it_39)) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_44))) - __f2dace_SOA_vn_d_0_s_4758_p_prog_43) + _for_it_45)];
                double p_prog_1_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_697) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_44))) + tmp_index_695)];
                double p_prog_2_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_709) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_44))) + tmp_index_707)];
                double p_prog_3_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_721) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_44))) + tmp_index_719)];
                double p_prog_4_in_vn_1 = v_p_prog_vn[((((__f2dace_SA_vn_d_0_s_4758_p_prog_43 * __f2dace_SA_vn_d_1_s_4759_p_prog_43) * tmp_index_733) + (__f2dace_SA_vn_d_0_s_4758_p_prog_43 * ((- __f2dace_SOA_vn_d_1_s_4759_p_prog_43) + _for_it_44))) + tmp_index_731)];
                double ptr_patch_0_in_edges_area_edge_1 = v_v_ptr_patch_edges_area_edge[(((__f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15 * ((- __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15) + _for_it_39)) - __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15) + _for_it_45)];
                double ptr_patch_1_in_edges_tangent_orientation_1 = v_v_ptr_patch_edges_tangent_orientation[(((__f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15 * ((- __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15) + _for_it_39)) - __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15) + _for_it_45)];
                double ptr_patch_2_in_edges_inv_primal_edge_length_1 = v_v_ptr_patch_edges_inv_primal_edge_length[(((__f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15 * ((- __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15) + _for_it_39)) - __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15) + _for_it_45)];
                double zeta_0_in_1 = zeta[(((_for_it_44 + (tmp_index_745 * tmp_struct_symbol_8)) + ((20480 * tmp_index_746) * tmp_struct_symbol_8)) - 1)];
                double zeta_1_in_1 = zeta[(((_for_it_44 + (tmp_index_754 * tmp_struct_symbol_8)) + ((20480 * tmp_index_755) * tmp_struct_symbol_8)) - 1)];
                double p_diag_out_ddt_vn_apc_pc_1;

                ///////////////////
                // Tasklet code (T_l685_c694)
                p_diag_out_ddt_vn_apc_pc_1 = (p_diag_0_in_ddt_vn_apc_pc_1 + ((difcoef_0_in * ptr_patch_0_in_edges_area_edge_1) * ((((((p_int_0_in_geofac_grdiv_1 * p_prog_0_in_vn_1) + (p_int_1_in_geofac_grdiv_1 * p_prog_1_in_vn_1)) + (p_int_2_in_geofac_grdiv_1 * p_prog_2_in_vn_1)) + (p_int_3_in_geofac_grdiv_1 * p_prog_3_in_vn_1)) + (p_int_4_in_geofac_grdiv_1 * p_prog_4_in_vn_1)) + ((ptr_patch_1_in_edges_tangent_orientation_1 * ptr_patch_2_in_edges_inv_primal_edge_length_1) * (zeta_0_in_1 - zeta_1_in_1)))));
                ///////////////////

                v_p_diag_ddt_vn_apc_pc[(((((((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) * tmp_index_673) + ((__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) * ((- __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45) + _for_it_39))) + (__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45 * ((- __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45) + _for_it_44))) - __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45) + _for_it_45)] = p_diag_out_ddt_vn_apc_pc_1;
            }

        }

    }

    
}

DACE_DFI void loop_body_30_7_0(const const double&  dtime, const double * __restrict__ flat_p_int_c_lin_e, const int * __restrict__ flat_v_ptr_patch_edges_cell_blk, const int * __restrict__ flat_v_ptr_patch_edges_cell_idx, const int * __restrict__ flat_v_ptr_patch_edges_quad_blk, const int * __restrict__ flat_v_ptr_patch_edges_quad_idx, const int * __restrict__ flat_v_ptr_patch_edges_vertex_blk, const int * __restrict__ flat_v_ptr_patch_edges_vertex_idx, const int * __restrict__ levelmask, const const int&  ntnd, const const double&  scalfac_exdiff, const double * __restrict__ v_p_int_geofac_grdiv, const double * __restrict__ v_p_metrics_ddqz_z_full_e, const double * __restrict__ v_p_prog_vn, const double * __restrict__ v_v_ptr_patch_edges_area_edge, const double * __restrict__ v_v_ptr_patch_edges_inv_primal_edge_length, const double * __restrict__ v_v_ptr_patch_edges_tangent_orientation, const double * __restrict__ z_w_con_c_full, const double * __restrict__ zeta, double * __restrict__ v_p_diag_ddt_vn_apc_pc, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_39, int _for_it_44, double cfl_w_limit, int i_endidx, int i_startidx, int tmp_struct_symbol_2, int tmp_struct_symbol_8) {


    if ((levelmask[(_for_it_44 - 1)] || levelmask[_for_it_44])) {
        {

            {
                int _for_it_45 = (blockIdx.y + i_startidx);
                if (_for_it_45 >= i_startidx && _for_it_45 < (i_endidx + 1)) {
                    loop_body_35_2_0(dtime, &flat_p_int_c_lin_e[0], &flat_v_ptr_patch_edges_cell_blk[0], &flat_v_ptr_patch_edges_cell_idx[0], &flat_v_ptr_patch_edges_quad_blk[0], &flat_v_ptr_patch_edges_quad_idx[0], &flat_v_ptr_patch_edges_vertex_blk[0], &flat_v_ptr_patch_edges_vertex_idx[0], ntnd, scalfac_exdiff, &v_p_int_geofac_grdiv[0], &v_p_metrics_ddqz_z_full_e[0], &v_p_prog_vn[0], &v_v_ptr_patch_edges_area_edge[0], &v_v_ptr_patch_edges_inv_primal_edge_length[0], &v_v_ptr_patch_edges_tangent_orientation[0], &z_w_con_c_full[0], &zeta[0], &v_p_diag_ddt_vn_apc_pc[0], __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, _for_it_39, _for_it_44, _for_it_45, cfl_w_limit, tmp_struct_symbol_2, tmp_struct_symbol_8);
                }
            }

        }

    }

    
}



int __dace_init_cuda(velocity_tendencies_state_t *__state, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_airmass_new_d_0_s_4851_p_diag_45, int __f2dace_SA_airmass_new_d_1_s_4852_p_diag_45, int __f2dace_SA_airmass_now_d_0_s_4848_p_diag_45, int __f2dace_SA_airmass_now_d_1_s_4849_p_diag_45, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SA_coeff_gradp_d_0_s_5282_p_metrics_44, int __f2dace_SA_coeff_gradp_d_1_s_5283_p_metrics_44, int __f2dace_SA_coeff_gradp_d_2_s_5284_p_metrics_44, int __f2dace_SA_d2dexdz2_fac1_mc_d_0_s_5312_p_metrics_44, int __f2dace_SA_d2dexdz2_fac1_mc_d_1_s_5313_p_metrics_44, int __f2dace_SA_d2dexdz2_fac2_mc_d_0_s_5315_p_metrics_44, int __f2dace_SA_d2dexdz2_fac2_mc_d_1_s_5316_p_metrics_44, int __f2dace_SA_d_exner_dz_ref_ic_d_0_s_5309_p_metrics_44, int __f2dace_SA_d_exner_dz_ref_ic_d_1_s_5310_p_metrics_44, int __f2dace_SA_ddqz_z_full_d_0_s_5147_p_metrics_44, int __f2dace_SA_ddqz_z_full_d_1_s_5148_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SA_ddt_exner_phy_d_0_s_4980_p_diag_45, int __f2dace_SA_ddt_exner_phy_d_1_s_4981_p_diag_45, int __f2dace_SA_ddt_pres_sfc_d_0_s_4823_p_diag_45, int __f2dace_SA_ddt_temp_dyn_d_0_s_5121_p_diag_45, int __f2dace_SA_ddt_temp_dyn_d_1_s_5122_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_0_s_4794_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_1_s_4795_p_diag_45, int __f2dace_SA_ddt_tracer_adv_d_2_s_4796_p_diag_45, int __f2dace_SA_ddt_ua_adv_d_0_s_5052_p_diag_45, int __f2dace_SA_ddt_ua_adv_d_1_s_5053_p_diag_45, int __f2dace_SA_ddt_ua_cen_d_0_s_5088_p_diag_45, int __f2dace_SA_ddt_ua_cen_d_1_s_5089_p_diag_45, int __f2dace_SA_ddt_ua_cor_d_0_s_5061_p_diag_45, int __f2dace_SA_ddt_ua_cor_d_1_s_5062_p_diag_45, int __f2dace_SA_ddt_ua_dmp_d_0_s_5034_p_diag_45, int __f2dace_SA_ddt_ua_dmp_d_1_s_5035_p_diag_45, int __f2dace_SA_ddt_ua_dyn_d_0_s_5025_p_diag_45, int __f2dace_SA_ddt_ua_dyn_d_1_s_5026_p_diag_45, int __f2dace_SA_ddt_ua_grf_d_0_s_5115_p_diag_45, int __f2dace_SA_ddt_ua_grf_d_1_s_5116_p_diag_45, int __f2dace_SA_ddt_ua_hdf_d_0_s_5043_p_diag_45, int __f2dace_SA_ddt_ua_hdf_d_1_s_5044_p_diag_45, int __f2dace_SA_ddt_ua_iau_d_0_s_5097_p_diag_45, int __f2dace_SA_ddt_ua_iau_d_1_s_5098_p_diag_45, int __f2dace_SA_ddt_ua_pgr_d_0_s_5070_p_diag_45, int __f2dace_SA_ddt_ua_pgr_d_1_s_5071_p_diag_45, int __f2dace_SA_ddt_ua_phd_d_0_s_5079_p_diag_45, int __f2dace_SA_ddt_ua_phd_d_1_s_5080_p_diag_45, int __f2dace_SA_ddt_ua_ray_d_0_s_5106_p_diag_45, int __f2dace_SA_ddt_ua_ray_d_1_s_5107_p_diag_45, int __f2dace_SA_ddt_va_adv_d_0_s_5055_p_diag_45, int __f2dace_SA_ddt_va_adv_d_1_s_5056_p_diag_45, int __f2dace_SA_ddt_va_cen_d_0_s_5091_p_diag_45, int __f2dace_SA_ddt_va_cen_d_1_s_5092_p_diag_45, int __f2dace_SA_ddt_va_cor_d_0_s_5064_p_diag_45, int __f2dace_SA_ddt_va_cor_d_1_s_5065_p_diag_45, int __f2dace_SA_ddt_va_dmp_d_0_s_5037_p_diag_45, int __f2dace_SA_ddt_va_dmp_d_1_s_5038_p_diag_45, int __f2dace_SA_ddt_va_dyn_d_0_s_5028_p_diag_45, int __f2dace_SA_ddt_va_dyn_d_1_s_5029_p_diag_45, int __f2dace_SA_ddt_va_grf_d_0_s_5118_p_diag_45, int __f2dace_SA_ddt_va_grf_d_1_s_5119_p_diag_45, int __f2dace_SA_ddt_va_hdf_d_0_s_5046_p_diag_45, int __f2dace_SA_ddt_va_hdf_d_1_s_5047_p_diag_45, int __f2dace_SA_ddt_va_iau_d_0_s_5100_p_diag_45, int __f2dace_SA_ddt_va_iau_d_1_s_5101_p_diag_45, int __f2dace_SA_ddt_va_pgr_d_0_s_5073_p_diag_45, int __f2dace_SA_ddt_va_pgr_d_1_s_5074_p_diag_45, int __f2dace_SA_ddt_va_phd_d_0_s_5082_p_diag_45, int __f2dace_SA_ddt_va_phd_d_1_s_5083_p_diag_45, int __f2dace_SA_ddt_va_ray_d_0_s_5109_p_diag_45, int __f2dace_SA_ddt_va_ray_d_1_s_5110_p_diag_45, int __f2dace_SA_ddt_vn_adv_d_0_s_5049_p_diag_45, int __f2dace_SA_ddt_vn_adv_d_1_s_5050_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_ddt_vn_cen_d_0_s_5085_p_diag_45, int __f2dace_SA_ddt_vn_cen_d_1_s_5086_p_diag_45, int __f2dace_SA_ddt_vn_cor_d_0_s_5058_p_diag_45, int __f2dace_SA_ddt_vn_cor_d_1_s_5059_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SA_ddt_vn_dmp_d_0_s_5031_p_diag_45, int __f2dace_SA_ddt_vn_dmp_d_1_s_5032_p_diag_45, int __f2dace_SA_ddt_vn_dyn_d_0_s_5022_p_diag_45, int __f2dace_SA_ddt_vn_dyn_d_1_s_5023_p_diag_45, int __f2dace_SA_ddt_vn_grf_d_0_s_5112_p_diag_45, int __f2dace_SA_ddt_vn_grf_d_1_s_5113_p_diag_45, int __f2dace_SA_ddt_vn_hdf_d_0_s_5040_p_diag_45, int __f2dace_SA_ddt_vn_hdf_d_1_s_5041_p_diag_45, int __f2dace_SA_ddt_vn_iau_d_0_s_5094_p_diag_45, int __f2dace_SA_ddt_vn_iau_d_1_s_5095_p_diag_45, int __f2dace_SA_ddt_vn_pgr_d_0_s_5067_p_diag_45, int __f2dace_SA_ddt_vn_pgr_d_1_s_5068_p_diag_45, int __f2dace_SA_ddt_vn_phd_d_0_s_5076_p_diag_45, int __f2dace_SA_ddt_vn_phd_d_1_s_5077_p_diag_45, int __f2dace_SA_ddt_vn_phy_d_0_s_4983_p_diag_45, int __f2dace_SA_ddt_vn_phy_d_1_s_4984_p_diag_45, int __f2dace_SA_ddt_vn_ray_d_0_s_5103_p_diag_45, int __f2dace_SA_ddt_vn_ray_d_1_s_5104_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_ddxn_z_full_c_d_0_s_5212_p_metrics_44, int __f2dace_SA_ddxn_z_full_c_d_1_s_5213_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SA_ddxn_z_full_v_d_0_s_5215_p_metrics_44, int __f2dace_SA_ddxn_z_full_v_d_1_s_5216_p_metrics_44, int __f2dace_SA_ddxn_z_half_c_d_0_s_5221_p_metrics_44, int __f2dace_SA_ddxn_z_half_c_d_1_s_5222_p_metrics_44, int __f2dace_SA_ddxn_z_half_e_d_0_s_5218_p_metrics_44, int __f2dace_SA_ddxn_z_half_e_d_1_s_5219_p_metrics_44, int __f2dace_SA_ddxt_z_full_c_d_0_s_5227_p_metrics_44, int __f2dace_SA_ddxt_z_full_c_d_1_s_5228_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SA_ddxt_z_full_v_d_0_s_5230_p_metrics_44, int __f2dace_SA_ddxt_z_full_v_d_1_s_5231_p_metrics_44, int __f2dace_SA_ddxt_z_half_c_d_0_s_5236_p_metrics_44, int __f2dace_SA_ddxt_z_half_c_d_1_s_5237_p_metrics_44, int __f2dace_SA_ddxt_z_half_e_d_0_s_5233_p_metrics_44, int __f2dace_SA_ddxt_z_half_e_d_1_s_5234_p_metrics_44, int __f2dace_SA_ddxt_z_half_v_d_0_s_5239_p_metrics_44, int __f2dace_SA_ddxt_z_half_v_d_1_s_5240_p_metrics_44, int __f2dace_SA_deepatmo_t1ifc_d_0_s_5364_p_metrics_44, int __f2dace_SA_deepatmo_t1mc_d_0_s_5362_p_metrics_44, int __f2dace_SA_deepatmo_t2mc_d_0_s_5366_p_metrics_44, int __f2dace_SA_dgeopot_mc_d_0_s_5159_p_metrics_44, int __f2dace_SA_dgeopot_mc_d_1_s_5160_p_metrics_44, int __f2dace_SA_div_d_0_s_4836_p_diag_45, int __f2dace_SA_div_d_1_s_4837_p_diag_45, int __f2dace_SA_div_ic_d_0_s_5010_p_diag_45, int __f2dace_SA_div_ic_d_1_s_5011_p_diag_45, int __f2dace_SA_dpres_mc_d_0_s_4825_p_diag_45, int __f2dace_SA_dpres_mc_d_1_s_4826_p_diag_45, int __f2dace_SA_dwdx_d_0_s_5016_p_diag_45, int __f2dace_SA_dwdx_d_1_s_5017_p_diag_45, int __f2dace_SA_dwdy_d_0_s_5019_p_diag_45, int __f2dace_SA_dwdy_d_1_s_5020_p_diag_45, int __f2dace_SA_dzgpot_mc_d_0_s_5359_p_metrics_44, int __f2dace_SA_dzgpot_mc_d_1_s_5360_p_metrics_44, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SA_end_blk_d_0_s_3150_cells_ptr_patch_4, int __f2dace_SA_end_blk_d_0_s_3272_edges_ptr_patch_15, int __f2dace_SA_end_blk_d_0_s_3319_verts_ptr_patch_25, int __f2dace_SA_end_blk_d_1_s_3151_cells_ptr_patch_4, int __f2dace_SA_end_blk_d_1_s_3273_edges_ptr_patch_15, int __f2dace_SA_end_blk_d_1_s_3320_verts_ptr_patch_25, int __f2dace_SA_end_block_d_0_s_3152_cells_ptr_patch_4, int __f2dace_SA_end_block_d_0_s_3274_edges_ptr_patch_15, int __f2dace_SA_end_block_d_0_s_3321_verts_ptr_patch_25, int __f2dace_SA_end_index_d_0_s_3146_cells_ptr_patch_4, int __f2dace_SA_end_index_d_0_s_3268_edges_ptr_patch_15, int __f2dace_SA_end_index_d_0_s_3315_verts_ptr_patch_25, int __f2dace_SA_exner_d_0_s_4764_p_prog_43, int __f2dace_SA_exner_d_1_s_4765_p_prog_43, int __f2dace_SA_exner_dyn_incr_d_0_s_4986_p_diag_45, int __f2dace_SA_exner_dyn_incr_d_1_s_4987_p_diag_45, int __f2dace_SA_exner_exfac_d_0_s_5286_p_metrics_44, int __f2dace_SA_exner_exfac_d_1_s_5287_p_metrics_44, int __f2dace_SA_exner_incr_d_0_s_4932_p_diag_45, int __f2dace_SA_exner_incr_d_1_s_4933_p_diag_45, int __f2dace_SA_exner_pr_d_0_s_4801_p_diag_45, int __f2dace_SA_exner_pr_d_1_s_4802_p_diag_45, int __f2dace_SA_exner_ref_mc_d_0_s_5300_p_metrics_44, int __f2dace_SA_exner_ref_mc_d_1_s_5301_p_metrics_44, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SA_geopot_agl_d_0_s_5153_p_metrics_44, int __f2dace_SA_geopot_agl_d_1_s_5154_p_metrics_44, int __f2dace_SA_geopot_agl_ifc_d_0_s_5156_p_metrics_44, int __f2dace_SA_geopot_agl_ifc_d_1_s_5157_p_metrics_44, int __f2dace_SA_geopot_d_0_s_5150_p_metrics_44, int __f2dace_SA_geopot_d_1_s_5151_p_metrics_44, int __f2dace_SA_grf_bdy_mflx_d_0_s_4866_p_diag_45, int __f2dace_SA_grf_bdy_mflx_d_1_s_4867_p_diag_45, int __f2dace_SA_grf_tend_mflx_d_0_s_4863_p_diag_45, int __f2dace_SA_grf_tend_mflx_d_1_s_4864_p_diag_45, int __f2dace_SA_grf_tend_rho_d_0_s_4860_p_diag_45, int __f2dace_SA_grf_tend_rho_d_1_s_4861_p_diag_45, int __f2dace_SA_grf_tend_thv_d_0_s_4869_p_diag_45, int __f2dace_SA_grf_tend_thv_d_1_s_4870_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_0_s_4872_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_1_s_4873_p_diag_45, int __f2dace_SA_grf_tend_tracer_d_2_s_4874_p_diag_45, int __f2dace_SA_grf_tend_vn_d_0_s_4854_p_diag_45, int __f2dace_SA_grf_tend_vn_d_1_s_4855_p_diag_45, int __f2dace_SA_grf_tend_w_d_0_s_4857_p_diag_45, int __f2dace_SA_grf_tend_w_d_1_s_4858_p_diag_45, int __f2dace_SA_hdef_ic_d_0_s_5013_p_diag_45, int __f2dace_SA_hdef_ic_d_1_s_5014_p_diag_45, int __f2dace_SA_hfl_tracer_d_0_s_4828_p_diag_45, int __f2dace_SA_hfl_tracer_d_1_s_4829_p_diag_45, int __f2dace_SA_hfl_tracer_d_2_s_4830_p_diag_45, int __f2dace_SA_hmask_dd3d_d_0_s_5166_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_d_0_s_5248_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_d_1_s_5249_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_e_d_0_s_5179_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_e_d_1_s_5180_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_v_d_0_s_5182_p_metrics_44, int __f2dace_SA_inv_ddqz_z_full_v_d_1_s_5183_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_d_0_s_5185_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_d_1_s_5186_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_e_d_0_s_5188_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_e_d_1_s_5189_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_v_d_0_s_5191_p_metrics_44, int __f2dace_SA_inv_ddqz_z_half_v_d_1_s_5192_p_metrics_44, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_mask_mtnpoints_d_0_s_5200_p_metrics_44, int __f2dace_SA_mask_mtnpoints_g_d_0_s_5202_p_metrics_44, int __f2dace_SA_mass_fl_e_d_0_s_4839_p_diag_45, int __f2dace_SA_mass_fl_e_d_1_s_4840_p_diag_45, int __f2dace_SA_mass_fl_e_sv_d_0_s_4995_p_diag_45, int __f2dace_SA_mass_fl_e_sv_d_1_s_4996_p_diag_45, int __f2dace_SA_mflx_ic_int_d_0_s_4900_p_diag_45, int __f2dace_SA_mflx_ic_int_d_1_s_4901_p_diag_45, int __f2dace_SA_mflx_ic_ubc_d_0_s_4903_p_diag_45, int __f2dace_SA_mflx_ic_ubc_d_1_s_4904_p_diag_45, int __f2dace_SA_mixing_length_sq_d_0_s_5197_p_metrics_44, int __f2dace_SA_mixing_length_sq_d_1_s_5198_p_metrics_44, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_0_s_3280_verts_ptr_patch_25, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3281_verts_ptr_patch_25, int __f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_2_s_3282_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3277_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3278_verts_ptr_patch_25, int __f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_2_s_3279_verts_ptr_patch_25, int __f2dace_SA_omega_d_0_s_4920_p_diag_45, int __f2dace_SA_omega_d_1_s_4921_p_diag_45, int __f2dace_SA_omega_z_d_0_s_4788_p_diag_45, int __f2dace_SA_omega_z_d_1_s_4789_p_diag_45, int __f2dace_SA_ovlp_halo_c_blk_d_0_s_5348_p_metrics_44, int __f2dace_SA_ovlp_halo_c_idx_d_0_s_5346_p_metrics_44, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SA_p_avginc_d_0_s_4914_p_diag_45, int __f2dace_SA_pres_d_0_s_4813_p_diag_45, int __f2dace_SA_pres_d_1_s_4814_p_diag_45, int __f2dace_SA_pres_ifc_d_0_s_4816_p_diag_45, int __f2dace_SA_pres_ifc_d_1_s_4817_p_diag_45, int __f2dace_SA_pres_msl_d_0_s_4918_p_diag_45, int __f2dace_SA_pres_sfc_d_0_s_4819_p_diag_45, int __f2dace_SA_pres_sfc_old_d_0_s_4821_p_diag_45, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SA_rh_avginc_d_0_s_4908_p_diag_45, int __f2dace_SA_rho_d_0_s_4761_p_prog_43, int __f2dace_SA_rho_d_1_s_4762_p_prog_43, int __f2dace_SA_rho_ic_d_0_s_4842_p_diag_45, int __f2dace_SA_rho_ic_d_1_s_4843_p_diag_45, int __f2dace_SA_rho_ic_int_d_0_s_4894_p_diag_45, int __f2dace_SA_rho_ic_int_d_1_s_4895_p_diag_45, int __f2dace_SA_rho_ic_ubc_d_0_s_4897_p_diag_45, int __f2dace_SA_rho_ic_ubc_d_1_s_4898_p_diag_45, int __f2dace_SA_rho_incr_d_0_s_4935_p_diag_45, int __f2dace_SA_rho_incr_d_1_s_4936_p_diag_45, int __f2dace_SA_rho_ref_corr_d_0_s_5318_p_metrics_44, int __f2dace_SA_rho_ref_corr_d_1_s_5319_p_metrics_44, int __f2dace_SA_rho_ref_mc_d_0_s_5303_p_metrics_44, int __f2dace_SA_rho_ref_mc_d_1_s_5304_p_metrics_44, int __f2dace_SA_rho_ref_me_d_0_s_5306_p_metrics_44, int __f2dace_SA_rho_ref_me_d_1_s_5307_p_metrics_44, int __f2dace_SA_rhoc_incr_d_0_s_4941_p_diag_45, int __f2dace_SA_rhoc_incr_d_1_s_4942_p_diag_45, int __f2dace_SA_rhog_incr_d_0_s_4953_p_diag_45, int __f2dace_SA_rhog_incr_d_1_s_4954_p_diag_45, int __f2dace_SA_rhoh_incr_d_0_s_4956_p_diag_45, int __f2dace_SA_rhoh_incr_d_1_s_4957_p_diag_45, int __f2dace_SA_rhoi_incr_d_0_s_4944_p_diag_45, int __f2dace_SA_rhoi_incr_d_1_s_4945_p_diag_45, int __f2dace_SA_rhonc_incr_d_0_s_4959_p_diag_45, int __f2dace_SA_rhonc_incr_d_1_s_4960_p_diag_45, int __f2dace_SA_rhong_incr_d_0_s_4971_p_diag_45, int __f2dace_SA_rhong_incr_d_1_s_4972_p_diag_45, int __f2dace_SA_rhonh_incr_d_0_s_4974_p_diag_45, int __f2dace_SA_rhonh_incr_d_1_s_4975_p_diag_45, int __f2dace_SA_rhoni_incr_d_0_s_4962_p_diag_45, int __f2dace_SA_rhoni_incr_d_1_s_4963_p_diag_45, int __f2dace_SA_rhonr_incr_d_0_s_4965_p_diag_45, int __f2dace_SA_rhonr_incr_d_1_s_4966_p_diag_45, int __f2dace_SA_rhons_incr_d_0_s_4968_p_diag_45, int __f2dace_SA_rhons_incr_d_1_s_4969_p_diag_45, int __f2dace_SA_rhor_incr_d_0_s_4947_p_diag_45, int __f2dace_SA_rhor_incr_d_1_s_4948_p_diag_45, int __f2dace_SA_rhos_incr_d_0_s_4950_p_diag_45, int __f2dace_SA_rhos_incr_d_1_s_4951_p_diag_45, int __f2dace_SA_rhov_incr_d_0_s_4938_p_diag_45, int __f2dace_SA_rhov_incr_d_1_s_4939_p_diag_45, int __f2dace_SA_slope_angle_d_0_s_5204_p_metrics_44, int __f2dace_SA_slope_azimuth_d_0_s_5206_p_metrics_44, int __f2dace_SA_start_blk_d_0_s_3147_cells_ptr_patch_4, int __f2dace_SA_start_blk_d_0_s_3269_edges_ptr_patch_15, int __f2dace_SA_start_blk_d_0_s_3316_verts_ptr_patch_25, int __f2dace_SA_start_blk_d_1_s_3148_cells_ptr_patch_4, int __f2dace_SA_start_blk_d_1_s_3270_edges_ptr_patch_15, int __f2dace_SA_start_blk_d_1_s_3317_verts_ptr_patch_25, int __f2dace_SA_start_block_d_0_s_3149_cells_ptr_patch_4, int __f2dace_SA_start_block_d_0_s_3271_edges_ptr_patch_15, int __f2dace_SA_start_block_d_0_s_3318_verts_ptr_patch_25, int __f2dace_SA_start_index_d_0_s_3143_cells_ptr_patch_4, int __f2dace_SA_start_index_d_0_s_3265_edges_ptr_patch_15, int __f2dace_SA_start_index_d_0_s_3312_verts_ptr_patch_25, int __f2dace_SA_t2m_bias_d_0_s_4906_p_diag_45, int __f2dace_SA_t_avginc_d_0_s_4910_p_diag_45, int __f2dace_SA_t_wgt_avginc_d_0_s_4912_p_diag_45, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_temp_d_0_s_4804_p_diag_45, int __f2dace_SA_temp_d_1_s_4805_p_diag_45, int __f2dace_SA_temp_ifc_d_0_s_4810_p_diag_45, int __f2dace_SA_temp_ifc_d_1_s_4811_p_diag_45, int __f2dace_SA_tempv_d_0_s_4807_p_diag_45, int __f2dace_SA_tempv_d_1_s_4808_p_diag_45, int __f2dace_SA_theta_ref_ic_d_0_s_5295_p_metrics_44, int __f2dace_SA_theta_ref_ic_d_1_s_5296_p_metrics_44, int __f2dace_SA_theta_ref_mc_d_0_s_5289_p_metrics_44, int __f2dace_SA_theta_ref_mc_d_1_s_5290_p_metrics_44, int __f2dace_SA_theta_ref_me_d_0_s_5292_p_metrics_44, int __f2dace_SA_theta_ref_me_d_1_s_5293_p_metrics_44, int __f2dace_SA_theta_v_d_0_s_4767_p_prog_43, int __f2dace_SA_theta_v_d_1_s_4768_p_prog_43, int __f2dace_SA_theta_v_ic_d_0_s_4845_p_diag_45, int __f2dace_SA_theta_v_ic_d_1_s_4846_p_diag_45, int __f2dace_SA_theta_v_ic_int_d_0_s_4888_p_diag_45, int __f2dace_SA_theta_v_ic_int_d_1_s_4889_p_diag_45, int __f2dace_SA_theta_v_ic_ubc_d_0_s_4891_p_diag_45, int __f2dace_SA_theta_v_ic_ubc_d_1_s_4892_p_diag_45, int __f2dace_SA_tke_d_0_s_4774_p_prog_43, int __f2dace_SA_tke_d_1_s_4775_p_prog_43, int __f2dace_SA_tracer_d_0_s_4770_p_prog_43, int __f2dace_SA_tracer_d_1_s_4771_p_prog_43, int __f2dace_SA_tracer_d_2_s_4772_p_prog_43, int __f2dace_SA_tracer_vi_d_0_s_4798_p_diag_45, int __f2dace_SA_tracer_vi_d_1_s_4799_p_diag_45, int __f2dace_SA_tsfc_ref_d_0_s_5298_p_metrics_44, int __f2dace_SA_u_d_0_s_4782_p_diag_45, int __f2dace_SA_u_d_1_s_4783_p_diag_45, int __f2dace_SA_v_d_0_s_4785_p_diag_45, int __f2dace_SA_v_d_1_s_4786_p_diag_45, int __f2dace_SA_vabs_avginc_d_0_s_4916_p_diag_45, int __f2dace_SA_vertex_blk_d_0_s_3123_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3124_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3125_cells_ptr_patch_4, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3120_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3121_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3122_cells_ptr_patch_4, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vertidx_gradp_d_0_s_5322_p_metrics_44, int __f2dace_SA_vertidx_gradp_d_1_s_5323_p_metrics_44, int __f2dace_SA_vertidx_gradp_d_2_s_5324_p_metrics_44, int __f2dace_SA_vfl_tracer_d_0_s_4832_p_diag_45, int __f2dace_SA_vfl_tracer_d_1_s_4833_p_diag_45, int __f2dace_SA_vfl_tracer_d_2_s_4834_p_diag_45, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vn_ie_int_d_0_s_4876_p_diag_45, int __f2dace_SA_vn_ie_int_d_1_s_4877_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SA_vn_incr_d_0_s_4929_p_diag_45, int __f2dace_SA_vn_incr_d_1_s_4930_p_diag_45, int __f2dace_SA_vor_d_0_s_4791_p_diag_45, int __f2dace_SA_vor_d_1_s_4792_p_diag_45, int __f2dace_SA_vor_u_d_0_s_4923_p_diag_45, int __f2dace_SA_vor_u_d_1_s_4924_p_diag_45, int __f2dace_SA_vor_v_d_0_s_4926_p_diag_45, int __f2dace_SA_vor_v_d_1_s_4927_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_vwind_expl_wgt_d_0_s_5168_p_metrics_44, int __f2dace_SA_vwind_impl_wgt_d_0_s_5170_p_metrics_44, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SA_w_int_d_0_s_4882_p_diag_45, int __f2dace_SA_w_int_d_1_s_4883_p_diag_45, int __f2dace_SA_w_ubc_d_0_s_4885_p_diag_45, int __f2dace_SA_w_ubc_d_1_s_4886_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SA_wgtfac_v_d_0_s_5194_p_metrics_44, int __f2dace_SA_wgtfac_v_d_1_s_5195_p_metrics_44, int __f2dace_SA_wgtfacq1_c_d_0_s_5263_p_metrics_44, int __f2dace_SA_wgtfacq1_c_d_1_s_5264_p_metrics_44, int __f2dace_SA_wgtfacq1_e_d_0_s_5266_p_metrics_44, int __f2dace_SA_wgtfacq1_e_d_1_s_5267_p_metrics_44, int __f2dace_SA_wgtfacq_c_d_0_s_5257_p_metrics_44, int __f2dace_SA_wgtfacq_c_d_1_s_5258_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SA_z_ifc_d_0_s_5141_p_metrics_44, int __f2dace_SA_z_ifc_d_1_s_5142_p_metrics_44, int __f2dace_SA_z_mc_d_0_s_5144_p_metrics_44, int __f2dace_SA_z_mc_d_1_s_5145_p_metrics_44, int __f2dace_SA_zd_blklist_d_0_s_5328_p_metrics_44, int __f2dace_SA_zd_e2cell_d_0_s_5176_p_metrics_44, int __f2dace_SA_zd_edgeblk_d_0_s_5332_p_metrics_44, int __f2dace_SA_zd_edgeidx_d_0_s_5330_p_metrics_44, int __f2dace_SA_zd_geofac_d_0_s_5174_p_metrics_44, int __f2dace_SA_zd_indlist_d_0_s_5326_p_metrics_44, int __f2dace_SA_zd_intcoef_d_0_s_5172_p_metrics_44, int __f2dace_SA_zd_vertidx_d_0_s_5334_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_0_s_5278_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_1_s_5279_p_metrics_44, int __f2dace_SA_zdiff_gradp_d_2_s_5280_p_metrics_44, int __f2dace_SA_zgpot_ifc_d_0_s_5353_p_metrics_44, int __f2dace_SA_zgpot_ifc_d_1_s_5354_p_metrics_44, int __f2dace_SA_zgpot_mc_d_0_s_5356_p_metrics_44, int __f2dace_SA_zgpot_mc_d_1_s_5357_p_metrics_44, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_end_blk_d_0_s_3319_verts_ptr_patch_25, int __f2dace_SOA_end_blk_d_1_s_3320_verts_ptr_patch_25, int __f2dace_SOA_end_block_d_0_s_3152_cells_ptr_patch_4, int __f2dace_SOA_end_block_d_0_s_3274_edges_ptr_patch_15, int __f2dace_SOA_end_block_d_0_s_3321_verts_ptr_patch_25, int __f2dace_SOA_end_index_d_0_s_3146_cells_ptr_patch_4, int __f2dace_SOA_end_index_d_0_s_3268_edges_ptr_patch_15, int __f2dace_SOA_end_index_d_0_s_3315_verts_ptr_patch_25, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_start_blk_d_0_s_3316_verts_ptr_patch_25, int __f2dace_SOA_start_blk_d_1_s_3317_verts_ptr_patch_25, int __f2dace_SOA_start_block_d_0_s_3149_cells_ptr_patch_4, int __f2dace_SOA_start_block_d_0_s_3271_edges_ptr_patch_15, int __f2dace_SOA_start_block_d_0_s_3318_verts_ptr_patch_25, int __f2dace_SOA_start_index_d_0_s_3143_cells_ptr_patch_4, int __f2dace_SOA_start_index_d_0_s_3265_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int lextra_diffu, int lvert_nest, int tmp_struct_symbol_0, int tmp_struct_symbol_1, int tmp_struct_symbol_10, int tmp_struct_symbol_11, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_15, int tmp_struct_symbol_16, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int tmp_struct_symbol_6, int tmp_struct_symbol_7, int tmp_struct_symbol_8, int tmp_struct_symbol_9) {
    int count;

    // Check that we are able to run cuda code
    if (cudaGetDeviceCount(&count) != cudaSuccess)
    {
        printf("ERROR: GPU drivers are not configured or cuda-capable device "
               "not found\n");
        return 1;
    }
    if (count == 0)
    {
        printf("ERROR: No cuda-capable devices found\n");
        return 2;
    }

    // Initialize cuda before we run the application
    float *dev_X;
    DACE_GPU_CHECK(cudaMalloc((void **) &dev_X, 1));
    DACE_GPU_CHECK(cudaFree(dev_X));

    

    __state->gpu_context = new dace::cuda::Context(13, 6);

    // Create cuda streams and events
    for(int i = 0; i < 13; ++i) {
        DACE_GPU_CHECK(cudaStreamCreateWithFlags(&__state->gpu_context->internal_streams[i], cudaStreamNonBlocking));
        __state->gpu_context->streams[i] = __state->gpu_context->internal_streams[i]; // Allow for externals to modify streams
    }
    for(int i = 0; i < 6; ++i) {
        DACE_GPU_CHECK(cudaEventCreateWithFlags(&__state->gpu_context->events[i], cudaEventDisableTiming));
    }

    

    return 0;
}

int __dace_exit_cuda(velocity_tendencies_state_t *__state) {
    

    // Synchronize and check for CUDA errors
    int __err = static_cast<int>(__state->gpu_context->lasterror);
    if (__err == 0)
        __err = static_cast<int>(cudaDeviceSynchronize());

    // Destroy cuda streams and events
    for(int i = 0; i < 13; ++i) {
        DACE_GPU_CHECK(cudaStreamDestroy(__state->gpu_context->internal_streams[i]));
    }
    for(int i = 0; i < 6; ++i) {
        DACE_GPU_CHECK(cudaEventDestroy(__state->gpu_context->events[i]));
    }

    delete __state->gpu_context;
    return __err;
}

DACE_EXPORTED bool __dace_gpu_set_stream(velocity_tendencies_state_t *__state, int streamid, gpuStream_t stream)
{
    if (streamid < 0 || streamid >= 13)
        return false;

    __state->gpu_context->streams[streamid] = stream;

    return true;
}

DACE_EXPORTED void __dace_gpu_set_all_streams(velocity_tendencies_state_t *__state, gpuStream_t stream)
{
    for (int i = 0; i < 13; ++i)
        __state->gpu_context->streams[i] = stream;
}

__global__ void __launch_bounds__(32) single_state_body_map_27_6_2(int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_blk, int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_idx, double * __restrict__ __27_gpu_p_int_cells_aw_verts_1, double * __restrict__ __27_gpu_p_prog_w_0, double * __restrict__ __27_gpu_z_w_v, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_46_0, long long i_endidx_7, int tmp_struct_symbol_6, int tmp_struct_symbol_7) {
    {
        int _for_it_47_0 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_47_0 >= 1 && _for_it_47_0 < (i_endidx_7 + 1)) {
            loop_body_27_6_0(&__27_gpu_flat_flat_ptr_patch_verts_cell_blk[0], &__27_gpu_flat_flat_ptr_patch_verts_cell_idx[0], &__27_gpu_p_int_cells_aw_verts_1[0], &__27_gpu_p_prog_w_0[0], &__27_gpu_z_w_v[0], __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_46_0, _for_it_47_0, tmp_struct_symbol_6);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_map_27_6_2(velocity_tendencies_state_t *__state, int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_blk, int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_idx, double * __restrict__ __27_gpu_p_int_cells_aw_verts_1, double * __restrict__ __27_gpu_p_prog_w_0, double * __restrict__ __27_gpu_z_w_v, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_46_0, long long i_endidx_7, int tmp_struct_symbol_6, int tmp_struct_symbol_7);
void __dace_runkernel_single_state_body_map_27_6_2(velocity_tendencies_state_t *__state, int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_blk, int * __restrict__ __27_gpu_flat_flat_ptr_patch_verts_cell_idx, double * __restrict__ __27_gpu_p_int_cells_aw_verts_1, double * __restrict__ __27_gpu_p_prog_w_0, double * __restrict__ __27_gpu_z_w_v, int __f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, int __f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, int __f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, int __f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, int __f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, int __f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_46_0, long long i_endidx_7, int tmp_struct_symbol_6, int tmp_struct_symbol_7)
{

    if ((int_ceil(int_ceil(i_endidx_7, 1), 32)) == 0 || (int_ceil(__f2dace_SA_w_d_1_s_4756_p_prog_43, 1)) == 0) {

        return;
    }

    void  *single_state_body_map_27_6_2_args[] = { (void *)&__27_gpu_flat_flat_ptr_patch_verts_cell_blk, (void *)&__27_gpu_flat_flat_ptr_patch_verts_cell_idx, (void *)&__27_gpu_p_int_cells_aw_verts_1, (void *)&__27_gpu_p_prog_w_0, (void *)&__27_gpu_z_w_v, (void *)&__f2dace_SA_cell_blk_d_0_s_3286_verts_ptr_patch_25, (void *)&__f2dace_SA_cell_blk_d_1_s_3287_verts_ptr_patch_25, (void *)&__f2dace_SA_cell_blk_d_2_s_3288_verts_ptr_patch_25, (void *)&__f2dace_SA_cell_idx_d_0_s_3283_verts_ptr_patch_25, (void *)&__f2dace_SA_cell_idx_d_1_s_3284_verts_ptr_patch_25, (void *)&__f2dace_SA_cell_idx_d_2_s_3285_verts_ptr_patch_25, (void *)&__f2dace_SA_cells_aw_verts_d_0_s_3990_p_int_38, (void *)&__f2dace_SA_cells_aw_verts_d_1_s_3991_p_int_38, (void *)&__f2dace_SA_cells_aw_verts_d_2_s_3992_p_int_38, (void *)&__f2dace_SA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SA_w_d_2_s_4757_p_prog_43, (void *)&__f2dace_SOA_cell_blk_d_0_s_3286_verts_ptr_patch_25, (void *)&__f2dace_SOA_cell_blk_d_1_s_3287_verts_ptr_patch_25, (void *)&__f2dace_SOA_cell_blk_d_2_s_3288_verts_ptr_patch_25, (void *)&__f2dace_SOA_cell_idx_d_0_s_3283_verts_ptr_patch_25, (void *)&__f2dace_SOA_cell_idx_d_1_s_3284_verts_ptr_patch_25, (void *)&__f2dace_SOA_cell_idx_d_2_s_3285_verts_ptr_patch_25, (void *)&__f2dace_SOA_cells_aw_verts_d_0_s_3990_p_int_38, (void *)&__f2dace_SOA_cells_aw_verts_d_1_s_3991_p_int_38, (void *)&__f2dace_SOA_cells_aw_verts_d_2_s_3992_p_int_38, (void *)&__f2dace_SOA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SOA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SOA_w_d_2_s_4757_p_prog_43, (void *)&_for_it_46_0, (void *)&i_endidx_7, (void *)&tmp_struct_symbol_6, (void *)&tmp_struct_symbol_7 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_map_27_6_2, dim3(int_ceil(int_ceil(i_endidx_7, 1), 32), int_ceil(__f2dace_SA_w_d_1_s_4756_p_prog_43, 1), 1), dim3(32, 1, 1), single_state_body_map_27_6_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_map_27_6_2", int_ceil(int_ceil(i_endidx_7, 1), 32), int_ceil(__f2dace_SA_w_d_1_s_4756_p_prog_43, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_0_map_24_6_2(int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_blk, int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_idx, double * __restrict__ __24_gpu_p_prog_vn_2, double * __restrict__ __24_gpu_v_ptr_int_0_geofac_rot, double * __restrict__ __24_gpu_zeta, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_49_0, long long i_endidx_6, int tmp_struct_symbol_8, int tmp_struct_symbol_9) {
    {
        int _for_it_50_0 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_50_0 >= 1 && _for_it_50_0 < (i_endidx_6 + 1)) {
            loop_body_24_6_0(&__24_gpu_flat_flat_ptr_patch_verts_edge_blk[0], &__24_gpu_flat_flat_ptr_patch_verts_edge_idx[0], &__24_gpu_p_prog_vn_2[0], &__24_gpu_v_ptr_int_0_geofac_rot[0], &__24_gpu_zeta[0], __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, _for_it_49_0, _for_it_50_0, tmp_struct_symbol_8);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_0_map_24_6_2(velocity_tendencies_state_t *__state, int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_blk, int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_idx, double * __restrict__ __24_gpu_p_prog_vn_2, double * __restrict__ __24_gpu_v_ptr_int_0_geofac_rot, double * __restrict__ __24_gpu_zeta, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_49_0, long long i_endidx_6, int tmp_struct_symbol_8, int tmp_struct_symbol_9);
void __dace_runkernel_single_state_body_0_map_24_6_2(velocity_tendencies_state_t *__state, int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_blk, int * __restrict__ __24_gpu_flat_flat_ptr_patch_verts_edge_idx, double * __restrict__ __24_gpu_p_prog_vn_2, double * __restrict__ __24_gpu_v_ptr_int_0_geofac_rot, double * __restrict__ __24_gpu_zeta, int __f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, int __f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, int __f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, int __f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, int __f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, int __f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_49_0, long long i_endidx_6, int tmp_struct_symbol_8, int tmp_struct_symbol_9)
{

    if ((int_ceil(int_ceil(i_endidx_6, 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_0_map_24_6_2_args[] = { (void *)&__24_gpu_flat_flat_ptr_patch_verts_edge_blk, (void *)&__24_gpu_flat_flat_ptr_patch_verts_edge_idx, (void *)&__24_gpu_p_prog_vn_2, (void *)&__24_gpu_v_ptr_int_0_geofac_rot, (void *)&__24_gpu_zeta, (void *)&__f2dace_SA_edge_blk_d_0_s_3292_verts_ptr_patch_25, (void *)&__f2dace_SA_edge_blk_d_1_s_3293_verts_ptr_patch_25, (void *)&__f2dace_SA_edge_blk_d_2_s_3294_verts_ptr_patch_25, (void *)&__f2dace_SA_edge_idx_d_0_s_3289_verts_ptr_patch_25, (void *)&__f2dace_SA_edge_idx_d_1_s_3290_verts_ptr_patch_25, (void *)&__f2dace_SA_edge_idx_d_2_s_3291_verts_ptr_patch_25, (void *)&__f2dace_SA_geofac_rot_d_0_s_4047_p_int_38, (void *)&__f2dace_SA_geofac_rot_d_1_s_4048_p_int_38, (void *)&__f2dace_SA_geofac_rot_d_2_s_4049_p_int_38, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_edge_blk_d_0_s_3292_verts_ptr_patch_25, (void *)&__f2dace_SOA_edge_blk_d_1_s_3293_verts_ptr_patch_25, (void *)&__f2dace_SOA_edge_blk_d_2_s_3294_verts_ptr_patch_25, (void *)&__f2dace_SOA_edge_idx_d_0_s_3289_verts_ptr_patch_25, (void *)&__f2dace_SOA_edge_idx_d_1_s_3290_verts_ptr_patch_25, (void *)&__f2dace_SOA_edge_idx_d_2_s_3291_verts_ptr_patch_25, (void *)&__f2dace_SOA_geofac_rot_d_0_s_4047_p_int_38, (void *)&__f2dace_SOA_geofac_rot_d_1_s_4048_p_int_38, (void *)&__f2dace_SOA_geofac_rot_d_2_s_4049_p_int_38, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&_for_it_49_0, (void *)&i_endidx_6, (void *)&tmp_struct_symbol_8, (void *)&tmp_struct_symbol_9 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_0_map_24_6_2, dim3(int_ceil(int_ceil(i_endidx_6, 1), 32), 0, 1), dim3(32, 1, 1), single_state_body_0_map_24_6_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_0_map_24_6_2", int_ceil(int_ceil(i_endidx_6, 1), 32), 0, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_2_map_40_4_5(double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev) {
    {
        int _for_it_3 = ((blockIdx.x * 32 + threadIdx.x) + 2);
        if (_for_it_3 >= 2 && _for_it_3 < (nlev + 1)) {
            loop_body_40_4_3(&__40_gpu_v_p_diag_vt[0], &__40_gpu_v_p_metrics_wgtfac_e[0], &__40_gpu_v_p_prog_vn[0], &__40_gpu_v_p_diag_vn_ie[0], &__40_gpu_z_kin_hor_e[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, _for_it_0, _for_it_3, i_endidx, i_startidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_2_map_40_4_5(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev);
void __dace_runkernel_single_state_body_2_map_40_4_5(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev)
{

    if ((int_ceil(int_ceil((nlev - 1), 1), 32)) == 0 || (int_ceil(((i_endidx - i_startidx) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_2_map_40_4_5_args[] = { (void *)&__40_gpu_v_p_diag_vn_ie, (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_metrics_wgtfac_e, (void *)&__40_gpu_v_p_prog_vn, (void *)&__40_gpu_z_kin_hor_e, (void *)&__f2dace_A_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_A_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_A_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_OA_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_OA_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_OA_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, (void *)&__f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, (void *)&__f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, (void *)&_for_it_0, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_2_map_40_4_5, dim3(int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1), dim3(32, 1, 1), single_state_body_2_map_40_4_5_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_2_map_40_4_5", int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_map_40_4_1(int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_blk, int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_idx, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_int_rbf_vec_coeff_e, double * __restrict__ __40_gpu_v_p_prog_vn, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev) {
    {
        int _for_it_1 = ((blockIdx.x * 32 + threadIdx.x) + i_startidx);
        if (_for_it_1 >= i_startidx && _for_it_1 < (i_endidx + 1)) {
            loop_body_40_4_0(&__40_gpu_flat_flat_ptr_patch_edges_quad_blk[0], &__40_gpu_flat_flat_ptr_patch_edges_quad_idx[0], &__40_gpu_v_p_int_rbf_vec_coeff_e[0], &__40_gpu_v_p_prog_vn[0], &__40_gpu_v_p_diag_vt[0], __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_0, _for_it_1, nlev);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_map_40_4_1(velocity_tendencies_state_t *__state, int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_blk, int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_idx, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_int_rbf_vec_coeff_e, double * __restrict__ __40_gpu_v_p_prog_vn, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev);
void __dace_runkernel_single_state_body_map_40_4_1(velocity_tendencies_state_t *__state, int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_blk, int * __restrict__ __40_gpu_flat_flat_ptr_patch_edges_quad_idx, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_int_rbf_vec_coeff_e, double * __restrict__ __40_gpu_v_p_prog_vn, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, int __f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev)
{

    if ((int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32)) == 0 || (int_ceil(nlev, 1)) == 0) {

        return;
    }

    void  *single_state_body_map_40_4_1_args[] = { (void *)&__40_gpu_flat_flat_ptr_patch_edges_quad_blk, (void *)&__40_gpu_flat_flat_ptr_patch_edges_quad_idx, (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_int_rbf_vec_coeff_e, (void *)&__40_gpu_v_p_prog_vn, (void *)&__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, (void *)&__f2dace_SA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, (void *)&__f2dace_SA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, (void *)&__f2dace_SA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, (void *)&__f2dace_SOA_rbf_vec_coeff_e_d_0_s_4035_p_int_38, (void *)&__f2dace_SOA_rbf_vec_coeff_e_d_1_s_4036_p_int_38, (void *)&__f2dace_SOA_rbf_vec_coeff_e_d_2_s_4037_p_int_38, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&_for_it_0, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_map_40_4_1, dim3(int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1), dim3(32, 1, 1), single_state_body_map_40_4_1_args, 0, __state->gpu_context->streams[1]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_map_40_4_1", int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_3_map_40_5_2(double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev) {
    {
        int _for_it_5 = ((blockIdx.x * 32 + threadIdx.x) + 2);
        if (_for_it_5 >= 2 && _for_it_5 < (nlev + 1)) {
            loop_body_40_5_0(&__40_gpu_v_p_diag_vt[0], &__40_gpu_v_p_metrics_wgtfac_e[0], &__40_gpu_z_vt_ie[0], __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, _for_it_0, _for_it_5, i_endidx, i_startidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_3_map_40_5_2(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev);
void __dace_runkernel_single_state_body_3_map_40_5_2(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfac_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, int __f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev)
{

    if ((int_ceil(int_ceil((nlev - 1), 1), 32)) == 0 || (int_ceil(((i_endidx - i_startidx) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_3_map_40_5_2_args[] = { (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_metrics_wgtfac_e, (void *)&__40_gpu_z_vt_ie, (void *)&__f2dace_A_z_vt_ie_d_0_s_2774, (void *)&__f2dace_A_z_vt_ie_d_1_s_2775, (void *)&__f2dace_A_z_vt_ie_d_2_s_2776, (void *)&__f2dace_OA_z_vt_ie_d_0_s_2774, (void *)&__f2dace_OA_z_vt_ie_d_1_s_2775, (void *)&__f2dace_OA_z_vt_ie_d_2_s_2776, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SA_wgtfac_e_d_0_s_5254_p_metrics_44, (void *)&__f2dace_SA_wgtfac_e_d_1_s_5255_p_metrics_44, (void *)&__f2dace_SA_wgtfac_e_d_2_s_5256_p_metrics_44, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_wgtfac_e_d_0_s_5254_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_e_d_1_s_5255_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_e_d_2_s_5256_p_metrics_44, (void *)&_for_it_0, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_3_map_40_5_2, dim3(int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1), dim3(32, 1, 1), single_state_body_3_map_40_5_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_3_map_40_5_2", int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_4_map_40_7_2(double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_ddxn_z_full, double * __restrict__ __40_gpu_v_p_metrics_ddxt_z_full, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev, const int nflatlev_jg) {
    {
        int nflatlev_jg = nflatlev_jg;
        int _for_it_7 = ((blockIdx.x * 32 + threadIdx.x) + nflatlev_jg);
        if (_for_it_7 >= nflatlev_jg && _for_it_7 < (nlev + 1)) {
            loop_body_40_7_0(&__40_gpu_v_p_diag_vt[0], &__40_gpu_v_p_metrics_ddxn_z_full[0], &__40_gpu_v_p_metrics_ddxt_z_full[0], &__40_gpu_v_p_prog_vn[0], &__40_gpu_z_w_concorr_me[0], __f2dace_A_z_w_concorr_me_d_0_s_2768, __f2dace_A_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_0_s_2768, __f2dace_OA_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_2_s_2770, __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_0, _for_it_7, i_endidx, i_startidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_4_map_40_7_2(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_ddxn_z_full, double * __restrict__ __40_gpu_v_p_metrics_ddxt_z_full, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev, const int nflatlev_jg);
void __dace_runkernel_single_state_body_4_map_40_7_2(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_ddxn_z_full, double * __restrict__ __40_gpu_v_p_metrics_ddxt_z_full, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, int __f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, int __f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_0, int i_endidx, long long i_startidx, int nlev, const int nflatlev_jg)
{
    int nflatlev_jg = nflatlev_jg;

    if ((int_ceil(int_ceil((((- nflatlev_jg) + nlev) + 1), 1), 32)) == 0 || (int_ceil(((i_endidx - i_startidx) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_4_map_40_7_2_args[] = { (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_metrics_ddxn_z_full, (void *)&__40_gpu_v_p_metrics_ddxt_z_full, (void *)&__40_gpu_v_p_prog_vn, (void *)&__40_gpu_z_w_concorr_me, (void *)&__f2dace_A_z_w_concorr_me_d_0_s_2768, (void *)&__f2dace_A_z_w_concorr_me_d_1_s_2769, (void *)&__f2dace_A_z_w_concorr_me_d_2_s_2770, (void *)&__f2dace_OA_z_w_concorr_me_d_0_s_2768, (void *)&__f2dace_OA_z_w_concorr_me_d_1_s_2769, (void *)&__f2dace_OA_z_w_concorr_me_d_2_s_2770, (void *)&__f2dace_SA_ddxn_z_full_d_0_s_5209_p_metrics_44, (void *)&__f2dace_SA_ddxn_z_full_d_1_s_5210_p_metrics_44, (void *)&__f2dace_SA_ddxn_z_full_d_2_s_5211_p_metrics_44, (void *)&__f2dace_SA_ddxt_z_full_d_0_s_5224_p_metrics_44, (void *)&__f2dace_SA_ddxt_z_full_d_1_s_5225_p_metrics_44, (void *)&__f2dace_SA_ddxt_z_full_d_2_s_5226_p_metrics_44, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_ddxn_z_full_d_0_s_5209_p_metrics_44, (void *)&__f2dace_SOA_ddxn_z_full_d_1_s_5210_p_metrics_44, (void *)&__f2dace_SOA_ddxn_z_full_d_2_s_5211_p_metrics_44, (void *)&__f2dace_SOA_ddxt_z_full_d_0_s_5224_p_metrics_44, (void *)&__f2dace_SOA_ddxt_z_full_d_1_s_5225_p_metrics_44, (void *)&__f2dace_SOA_ddxt_z_full_d_2_s_5226_p_metrics_44, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&_for_it_0, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&nflatlev_jg };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_4_map_40_7_2, dim3(int_ceil(int_ceil((((- nflatlev_jg) + nlev) + 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1), dim3(32, 1, 1), single_state_body_4_map_40_7_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_4_map_40_7_2", int_ceil(int_ceil((((- nflatlev_jg) + nlev) + 1), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_0_map_40_6_4(double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vn_ie_ubc, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, const double dt_linintp_ubc, int i_endidx, long long i_startidx, int nlev, int nlevp1) {
    {
        int _for_it_10 = ((blockIdx.x * 32 + threadIdx.x) + i_startidx);
        if (_for_it_10 >= i_startidx && _for_it_10 < (i_endidx + 1)) {
            loop_body_40_6_0(dt_linintp_ubc, &__40_gpu_v_p_diag_vn_ie_ubc[0], &__40_gpu_v_p_diag_vt[0], &__40_gpu_v_p_metrics_wgtfacq_e[0], &__40_gpu_v_p_prog_vn[0], &__40_gpu_v_p_diag_vn_ie[0], &__40_gpu_z_kin_hor_e[0], &__40_gpu_z_vt_ie[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, _for_it_0, _for_it_10, nlev, nlevp1);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_0_map_40_6_4(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vn_ie_ubc, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, const double dt_linintp_ubc, int i_endidx, long long i_startidx, int nlev, int nlevp1);
void __dace_runkernel_single_state_body_0_map_40_6_4(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vn_ie_ubc, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, int __f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, const double dt_linintp_ubc, int i_endidx, long long i_startidx, int nlev, int nlevp1)
{

    if ((int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_0_map_40_6_4_args[] = { (void *)&__40_gpu_v_p_diag_vn_ie, (void *)&__40_gpu_v_p_diag_vn_ie_ubc, (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_metrics_wgtfacq_e, (void *)&__40_gpu_v_p_prog_vn, (void *)&__40_gpu_z_kin_hor_e, (void *)&__40_gpu_z_vt_ie, (void *)&__f2dace_A_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_A_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_A_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_A_z_vt_ie_d_0_s_2774, (void *)&__f2dace_A_z_vt_ie_d_1_s_2775, (void *)&__f2dace_A_z_vt_ie_d_2_s_2776, (void *)&__f2dace_OA_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_OA_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_OA_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_OA_z_vt_ie_d_0_s_2774, (void *)&__f2dace_OA_z_vt_ie_d_1_s_2775, (void *)&__f2dace_OA_z_vt_ie_d_2_s_2776, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SA_vn_ie_ubc_d_0_s_4879_p_diag_45, (void *)&__f2dace_SA_vn_ie_ubc_d_1_s_4880_p_diag_45, (void *)&__f2dace_SA_vn_ie_ubc_d_2_s_4881_p_diag_45, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, (void *)&__f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, (void *)&__f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SOA_vn_ie_ubc_d_0_s_4879_p_diag_45, (void *)&__f2dace_SOA_vn_ie_ubc_d_1_s_4880_p_diag_45, (void *)&__f2dace_SOA_vn_ie_ubc_d_2_s_4881_p_diag_45, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, (void *)&__f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, (void *)&__f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, (void *)&_for_it_0, (void *)&dt_linintp_ubc, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&nlevp1 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_0_map_40_6_4, dim3(int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), 1, 1), dim3(32, 1, 1), single_state_body_0_map_40_6_4_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_0_map_40_6_4", int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_1_map_40_8_3(double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev, int nlevp1) {
    {
        int _for_it_9 = ((blockIdx.x * 32 + threadIdx.x) + i_startidx);
        if (_for_it_9 >= i_startidx && _for_it_9 < (i_endidx + 1)) {
            loop_body_40_8_0(&__40_gpu_v_p_diag_vt[0], &__40_gpu_v_p_metrics_wgtfacq_e[0], &__40_gpu_v_p_prog_vn[0], &__40_gpu_v_p_diag_vn_ie[0], &__40_gpu_z_kin_hor_e[0], &__40_gpu_z_vt_ie[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, _for_it_0, _for_it_9, nlev, nlevp1);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_1_map_40_8_3(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev, int nlevp1);
void __dace_runkernel_single_state_body_1_map_40_8_3(velocity_tendencies_state_t *__state, double * __restrict__ __40_gpu_v_p_diag_vn_ie, double * __restrict__ __40_gpu_v_p_diag_vt, double * __restrict__ __40_gpu_v_p_metrics_wgtfacq_e, double * __restrict__ __40_gpu_v_p_prog_vn, double * __restrict__ __40_gpu_z_kin_hor_e, double * __restrict__ __40_gpu_z_vt_ie, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, int __f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, int _for_it_0, int i_endidx, long long i_startidx, int nlev, int nlevp1)
{

    if ((int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_1_map_40_8_3_args[] = { (void *)&__40_gpu_v_p_diag_vn_ie, (void *)&__40_gpu_v_p_diag_vt, (void *)&__40_gpu_v_p_metrics_wgtfacq_e, (void *)&__40_gpu_v_p_prog_vn, (void *)&__40_gpu_z_kin_hor_e, (void *)&__40_gpu_z_vt_ie, (void *)&__f2dace_A_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_A_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_A_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_A_z_vt_ie_d_0_s_2774, (void *)&__f2dace_A_z_vt_ie_d_1_s_2775, (void *)&__f2dace_A_z_vt_ie_d_2_s_2776, (void *)&__f2dace_OA_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_OA_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_OA_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_OA_z_vt_ie_d_0_s_2774, (void *)&__f2dace_OA_z_vt_ie_d_1_s_2775, (void *)&__f2dace_OA_z_vt_ie_d_2_s_2776, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SA_wgtfacq_e_d_0_s_5260_p_metrics_44, (void *)&__f2dace_SA_wgtfacq_e_d_1_s_5261_p_metrics_44, (void *)&__f2dace_SA_wgtfacq_e_d_2_s_5262_p_metrics_44, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_wgtfacq_e_d_0_s_5260_p_metrics_44, (void *)&__f2dace_SOA_wgtfacq_e_d_1_s_5261_p_metrics_44, (void *)&__f2dace_SOA_wgtfacq_e_d_2_s_5262_p_metrics_44, (void *)&_for_it_0, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&nlevp1 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_1_map_40_8_3, dim3(int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), 1, 1), dim3(32, 1, 1), single_state_body_1_map_40_8_3_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_1_map_40_8_3", int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_map_37_3_4(int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __37_gpu_v_p_diag_vn_ie, double * __restrict__ __37_gpu_v_p_prog_w, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __37_gpu_z_v_grad_w, double * __restrict__ __37_gpu_z_vt_ie, double * __restrict__ __37_gpu_z_w_v, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_11, int i_endidx, long long i_startidx, int nlev, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int tmp_struct_symbol_6, int tmp_struct_symbol_7) {
    {
        int _for_it_12 = ((blockIdx.x * 32 + threadIdx.x) + i_startidx);
        if (_for_it_12 >= i_startidx && _for_it_12 < (i_endidx + 1)) {
            loop_body_37_3_0(&__37_gpu_flat_v_ptr_patch_edges_cell_blk[0], &__37_gpu_flat_v_ptr_patch_edges_cell_idx[0], &__37_gpu_flat_v_ptr_patch_edges_vertex_blk[0], &__37_gpu_flat_v_ptr_patch_edges_vertex_idx[0], &__37_gpu_v_p_diag_vn_ie[0], &__37_gpu_v_p_prog_w[0], &__37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length[0], &__37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length[0], &__37_gpu_v_v_ptr_patch_edges_tangent_orientation[0], &__37_gpu_z_vt_ie[0], &__37_gpu_z_w_v[0], &__37_gpu_z_v_grad_w[0], __f2dace_A_z_vt_ie_d_0_s_2774, __f2dace_A_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_0_s_2774, __f2dace_OA_z_vt_ie_d_1_s_2775, __f2dace_OA_z_vt_ie_d_2_s_2776, __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_11, _for_it_12, nlev, tmp_struct_symbol_4, tmp_struct_symbol_6);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_map_37_3_4(velocity_tendencies_state_t *__state, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __37_gpu_v_p_diag_vn_ie, double * __restrict__ __37_gpu_v_p_prog_w, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __37_gpu_z_v_grad_w, double * __restrict__ __37_gpu_z_vt_ie, double * __restrict__ __37_gpu_z_w_v, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_11, int i_endidx, long long i_startidx, int nlev, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int tmp_struct_symbol_6, int tmp_struct_symbol_7);
void __dace_runkernel_single_state_body_map_37_3_4(velocity_tendencies_state_t *__state, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __37_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __37_gpu_v_p_diag_vn_ie, double * __restrict__ __37_gpu_v_p_prog_w, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __37_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __37_gpu_z_v_grad_w, double * __restrict__ __37_gpu_z_vt_ie, double * __restrict__ __37_gpu_z_w_v, int __f2dace_A_z_vt_ie_d_0_s_2774, int __f2dace_A_z_vt_ie_d_1_s_2775, int __f2dace_A_z_vt_ie_d_2_s_2776, int __f2dace_OA_z_vt_ie_d_0_s_2774, int __f2dace_OA_z_vt_ie_d_1_s_2775, int __f2dace_OA_z_vt_ie_d_2_s_2776, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, int __f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_11, int i_endidx, long long i_startidx, int nlev, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int tmp_struct_symbol_6, int tmp_struct_symbol_7)
{

    if ((int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32)) == 0 || (int_ceil(nlev, 1)) == 0) {

        return;
    }

    void  *single_state_body_map_37_3_4_args[] = { (void *)&__37_gpu_flat_v_ptr_patch_edges_cell_blk, (void *)&__37_gpu_flat_v_ptr_patch_edges_cell_idx, (void *)&__37_gpu_flat_v_ptr_patch_edges_vertex_blk, (void *)&__37_gpu_flat_v_ptr_patch_edges_vertex_idx, (void *)&__37_gpu_v_p_diag_vn_ie, (void *)&__37_gpu_v_p_prog_w, (void *)&__37_gpu_v_v_ptr_patch_edges_inv_dual_edge_length, (void *)&__37_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, (void *)&__37_gpu_v_v_ptr_patch_edges_tangent_orientation, (void *)&__37_gpu_z_v_grad_w, (void *)&__37_gpu_z_vt_ie, (void *)&__37_gpu_z_w_v, (void *)&__f2dace_A_z_vt_ie_d_0_s_2774, (void *)&__f2dace_A_z_vt_ie_d_1_s_2775, (void *)&__f2dace_A_z_vt_ie_d_2_s_2776, (void *)&__f2dace_OA_z_vt_ie_d_0_s_2774, (void *)&__f2dace_OA_z_vt_ie_d_1_s_2775, (void *)&__f2dace_OA_z_vt_ie_d_2_s_2776, (void *)&__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, (void *)&__f2dace_SA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, (void *)&__f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, (void *)&__f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, (void *)&__f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, (void *)&__f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SA_w_d_2_s_4757_p_prog_43, (void *)&__f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SOA_inv_dual_edge_length_d_0_s_3235_edges_ptr_patch_15, (void *)&__f2dace_SOA_inv_dual_edge_length_d_1_s_3236_edges_ptr_patch_15, (void *)&__f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, (void *)&__f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, (void *)&__f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, (void *)&__f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SOA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SOA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SOA_w_d_2_s_4757_p_prog_43, (void *)&_for_it_11, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&tmp_struct_symbol_4, (void *)&tmp_struct_symbol_5, (void *)&tmp_struct_symbol_6, (void *)&tmp_struct_symbol_7 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_map_37_3_4, dim3(int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1), dim3(32, 1, 1), single_state_body_map_37_3_4_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_map_37_3_4", int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_5_map_1_18_3(int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_ekinh, double * __restrict__ __1_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_10, int tmp_struct_symbol_11) {
    {
        int _for_it_15 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_15 >= 1 && _for_it_15 < (i_endidx + 1)) {
            loop_body_1_18_0(&__1_gpu_flat_flat_ptr_patch_cells_edge_blk[0], &__1_gpu_flat_flat_ptr_patch_cells_edge_idx[0], &__1_gpu_v_p_int_e_bln_c_s[0], &__1_gpu_z_kin_hor_e[0], &__1_gpu_z_ekinh[0], __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, _for_it_14, _for_it_15, nlev, tmp_struct_symbol_10);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_5_map_1_18_3(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_ekinh, double * __restrict__ __1_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_10, int tmp_struct_symbol_11);
void __dace_runkernel_single_state_body_5_map_1_18_3(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_ekinh, double * __restrict__ __1_gpu_z_kin_hor_e, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_10, int tmp_struct_symbol_11)
{

    if ((int_ceil(int_ceil(i_endidx, 1), 32)) == 0 || (int_ceil(nlev, 1)) == 0) {

        return;
    }

    void  *single_state_body_5_map_1_18_3_args[] = { (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_blk, (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_idx, (void *)&__1_gpu_v_p_int_e_bln_c_s, (void *)&__1_gpu_z_ekinh, (void *)&__1_gpu_z_kin_hor_e, (void *)&__f2dace_A_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_A_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_A_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_OA_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_OA_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_OA_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&__f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nlev, (void *)&tmp_struct_symbol_10, (void *)&tmp_struct_symbol_11 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_5_map_1_18_3, dim3(int_ceil(int_ceil(i_endidx, 1), 32), int_ceil(nlev, 1), 1), dim3(32, 1, 1), single_state_body_5_map_1_18_3_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_5_map_1_18_3", int_ceil(int_ceil(i_endidx, 1), 32), int_ceil(nlev, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_8_map_1_16_4(int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_w_concorr_mc, double * __restrict__ __1_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, const int nflatlev_jg, int nlev, int tmp_struct_symbol_0) {
    {
        int _for_it_17 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_17 >= 1 && _for_it_17 < (i_endidx + 1)) {
            loop_body_1_16_0(&__1_gpu_flat_flat_ptr_patch_cells_edge_blk[0], &__1_gpu_flat_flat_ptr_patch_cells_edge_idx[0], nflatlev_jg, &__1_gpu_v_p_int_e_bln_c_s[0], &__1_gpu_z_w_concorr_me[0], &__1_gpu_z_w_concorr_mc[0], __f2dace_A_z_w_concorr_me_d_0_s_2768, __f2dace_A_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_0_s_2768, __f2dace_OA_z_w_concorr_me_d_1_s_2769, __f2dace_OA_z_w_concorr_me_d_2_s_2770, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, _for_it_14, _for_it_17, nlev);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_8_map_1_16_4(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_w_concorr_mc, double * __restrict__ __1_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, const int nflatlev_jg, int nlev, int tmp_struct_symbol_0);
void __dace_runkernel_single_state_body_8_map_1_16_4(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_w_concorr_mc, double * __restrict__ __1_gpu_z_w_concorr_me, int __f2dace_A_z_w_concorr_me_d_0_s_2768, int __f2dace_A_z_w_concorr_me_d_1_s_2769, int __f2dace_A_z_w_concorr_me_d_2_s_2770, int __f2dace_OA_z_w_concorr_me_d_0_s_2768, int __f2dace_OA_z_w_concorr_me_d_1_s_2769, int __f2dace_OA_z_w_concorr_me_d_2_s_2770, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx, const int nflatlev_jg, int nlev, int tmp_struct_symbol_0)
{

    if ((int_ceil(int_ceil(i_endidx, 1), 32)) == 0 || (int_ceil((((- nflatlev_jg) + nlev) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_8_map_1_16_4_args[] = { (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_blk, (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_idx, (void *)&__1_gpu_v_p_int_e_bln_c_s, (void *)&__1_gpu_z_w_concorr_mc, (void *)&__1_gpu_z_w_concorr_me, (void *)&__f2dace_A_z_w_concorr_me_d_0_s_2768, (void *)&__f2dace_A_z_w_concorr_me_d_1_s_2769, (void *)&__f2dace_A_z_w_concorr_me_d_2_s_2770, (void *)&__f2dace_OA_z_w_concorr_me_d_0_s_2768, (void *)&__f2dace_OA_z_w_concorr_me_d_1_s_2769, (void *)&__f2dace_OA_z_w_concorr_me_d_2_s_2770, (void *)&__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&__f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nflatlev_jg, (void *)&nlev, (void *)&tmp_struct_symbol_0 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_8_map_1_16_4, dim3(int_ceil(int_ceil(i_endidx, 1), 32), int_ceil((((- nflatlev_jg) + nlev) + 1), 1), 1), dim3(32, 1, 1), single_state_body_8_map_1_16_4_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_8_map_1_16_4", int_ceil(int_ceil(i_endidx, 1), 32), int_ceil((((- nflatlev_jg) + nlev) + 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_7_map_1_16_7(double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_v_p_metrics_wgtfac_c, double * __restrict__ __1_gpu_z_w_concorr_mc, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_0, const int nflatlev_jg) {
    {
        int nflatlev_jg = nflatlev_jg;
        int _for_it_19 = (((blockIdx.x * 32 + threadIdx.x) + nflatlev_jg) + 1);
        if (_for_it_19 >= (nflatlev_jg + 1) && _for_it_19 < (nlev + 1)) {
            loop_body_1_16_6(&__1_gpu_v_p_metrics_wgtfac_c[0], &__1_gpu_z_w_concorr_mc[0], &__1_gpu_v_p_diag_w_concorr_c[0], __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, _for_it_14, _for_it_19, i_endidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_7_map_1_16_7(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_v_p_metrics_wgtfac_c, double * __restrict__ __1_gpu_z_w_concorr_mc, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_0, const int nflatlev_jg);
void __dace_runkernel_single_state_body_7_map_1_16_7(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_v_p_metrics_wgtfac_c, double * __restrict__ __1_gpu_z_w_concorr_mc, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, int __f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_0, const int nflatlev_jg)
{
    int nflatlev_jg = nflatlev_jg;

    if ((int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32)) == 0 || (int_ceil(i_endidx, 1)) == 0) {

        return;
    }

    void  *single_state_body_7_map_1_16_7_args[] = { (void *)&__1_gpu_v_p_diag_w_concorr_c, (void *)&__1_gpu_v_p_metrics_wgtfac_c, (void *)&__1_gpu_z_w_concorr_mc, (void *)&__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, (void *)&__f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, (void *)&__f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, (void *)&__f2dace_SA_wgtfac_c_d_0_s_5251_p_metrics_44, (void *)&__f2dace_SA_wgtfac_c_d_1_s_5252_p_metrics_44, (void *)&__f2dace_SA_wgtfac_c_d_2_s_5253_p_metrics_44, (void *)&__f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, (void *)&__f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, (void *)&__f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, (void *)&__f2dace_SOA_wgtfac_c_d_0_s_5251_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_c_d_1_s_5252_p_metrics_44, (void *)&__f2dace_SOA_wgtfac_c_d_2_s_5253_p_metrics_44, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nlev, (void *)&tmp_struct_symbol_0, (void *)&nflatlev_jg };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_7_map_1_16_7, dim3(int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32), int_ceil(i_endidx, 1), 1), dim3(32, 1, 1), single_state_body_7_map_1_16_7_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_7_map_1_16_7", int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32), int_ceil(i_endidx, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_2_map_1_19_2(double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1) {
    {
        int _for_it_21 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_21 >= 1 && _for_it_21 < (nlev + 1)) {
            loop_body_1_19_0(&__1_gpu_v_p_prog_w[0], &__1_gpu_z_w_con_c[0], __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_21, i_endidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_2_map_1_19_2(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1);
void __dace_runkernel_single_state_body_2_map_1_19_2(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1)
{

    if ((int_ceil(int_ceil(nlev, 1), 32)) == 0 || (int_ceil(i_endidx, 1)) == 0) {

        return;
    }

    void  *single_state_body_2_map_1_19_2_args[] = { (void *)&__1_gpu_v_p_prog_w, (void *)&__1_gpu_z_w_con_c, (void *)&__f2dace_SA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SA_w_d_2_s_4757_p_prog_43, (void *)&__f2dace_SOA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SOA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SOA_w_d_2_s_4757_p_prog_43, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nlev, (void *)&tmp_struct_symbol_1 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_2_map_1_19_2, dim3(int_ceil(int_ceil(nlev, 1), 32), int_ceil(i_endidx, 1), 1), dim3(32, 1, 1), single_state_body_2_map_1_19_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_2_map_1_19_2", int_ceil(int_ceil(nlev, 1), 32), int_ceil(i_endidx, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_4_map_1_2_2(double * __restrict__ __1_gpu_z_w_con_c, int i_endidx, int nlevp1, int tmp_struct_symbol_1) {
    {
        int _for_it_23 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_23 >= 1 && _for_it_23 < (i_endidx + 1)) {
            loop_body_1_2_0(&__1_gpu_z_w_con_c[0], _for_it_23, nlevp1);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_4_map_1_2_2(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_z_w_con_c, int i_endidx, int nlevp1, int tmp_struct_symbol_1);
void __dace_runkernel_single_state_body_4_map_1_2_2(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_z_w_con_c, int i_endidx, int nlevp1, int tmp_struct_symbol_1)
{

    if ((int_ceil(int_ceil(i_endidx, 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_4_map_1_2_2_args[] = { (void *)&__1_gpu_z_w_con_c, (void *)&i_endidx, (void *)&nlevp1, (void *)&tmp_struct_symbol_1 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_4_map_1_2_2, dim3(int_ceil(int_ceil(i_endidx, 1), 32), 1, 1), dim3(32, 1, 1), single_state_body_4_map_1_2_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_4_map_1_2_2", int_ceil(int_ceil(i_endidx, 1), 32), 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_10_map_1_2_6(double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, const int nflatlev_jg) {
    {
        int nflatlev_jg = nflatlev_jg;
        int _for_it_24 = (((blockIdx.x * 32 + threadIdx.x) + nflatlev_jg) + 1);
        if (_for_it_24 >= (nflatlev_jg + 1) && _for_it_24 < (nlev + 1)) {
            loop_body_1_2_4(&__1_gpu_v_p_diag_w_concorr_c[0], &__1_gpu_z_w_con_c[0], __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, _for_it_14, _for_it_24, i_endidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_10_map_1_2_6(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, const int nflatlev_jg);
void __dace_runkernel_single_state_body_10_map_1_2_6(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_w_concorr_c, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, int __f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, int __f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, int __f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, const int nflatlev_jg)
{
    int nflatlev_jg = nflatlev_jg;

    if ((int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32)) == 0 || (int_ceil(i_endidx, 1)) == 0) {

        return;
    }

    void  *single_state_body_10_map_1_2_6_args[] = { (void *)&__1_gpu_v_p_diag_w_concorr_c, (void *)&__1_gpu_z_w_con_c, (void *)&__f2dace_SA_w_concorr_c_d_0_s_4992_p_diag_45, (void *)&__f2dace_SA_w_concorr_c_d_1_s_4993_p_diag_45, (void *)&__f2dace_SA_w_concorr_c_d_2_s_4994_p_diag_45, (void *)&__f2dace_SOA_w_concorr_c_d_0_s_4992_p_diag_45, (void *)&__f2dace_SOA_w_concorr_c_d_1_s_4993_p_diag_45, (void *)&__f2dace_SOA_w_concorr_c_d_2_s_4994_p_diag_45, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nlev, (void *)&tmp_struct_symbol_1, (void *)&nflatlev_jg };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_10_map_1_2_6, dim3(int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32), int_ceil(i_endidx, 1), 1), dim3(32, 1, 1), single_state_body_10_map_1_2_6_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_10_map_1_2_6", int_ceil(int_ceil(((- nflatlev_jg) + nlev), 1), 32), int_ceil(i_endidx, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_6_map_1_2_11(int * __restrict__ __1_gpu_levmask, int _for_it_14, int nlev, long long tmp_arg_0, int tmp_struct_symbol_13, int tmp_struct_symbol_14) {
    {
        int _for_it_26 = ((blockIdx.x * 32 + threadIdx.x) + Max(3, tmp_arg_0));
        if (_for_it_26 >= Max(3, tmp_arg_0) && _for_it_26 < (nlev - 2)) {
            loop_body_1_2_9(&__1_gpu_levmask[0], _for_it_14, _for_it_26, tmp_struct_symbol_13);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_6_map_1_2_11(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_levmask, int _for_it_14, int nlev, long long tmp_arg_0, int tmp_struct_symbol_13, int tmp_struct_symbol_14);
void __dace_runkernel_single_state_body_6_map_1_2_11(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_levmask, int _for_it_14, int nlev, long long tmp_arg_0, int tmp_struct_symbol_13, int tmp_struct_symbol_14)
{

    if ((int_ceil(int_ceil(((nlev - Max(3, tmp_arg_0)) - 2), 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_6_map_1_2_11_args[] = { (void *)&__1_gpu_levmask, (void *)&_for_it_14, (void *)&nlev, (void *)&tmp_arg_0, (void *)&tmp_struct_symbol_13, (void *)&tmp_struct_symbol_14 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_6_map_1_2_11, dim3(int_ceil(int_ceil(((nlev - Max(3, tmp_arg_0)) - 2), 1), 32), 1, 1), dim3(32, 1, 1), single_state_body_6_map_1_2_11_args, 0, __state->gpu_context->streams[1]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_6_map_1_2_11", int_ceil(int_ceil(((nlev - Max(3, tmp_arg_0)) - 2), 1), 32), 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) T_l493_c493_map_1_2_16(double maxvcfl) {
    {
        int T_l493_c493__mapi = (blockIdx.x * 32 + threadIdx.x);
        if (T_l493_c493__mapi < 1) {
            {
                double maxvcfl_out;

                ///////////////////
                // Tasklet code (T_l493_c493)
                maxvcfl_out = 0;
                ///////////////////

                maxvcfl = maxvcfl_out;
            }
        }
    }
}


DACE_EXPORTED void __dace_runkernel_T_l493_c493_map_1_2_16(velocity_tendencies_state_t *__state, double maxvcfl);
void __dace_runkernel_T_l493_c493_map_1_2_16(velocity_tendencies_state_t *__state, double maxvcfl)
{

    void  *T_l493_c493_map_1_2_16_args[] = { (void *)&maxvcfl };
    gpuError_t __err = cudaLaunchKernel((void*)T_l493_c493_map_1_2_16, dim3(1, 1, 1), dim3(32, 1, 1), T_l493_c493_map_1_2_16_args, 0, __state->gpu_context->streams[2]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "T_l493_c493_map_1_2_16", 1, 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_9_map_1_17_9(int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx, double maxvcfl, int nlev, long long tmp_arg_1, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16) {
    {
        int _for_it_27 = ((blockIdx.x * 32 + threadIdx.x) + Max(3, tmp_arg_1));
        if (_for_it_27 >= Max(3, tmp_arg_1) && _for_it_27 < (nlev - 2)) {
            loop_body_1_17_0(dtime, &__1_gpu_v_p_metrics_ddqz_z_half[0], &__1_gpu_cfl_clipping[0], &__1_gpu_levmask[0], maxvcfl, &__1_gpu_z_w_con_c[0], __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, _for_it_14, _for_it_27, cfl_w_limit, i_endidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_9_map_1_17_9(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx, double maxvcfl, int nlev, long long tmp_arg_1, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16);
void __dace_runkernel_single_state_body_9_map_1_17_9(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx, double maxvcfl, int nlev, long long tmp_arg_1, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16)
{

    if ((int_ceil(int_ceil(((nlev - Max(3, tmp_arg_1)) - 2), 1), 32)) == 0 || (int_ceil(i_endidx, 1)) == 0) {

        return;
    }

    void  *single_state_body_9_map_1_17_9_args[] = { (void *)&__1_gpu_cfl_clipping, (void *)&__1_gpu_levmask, (void *)&__1_gpu_v_p_metrics_ddqz_z_half, (void *)&__1_gpu_z_w_con_c, (void *)&__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, (void *)&_for_it_14, (void *)&cfl_w_limit, (void *)&dtime, (void *)&i_endidx, (void *)&maxvcfl, (void *)&nlev, (void *)&tmp_arg_1, (void *)&tmp_struct_symbol_1, (void *)&tmp_struct_symbol_13, (void *)&tmp_struct_symbol_14, (void *)&tmp_struct_symbol_16 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_9_map_1_17_9, dim3(int_ceil(int_ceil(((nlev - Max(3, tmp_arg_1)) - 2), 1), 32), int_ceil(i_endidx, 1), 1), dim3(32, 1, 1), single_state_body_9_map_1_17_9_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_9_map_1_17_9", int_ceil(int_ceil(((nlev - Max(3, tmp_arg_1)) - 2), 1), 32), int_ceil(i_endidx, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_11_map_1_17_13(double * __restrict__ __1_gpu_z_w_con_c, double * __restrict__ __1_gpu_z_w_con_c_full, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, int tmp_struct_symbol_2, int tmp_struct_symbol_3) {
    {
        int _for_it_30 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_30 >= 1 && _for_it_30 < (nlev + 1)) {
            loop_body_1_17_11(&__1_gpu_z_w_con_c[0], &__1_gpu_z_w_con_c_full[0], _for_it_14, _for_it_30, i_endidx, tmp_struct_symbol_2);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_11_map_1_17_13(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_z_w_con_c, double * __restrict__ __1_gpu_z_w_con_c_full, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, int tmp_struct_symbol_2, int tmp_struct_symbol_3);
void __dace_runkernel_single_state_body_11_map_1_17_13(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_z_w_con_c, double * __restrict__ __1_gpu_z_w_con_c_full, int _for_it_14, int i_endidx, int nlev, int tmp_struct_symbol_1, int tmp_struct_symbol_2, int tmp_struct_symbol_3)
{

    if ((int_ceil(int_ceil(nlev, 1), 32)) == 0 || (int_ceil(i_endidx, 1)) == 0) {

        return;
    }

    void  *single_state_body_11_map_1_17_13_args[] = { (void *)&__1_gpu_z_w_con_c, (void *)&__1_gpu_z_w_con_c_full, (void *)&_for_it_14, (void *)&i_endidx, (void *)&nlev, (void *)&tmp_struct_symbol_1, (void *)&tmp_struct_symbol_2, (void *)&tmp_struct_symbol_3 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_11_map_1_17_13, dim3(int_ceil(int_ceil(nlev, 1), 32), int_ceil(i_endidx, 1), 1), dim3(32, 1, 1), single_state_body_11_map_1_17_13_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_11_map_1_17_13", int_ceil(int_ceil(nlev, 1), 32), int_ceil(i_endidx, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_0_map_1_15_3(double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_metrics_coeff1_dwdz, double * __restrict__ __1_gpu_v_p_metrics_coeff2_dwdz, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_1, int * __restrict__ __6_ntnd_0) {
    {
        int _for_it_32 = ((blockIdx.x * 32 + threadIdx.x) + 2);
        if (_for_it_32 >= 2 && _for_it_32 < (nlev + 1)) {
            loop_body_1_15_0(ntnd, &__1_gpu_v_p_metrics_coeff1_dwdz[0], &__1_gpu_v_p_metrics_coeff2_dwdz[0], &__1_gpu_v_p_prog_w[0], &__1_gpu_z_w_con_c[0], &__1_gpu_v_p_diag_ddt_w_adv_pc[0], __6_ntnd_0, __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_32, i_endidx_2);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_0_map_1_15_3(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_metrics_coeff1_dwdz, double * __restrict__ __1_gpu_v_p_metrics_coeff2_dwdz, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_1, int * __restrict__ __6_ntnd_0);
void __dace_runkernel_single_state_body_0_map_1_15_3(velocity_tendencies_state_t *__state, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_metrics_coeff1_dwdz, double * __restrict__ __1_gpu_v_p_metrics_coeff2_dwdz, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, int __f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, int __f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_1, int * __restrict__ __6_ntnd_0)
{

    if ((int_ceil(int_ceil((nlev - 1), 1), 32)) == 0 || (int_ceil(i_endidx_2, 1)) == 0) {

        return;
    }

    void  *single_state_body_0_map_1_15_3_args[] = { (void *)&__1_gpu_v_p_diag_ddt_w_adv_pc, (void *)&__1_gpu_v_p_metrics_coeff1_dwdz, (void *)&__1_gpu_v_p_metrics_coeff2_dwdz, (void *)&__1_gpu_v_p_prog_w, (void *)&__1_gpu_z_w_con_c, (void *)&__f2dace_SA_coeff1_dwdz_d_0_s_5272_p_metrics_44, (void *)&__f2dace_SA_coeff1_dwdz_d_1_s_5273_p_metrics_44, (void *)&__f2dace_SA_coeff1_dwdz_d_2_s_5274_p_metrics_44, (void *)&__f2dace_SA_coeff2_dwdz_d_0_s_5275_p_metrics_44, (void *)&__f2dace_SA_coeff2_dwdz_d_1_s_5276_p_metrics_44, (void *)&__f2dace_SA_coeff2_dwdz_d_2_s_5277_p_metrics_44, (void *)&__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SA_w_d_2_s_4757_p_prog_43, (void *)&__f2dace_SOA_coeff1_dwdz_d_0_s_5272_p_metrics_44, (void *)&__f2dace_SOA_coeff1_dwdz_d_1_s_5273_p_metrics_44, (void *)&__f2dace_SOA_coeff1_dwdz_d_2_s_5274_p_metrics_44, (void *)&__f2dace_SOA_coeff2_dwdz_d_0_s_5275_p_metrics_44, (void *)&__f2dace_SOA_coeff2_dwdz_d_1_s_5276_p_metrics_44, (void *)&__f2dace_SOA_coeff2_dwdz_d_2_s_5277_p_metrics_44, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SOA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SOA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SOA_w_d_2_s_4757_p_prog_43, (void *)&_for_it_14, (void *)&i_endidx_2, (void *)&nlev, (void *)&ntnd, (void *)&tmp_struct_symbol_1, (void *)&__6_ntnd_0 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_0_map_1_15_3, dim3(int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(i_endidx_2, 1), 1), dim3(32, 1, 1), single_state_body_0_map_1_15_3_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_0_map_1_15_3", int_ceil(int_ceil((nlev - 1), 1), 32), int_ceil(i_endidx_2, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_1_map_1_15_7(int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_v_grad_w, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int * __restrict__ __8_ntnd_0) {
    {
        int _for_it_34 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_34 >= 1 && _for_it_34 < (i_endidx_2 + 1)) {
            loop_body_1_15_5(&__1_gpu_flat_flat_ptr_patch_cells_edge_blk[0], &__1_gpu_flat_flat_ptr_patch_cells_edge_idx[0], ntnd, &__1_gpu_v_p_int_e_bln_c_s[0], &__1_gpu_z_v_grad_w[0], &__1_gpu_v_p_diag_ddt_w_adv_pc[0], __8_ntnd_0, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, _for_it_14, _for_it_34, nlev, tmp_struct_symbol_4);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_1_map_1_15_7(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_v_grad_w, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int * __restrict__ __8_ntnd_0);
void __dace_runkernel_single_state_body_1_map_1_15_7(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_blk, int * __restrict__ __1_gpu_flat_flat_ptr_patch_cells_edge_idx, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_e_bln_c_s, double * __restrict__ __1_gpu_z_v_grad_w, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, int __f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, int __f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, int __f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, int __f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, int __f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, int _for_it_14, int i_endidx_2, int nlev, const int ntnd, int tmp_struct_symbol_4, int tmp_struct_symbol_5, int * __restrict__ __8_ntnd_0)
{

    if ((int_ceil(int_ceil(i_endidx_2, 1), 32)) == 0 || (int_ceil((nlev - 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_1_map_1_15_7_args[] = { (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_blk, (void *)&__1_gpu_flat_flat_ptr_patch_cells_edge_idx, (void *)&__1_gpu_v_p_diag_ddt_w_adv_pc, (void *)&__1_gpu_v_p_int_e_bln_c_s, (void *)&__1_gpu_z_v_grad_w, (void *)&__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SOA_e_bln_c_s_d_0_s_3959_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_1_s_3960_p_int_38, (void *)&__f2dace_SOA_e_bln_c_s_d_2_s_3961_p_int_38, (void *)&__f2dace_SOA_edge_blk_d_0_s_3117_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_1_s_3118_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_blk_d_2_s_3119_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_0_s_3114_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_1_s_3115_cells_ptr_patch_4, (void *)&__f2dace_SOA_edge_idx_d_2_s_3116_cells_ptr_patch_4, (void *)&_for_it_14, (void *)&i_endidx_2, (void *)&nlev, (void *)&ntnd, (void *)&tmp_struct_symbol_4, (void *)&tmp_struct_symbol_5, (void *)&__8_ntnd_0 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_1_map_1_15_7, dim3(int_ceil(int_ceil(i_endidx_2, 1), 32), int_ceil((nlev - 1), 1), 1), dim3(32, 1, 1), single_state_body_1_map_1_15_7_args, 0, __state->gpu_context->streams[1]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_1_map_1_15_7", int_ceil(int_ceil(i_endidx_2, 1), 32), int_ceil((nlev - 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_3_map_1_20_7(int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_blk, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_idx, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_geofac_n2s, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_v_v_ptr_patch_cells_area, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx_2, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_2, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16) {
    {
        int _for_it_36 = ((blockIdx.x * 32 + threadIdx.x) + Max(3, tmp_arg_2));
        if (_for_it_36 >= Max(3, tmp_arg_2) && _for_it_36 < (nlev - 2)) {
            loop_body_1_20_0(&__1_gpu_cfl_clipping[0], dtime, &__1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask[0], &__1_gpu_flat_v_ptr_patch_cells_neighbor_blk[0], &__1_gpu_flat_v_ptr_patch_cells_neighbor_idx[0], &__1_gpu_levmask[0], ntnd, scalfac_exdiff, &__1_gpu_v_p_int_geofac_n2s[0], &__1_gpu_v_p_metrics_ddqz_z_half[0], &__1_gpu_v_p_prog_w[0], &__1_gpu_v_v_ptr_patch_cells_area[0], &__1_gpu_z_w_con_c[0], &__1_gpu_v_p_diag_ddt_w_adv_pc[0], __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, __f2dace_SA_w_d_0_s_4755_p_prog_43, __f2dace_SA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, __f2dace_SOA_w_d_0_s_4755_p_prog_43, __f2dace_SOA_w_d_1_s_4756_p_prog_43, __f2dace_SOA_w_d_2_s_4757_p_prog_43, _for_it_14, _for_it_36, cfl_w_limit, i_endidx_2, tmp_struct_symbol_13);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_3_map_1_20_7(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_blk, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_idx, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_geofac_n2s, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_v_v_ptr_patch_cells_area, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx_2, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_2, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16);
void __dace_runkernel_single_state_body_3_map_1_20_7(velocity_tendencies_state_t *__state, int * __restrict__ __1_gpu_cfl_clipping, int * __restrict__ __1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_blk, int * __restrict__ __1_gpu_flat_v_ptr_patch_cells_neighbor_idx, int * __restrict__ __1_gpu_levmask, double * __restrict__ __1_gpu_v_p_diag_ddt_w_adv_pc, double * __restrict__ __1_gpu_v_p_int_geofac_n2s, double * __restrict__ __1_gpu_v_p_metrics_ddqz_z_half, double * __restrict__ __1_gpu_v_p_prog_w, double * __restrict__ __1_gpu_v_v_ptr_patch_cells_area, double * __restrict__ __1_gpu_z_w_con_c, int __f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SA_w_d_0_s_4755_p_prog_43, int __f2dace_SA_w_d_1_s_4756_p_prog_43, int __f2dace_SA_w_d_2_s_4757_p_prog_43, int __f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, int __f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, int __f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, int __f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, int __f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, int __f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, int __f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, int __f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, int __f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, int __f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, int __f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, int __f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, int __f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, int __f2dace_SOA_w_d_0_s_4755_p_prog_43, int __f2dace_SOA_w_d_1_s_4756_p_prog_43, int __f2dace_SOA_w_d_2_s_4757_p_prog_43, int _for_it_14, double cfl_w_limit, const double dtime, int i_endidx_2, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_2, int tmp_struct_symbol_1, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_16)
{

    if ((int_ceil(int_ceil(((nlev - Max(3, tmp_arg_2)) - 2), 1), 32)) == 0 || (int_ceil(i_endidx_2, 1)) == 0) {

        return;
    }

    void  *single_state_body_3_map_1_20_7_args[] = { (void *)&__1_gpu_cfl_clipping, (void *)&__1_gpu_flat_flat_v_ptr_patch_cells_decomp_info_owner_mask, (void *)&__1_gpu_flat_v_ptr_patch_cells_neighbor_blk, (void *)&__1_gpu_flat_v_ptr_patch_cells_neighbor_idx, (void *)&__1_gpu_levmask, (void *)&__1_gpu_v_p_diag_ddt_w_adv_pc, (void *)&__1_gpu_v_p_int_geofac_n2s, (void *)&__1_gpu_v_p_metrics_ddqz_z_half, (void *)&__1_gpu_v_p_prog_w, (void *)&__1_gpu_v_v_ptr_patch_cells_area, (void *)&__1_gpu_z_w_con_c, (void *)&__f2dace_SA_area_d_0_s_3131_cells_ptr_patch_4, (void *)&__f2dace_SA_area_d_1_s_3132_cells_ptr_patch_4, (void *)&__f2dace_SA_ddqz_z_half_d_0_s_5245_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_half_d_1_s_5246_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_half_d_2_s_5247_p_metrics_44, (void *)&__f2dace_SA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SA_geofac_n2s_d_0_s_4050_p_int_38, (void *)&__f2dace_SA_geofac_n2s_d_1_s_4051_p_int_38, (void *)&__f2dace_SA_geofac_n2s_d_2_s_4052_p_int_38, (void *)&__f2dace_SA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, (void *)&__f2dace_SA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, (void *)&__f2dace_SA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, (void *)&__f2dace_SA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, (void *)&__f2dace_SA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, (void *)&__f2dace_SA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, (void *)&__f2dace_SA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, (void *)&__f2dace_SA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, (void *)&__f2dace_SA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SA_w_d_2_s_4757_p_prog_43, (void *)&__f2dace_SOA_area_d_0_s_3131_cells_ptr_patch_4, (void *)&__f2dace_SOA_area_d_1_s_3132_cells_ptr_patch_4, (void *)&__f2dace_SOA_ddqz_z_half_d_0_s_5245_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_half_d_1_s_5246_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_half_d_2_s_5247_p_metrics_44, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_0_s_5006_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_1_s_5007_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_2_s_5008_p_diag_45, (void *)&__f2dace_SOA_ddt_w_adv_pc_d_3_s_5009_p_diag_45, (void *)&__f2dace_SOA_geofac_n2s_d_0_s_4050_p_int_38, (void *)&__f2dace_SOA_geofac_n2s_d_1_s_4051_p_int_38, (void *)&__f2dace_SOA_geofac_n2s_d_2_s_4052_p_int_38, (void *)&__f2dace_SOA_neighbor_blk_d_0_s_3111_cells_ptr_patch_4, (void *)&__f2dace_SOA_neighbor_blk_d_1_s_3112_cells_ptr_patch_4, (void *)&__f2dace_SOA_neighbor_blk_d_2_s_3113_cells_ptr_patch_4, (void *)&__f2dace_SOA_neighbor_idx_d_0_s_3108_cells_ptr_patch_4, (void *)&__f2dace_SOA_neighbor_idx_d_1_s_3109_cells_ptr_patch_4, (void *)&__f2dace_SOA_neighbor_idx_d_2_s_3110_cells_ptr_patch_4, (void *)&__f2dace_SOA_owner_mask_d_0_s_3686_decomp_info_verts_ptr_patch_26, (void *)&__f2dace_SOA_owner_mask_d_1_s_3687_decomp_info_verts_ptr_patch_26, (void *)&__f2dace_SOA_w_d_0_s_4755_p_prog_43, (void *)&__f2dace_SOA_w_d_1_s_4756_p_prog_43, (void *)&__f2dace_SOA_w_d_2_s_4757_p_prog_43, (void *)&_for_it_14, (void *)&cfl_w_limit, (void *)&dtime, (void *)&i_endidx_2, (void *)&nlev, (void *)&ntnd, (void *)&scalfac_exdiff, (void *)&tmp_arg_2, (void *)&tmp_struct_symbol_1, (void *)&tmp_struct_symbol_13, (void *)&tmp_struct_symbol_14, (void *)&tmp_struct_symbol_16 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_3_map_1_20_7, dim3(int_ceil(int_ceil(((nlev - Max(3, tmp_arg_2)) - 2), 1), 32), int_ceil(i_endidx_2, 1), 1), dim3(32, 1, 1), single_state_body_3_map_1_20_7_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_3_map_1_20_7", int_ceil(int_ceil(((nlev - Max(3, tmp_arg_2)) - 2), 1), 32), int_ceil(i_endidx_2, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_1_map_0_24_3(int * __restrict__ gpu_levelmask, const int * __restrict__ gpu_levmask, int nlev, long long tmp_arg_5, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_15) {
    {
        int _for_it_38 = ((blockIdx.x * 32 + threadIdx.x) + Max(3, tmp_arg_5));
        if (_for_it_38 >= Max(3, tmp_arg_5) && _for_it_38 < (nlev - 2)) {
            loop_body_0_24_2(&gpu_levmask[0], &gpu_levelmask[0], _for_it_38);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_1_map_0_24_3(velocity_tendencies_state_t *__state, int * __restrict__ gpu_levelmask, const int * __restrict__ gpu_levmask, int nlev, long long tmp_arg_5, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_15);
void __dace_runkernel_single_state_body_1_map_0_24_3(velocity_tendencies_state_t *__state, int * __restrict__ gpu_levelmask, const int * __restrict__ gpu_levmask, int nlev, long long tmp_arg_5, int tmp_struct_symbol_13, int tmp_struct_symbol_14, int tmp_struct_symbol_15)
{

    if ((int_ceil(int_ceil(((nlev - Max(3, tmp_arg_5)) - 2), 1), 32)) == 0) {

        return;
    }

    void  *single_state_body_1_map_0_24_3_args[] = { (void *)&gpu_levelmask, (void *)&gpu_levmask, (void *)&nlev, (void *)&tmp_arg_5, (void *)&tmp_struct_symbol_13, (void *)&tmp_struct_symbol_14, (void *)&tmp_struct_symbol_15 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_1_map_0_24_3, dim3(int_ceil(int_ceil(((nlev - Max(3, tmp_arg_5)) - 2), 1), 32), 1, 1), dim3(32, 1, 1), single_state_body_1_map_0_24_3_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_1_map_0_24_3", int_ceil(int_ceil(((nlev - Max(3, tmp_arg_5)) - 2), 1), 32), 1, 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_1_map_30_6_6(int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_diag_vn_ie, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_p_int_c_lin_e, double * __restrict__ __30_gpu_v_p_metrics_coeff_gradekin, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, double * __restrict__ __30_gpu_z_ekinh, double * __restrict__ __30_gpu_z_kin_hor_e, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int tmp_struct_symbol_10, int tmp_struct_symbol_11, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9, int * __restrict__ __33_ntnd_0) {
    {
        int _for_it_40 = ((blockIdx.x * 32 + threadIdx.x) + i_startidx);
        if (_for_it_40 >= i_startidx && _for_it_40 < (i_endidx + 1)) {
            loop_body_30_6_0(&__30_gpu_flat_v_ptr_patch_edges_cell_blk[0], &__30_gpu_flat_v_ptr_patch_edges_cell_idx[0], &__30_gpu_flat_v_ptr_patch_edges_vertex_blk[0], &__30_gpu_flat_v_ptr_patch_edges_vertex_idx[0], ntnd, &__30_gpu_v_p_diag_vn_ie[0], &__30_gpu_v_p_diag_vt[0], &__30_gpu_v_p_int_c_lin_e[0], &__30_gpu_v_p_metrics_coeff_gradekin[0], &__30_gpu_v_p_metrics_ddqz_z_full_e[0], &__30_gpu_v_v_ptr_patch_edges_f_e[0], &__30_gpu_z_ekinh[0], &__30_gpu_z_kin_hor_e[0], &__30_gpu_z_w_con_c_full[0], &__30_gpu_zeta[0], &__30_gpu_v_p_diag_ddt_vn_apc_pc[0], __33_ntnd_0, __f2dace_A_z_kin_hor_e_d_0_s_2771, __f2dace_A_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_0_s_2771, __f2dace_OA_z_kin_hor_e_d_1_s_2772, __f2dace_OA_z_kin_hor_e_d_2_s_2773, __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_39, _for_it_40, nlev, tmp_struct_symbol_10, tmp_struct_symbol_2, tmp_struct_symbol_8);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_1_map_30_6_6(velocity_tendencies_state_t *__state, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_diag_vn_ie, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_p_int_c_lin_e, double * __restrict__ __30_gpu_v_p_metrics_coeff_gradekin, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, double * __restrict__ __30_gpu_z_ekinh, double * __restrict__ __30_gpu_z_kin_hor_e, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int tmp_struct_symbol_10, int tmp_struct_symbol_11, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9, int * __restrict__ __33_ntnd_0);
void __dace_runkernel_single_state_body_1_map_30_6_6(velocity_tendencies_state_t *__state, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_diag_vn_ie, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_p_int_c_lin_e, double * __restrict__ __30_gpu_v_p_metrics_coeff_gradekin, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, double * __restrict__ __30_gpu_z_ekinh, double * __restrict__ __30_gpu_z_kin_hor_e, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_A_z_kin_hor_e_d_0_s_2771, int __f2dace_A_z_kin_hor_e_d_1_s_2772, int __f2dace_A_z_kin_hor_e_d_2_s_2773, int __f2dace_OA_z_kin_hor_e_d_0_s_2771, int __f2dace_OA_z_kin_hor_e_d_1_s_2772, int __f2dace_OA_z_kin_hor_e_d_2_s_2773, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, int __f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, int __f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, int __f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int tmp_struct_symbol_10, int tmp_struct_symbol_11, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9, int * __restrict__ __33_ntnd_0)
{

    if ((int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32)) == 0 || (int_ceil(nlev, 1)) == 0) {

        return;
    }

    void  *single_state_body_1_map_30_6_6_args[] = { (void *)&__30_gpu_flat_v_ptr_patch_edges_cell_blk, (void *)&__30_gpu_flat_v_ptr_patch_edges_cell_idx, (void *)&__30_gpu_flat_v_ptr_patch_edges_vertex_blk, (void *)&__30_gpu_flat_v_ptr_patch_edges_vertex_idx, (void *)&__30_gpu_v_p_diag_ddt_vn_apc_pc, (void *)&__30_gpu_v_p_diag_vn_ie, (void *)&__30_gpu_v_p_diag_vt, (void *)&__30_gpu_v_p_int_c_lin_e, (void *)&__30_gpu_v_p_metrics_coeff_gradekin, (void *)&__30_gpu_v_p_metrics_ddqz_z_full_e, (void *)&__30_gpu_v_v_ptr_patch_edges_f_e, (void *)&__30_gpu_z_ekinh, (void *)&__30_gpu_z_kin_hor_e, (void *)&__30_gpu_z_w_con_c_full, (void *)&__30_gpu_zeta, (void *)&__f2dace_A_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_A_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_A_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_OA_z_kin_hor_e_d_0_s_2771, (void *)&__f2dace_OA_z_kin_hor_e_d_1_s_2772, (void *)&__f2dace_OA_z_kin_hor_e_d_2_s_2773, (void *)&__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, (void *)&__f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, (void *)&__f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, (void *)&__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SA_coeff_gradekin_d_0_s_5269_p_metrics_44, (void *)&__f2dace_SA_coeff_gradekin_d_1_s_5270_p_metrics_44, (void *)&__f2dace_SA_coeff_gradekin_d_2_s_5271_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, (void *)&__f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, (void *)&__f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, (void *)&__f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, (void *)&__f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, (void *)&__f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SOA_coeff_gradekin_d_0_s_5269_p_metrics_44, (void *)&__f2dace_SOA_coeff_gradekin_d_1_s_5270_p_metrics_44, (void *)&__f2dace_SOA_coeff_gradekin_d_2_s_5271_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, (void *)&__f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, (void *)&__f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SOA_vn_ie_d_0_s_4989_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_1_s_4990_p_diag_45, (void *)&__f2dace_SOA_vn_ie_d_2_s_4991_p_diag_45, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&_for_it_39, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&ntnd, (void *)&tmp_struct_symbol_10, (void *)&tmp_struct_symbol_11, (void *)&tmp_struct_symbol_2, (void *)&tmp_struct_symbol_3, (void *)&tmp_struct_symbol_8, (void *)&tmp_struct_symbol_9, (void *)&__33_ntnd_0 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_1_map_30_6_6, dim3(int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1), dim3(32, 1, 1), single_state_body_1_map_30_6_6_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_1_map_30_6_6", int_ceil(int_ceil(((i_endidx - i_startidx) + 1), 1), 32), int_ceil(nlev, 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_map_30_5_2(double * __restrict__ __30_gpu_v_p_diag_ddt_vn_cor_pc, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int * __restrict__ __31_ntnd_0) {
    {
        int _for_it_42 = ((blockIdx.x * 32 + threadIdx.x) + 1);
        if (_for_it_42 >= 1 && _for_it_42 < (nlev + 1)) {
            loop_body_30_5_0(ntnd, &__30_gpu_v_p_diag_vt[0], &__30_gpu_v_v_ptr_patch_edges_f_e[0], &__30_gpu_v_p_diag_ddt_vn_cor_pc[0], __31_ntnd_0, __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SA_vt_d_0_s_4977_p_diag_45, __f2dace_SA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, __f2dace_SOA_vt_d_0_s_4977_p_diag_45, __f2dace_SOA_vt_d_1_s_4978_p_diag_45, __f2dace_SOA_vt_d_2_s_4979_p_diag_45, _for_it_39, _for_it_42, i_endidx, i_startidx);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_map_30_5_2(velocity_tendencies_state_t *__state, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_cor_pc, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int * __restrict__ __31_ntnd_0);
void __dace_runkernel_single_state_body_map_30_5_2(velocity_tendencies_state_t *__state, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_cor_pc, double * __restrict__ __30_gpu_v_p_diag_vt, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_f_e, int __f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SA_vt_d_0_s_4977_p_diag_45, int __f2dace_SA_vt_d_1_s_4978_p_diag_45, int __f2dace_SA_vt_d_2_s_4979_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, int __f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, int __f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, int __f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, int __f2dace_SOA_vt_d_0_s_4977_p_diag_45, int __f2dace_SOA_vt_d_1_s_4978_p_diag_45, int __f2dace_SOA_vt_d_2_s_4979_p_diag_45, int _for_it_39, int i_endidx, long long i_startidx, int nlev, const int ntnd, int * __restrict__ __31_ntnd_0)
{

    if ((int_ceil(int_ceil(nlev, 1), 32)) == 0 || (int_ceil(((i_endidx - i_startidx) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_map_30_5_2_args[] = { (void *)&__30_gpu_v_p_diag_ddt_vn_cor_pc, (void *)&__30_gpu_v_p_diag_vt, (void *)&__30_gpu_v_v_ptr_patch_edges_f_e, (void *)&__f2dace_SA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, (void *)&__f2dace_SA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, (void *)&__f2dace_SA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, (void *)&__f2dace_SA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, (void *)&__f2dace_SA_f_e_d_0_s_3253_edges_ptr_patch_15, (void *)&__f2dace_SA_f_e_d_1_s_3254_edges_ptr_patch_15, (void *)&__f2dace_SA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SA_vt_d_2_s_4979_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_cor_pc_d_0_s_5002_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_cor_pc_d_1_s_5003_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_cor_pc_d_2_s_5004_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_cor_pc_d_3_s_5005_p_diag_45, (void *)&__f2dace_SOA_f_e_d_0_s_3253_edges_ptr_patch_15, (void *)&__f2dace_SOA_f_e_d_1_s_3254_edges_ptr_patch_15, (void *)&__f2dace_SOA_vt_d_0_s_4977_p_diag_45, (void *)&__f2dace_SOA_vt_d_1_s_4978_p_diag_45, (void *)&__f2dace_SOA_vt_d_2_s_4979_p_diag_45, (void *)&_for_it_39, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&ntnd, (void *)&__31_ntnd_0 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_map_30_5_2, dim3(int_ceil(int_ceil(nlev, 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1), dim3(32, 1, 1), single_state_body_map_30_5_2_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_map_30_5_2", int_ceil(int_ceil(nlev, 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1, 32, 1, 1);
}
__global__ void __launch_bounds__(32) single_state_body_2_map_30_7_7(double * __restrict__ __30_gpu_flat_p_int_c_lin_e, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, int * __restrict__ __30_gpu_levelmask, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_int_geofac_grdiv, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_p_prog_vn, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_area_edge, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_39, double cfl_w_limit, const double dtime, int i_endidx, long long i_startidx, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_6, int tmp_struct_symbol_15, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9) {
    {
        int _for_it_44 = ((blockIdx.x * 32 + threadIdx.x) + Max(3, tmp_arg_6));
        if (_for_it_44 >= Max(3, tmp_arg_6) && _for_it_44 < (nlev - 3)) {
            loop_body_30_7_0(dtime, &__30_gpu_flat_p_int_c_lin_e[0], &__30_gpu_flat_v_ptr_patch_edges_cell_blk[0], &__30_gpu_flat_v_ptr_patch_edges_cell_idx[0], &__30_gpu_flat_v_ptr_patch_edges_quad_blk[0], &__30_gpu_flat_v_ptr_patch_edges_quad_idx[0], &__30_gpu_flat_v_ptr_patch_edges_vertex_blk[0], &__30_gpu_flat_v_ptr_patch_edges_vertex_idx[0], &__30_gpu_levelmask[0], ntnd, scalfac_exdiff, &__30_gpu_v_p_int_geofac_grdiv[0], &__30_gpu_v_p_metrics_ddqz_z_full_e[0], &__30_gpu_v_p_prog_vn[0], &__30_gpu_v_v_ptr_patch_edges_area_edge[0], &__30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length[0], &__30_gpu_v_v_ptr_patch_edges_tangent_orientation[0], &__30_gpu_z_w_con_c_full[0], &__30_gpu_zeta[0], &__30_gpu_v_p_diag_ddt_vn_apc_pc[0], __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SA_vn_d_0_s_4758_p_prog_43, __f2dace_SA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, __f2dace_SOA_vn_d_0_s_4758_p_prog_43, __f2dace_SOA_vn_d_1_s_4759_p_prog_43, __f2dace_SOA_vn_d_2_s_4760_p_prog_43, _for_it_39, _for_it_44, cfl_w_limit, i_endidx, i_startidx, tmp_struct_symbol_2, tmp_struct_symbol_8);
        }
    }
}


DACE_EXPORTED void __dace_runkernel_single_state_body_2_map_30_7_7(velocity_tendencies_state_t *__state, double * __restrict__ __30_gpu_flat_p_int_c_lin_e, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, int * __restrict__ __30_gpu_levelmask, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_int_geofac_grdiv, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_p_prog_vn, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_area_edge, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_39, double cfl_w_limit, const double dtime, int i_endidx, long long i_startidx, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_6, int tmp_struct_symbol_15, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9);
void __dace_runkernel_single_state_body_2_map_30_7_7(velocity_tendencies_state_t *__state, double * __restrict__ __30_gpu_flat_p_int_c_lin_e, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_cell_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_quad_idx, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_blk, int * __restrict__ __30_gpu_flat_v_ptr_patch_edges_vertex_idx, int * __restrict__ __30_gpu_levelmask, double * __restrict__ __30_gpu_v_p_diag_ddt_vn_apc_pc, double * __restrict__ __30_gpu_v_p_int_geofac_grdiv, double * __restrict__ __30_gpu_v_p_metrics_ddqz_z_full_e, double * __restrict__ __30_gpu_v_p_prog_vn, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_area_edge, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, double * __restrict__ __30_gpu_v_v_ptr_patch_edges_tangent_orientation, double * __restrict__ __30_gpu_z_w_con_c_full, double * __restrict__ __30_gpu_zeta, int __f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SA_vn_d_0_s_4758_p_prog_43, int __f2dace_SA_vn_d_1_s_4759_p_prog_43, int __f2dace_SA_vn_d_2_s_4760_p_prog_43, int __f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, int __f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, int __f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, int __f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, int __f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, int __f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, int __f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, int __f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, int __f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, int __f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, int __f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, int __f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, int __f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, int __f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, int __f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, int __f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, int __f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, int __f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, int __f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, int __f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, int __f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, int __f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, int __f2dace_SOA_vn_d_0_s_4758_p_prog_43, int __f2dace_SOA_vn_d_1_s_4759_p_prog_43, int __f2dace_SOA_vn_d_2_s_4760_p_prog_43, int _for_it_39, double cfl_w_limit, const double dtime, int i_endidx, long long i_startidx, int nlev, const int ntnd, const double scalfac_exdiff, long long tmp_arg_6, int tmp_struct_symbol_15, int tmp_struct_symbol_2, int tmp_struct_symbol_3, int tmp_struct_symbol_8, int tmp_struct_symbol_9)
{

    if ((int_ceil(int_ceil(((nlev - Max(3, tmp_arg_6)) - 3), 1), 32)) == 0 || (int_ceil(((i_endidx - i_startidx) + 1), 1)) == 0) {

        return;
    }

    void  *single_state_body_2_map_30_7_7_args[] = { (void *)&__30_gpu_flat_p_int_c_lin_e, (void *)&__30_gpu_flat_v_ptr_patch_edges_cell_blk, (void *)&__30_gpu_flat_v_ptr_patch_edges_cell_idx, (void *)&__30_gpu_flat_v_ptr_patch_edges_quad_blk, (void *)&__30_gpu_flat_v_ptr_patch_edges_quad_idx, (void *)&__30_gpu_flat_v_ptr_patch_edges_vertex_blk, (void *)&__30_gpu_flat_v_ptr_patch_edges_vertex_idx, (void *)&__30_gpu_levelmask, (void *)&__30_gpu_v_p_diag_ddt_vn_apc_pc, (void *)&__30_gpu_v_p_int_geofac_grdiv, (void *)&__30_gpu_v_p_metrics_ddqz_z_full_e, (void *)&__30_gpu_v_p_prog_vn, (void *)&__30_gpu_v_v_ptr_patch_edges_area_edge, (void *)&__30_gpu_v_v_ptr_patch_edges_inv_primal_edge_length, (void *)&__30_gpu_v_v_ptr_patch_edges_tangent_orientation, (void *)&__30_gpu_z_w_con_c_full, (void *)&__30_gpu_zeta, (void *)&__f2dace_SA_area_edge_d_0_s_3245_edges_ptr_patch_15, (void *)&__f2dace_SA_area_edge_d_1_s_3246_edges_ptr_patch_15, (void *)&__f2dace_SA_c_lin_e_d_0_s_3956_p_int_38, (void *)&__f2dace_SA_c_lin_e_d_1_s_3957_p_int_38, (void *)&__f2dace_SA_c_lin_e_d_2_s_3958_p_int_38, (void *)&__f2dace_SA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, (void *)&__f2dace_SA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, (void *)&__f2dace_SA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, (void *)&__f2dace_SA_geofac_grdiv_d_0_s_4044_p_int_38, (void *)&__f2dace_SA_geofac_grdiv_d_1_s_4045_p_int_38, (void *)&__f2dace_SA_geofac_grdiv_d_2_s_4046_p_int_38, (void *)&__f2dace_SA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, (void *)&__f2dace_SA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_blk_d_0_s_3193_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_blk_d_1_s_3194_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_blk_d_2_s_3195_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_0_s_3190_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_1_s_3191_edges_ptr_patch_15, (void *)&__f2dace_SA_quad_idx_d_2_s_3192_edges_ptr_patch_15, (void *)&__f2dace_SA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, (void *)&__f2dace_SA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SA_vn_d_2_s_4760_p_prog_43, (void *)&__f2dace_SOA_area_edge_d_0_s_3245_edges_ptr_patch_15, (void *)&__f2dace_SOA_area_edge_d_1_s_3246_edges_ptr_patch_15, (void *)&__f2dace_SOA_c_lin_e_d_0_s_3956_p_int_38, (void *)&__f2dace_SOA_c_lin_e_d_1_s_3957_p_int_38, (void *)&__f2dace_SOA_c_lin_e_d_2_s_3958_p_int_38, (void *)&__f2dace_SOA_cell_blk_d_0_s_3179_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_1_s_3180_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_blk_d_2_s_3181_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_0_s_3176_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_1_s_3177_edges_ptr_patch_15, (void *)&__f2dace_SOA_cell_idx_d_2_s_3178_edges_ptr_patch_15, (void *)&__f2dace_SOA_ddqz_z_full_e_d_0_s_5242_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_full_e_d_1_s_5243_p_metrics_44, (void *)&__f2dace_SOA_ddqz_z_full_e_d_2_s_5244_p_metrics_44, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_0_s_4998_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_1_s_4999_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_2_s_5000_p_diag_45, (void *)&__f2dace_SOA_ddt_vn_apc_pc_d_3_s_5001_p_diag_45, (void *)&__f2dace_SOA_geofac_grdiv_d_0_s_4044_p_int_38, (void *)&__f2dace_SOA_geofac_grdiv_d_1_s_4045_p_int_38, (void *)&__f2dace_SOA_geofac_grdiv_d_2_s_4046_p_int_38, (void *)&__f2dace_SOA_inv_primal_edge_length_d_0_s_3231_edges_ptr_patch_15, (void *)&__f2dace_SOA_inv_primal_edge_length_d_1_s_3232_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_blk_d_0_s_3193_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_blk_d_1_s_3194_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_blk_d_2_s_3195_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_0_s_3190_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_1_s_3191_edges_ptr_patch_15, (void *)&__f2dace_SOA_quad_idx_d_2_s_3192_edges_ptr_patch_15, (void *)&__f2dace_SOA_tangent_orientation_d_0_s_3188_edges_ptr_patch_15, (void *)&__f2dace_SOA_tangent_orientation_d_1_s_3189_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_0_s_3185_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_1_s_3186_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_blk_d_2_s_3187_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_0_s_3182_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_1_s_3183_edges_ptr_patch_15, (void *)&__f2dace_SOA_vertex_idx_d_2_s_3184_edges_ptr_patch_15, (void *)&__f2dace_SOA_vn_d_0_s_4758_p_prog_43, (void *)&__f2dace_SOA_vn_d_1_s_4759_p_prog_43, (void *)&__f2dace_SOA_vn_d_2_s_4760_p_prog_43, (void *)&_for_it_39, (void *)&cfl_w_limit, (void *)&dtime, (void *)&i_endidx, (void *)&i_startidx, (void *)&nlev, (void *)&ntnd, (void *)&scalfac_exdiff, (void *)&tmp_arg_6, (void *)&tmp_struct_symbol_15, (void *)&tmp_struct_symbol_2, (void *)&tmp_struct_symbol_3, (void *)&tmp_struct_symbol_8, (void *)&tmp_struct_symbol_9 };
    gpuError_t __err = cudaLaunchKernel((void*)single_state_body_2_map_30_7_7, dim3(int_ceil(int_ceil(((nlev - Max(3, tmp_arg_6)) - 3), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1), dim3(32, 1, 1), single_state_body_2_map_30_7_7_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "single_state_body_2_map_30_7_7", int_ceil(int_ceil(((nlev - Max(3, tmp_arg_6)) - 3), 1), 32), int_ceil(((i_endidx - i_startidx) + 1), 1), 1, 32, 1, 1);
}

