# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CUDA"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CUDA
  "/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/src/cuda/velocity_tendencies_cuda.cu" "/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/build/CMakeFiles/velocity_tendencies.dir/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/src/cuda/velocity_tendencies_cuda.cu.o"
  )
set(CMAKE_CUDA_COMPILER_ID "NVIDIA")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CUDA
  "DACE_BINARY_DIR=\"/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/build\""
  "WITH_CUDA"
  "velocity_tendencies_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CUDA_TARGET_INCLUDE_PATH
  "/scratch/snx3000/lfusco/dace/dace/codegen/../runtime/include"
  "/users/lfusco/spack/opt/spack/linux-sles15-haswell/gcc-12.4.0/cuda-12.4.1-kd2dbrwmmsxadnfjq4o47kj27ksxy24p/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/src/cpu/velocity_tendencies.cpp" "/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/build/CMakeFiles/velocity_tendencies.dir/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/src/cpu/velocity_tendencies.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DACE_BINARY_DIR=\"/users/lfusco/dace_experiments/velocity_tendencies/.dacecache/velocity_tendencies/build\""
  "WITH_CUDA"
  "velocity_tendencies_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/scratch/snx3000/lfusco/dace/dace/codegen/../runtime/include"
  "/users/lfusco/spack/opt/spack/linux-sles15-haswell/gcc-12.4.0/cuda-12.4.1-kd2dbrwmmsxadnfjq4o47kj27ksxy24p/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
