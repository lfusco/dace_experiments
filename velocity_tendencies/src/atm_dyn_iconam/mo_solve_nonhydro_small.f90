!!
!! mo_solve_nonhydro
!!
!! This module contains the nonhydrostatic dynamical core for the triangular version
!! Its routines were previously contained in mo_divergent_modes and mo_vector_operations
!! but have been extracted for better memory efficiency
!!
!! @author Guenther Zaengl, DWD
!!
!! @par Revision History
!! Initial release by Guenther Zaengl (2010-10-13) based on earlier work
!! by Almut Gassmann, MPI-M
!! Modification by William Sawyer, CSCS (2015-02-06)
!! - OpenACC implementation
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!

!----------------------------

!----------------------------

MODULE mo_solve_nonhydro

  USE mo_kind,                 ONLY: wp, vp
  USE mo_nonhydrostatic_config,ONLY: itime_scheme,iadv_rhotheta, igradp_method,             &
                                     kstart_moist, lhdiff_rcf, divdamp_order,               &
                                     divdamp_fac, divdamp_fac2, divdamp_fac3, divdamp_fac4, &
                                     divdamp_z, divdamp_z2, divdamp_z3, divdamp_z4,         &
                                     divdamp_type, rayleigh_type, rhotheta_offctr,          &
                                     veladv_offctr, divdamp_fac_o2, kstart_dd3d, ndyn_substeps_var
  USE mo_dynamics_config,   ONLY: idiv_method
  USE mo_parallel_config,   ONLY: nproma, p_test_run, itype_comm, use_dycore_barrier, &
    & cpu_min_nproma
  USE mo_run_config,        ONLY: ltimer, timers_level, lvert_nest
  USE mo_model_domain,      ONLY: t_patch
  USE mo_grid_config,       ONLY: l_limited_area
  USE mo_gridref_config,    ONLY: grf_intmethod_e
  USE mo_interpol_config,   ONLY: nudge_max_coeff
  USE mo_intp_data_strc,    ONLY: t_int_state
  !USE mo_intp,              ONLY: cells2verts_scalar
  USE mo_icon_interpolation_scalar, ONLY: cells2verts_scalar
  USE mo_nonhydro_types,    ONLY: t_nh_state
  USE mo_physical_constants,ONLY: cpd, rd, cvd, cvd_o_rd, grav, rd_o_cpd, p0ref
  USE mo_math_gradients,    ONLY: grad_green_gauss_cell
  USE mo_math_constants,    ONLY: dbl_eps
  USE mo_math_divrot,       ONLY: div_avg
  USE mo_vertical_grid,     ONLY: nrdmax, nflat_gradp
  USE mo_init_vgrid,        ONLY: nflatlev
  USE mo_loopindices,       ONLY: get_indices_c, get_indices_e
  USE mo_impl_constants,    ONLY: min_rlcell_int, min_rledge_int, min_rlvert_int, &
    &                             min_rlcell, RAYLEIGH_CLASSIC, RAYLEIGH_KLEMP
  USE mo_impl_constants_grf,ONLY: grf_bdywidth_c, grf_bdywidth_e
  !USE mo_sync,              ONLY: SYNC_E, SYNC_C, sync_patch_array,                             &
  !                                sync_patch_array_mult, sync_patch_array_mult_mp
  USE mo_mpi,               ONLY: my_process_is_mpi_all_seq, work_mpi_barrier, i_am_accel_node
  USE mo_timer,             ONLY: timer_solve_nh, timer_barrier, timer_start, timer_stop,       &
                                  timer_solve_nh_cellcomp, timer_solve_nh_edgecomp,             &
                                  timer_solve_nh_vnupd, timer_solve_nh_vimpl, timer_solve_nh_exch
  USE mo_icon_comm_lib,     ONLY: icon_comm_sync
  USE mo_vertical_coord_table,ONLY: vct_a
  USE mo_prepadv_types,     ONLY: t_prepare_adv
  USE mo_initicon_config,   ONLY: is_iau_active, iau_wgt_dyn
  USE mo_fortran_tools,     ONLY: init_zero_contiguous_dp, init_zero_contiguous_sp ! Import both for mixed prec.





  IMPLICIT NONE

  PRIVATE


  REAL(wp), PARAMETER :: rd_o_cvd = 1._wp / cvd_o_rd
  REAL(wp), PARAMETER :: cpd_o_rd = 1._wp / rd_o_cpd
  REAL(wp), PARAMETER :: rd_o_p0ref = rd / p0ref
  REAL(wp), PARAMETER :: grav_o_cpd = grav / cpd

  PUBLIC :: solve_nh





  ! On the vectorizing DWD-NEC the diagnostics for the tendencies of the normal wind
  ! from terms xyz, ddt_vn_xyz, is disabled by default due to the fear that the
  ! conditional storage in conditionally allocated global fields is attempted even if
  ! the condition is not given and therefore the global field not allocated. If this
  ! happens, this would results in a corrupted memory.
  ! (Requested by G. Zaengl based on earlier problems with similar constructs.)




  CONTAINS


  !>
  !! solve_nh
  !!
  !! Main solver routine for nonhydrostatic dynamical core
  !!
  !! @par Revision History
  !! Development started by Guenther Zaengl on 2010-02-03
  !! Modification by Sebastian Borchert, DWD (2017-07-07)
  !! (Dear developer, for computational efficiency reasons, a copy of this subroutine 
  !! exists in 'src/atm_dyn_iconam/mo_nh_deepatmo_solve'. If you would change something here, 
  !! please consider to apply your development there, too, in order to help preventing 
  !! the copy from diverging and becoming a code corpse sooner or later. Thank you!)
  !!
  SUBROUTINE solve_nh (p_nh, ptr_patch, p_int, prep_adv, nnow, nnew, l_init, l_recompute, lsave_mflx, &
                       lprep_adv, lclean_mflx, idyn_timestep, jstep, dtime)

    TYPE(t_nh_state),    TARGET, INTENT(INOUT) :: p_nh
    TYPE(t_int_state),   TARGET, INTENT(IN)    :: p_int
    TYPE(t_patch),       TARGET, INTENT(INOUT) :: ptr_patch
    TYPE(t_prepare_adv), TARGET, INTENT(INOUT) :: prep_adv

    ! Initialization switch that has to be .TRUE. at the initial time step only (not for restart)
    LOGICAL,                   INTENT(IN)    :: l_init
    ! Switch to recompute velocity tendencies after a physics call irrespective of the time scheme option
    LOGICAL,                   INTENT(IN)    :: l_recompute
    ! Switch if mass flux needs to be saved for nest boundary interpolation tendency computation
    LOGICAL,                   INTENT(IN)    :: lsave_mflx
    ! Switch if preparations for tracer advection shall be computed
    LOGICAL,                   INTENT(IN)    :: lprep_adv
    ! Switch if mass fluxes computed for tracer advection need to be reinitialized
    LOGICAL,                   INTENT(IN)    :: lclean_mflx
    ! Counter of dynamics time step within a large time step (ranges from 1 to ndyn_substeps)
    INTEGER,                   INTENT(IN)    :: idyn_timestep
    ! Time step count since last boundary interpolation (ranges from 0 to 2*ndyn_substeps-1)
    INTEGER,                   INTENT(IN)    :: jstep
    ! Time levels
    INTEGER,                   INTENT(IN)    :: nnow, nnew
    ! Dynamics time step
    REAL(wp),                  INTENT(IN)    :: dtime

    ! Local variables
    INTEGER  :: jb, jk, jc, je, jks, jg
    INTEGER  :: nlev, nlevp1              !< number of full levels
    INTEGER  :: i_startblk, i_endblk, i_startidx, i_endidx, ishift
    INTEGER  :: rl_start, rl_end, istep, ntl1, ntl2, nvar, nshift, nshift_total
    INTEGER  :: ic, ie, ilc0, ibc0, ikp1, ikp2

    REAL(wp) :: z_theta_v_fl_e  (nproma,ptr_patch%nlev  ,ptr_patch%nblks_e), &
                z_theta_v_e     (nproma,ptr_patch%nlev  ,ptr_patch%nblks_e), &
                z_rho_e         (nproma,ptr_patch%nlev  ,ptr_patch%nblks_e), &
                z_mass_fl_div   (nproma,ptr_patch%nlev  ,ptr_patch%nblks_c), & ! used for idiv_method=2 only
                z_theta_v_fl_div(nproma,ptr_patch%nlev  ,ptr_patch%nblks_c), & ! used for idiv_method=2 only
                z_theta_v_v     (nproma,ptr_patch%nlev  ,ptr_patch%nblks_v), & ! used for iadv_rhotheta=1 only
                z_rho_v         (nproma,ptr_patch%nlev  ,ptr_patch%nblks_v)    ! used for iadv_rhotheta=1 only

    ! The data type vp (variable precision) is by default the same as wp but reduces
    ! to single precision when the __MIXED_PRECISION cpp flag is set at compile time
    REAL(vp) :: z_th_ddz_exner_c(nproma,ptr_patch%nlev,ptr_patch%nblks_c), &
                z_dexner_dz_c (2,nproma,ptr_patch%nlev,ptr_patch%nblks_c), &
                z_vt_ie         (nproma,ptr_patch%nlev,ptr_patch%nblks_e), &
                z_kin_hor_e     (nproma,ptr_patch%nlev,ptr_patch%nblks_e), &
                z_exner_ex_pr (nproma,ptr_patch%nlevp1,ptr_patch%nblks_c), & ! nlevp1 is intended here
                z_gradh_exner   (nproma,ptr_patch%nlev,ptr_patch%nblks_e), &
                z_rth_pr      (2,nproma,ptr_patch%nlev,ptr_patch%nblks_c), &
                z_grad_rth    (4,nproma,ptr_patch%nlev,ptr_patch%nblks_c), &
                z_w_concorr_me  (nproma,ptr_patch%nlev,ptr_patch%nblks_e)

    ! This field in addition has reversed index order (vertical first) for optimization

    REAL(vp) :: z_graddiv_vn    (ptr_patch%nlev,nproma,ptr_patch%nblks_e)




    REAL(wp) :: z_w_expl        (nproma,ptr_patch%nlevp1),          &
                z_vn_avg        (nproma,ptr_patch%nlev  ),          &
                z_mflx_top      (nproma,ptr_patch%nblks_c),         &
                z_contr_w_fl_l  (nproma,ptr_patch%nlevp1),          &
                z_rho_expl      (nproma,ptr_patch%nlev  ),          &
                z_exner_expl    (nproma,ptr_patch%nlev  )
    REAL(wp) :: z_theta_tavg_m1, z_theta_tavg, z_rho_tavg_m1, z_rho_tavg



    ! The data type vp (variable precision) is by default the same as wp but reduces
    ! to single precision when the __MIXED_PRECISION cpp flag is set at compile time

    ! TODO :  of these, fairly easy to scalarize:  z_theta_v_pr_ic
    REAL(vp) :: z_alpha         (nproma,ptr_patch%nlevp1),          &
                z_beta          (nproma,ptr_patch%nlev  ),          &
                z_q             (nproma,ptr_patch%nlev  ),          &
                z_graddiv2_vn   (nproma,ptr_patch%nlev  ),          &
                z_theta_v_pr_ic (nproma,ptr_patch%nlevp1),          &
                z_exner_ic      (nproma,ptr_patch%nlevp1),          &
                z_w_concorr_mc  (nproma,ptr_patch%nlev  ),          &
                z_flxdiv_mass   (nproma,ptr_patch%nlev  ),          &
                z_flxdiv_theta  (nproma,ptr_patch%nlev  ),          &
                z_hydro_corr    (nproma,ptr_patch%nblks_e)

    REAL(vp) :: z_a, z_b, z_c, z_g, z_gamma,      &
                z_w_backtraj, z_theta_v_pr_mc_m1, z_theta_v_pr_mc





    REAL(wp) :: z_theta1, z_theta2, wgt_nnow_vel, wgt_nnew_vel,     &
               dt_shift, wgt_nnow_rth, wgt_nnew_rth, dthalf,        &
               r_nsubsteps, r_dtimensubsteps, scal_divdamp_o2,      &
               alin, dz32, df32, dz42, df42, bqdr, aqdr,            &
               zf, dzlin, dzqdr
    ! time shifts for linear interpolation of nest UBC
    REAL(wp) :: dt_linintp_ubc, dt_linintp_ubc_nnow, dt_linintp_ubc_nnew
    REAL(wp) :: z_raylfac(nrdmax(ptr_patch%id))
    REAL(wp) :: z_ntdistv_bary_1, distv_bary_1, z_ntdistv_bary_2, distv_bary_2

    REAL(wp), DIMENSION(ptr_patch%nlev) :: scal_divdamp, bdy_divdamp, enh_divdamp_fac, tmp_scal_divdamp
    INTEGER:: my_index
    !REAL(vp) :: z_dwdz_dd(nproma,my_index:ptr_patch%nlev,ptr_patch%nblks_c)
    REAL(vp) :: z_dwdz_dd(nproma,ptr_patch%nlev,ptr_patch%nblks_c)

    ! Local variables for normal wind tendencies and differentials
    REAL(wp) :: z_ddt_vn_dyn, z_ddt_vn_apc, z_ddt_vn_cor, &
      &         z_ddt_vn_pgr, z_ddt_vn_ray,               &
      &         z_d_vn_dmp, z_d_vn_iau


    INTEGER :: nproma_gradp, nblks_gradp, npromz_gradp, nlen_gradp, jk_start
    LOGICAL :: lvn_only, lvn_pos

    ! Local variables to control vertical nesting
    LOGICAL :: l_vert_nested, l_child_vertnest

    ! Pointers
    INTEGER, POINTER, CONTIGUOUS :: &
      ! to cell indices
      icidx(:,:,:), icblk(:,:,:), &
      ! to edge indices
      ieidx(:,:,:), ieblk(:,:,:), &
      ! to vertex indices
      ividx(:,:,:), ivblk(:,:,:), &
      ! to vertical neighbor indices for pressure gradient computation
      ikidx(:,:,:,:),             &
      ! to quad edge indices
      iqidx(:,:,:), iqblk(:,:,:), &
      ! for igradp_method = 3
      iplev(:), ipeidx(:), ipeblk(:)





    my_index=kstart_dd3d(ptr_patch%id)
    nrdmax(1)=9
    nflatlev(1)=31
    
    jg = ptr_patch%id

    IF (lvert_nest .AND. (ptr_patch%nshift_total > 0)) THEN
      l_vert_nested = .TRUE.
      nshift_total  = ptr_patch%nshift_total
    ELSE
      l_vert_nested = .FALSE.
      nshift_total  = 0
    ENDIF
    IF (lvert_nest .AND. ptr_patch%n_childdom > 0 .AND.              &
      (ptr_patch%nshift_child > 0 .OR. ptr_patch%nshift_total > 0)) THEN
      l_child_vertnest = .TRUE.
      nshift = ptr_patch%nshift_child + 1
    ELSE
      l_child_vertnest = .FALSE.
      nshift = 0
    ENDIF
    dthalf  = 0.5_wp*dtime

    !IF (ltimer) CALL timer_start(timer_solve_nh)

    ! Inverse value of ndyn_substeps for tracer advection precomputations
    r_nsubsteps = 1._wp/REAL(ndyn_substeps_var(jg),wp)

    ! Inverse value of dtime * ndyn_substeps_var
    r_dtimensubsteps = 1._wp/(dtime*REAL(ndyn_substeps_var(jg),wp))

    ! number of vertical levels
    nlev   = ptr_patch%nlev
    nlevp1 = ptr_patch%nlevp1

    ! Set pointers to neighbor cells
    icidx => ptr_patch%edges%cell_idx
    icblk => ptr_patch%edges%cell_blk

    ! Set pointers to neighbor edges
    ieidx => ptr_patch%cells%edge_idx
    ieblk => ptr_patch%cells%edge_blk

    ! Set pointers to vertices of an edge
    ividx => ptr_patch%edges%vertex_idx
    ivblk => ptr_patch%edges%vertex_blk

    ! Set pointer to vertical neighbor indices for pressure gradient
    ikidx => p_nh%metrics%vertidx_gradp

    ! Set pointers to quad edges
    iqidx => ptr_patch%edges%quad_idx
    iqblk => ptr_patch%edges%quad_blk

    ! DA: moved from below to here to get into the same ACC data section
    iplev  => p_nh%metrics%pg_vertidx
    ipeidx => p_nh%metrics%pg_edgeidx
    ipeblk => p_nh%metrics%pg_edgeblk

    
    ! Precompute Rayleigh damping factor
    DO jk = 2, nrdmax(jg)
       z_raylfac(jk) = 1.0_wp/(1.0_wp+dtime*p_nh%metrics%rayleigh_w(jk))
    ENDDO

    ! Fourth-order divergence damping
    !
    ! The divergence damping factor enh_divdamp_fac is defined as a profile in height z
    ! above sea level with 4 height sections:
    !
    ! enh_divdamp_fac(z) = divdamp_fac                                              !               z <= divdamp_z
    ! enh_divdamp_fac(z) = divdamp_fac  + (z-divdamp_z )* alin                      ! divdamp_z  <= z <= divdamp_z2
    ! enh_divdamp_fac(z) = divdamp_fac2 + (z-divdamp_z2)*(aqdr+(z-divdamp_z2)*bqdr) ! divdamp_z2 <= z <= divdamp_z4
    ! enh_divdamp_fac(z) = divdamp_fac4                                             ! divdamp_z4 <= z
    !
    alin = (divdamp_fac2-divdamp_fac)/(divdamp_z2-divdamp_z)
    !
    df32 = divdamp_fac3-divdamp_fac2; dz32 = divdamp_z3-divdamp_z2
    df42 = divdamp_fac4-divdamp_fac2; dz42 = divdamp_z4-divdamp_z2
    !
    bqdr = (df42*dz32-df32*dz42)/(dz32*dz42*(dz42-dz32))
    aqdr = df32/dz32-bqdr*dz32
    !
    DO jk = 1, nlev
      jks = jk + nshift_total
      zf = 0.5_wp*(vct_a(jks)+vct_a(jks+1))
      dzlin = MIN(divdamp_z2-divdamp_z ,MAX(0._wp,zf-divdamp_z ))
      dzqdr = MIN(divdamp_z4-divdamp_z2,MAX(0._wp,zf-divdamp_z2))
      !
      IF (divdamp_order == 24) THEN
        enh_divdamp_fac(jk) = MAX( 0._wp, divdamp_fac + dzlin*alin + dzqdr*(aqdr+dzqdr*bqdr) - 0.25_wp*divdamp_fac_o2 )
      ELSE
        enh_divdamp_fac(jk) =             divdamp_fac + dzlin*alin + dzqdr*(aqdr+dzqdr*bqdr)
      ENDIF
    ENDDO

    scal_divdamp(:) = - enh_divdamp_fac(:) * ptr_patch%geometry_info%mean_cell_area**2

    ! Time increment for backward-shifting of lateral boundary mass flux
    dt_shift = dtime*REAL(2*ndyn_substeps_var(jg)-1,wp)/2._wp    ! == dt_phy - 0.5*dtime

    ! Time increment for linear interpolation of nest UBC.
    ! The linear interpolation is of the form
    ! \phi(t) = \phi0 + (t-t0)*dphi/dt, with t=(jstep+0.5)*dtime, and t0=dt_phy
    !
    ! dt_linintp_ubc == (t-t0)
    dt_linintp_ubc      = jstep*dtime - dt_shift ! valid for center of current time step
    dt_linintp_ubc_nnow = dt_linintp_ubc - 0.5_wp*dtime
    dt_linintp_ubc_nnew = dt_linintp_ubc + 0.5_wp*dtime

    ! Coefficient for reduced fourth-order divergence damping along nest boundaries
    DO jk = 1, nlev
    tmp_scal_divdamp(jk)=ABS(scal_divdamp(jk))
    ENDDO

    bdy_divdamp(:) = 0.75_wp/(nudge_max_coeff + dbl_eps)*tmp_scal_divdamp(:)
    !bdy_divdamp(:) = 0.75_wp/(nudge_max_coeff + dbl_eps)*ABS(scal_divdamp(:))

    !$ACC DATA CREATE(z_kin_hor_e, z_vt_ie, z_w_concorr_me, z_mass_fl_div, z_theta_v_fl_e, z_theta_v_fl_div) &
    !$ACC   CREATE(z_dexner_dz_c, z_exner_ex_pr, z_gradh_exner, z_rth_pr, z_grad_rth) &
    !$ACC   CREATE(z_theta_v_pr_ic, z_th_ddz_exner_c, z_w_concorr_mc) &
    !$ACC   CREATE(z_vn_avg, z_rho_e, z_theta_v_e, z_dwdz_dd, z_mflx_top) &
    !$ACC   CREATE(z_exner_ic, z_alpha, z_beta, z_q, z_contr_w_fl_l, z_exner_expl) &
    !$ACC   CREATE(z_flxdiv_mass, z_flxdiv_theta, z_rho_expl, z_w_expl) &
    !$ACC   CREATE(z_rho_v, z_theta_v_v, z_graddiv_vn, z_hydro_corr, z_graddiv2_vn) &
    !$ACC   COPYIN(nflatlev, nflat_gradp, kstart_dd3d, kstart_moist, nrdmax) &
    !$ACC   COPYIN(z_raylfac, ndyn_substeps_var, scal_divdamp, bdy_divdamp) &
    !$ACC   PRESENT(prep_adv, p_int, ptr_patch, p_nh) &
    !$ACC   PRESENT(icidx, icblk, ividx, ivblk, ieidx, ieblk, ikidx, iqidx, iqblk) &
    !$ACC   PRESENT(ipeidx, ipeblk, iplev) &
    !$ACC   IF(i_am_accel_node)

    ! scaling factor for second-order divergence damping: divdamp_fac_o2*delta_x**2
    ! delta_x**2 is approximated by the mean cell area
    scal_divdamp_o2 = divdamp_fac_o2 * ptr_patch%geometry_info%mean_cell_area


    IF (p_test_run) THEN
      !$ACC KERNELS IF(i_am_accel_node) DEFAULT(PRESENT) ASYNC(1)
      z_rho_e(:,:,:)     = 0._wp
      z_theta_v_e(:,:,:) = 0._wp
      z_dwdz_dd(:,:,:)   = 0._wp
      z_graddiv_vn(:,:,:)= 0._wp
      !$ACC END KERNELS
    ENDIF

    ! Set time levels of ddt_adv fields for call to velocity_tendencies
    IF (itime_scheme >= 4) THEN ! Velocity advection averaging nnow and nnew tendencies
      ntl1 = nnow
      ntl2 = nnew
    ELSE                        ! Velocity advection is taken at nnew only
      ntl1 = 1
      ntl2 = 1
    ENDIF

    ! Weighting coefficients for velocity advection if tendency averaging is used
    ! The off-centering specified here turned out to be beneficial to numerical
    ! stability in extreme situations
    wgt_nnow_vel = 0.5_wp - veladv_offctr ! default value for veladv_offctr is 0.25
    wgt_nnew_vel = 0.5_wp + veladv_offctr

    ! Weighting coefficients for rho and theta at interface levels in the corrector step
    ! This empirically determined weighting minimizes the vertical wind off-centering
    ! needed for numerical stability of vertical sound wave propagation
    wgt_nnew_rth = 0.5_wp + rhotheta_offctr ! default value for rhotheta_offctr is -0.1
    wgt_nnow_rth = 1._wp - wgt_nnew_rth

      ! Compute rho and theta at edges for horizontal flux divergence term
      IF (istep == 1) THEN
        IF (iadv_rhotheta == 1) THEN ! Simplified Miura scheme
          !DA: TODO: remove the wait after everything is async
          !$ACC WAIT
          ! Compute density and potential temperature at vertices
          CALL cells2verts_scalar(p_nh%prog(nnow)%rho,ptr_patch, p_int%cells_aw_verts, &
            z_rho_v, opt_rlend=min_rlvert_int-1)
       
        ENDIF
      ENDIF ! istep = 1


  END SUBROUTINE solve_nh



END MODULE mo_solve_nonhydro



















































