



!>
!! @brief Namelist reading for synthetic radar data on the model grid
!!
!! Namelist reading for synthetic radar data on the model grid
!!
!! @author Ulrich Blahak, DWD
!!
!!
!! @par Revision History
!! Initial revision by Ulrich Blahak, DWD (2021-11-29)
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!
MODULE mo_synradar_nml



  IMPLICIT NONE
  PUBLIC :: read_synradar_namelist

  ! module name
  CHARACTER(*), PARAMETER :: modname = "mo_synradar_nml"
  
CONTAINS
  !>
  !! Read Namelist for I/O.
  !!
  !! This subroutine
  !! - reads the Namelist for I/O
  !! - sets default values
  !! - potentially overwrites the defaults by values used in a
  !!   previous integration (if this is a resumed run)
  !! - reads the user's (new) specifications
  !! - stores the Namelist for restart
  !! - fills the configuration state (partly)
  !!
  !! @par Revision History
  !!  by Ulrich Blahak, DWD (2021-11-30)
  !!
  SUBROUTINE read_synradar_namelist( filename )

    CHARACTER(LEN=*), INTENT(IN)   :: filename


    
  END SUBROUTINE read_synradar_namelist

END MODULE mo_synradar_nml


















































