
MODULE mo_math_utility_solvers2
  !-------------------------------------------------------------------------
  !
  !    ProTeX FORTRAN source: Style 2
  !    modified for ICON project, DWD/MPI-M 2006
  !
  !-------------------------------------------------------------------------
  !
  USE mo_kind,                ONLY: wp
  USE mo_exception,           ONLY: message, finish
  !USE mo_parallel_config,     ONLY: nproma
  INTEGER, PARAMETER :: nproma
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: solve_lu
  

 
  PUBLIC :: inv_mat

  CHARACTER(len=*), PARAMETER :: modname = 'mo_math_utility_solvers'

CONTAINS

  
  !-------------------------------------------------------------------------

  !-------------------------------------------------------------------------
  !>
  !! Compute the inverse of matrix a (use only for VERY small matrices!).
  !!
  !!
  !! @par Revision History
  !! Original version by Marco Restelli (2007-11-22)
  !!
  SUBROUTINE inv_mat (a)

    REAL(wp), INTENT(inout)   :: a(:,:)     ! input matrix

    INTEGER :: k_pivot(SIZE(a,1)), j
    REAL(wp) :: b(SIZE(a,1),SIZE(a,1)), e(SIZE(a,1))
    !-----------------------------------------------------------------------

    
    DO j=1,SIZE(a,1)
      CALL solve_lu(SIZE(a,1),b,e,a(:,j),k_pivot)
    ENDDO

  END SUBROUTINE inv_mat
  !-------------------------------------------------------------------------

  

  !-------------------------------------------------------------------------
  !>
  !!  home made routine for backsubstitution.
  !!
  !!  home made routine for backsubstitution
  !!  to be substituted by call from linear algebra library
  !!  dim: matrix dimension
  !!  a: input matrix  lu decomposed by ludec (otherwise it does not work)
  !!  b: right hand side, row pivot  exchanges to be performed using ind
  !!  ind: integer index array recording row pivot  exchanges
  !!
  SUBROUTINE solve_lu (k_dim, p_a, p_b, p_x, k_pivot)
    !
    INTEGER,  INTENT(in)    :: k_dim                   ! matrix dimension
    INTEGER,  INTENT(in)    :: k_pivot(k_dim)          ! pivoting index
    REAL(wp), INTENT(in)    :: p_a(k_dim,k_dim)        ! matrix

    REAL(wp), INTENT(inout) :: p_b(k_dim)              ! input vector

    REAL(wp), INTENT(out)   :: p_x(k_dim)              ! output vector

    INTEGER :: ji, jj, ip              ! integer over dimensions
    REAL(wp)                :: z_sum                   ! sum

    !-----------------------------------------------------------------------
    ! forward
    p_x(1) = 5.6
    p_b(1) = p_a(1,1)*k_pivot(1)

  END SUBROUTINE solve_lu
  !-------------------------------------------------------------------------


END MODULE mo_math_utility_solvers2



















































