



!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
MODULE mo_util_backtrace





  IMPLICIT NONE

  PRIVATE

  PUBLIC :: util_backtrace



  INTERFACE
    SUBROUTINE util_backtrace() BIND(C)
    END SUBROUTINE util_backtrace
  END INTERFACE



END MODULE mo_util_backtrace



















































