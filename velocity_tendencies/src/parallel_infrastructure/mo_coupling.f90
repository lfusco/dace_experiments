



!----------------------------------
!>
!! Routines to access basic coupler functionality
!!
!! @par Revision History
!! First version by Moritz Hanke,  DKRZ, May 2022.
!!
!! @par
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!
!----------------------------



!--------------------------------------------------
! timers definition
!needs:
!   USE mo_timer, ONLY: timer_start, timer_stop, timers_level, <timers_names>...
!


















!----------------------------
MODULE mo_coupling

  USE mo_io_units, ONLY: nerr






  IMPLICIT NONE

  PRIVATE

  PUBLIC :: init_coupler
  PUBLIC :: finalize_coupler
  PUBLIC :: coupler_config_files_exist

  CHARACTER(LEN=*), PARAMETER :: xml_filename = "coupling.xml"
  CHARACTER(LEN=*), PARAMETER :: xsd_filename = "coupling.xsd"

  LOGICAL :: config_files_have_been_checked = .FALSE.
  LOGICAL :: config_files_exist = .FALSE.
  LOGICAL :: yac_is_initialised = .FALSE.

  CHARACTER(*), PARAMETER :: modname = "mo_coupling"

CONTAINS

  LOGICAL FUNCTION coupler_config_files_exist()

    LOGICAL :: xml_exists, xsd_exists

    IF (config_files_have_been_checked) THEN

      coupler_config_files_exist = config_files_exist

    ELSE

      INQUIRE(FILE=TRIM(ADJUSTL(xml_filename)), EXIST=xml_exists)
      INQUIRE(FILE=TRIM(ADJUSTL(xsd_filename)), EXIST=xsd_exists)

      config_files_have_been_checked = .TRUE.
      config_files_exist = xml_exists .AND. xsd_exists
      coupler_config_files_exist = config_files_exist

    END IF

  END FUNCTION

  SUBROUTINE init_coupler(world_communicator, global_name)

    INTEGER, INTENT(INOUT) :: world_communicator
    CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: global_name



  END SUBROUTINE init_coupler

  SUBROUTINE finalize_coupler





  END SUBROUTINE finalize_coupler

  SUBROUTINE print_info_stderr (name, text)
    CHARACTER (len=*), INTENT(in) :: name
    CHARACTER (len=*), INTENT(in) :: text

    WRITE (nerr,'(4a)') " ", TRIM(name), ": ", TRIM(text)

  END SUBROUTINE print_info_stderr

END MODULE mo_coupling
























































