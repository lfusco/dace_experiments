
MODULE ppm_extents
  IMPLICIT NONE
  PRIVATE
  
  !TYPE, PUBLIC :: extent
  TYPE extent
    !SEQUENCE
    !> range is anchored by this value
    INTEGER :: first
    !> range has this size
    INTEGER :: size
  END TYPE extent

END MODULE ppm_extents
