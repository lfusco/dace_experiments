# 1 "src/atm_phy_aes/mo_cloud_mig/mo_cloud_mig_config.f90"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "src/atm_phy_aes/mo_cloud_mig/mo_cloud_mig_config.f90"
!>
!! Configuration of the parameterization for cloud microphysics "graupel",
!! from DWD that is used in the sapphire physics package.
!!
!! @author Monika Esch, MPI-M
!!
!! @par Revision History
!! First version by Monika Esch, MPI-M (2018-06)
!!
!! Based on earlier codes of:
!!     ...
!!
!! References: 
!!     ...
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!
MODULE mo_cloud_mig_config

  USE mo_exception            ,ONLY: message, print_value
  USE mo_kind                 ,ONLY: wp
  USE mo_impl_constants       ,ONLY: max_dom

  USE mo_cloud_mig_types      ,ONLY: t_cloud_mig_config

  IMPLICIT NONE
  PRIVATE

  ! configuration
  PUBLIC ::         cloud_mig_config   !< user specified configuration parameters
  PUBLIC ::    init_cloud_mig_config   !< allocate and initialize cloud_mig_config
!!$  PUBLIC ::    eval_cloud_mig_config   !< evaluate cloud_mig_config
  PUBLIC ::   print_cloud_mig_config   !< print out

  !>
  !! Configuration state vectors, for multiple domains/grids.
  !!
  TYPE(t_cloud_mig_config), TARGET :: cloud_mig_config(max_dom)
  
CONTAINS

  !----

  !>
  !! Initialize the configuration state vector
  !!
  SUBROUTINE init_cloud_mig_config
    !
    ! Graupel microphyiscs configuration
    ! --------------------------------------
    !
    ! grid scale microphysics
    cloud_mig_config(:)% zceff_min      = 0.01_wp
    cloud_mig_config(:)% v0snow         = 25.0_wp
    cloud_mig_config(:)% zvz0i          = 1.25_wp      ! original value of Heymsfield+Donner 1990: 3.29
    cloud_mig_config(:)% icesedi_exp    = 0.33_wp
    cloud_mig_config(:)% mu_rain        = 0.0_wp
    cloud_mig_config(:)% rain_n0_factor = 1.0_wp
    !
  END SUBROUTINE init_cloud_mig_config

  !----

!!$  !>
!!$  !! Evaluate additional derived parameters
!!$  !!
!!$  SUBROUTINE eval_cloud_mig_config
!!$    !
!!$    ...
!!$    !
!!$  END SUBROUTINE eval_cloud_mig_config

  !----

  !>
  !! Print out the user controlled configuration state
  !!
  SUBROUTINE print_cloud_mig_config(ng)
    !
    INTEGER, INTENT(in) :: ng
    !
    INTEGER             :: jg
    CHARACTER(LEN=2)    :: cg
    !
    CALL message    ('','')
    CALL message    ('','========================================================================')
    CALL message    ('','')
    CALL message    ('','Cloud microphyiscs "graupel" configuration')
    CALL message    ('','==========================================')
    CALL message    ('','')
    !
    DO jg = 1,ng
       !
       WRITE(cg,'(i0)') jg
       !
       CALL message    ('','For domain '//cg)
       CALL message    ('','------------')
       CALL message    ('','')
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% zceff_min      ',cloud_mig_config(jg)% zceff_min      )
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% v0snow         ',cloud_mig_config(jg)% v0snow         )
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% zvz0i          ',cloud_mig_config(jg)% zvz0i          )
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% icesedi_exp    ',cloud_mig_config(jg)% icesedi_exp    )
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% mu_rain        ',cloud_mig_config(jg)% mu_rain        )
       CALL print_value('    cloud_mig_config('//TRIM(cg)//')% rain_n0_factor ',cloud_mig_config(jg)% rain_n0_factor )
       CALL message    ('','')
       !
    END DO
    !
  END SUBROUTINE print_cloud_mig_config

  !----

END MODULE mo_cloud_mig_config
#define __ATOMIC_ACQUIRE 2
#define __CHAR_BIT__ 8
#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __ORDER_PDP_ENDIAN__ 3412
#define __GFC_REAL_10__ 1
#define __FINITE_MATH_ONLY__ 0
#define __GNUC_PATCHLEVEL__ 0
#define __GFC_INT_2__ 1
#define __ICON__ 1
#define __SIZEOF_INT__ 4
#define __SIZEOF_POINTER__ 8
#define __GFORTRAN__ 1
#define __GFC_REAL_16__ 1
#define __STDC_HOSTED__ 0
#define __NO_MATH_ERRNO__ 1
#define __SIZEOF_FLOAT__ 4
#define __pic__ 2
#define _LANGUAGE_FORTRAN 1
#define __SIZEOF_LONG__ 8
#define __GFC_INT_8__ 1
#define __NO_ICON_TESTBED__ 1
#define __NO_ICON_WAVES__ 1
#define __SIZEOF_SHORT__ 2
#define __GNUC__ 11
#define __pie__ 2
#define __SIZEOF_LONG_DOUBLE__ 16
#define __LOOP_EXCHANGE 1
#define __BIGGEST_ALIGNMENT__ 16
#define __ATOMIC_RELAXED 0
#define _LP64 1
#define HAVE_FC_ATTRIBUTE_CONTIGUOUS 1
#define __GFC_INT_1__ 1
#define __ORDER_BIG_ENDIAN__ 4321
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __SIZEOF_SIZE_T__ 8
#define __PIC__ 2
#define __SIZEOF_DOUBLE__ 8
#define NOMPI 1
#define __ATOMIC_CONSUME 1
#define __GNUC_MINOR__ 1
#define __GFC_INT_16__ 1
#define __PIE__ 2
#define __LP64__ 1
#define __ATOMIC_SEQ_CST 5
#define __SIZEOF_LONG_LONG__ 8
#define __ATOMIC_ACQ_REL 4
#define __ATOMIC_RELEASE 3
#define __VERSION__ "11.1.0"

