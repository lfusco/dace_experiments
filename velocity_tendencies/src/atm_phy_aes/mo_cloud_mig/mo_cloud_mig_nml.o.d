# 1 "src/atm_phy_aes/mo_cloud_mig/mo_cloud_mig_nml.f90"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "src/atm_phy_aes/mo_cloud_mig/mo_cloud_mig_nml.f90"
!>
!! Read configuration parameters as Fortran namelist from an external file. 
!!
!! @author Monika Esch, MPI-M, 2018-06
!!
!! @par Revision History
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!
MODULE mo_cloud_mig_nml

  USE mo_cloud_mig_config ,ONLY: cloud_mig_config, init_cloud_mig_config
  USE mo_process_nml      ,ONLY: process_nml
  
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: process_cloud_mig_nml

  NAMELIST /cloud_mig_nml/ cloud_mig_config

CONTAINS

  SUBROUTINE process_cloud_mig_nml(filename)
    !
    CHARACTER(LEN=*), INTENT(in) :: filename
    !
    CALL init_cloud_mig_config
    !
    CALL process_nml(filename, 'cloud_mig_nml', nml_read, nml_write)
    !
  CONTAINS
    !
    SUBROUTINE nml_read(funit)
      INTEGER, INTENT(in) :: funit
      READ(funit, NML=cloud_mig_nml)
    END SUBROUTINE nml_read
    !
    SUBROUTINE nml_write(funit)
      INTEGER, INTENT(in) :: funit
      WRITE(funit, NML=cloud_mig_nml)
    END SUBROUTINE nml_write
    !
  END SUBROUTINE process_cloud_mig_nml

END MODULE mo_cloud_mig_nml
#define __ATOMIC_ACQUIRE 2
#define __CHAR_BIT__ 8
#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __ORDER_PDP_ENDIAN__ 3412
#define __GFC_REAL_10__ 1
#define __FINITE_MATH_ONLY__ 0
#define __GNUC_PATCHLEVEL__ 0
#define __GFC_INT_2__ 1
#define __ICON__ 1
#define __SIZEOF_INT__ 4
#define __SIZEOF_POINTER__ 8
#define __GFORTRAN__ 1
#define __GFC_REAL_16__ 1
#define __STDC_HOSTED__ 0
#define __NO_MATH_ERRNO__ 1
#define __SIZEOF_FLOAT__ 4
#define __pic__ 2
#define _LANGUAGE_FORTRAN 1
#define __SIZEOF_LONG__ 8
#define __GFC_INT_8__ 1
#define __NO_ICON_TESTBED__ 1
#define __NO_ICON_WAVES__ 1
#define __SIZEOF_SHORT__ 2
#define __GNUC__ 11
#define __pie__ 2
#define __SIZEOF_LONG_DOUBLE__ 16
#define __LOOP_EXCHANGE 1
#define __BIGGEST_ALIGNMENT__ 16
#define __ATOMIC_RELAXED 0
#define _LP64 1
#define HAVE_FC_ATTRIBUTE_CONTIGUOUS 1
#define __GFC_INT_1__ 1
#define __ORDER_BIG_ENDIAN__ 4321
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __SIZEOF_SIZE_T__ 8
#define __PIC__ 2
#define __SIZEOF_DOUBLE__ 8
#define NOMPI 1
#define __ATOMIC_CONSUME 1
#define __GNUC_MINOR__ 1
#define __GFC_INT_16__ 1
#define __PIE__ 2
#define __LP64__ 1
#define __ATOMIC_SEQ_CST 5
#define __SIZEOF_LONG_LONG__ 8
#define __ATOMIC_ACQ_REL 4
#define __ATOMIC_RELEASE 3
#define __VERSION__ "11.1.0"

