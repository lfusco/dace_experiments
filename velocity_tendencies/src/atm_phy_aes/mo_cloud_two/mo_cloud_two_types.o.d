# 1 "src/atm_phy_aes/mo_cloud_two/mo_cloud_two_types.f90"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "src/atm_phy_aes/mo_cloud_two/mo_cloud_two_types.f90"
!>
!! Data types for the two-moment bulk microphysics by Seifert and Beheng (2006)
!!                  with prognostic cloud droplet number parameterization
!!
!! This module provides the data types for the variables used to
!! configure the parameterization and to store the input and output
!! for the parameterization.
!!
!! @author Monika Esch (MPI-M)
!!
!! @par Revision History
!! First version by Monika Esch, 2020-04-06.
!!
!! @par Copyright and License
!!
!! This code is subject to the DWD and MPI-M-Software-License-Agreement in
!! its most recent form.
!! Please see the file LICENSE in the root of the source tree for this code.
!! Where software is supplied by third parties, it is indicated in the
!! headers of the routines.
!!
MODULE mo_cloud_two_types

  USE mo_kind, ONLY: wp

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: t_cloud_two_config, t_cloud_two_input, t_cloud_two_output

  !-----------------------------------------------------------------------------
  
  TYPE t_cloud_two_config
     !
     ! configuration parameters
     ! ------------------------
     ! no parameters available.....
     !
     ! thresholds
     !
     ! grid scale microphysics
     !
  END TYPE t_cloud_two_config

  !-----------------------------------------------------------------------------
  
  TYPE t_cloud_two_input
     !
     ! Input arguments: parameters
     ! ---------------------------
     !
     INTEGER , POINTER :: jcs       (:,  :)=>NULL() !< column start index
     INTEGER , POINTER :: jce       (:,  :)=>NULL() !< column end   index
     INTEGER , POINTER :: msg_level (:,  :)=>NULL() !< message level
     REAL(wp), POINTER :: pdtime    (:,  :)=>NULL() !< [s] physics time step
     !
     ! Input arguments: fields
     ! -----------------------
     !
     ! atmospheric state
     REAL(wp), POINTER :: dz        (:,:,:)=>NULL() !< [m]      cell thickness
     REAL(wp), POINTER :: zh        (:,:,:)=>NULL() !< [m]      height of half levels
     REAL(wp), POINTER :: rho       (:,:,:)=>NULL() !< [kg/m3]  air density
     REAL(wp), POINTER :: pf        (:,:,:)=>NULL() !< [Pa]     air pressure
     REAL(wp), POINTER :: cpair     (:,:,:)=>NULL() !< [J/K/kg] specific heat of air
!inout
     REAL(wp), POINTER :: qv        (:,:,:)=>NULL() !< [kg/kg]  specific humidity
     REAL(wp), POINTER :: qc        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud water in air
     REAL(wp), POINTER :: qnc       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud droplet number
     REAL(wp), POINTER :: qr        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of rain        in air
     REAL(wp), POINTER :: qnr       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of rain droplet number
     REAL(wp), POINTER :: qi        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud ice   in air
     REAL(wp), POINTER :: qni       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud ice number
     REAL(wp), POINTER :: qs        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of snow        in air
     REAL(wp), POINTER :: qns       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of snow number
     REAL(wp), POINTER :: qg        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of graupel     in air
     REAL(wp), POINTER :: qng       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of graupel number
     REAL(wp), POINTER :: qh        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of hail        in air
     REAL(wp), POINTER :: qnh       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of hail number
     REAL(wp), POINTER :: ninact    (:,:,:)=>NULL() !< [kg/kg]  activated ice nuclei
     REAL(wp), POINTER :: ta        (:,:,:)=>NULL() !< [K]      air temperature
     REAL(wp), POINTER :: w         (:,:,:)=>NULL() !<          w
     ! fluxes at the surface
     REAL(wp), POINTER :: pr_rain     (:,  :)=>NULL() !< [kg/m2/s] sfc rain    flux
     REAL(wp), POINTER :: pr_ice      (:,  :)=>NULL() !< [kg/m2/s] sfc ice     flux
     REAL(wp), POINTER :: pr_snow     (:,  :)=>NULL() !< [kg/m2/s] sfc snow    flux
     REAL(wp), POINTER :: pr_grpl     (:,  :)=>NULL() !< [kg/m2/s] sfc graupel flux
     REAL(wp), POINTER :: pr_hail     (:,  :)=>NULL() !< [kg/m2/s] sfc hail    flux
     !
     !
  END TYPE t_cloud_two_input

  !-----------------------------------------------------------------------------
  
  TYPE t_cloud_two_output
     !
     ! Output arguments: fields
     ! ------------------------
     !
     ! tendencies in the atmosphere
     REAL(wp), POINTER :: tend_ta_two  (:,:,:)=>NULL() !< [K/s] tendency of temperature (cp)
     REAL(wp), POINTER :: tend_qv_two  (:,:,:)=>NULL() !< [1/s] tendency of specific humidity
     REAL(wp), POINTER :: tend_qc_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of cloud water
     REAL(wp), POINTER :: tend_qnc_two (:,:,:)=>NULL() !< [1/s] tendency of number of cloud water
     REAL(wp), POINTER :: tend_qi_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of cloud ice
     REAL(wp), POINTER :: tend_qni_two (:,:,:)=>NULL() !< [1/s] tendency of number of cloud ice
     REAL(wp), POINTER :: tend_qr_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of rain
     REAL(wp), POINTER :: tend_qnr_two (:,:,:)=>NULL() !< [1/s] tendency of number of rain
     REAL(wp), POINTER :: tend_qs_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of snow
     REAL(wp), POINTER :: tend_qns_two (:,:,:)=>NULL() !< [1/s] tendency of number of snow
     REAL(wp), POINTER :: tend_qg_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of graupel
     REAL(wp), POINTER :: tend_qng_two (:,:,:)=>NULL() !< [1/s] tendency of number of graupel
     REAL(wp), POINTER :: tend_qh_two  (:,:,:)=>NULL() !< [1/s] tendency of mass fraction of hail
     REAL(wp), POINTER :: tend_qnh_two (:,:,:)=>NULL() !< [1/s] tendency of number of hail
     REAL(wp), POINTER :: tend_ninact_two  (:,:,:)=>NULL() !< [1/s] tendency of activated ice nuclei

!inout
     REAL(wp), POINTER :: qv        (:,:,:)=>NULL() !< [kg/kg]  specific humidity
     REAL(wp), POINTER :: qc        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud water in air
     REAL(wp), POINTER :: qnc       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud droplet number
     REAL(wp), POINTER :: qr        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of rain        in air
     REAL(wp), POINTER :: qnr       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of rain droplet number
     REAL(wp), POINTER :: qi        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud ice   in air
     REAL(wp), POINTER :: qni       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of cloud ice number
     REAL(wp), POINTER :: qs        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of snow        in air
     REAL(wp), POINTER :: qns       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of snow number
     REAL(wp), POINTER :: qg        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of graupel     in air
     REAL(wp), POINTER :: qng       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of graupel number
     REAL(wp), POINTER :: qh        (:,:,:)=>NULL() !< [kg/kg]  mass fraction of hail        in air
     REAL(wp), POINTER :: qnh       (:,:,:)=>NULL() !< [kg/kg]  mass fraction of hail number
     REAL(wp), POINTER :: ninact    (:,:,:)=>NULL() !< [kg/kg]  activated ice nuclei
     REAL(wp), POINTER :: ta        (:,:,:)=>NULL() !< [K]      air temperature
     REAL(wp), POINTER :: w         (:,:,:)=>NULL() !<          w
     !
!inout
     ! fluxes at the surface
     REAL(wp), POINTER :: pr_rain     (:,  :)=>NULL() !< [kg/m2/s] sfc rain    flux
     REAL(wp), POINTER :: pr_ice      (:,  :)=>NULL() !< [kg/m2/s] sfc ice     flux
     REAL(wp), POINTER :: pr_snow     (:,  :)=>NULL() !< [kg/m2/s] sfc snow    flux
     REAL(wp), POINTER :: pr_grpl     (:,  :)=>NULL() !< [kg/m2/s] sfc graupel flux
     REAL(wp), POINTER :: pr_hail     (:,  :)=>NULL() !< [kg/m2/s] sfc hail    flux
     !
  END TYPE t_cloud_two_output

  !-----------------------------------------------------------------------------
  
END MODULE mo_cloud_two_types
#define __ATOMIC_ACQUIRE 2
#define __CHAR_BIT__ 8
#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __ORDER_PDP_ENDIAN__ 3412
#define __GFC_REAL_10__ 1
#define __FINITE_MATH_ONLY__ 0
#define __GNUC_PATCHLEVEL__ 0
#define __GFC_INT_2__ 1
#define __ICON__ 1
#define __SIZEOF_INT__ 4
#define __SIZEOF_POINTER__ 8
#define __GFORTRAN__ 1
#define __GFC_REAL_16__ 1
#define __STDC_HOSTED__ 0
#define __NO_MATH_ERRNO__ 1
#define __SIZEOF_FLOAT__ 4
#define __pic__ 2
#define _LANGUAGE_FORTRAN 1
#define __SIZEOF_LONG__ 8
#define __GFC_INT_8__ 1
#define __NO_ICON_TESTBED__ 1
#define __NO_ICON_WAVES__ 1
#define __SIZEOF_SHORT__ 2
#define __GNUC__ 11
#define __pie__ 2
#define __SIZEOF_LONG_DOUBLE__ 16
#define __LOOP_EXCHANGE 1
#define __BIGGEST_ALIGNMENT__ 16
#define __ATOMIC_RELAXED 0
#define _LP64 1
#define HAVE_FC_ATTRIBUTE_CONTIGUOUS 1
#define __GFC_INT_1__ 1
#define __ORDER_BIG_ENDIAN__ 4321
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __SIZEOF_SIZE_T__ 8
#define __PIC__ 2
#define __SIZEOF_DOUBLE__ 8
#define NOMPI 1
#define __ATOMIC_CONSUME 1
#define __GNUC_MINOR__ 1
#define __GFC_INT_16__ 1
#define __PIE__ 2
#define __LP64__ 1
#define __ATOMIC_SEQ_CST 5
#define __SIZEOF_LONG_LONG__ 8
#define __ATOMIC_ACQ_REL 4
#define __ATOMIC_RELEASE 3
#define __VERSION__ "11.1.0"

