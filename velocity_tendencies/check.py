import dace

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('optimized.sdfg')
sdfg.simplify()
# sdfg.validate()
# sdfg.compile()


# from dace.transformation.auto import auto_optimize as aopt

# opt_sdfg = aopt.auto_optimize(sdfg, dace.DeviceType.CPU)
