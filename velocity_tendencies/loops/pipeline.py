import dace
print('using dace:', dace.__file__)
from dace.transformation.interstate import LoopToMap, MapSplit, StateFusion, InlineMultistateSDFG, InlineSDFG
from dace.transformation.dataflow import MapCollapse, MapFusion, MapSymbolDefinition
from dace.sdfg.propagation import propagate_memlets_sdfg
from dace.transformation.pass_pipeline import Pipeline
from dace.transformation.passes.symbol_ssa import StrictSymbolSSA
from dace.transformation.passes.constant_propagation import ConstantPropagation
from dace.sdfg import nodes

def get_state(sdfg, label):
    for s in sdfg.nodes():
        if s.label == label:
            return s

sdfg = dace.sdfg.SDFG.from_file('/users/lfusco/dace_experiments/velocity_tendencies/loops/conditional.sdfgz')

#sdfg.save('base_ssa.sdfgz', compress=True)
propagate_memlets_sdfg(sdfg)
for i in range(2):
    applied = sdfg.apply_transformations_repeated([LoopToMap])
    print('applied:', applied)
    propagate_memlets_sdfg(sdfg)
    propagate_memlets_sdfg(sdfg)

sdfg.save('pipeline.sdfgz', compress=True)
sdfg = dace.sdfg.SDFG.from_file('pipeline.sdfgz')

sdfg.apply_transformations_repeated([LoopToMap], validate=False)
propagate_memlets_sdfg(sdfg)
sdfg.save('pipeline_before_map_split.sdfgz', compress=True)
# sdfg = dace.sdfg.SDFG.from_file('pipeline_before_map_split.sdfgz')
sdfg.apply_transformations_repeated([MapSplit], validate=False) # from now on simplify breaks things
propagate_memlets_sdfg(sdfg)
sdfg.apply_transformations_repeated([StateFusion], validate=False)
sdfg.save('pipeline.sdfgz', compress=True)
sdfg = dace.sdfg.SDFG.from_file('pipeline.sdfgz')

e = sdfg.out_edges(get_state(sdfg, 'single_state_body_4'))[0]
e.data.assignments['i_startblk_new'] = e.data.assignments['i_startblk']
del e.data.assignments['i_startblk']
e.data.assignments['i_endblk_new'] = e.data.assignments['i_endblk']
del e.data.assignments['i_endblk']
e.dst.replace('i_startblk', 'i_startblk_new')
e.dst.replace('i_endblk', 'i_endblk_new')

e = sdfg.out_edges(get_state(sdfg, '_state_l163_c163_0'))[0]
e.data.assignments['i_startblk_new_new'] = e.data.assignments['i_startblk']
del e.data.assignments['i_startblk']
e.data.assignments['i_endblk_new_new'] = e.data.assignments['i_endblk']
del e.data.assignments['i_endblk']
e.dst.replace('i_startblk', 'i_startblk_new_new')
e.dst.replace('i_endblk', 'i_endblk_new_new')

# ######### MOVE SOME ASSIGNMENTS TO A PRECEDING STATE IN ORDER TO FUSE SUBSEQUENT STATES ###################
from_state = get_state(sdfg, '_state_l163_c163_0')
to_state = get_state(sdfg, 'state_18')

for l in ['T_l163_c163', 'T_l164_c164']:
    to_remove = []
    cur = get_state(from_state, l)
    to_state.add_node(cur)
    while True:
        to_remove.append(cur)
        oe = from_state.out_edges(cur)
        if len(oe) > 0:
            oe = oe[0]
            to_state.add_node(oe.dst)
            to_state.add_edge(oe.src, oe.src_conn, oe.dst, oe.dst_conn, oe.data)
            cur = oe.dst
        else:
            break
    from_state.remove_nodes_from(to_remove)

sdfg.in_edges(from_state)[0].data.assignments['tmp_arg_5'] = sdfg.out_edges(from_state)[0].data.assignments['tmp_arg_5']
del sdfg.out_edges(from_state)[0].data.assignments['tmp_arg_5']

sdfg.start_block = sdfg.node_id(to_state)
# ###########################################################################################################

sdfg.apply_transformations_repeated([StateFusion], validate=False)

# sdfg = dace.sdfg.SDFG.from_file('pipeline_ssa.sdfgz')

# ######### MOVE INTERSTATE SYMBOL ASSIGNMENTS FROM NESTED SDFGS TO OUTER ###################################

state = get_state(sdfg, '_state_l163_c163_0')
target_edge = sdfg.in_edges(state)[0]
for n in state.nodes():
    if isinstance(n, nodes.NestedSDFG):
        start_block = n.sdfg.start_block
        oe = n.sdfg.out_edges(start_block)
        if len(oe) != 1:
            continue
        oe = oe[0]
        for name, data in oe.data.assignments.items():
            newname = sdfg.find_new_symbol(name)
            sdfg.symbols[newname] = n.sdfg.symbols[name]
            target_edge.data.assignments[newname] = oe.data.assignments[name]
            n.symbol_mapping[name] = newname
        oe.data.assignments = {}

# ###########################################################################################################

sdfg.apply_transformations_repeated([StateFusion, InlineSDFG], validate=False)
sdfg.apply_transformations_repeated([MapFusion, MapCollapse], validate=False)

sdfg = dace.sdfg.SDFG.from_file('pipeline_ssa_ultimate.sdfgz')

sdfg.apply_transformations_repeated([MapSymbolDefinition], validate=False)
sdfg.apply_transformations_repeated([StateFusion, InlineSDFG], validate=False)

sdfg.save('pipeline_ssa_ultimate_redefined.sdfgz', compress=True)
# sdfg = dace.sdfg.SDFG.from_file('pipeline_ssa_ultimate_redefined.sdfgz')

sdfg.compile(validate=False)