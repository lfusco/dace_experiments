import dace
from dace.transformation.interstate import InlineSDFG

N = dace.symbol('N', dace.int64)
K = dace.symbol('K', dace.int64)

@dace.program
def program(s: dace.int32, in_arr: dace.float64[N,N], arr: dace.float64[N,N]):
    for i, j in dace.map[0:N, 0:s]:
        arr[i,j] = in_arr[i,j]

sdfg = program.to_sdfg()
#sdfg.simplify()
sdfg.save('simple_loop.sdfg')
