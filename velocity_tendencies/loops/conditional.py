import dace
print('using dace:', dace.__file__)
from dace.transformation.interstate import ResolveCondition


sdfg = dace.sdfg.SDFG.from_file('velocity_tendencies_simplified.sdfgz')
sdfg.apply_transformations([ResolveCondition])
sdfg.simplify()
sdfg.save('conditional.sdfgz', compress=True)
