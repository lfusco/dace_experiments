import dace
from dace.dtypes import DataInstrumentationType

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.SDFG.from_file('icon_backup.sdfg')

with dace.builtin_hooks.instrument_data(DataInstrumentationType.Restore):
    sdfg()