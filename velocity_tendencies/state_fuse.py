import dace
from dace.sdfg.utils import fuse_states

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('optimized.sdfgz')
fused = fuse_states(sdfg)
print(f'fused {fused} states')
sdfg.simplify()
sdfg.save('optimized_fused.sdfgz', compress=True)