import dace

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('optimized_fused_gpu.sdfgz')

sdfg.compile()

