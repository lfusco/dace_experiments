import dace

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('velocity_tendencies_simplified.sdfgz')

from dace.transformation.auto import auto_optimize as aopt

sdfg = aopt.auto_optimize(sdfg, dace.DeviceType.CPU)
sdfg.simplify()
sdfg.save('optimized.sdfgz', compress=True)