import dace
from dace.sdfg.utils import fuse_states

dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('optimized_fused.sdfgz')

sdfg.apply_gpu_transformations()

sdfg.save('optimized_fused_gpu.sdfgz', compress=True)