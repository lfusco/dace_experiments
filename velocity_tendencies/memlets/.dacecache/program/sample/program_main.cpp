#include <cstdlib>
#include "../include/program.h"

int main(int argc, char **argv) {
    programHandle_t handle;
    int err;
    long long N = 42;
    bool flag = 42;
    long long * __restrict__ coord_arr = (long long*) calloc(N, sizeof(long long));
    double * __restrict__ in_arr = (double*) calloc(N, sizeof(double));
    double * __restrict__ out_arr = (double*) calloc(N, sizeof(double));


    handle = __dace_init_program(N);
    __program_program(handle, coord_arr, in_arr, out_arr, N, flag);
    err = __dace_exit_program(handle);

    free(coord_arr);
    free(in_arr);
    free(out_arr);


    return err;
}
