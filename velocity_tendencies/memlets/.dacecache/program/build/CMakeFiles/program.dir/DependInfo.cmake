# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/users/lfusco/dace_experiments/velocity_tendencies/memlets/.dacecache/program/src/cpu/program.cpp" "/users/lfusco/dace_experiments/velocity_tendencies/memlets/.dacecache/program/build/CMakeFiles/program.dir/users/lfusco/dace_experiments/velocity_tendencies/memlets/.dacecache/program/src/cpu/program.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DACE_BINARY_DIR=\"/users/lfusco/dace_experiments/velocity_tendencies/memlets/.dacecache/program/build\""
  "program_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/scratch/snx3000/lfusco/dace/dace/codegen/../runtime/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
