import dace
from dace.transformation.interstate import LoopToMap
from dace.sdfg.propagation import propagate_memlets_sdfg

N = dace.symbol('N', dace.int64)
K = dace.symbol('K', dace.int64)

@dace.program
def program(flag: dace.bool, in_arr: dace.float64[N,K], out_arr: dace.float64[N,K], coord_arr: dace.int64[N,K]):
    for i in range(N):
        if i == N-1:
            M = K
        else:
            M = K-1
        for j in range(K):
            coord = coord_arr[i, j]
            out_arr[i, j] = in_arr[coord, coord]

sdfg = program.to_sdfg()
sdfg.apply_transformations_repeated([LoopToMap])
sdfg.simplify()
propagate_memlets_sdfg(sdfg)
sdfg.apply_transformations_repeated([LoopToMap])
sdfg.simplify()
propagate_memlets_sdfg(sdfg)
propagate_memlets_sdfg(sdfg)
sdfg.save('memlet.sdfg')