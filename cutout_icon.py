import dace

sdfg = dace.sdfg.SDFG.from_file('tiny_icon.sdfg')
to_delete = [n for n in sdfg.nodes() if n.label != 'state_1_1']
sdfg.remove_nodes_from(to_delete)

sdfg.save('tiny_icon.sdfg')