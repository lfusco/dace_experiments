import sys
import dace

sdfg = dace.SDFG.from_file('program.sdfg')

from dace.transformation.interstate.state_replication import StateReplication

sdfg.apply_transformations(StateReplication, validate=False)

sdfg.save('program.sdfg')
sdfg = dace.SDFG.from_file('program.sdfg')

sdfg.validate()