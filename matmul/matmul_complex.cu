#include <mma.h>
#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>
#include <cudaTypedefs.h> // PFN_cuTensorMapEncodeTiled, CUtensorMap

using namespace nvcuda;

#define CEIL(x,y) ((x+y-1)/y)

/*
template <int N, int BLOCK_SIZE_X, int BLOCK_SIZE_Y, int CACHE_WIDTH, int BLOCK_TILE, int BLOCK_TILE_K, int COALESCING>
__global__ void matmul_tiled_coalesced_strided(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE_X;
    int by = blockIdx.y * BLOCK_SIZE_Y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE_Y*COALESCING][CACHE_WIDTH];
    __shared__ float local_B[CACHE_WIDTH][BLOCK_SIZE_X*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += CACHE_WIDTH) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[ty+cy*BLOCK_SIZE_Y][???] = A[(by + ty+cy*BLOCK_SIZE_Y) * N + kk + ???];
                local_B[???][tx+cx*BLOCK_SIZE_Y] = B[(kk + ???) * N + bx + tx+cx*BLOCK_SIZE_Y];
            }
        }
        __syncthreads();

        for (int k = 0; k < CACHE_WIDTH; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy*BLOCK_SIZE_Y][k] * local_B[k][tx+cx*BLOCK_SIZE_X];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE] = local_C[cy][cx];
        }
    }
}
*/

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_strided_halved(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE;
    int by = blockIdx.y * BLOCK_SIZE;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE];
    __shared__ float local_B[BLOCK_SIZE][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE) {
        #pragma unroll
        for (int c = 0; c < COALESCING; ++c) {
            local_A[ty+c*BLOCK_SIZE][tx] = A[(by + ty+c*BLOCK_SIZE) * N + kk + tx];
            local_B[ty][tx+c*BLOCK_SIZE] = B[(kk + ty) * N + bx + tx+c*BLOCK_SIZE];
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy*BLOCK_SIZE][k] * local_B[k][tx+cx*BLOCK_SIZE];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_strided_halved_async(float *A, float *B, float *C, const __grid_constant__ CUtensorMap A_map, const __grid_constant__ CUtensorMap B_map) {
    int bx = blockIdx.x * BLOCK_SIZE;
    int by = blockIdx.y * BLOCK_SIZE;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ alignas(128) float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE];
    __shared__ alignas(128) float local_B[BLOCK_SIZE][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    __shared__ barrier bar;

    if (threadIdx.x == 0) {
        init(&bar, blockDim.x);
        cde::fence_proxy_async_shared_cta();    
    }
    __syncthreads();

    for (int kk = 0; kk < N; kk += BLOCK_SIZE) {
        barrier::arrival_token token;
        if (threadIdx.x == 0) {
            cde::cp_async_bulk_tensor_2d_global_to_shared(&local_A, &A_map, kk + tx, by + ty, bar);
            cde::cp_async_bulk_tensor_2d_global_to_shared(&local_B, &A_map, bx + tx, kk + ty, bar);
            token = cuda::device::barrier_arrive_tx(bar, 1, sizeof(local_A) + sizeof(local_B));
        } else {
            token = bar.arrive();
        }
        // Wait for the data to have arrived.
        bar.wait(std::move(token));

        for (int k = 0; k < BLOCK_SIZE; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy*BLOCK_SIZE][k] * local_B[k][tx+cx*BLOCK_SIZE];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE] = local_C[cy][cx];
        }
    }
}

// assume launched 1D, with enough WARPS to cover the output matrix
// needs (N*N)/(16*16) warps -> 2*N*N/16 threads -> 2*N*N/(16*BLOCK_SIZE*BLOCK_SIZE) blocks
template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void wmma_simple(float *A, float *B, float *C) {
    // Tile using a 2D grid
    int wid = (BLOCK_SIZE * BLOCK_SIZE * blockIdx.x + threadIdx.x) / 32;
    int wx = wid % (N/16);
    int wy = wid / (N/16); 

    // Declare the fragments
    wmma::fragment<wmma::matrix_a, 16, 16, 8, wmma::precision::tf32, wmma::row_major> a_frag;
    wmma::fragment<wmma::matrix_b, 16, 16, 8, wmma::precision::tf32, wmma::row_major> b_frag;
    wmma::fragment<wmma::accumulator, 16, 16, 8, float> acc_frag;

    wmma::fill_fragment(acc_frag, 0.0f);

    for (int k = 0; k < N; k += 8) {
        wmma::load_matrix_sync(a_frag, A + k + wy * 16 * N, N);
        wmma::load_matrix_sync(b_frag, B + wx * 16 + k * N, N);
        wmma::mma_sync(acc_frag, a_frag, b_frag, acc_frag);
    }

    wmma::store_matrix_sync(C + wx * 16 + wy * 16 * N, acc_frag, N, wmma::mem_row_major);
}

// coalescing is performed at the WARP level (we do not control the single THREAD, so WARP is the smallest granularity)
// COALESCING is the coalescing factor per dimension
template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void wmma_coalesced(float *A, float *B, float *C) {
    // Tile using a 2D grid
    int wid = (BLOCK_SIZE * BLOCK_SIZE * blockIdx.x + threadIdx.x) / 32;
    int wx = wid % (N/16);
    int wy = wid / (N/16);

    // Declare the fragments. WARP level data!
    wmma::fragment<wmma::matrix_a, 16, 16, 8, wmma::precision::tf32, wmma::row_major> a_frag[COALESCING];
    wmma::fragment<wmma::matrix_b, 16, 16, 8, wmma::precision::tf32, wmma::row_major> b_frag[COALESCING];
    wmma::fragment<wmma::accumulator, 16, 16, 8, float> acc_frag[COALESCING][COALESCING];

    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            wmma::fill_fragment(acc_frag[cx][cy], 0.0f);
        }
    }

    for (int k = 0; k < N; k += 8) {
        #pragma unroll
        for (int c = 0; c < COALESCING; ++c) {
            wmma::load_matrix_sync(a_frag[c], A + k + (wy * COALESCING + c) * 16 * N, N);
            wmma::load_matrix_sync(b_frag[c], B + (wx * COALESCING + c) * 16 + k * N, N);
        }

        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                wmma::mma_sync(acc_frag[cy][cx], a_frag[cy], b_frag[cx], acc_frag[cy][cx]);
            }
        }
    }

    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            wmma::store_matrix_sync(C + (wx * COALESCING + cx) * 16 + (wy * COALESCING + cy) * 16 * N, acc_frag[cy][cx], N, wmma::mem_row_major);
        }
    }
}

// template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
// __global__ void wmma_tiled_coalesced(float *A, float *B, float *C) {
//     // Tile using a 2D grid
//     int wid = (BLOCK_SIZE * BLOCK_SIZE * blockIdx.x + threadIdx.x) / 32;
//     int tid = (BLOCK_SIZE * BLOCK_SIZE * blockIdx.x + threadIdx.x) % 32;
//     int wx = wid % (N/16);
//     int wy = wid / (N/16);

//     auto block = cooperative_groups::this_thread_block();

//     // Declare the fragments. WARP level data!
//     wmma::fragment<wmma::matrix_a, 16, 16, 8, wmma::precision::tf32, wmma::row_major> a_frag[COALESCING];
//     wmma::fragment<wmma::matrix_b, 16, 16, 8, wmma::precision::tf32, wmma::row_major> b_frag[COALESCING];
//     wmma::fragment<wmma::accumulator, 16, 16, 8, float> acc_frag[COALESCING][COALESCING];

//     constexpr int shmem_size = 16*COALESCING*BLOCK_TILE;
//     __shared__ float a_block[8][shmem_size];
//     __shared__ float b_block[shmem_size][8];

//     // out fragments initialization
//     #pragma unroll
//     for (int cy = 0; cy < COALESCING; ++cy) {
//         #pragma unroll
//         for (int cx = 0; cx < COALESCING; ++cx) {
//             wmma::fill_fragment(acc_frag[cx][cy], 0.0f);
//         }
//     }

//     for (int k = 0; k < N; k += 8) {
//         for (int b = 0; b < BLOCK_TILE; ++b) {
//             // divide well among warp. BLOCK_TILE is at least 2
//             for (int i = 0; i < 8; ++i) {
//                 cooperative_groups::memcpy_async(block, a_block, A + k, shmem_size);
//             }
//             cooperative_gropus::wait(block);
//         }

//         #pragma unroll
//         for (int c = 0; c < COALESCING; ++c) {
//             wmma::load_matrix_sync(a_frag[c], A + k + (wy * COALESCING + c) * 16 * N, N);
//             wmma::load_matrix_sync(b_frag[c], B + (wx * COALESCING + c) * 16 + k * N, N);
//         }

//         #pragma unroll
//         for (int cy = 0; cy < COALESCING; ++cy) {
//             #pragma unroll
//             for (int cx = 0; cx < COALESCING; ++cx) {
//                 wmma::mma_sync(acc_frag[cy][cx], a_frag[cy], b_frag[cx], acc_frag[cy][cx]);
//             }
//         }

//         block.sync();
//     }

//     #pragma unroll
//     for (int cy = 0; cy < COALESCING; ++cy) {
//         #pragma unroll
//         for (int cx = 0; cx < COALESCING; ++cx) {
//             wmma::store_matrix_sync(C + (wx * COALESCING + cx) * 16 + (wy * COALESCING + cy) * 16 * N, acc_frag[cy][cx], N, wmma::mem_row_major);
//         }
//     }
// }