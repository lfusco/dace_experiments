#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <cuda/barrier>
#include <cuda/std/utility>
#include <cublas_v2.h>
#include <cuda.h>
#include "matmul_complex.cu"

#include <cudaTypedefs.h> // PFN_cuTensorMapEncodeTiled, CUtensorMap

PFN_cuTensorMapEncodeTiled_v12000 get_cuTensorMapEncodeTiled() {
    // Get pointer to cuGetProcAddress
    cudaDriverEntryPointQueryResult driver_status;
    void* cuGetProcAddress_ptr = nullptr;
    cudaGetDriverEntryPoint("cuGetProcAddress", &cuGetProcAddress_ptr, cudaEnableDefault, &driver_status);
    assert(driver_status == cudaDriverEntryPointSuccess);
    PFN_cuGetProcAddress_v12000 cuGetProcAddress = reinterpret_cast<PFN_cuGetProcAddress_v12000>(cuGetProcAddress_ptr);

    // Use cuGetProcAddress to get a pointer to the CTK 12.0 version of cuTensorMapEncodeTiled
    CUdriverProcAddressQueryResult symbol_status;
    void* cuTensorMapEncodeTiled_ptr = nullptr;
    CUresult res = cuGetProcAddress("cuTensorMapEncodeTiled", &cuTensorMapEncodeTiled_ptr, 12000, CU_GET_PROC_ADDRESS_DEFAULT, &symbol_status);
    assert(res == CUDA_SUCCESS && symbol_status == CU_GET_PROC_ADDRESS_SUCCESS);

    return reinterpret_cast<PFN_cuTensorMapEncodeTiled_v12000>(cuTensorMapEncodeTiled_ptr);
}

// #define CHECK

float *C_check, *C_true;

cublasHandle_t handle;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_coalesced(float *A, float *B, float *C) {
    int j = (blockIdx.x * BLOCK_SIZE + threadIdx.x) * COALESCING;
    int i = (blockIdx.y * BLOCK_SIZE + threadIdx.y) * COALESCING;

    float tmp[COALESCING][COALESCING] = {0};
    for (int k = 0; k < N; ++k) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                tmp[cx][cy] += A[(i+cx)*N + k] * B[k*N + j+cy];
            }
        }
    }

    #pragma unroll
    for (int cx = 0; cx < COALESCING; ++cx) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            C[(i+cx)*N + j+cy] = tmp[cx][cy];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_coalesced_strided(float *A, float *B, float *C) {
    int j = (blockIdx.x * BLOCK_SIZE + threadIdx.x);
    int i = (blockIdx.y * BLOCK_SIZE + threadIdx.y);

    float tmp[COALESCING][COALESCING] = {0};
    for (int k = 0; k < N; ++k) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                tmp[cx][cy] += A[(i+cx*BLOCK_SIZE)*N + k] * B[k*N + j+cy*BLOCK_SIZE];
            }
        }
    }

    #pragma unroll
    for (int cx = 0; cx < COALESCING; ++cx) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            C[(i+cx*BLOCK_SIZE)*N + j+cy*BLOCK_SIZE] = tmp[cx][cy];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matreduce_A(float *A, float *B, float *C) {
    int j = (blockIdx.x * blockDim.x + threadIdx.x) * COALESCING;
    int i = (blockIdx.y * blockDim.y + threadIdx.y) * COALESCING;

    float tmp[COALESCING][COALESCING] = {0};
    for (int k = 0; k < N; ++k) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                tmp[cx][cy] += A[(i+cx)*N + k];
            }
        }
    }

    #pragma unroll
    for (int cx = 0; cx < COALESCING; ++cx) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            C[(i+cx)*N + j+cy] = tmp[cx][cy];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matreduce_B(float *A, float *B, float *C) {
    int j = (blockIdx.x * blockDim.x + threadIdx.x) * COALESCING;
    int i = (blockIdx.y * blockDim.y + threadIdx.y) * COALESCING;

    float tmp[COALESCING][COALESCING] = {0};
    for (int k = 0; k < N; ++k) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                tmp[cx][cy] += B[k*N + j+cy];
            }
        }
    }

    #pragma unroll
    for (int cx = 0; cx < COALESCING; ++cx) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            C[(i+cx)*N + j+cy] = tmp[cx][cy];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_coalesced_hor(float *A, float *B, float *C) {
    int j = (blockIdx.x * blockDim.x + threadIdx.x) * COALESCING * COALESCING;
    int i = (blockIdx.y * blockDim.y + threadIdx.y);

    float tmp[COALESCING*COALESCING] = {0};
    for (int k = 0; k < N; ++k) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING*COALESCING; ++cy) {
            tmp[cy] += A[i*N + k] * B[k*N + j + cy];
        }
    }

    #pragma unroll
    for (int cy = 0; cy < COALESCING*COALESCING; ++cy) {
        C[i*N + j + cy] = tmp[cy];
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void simple_matmul(float *A, float *B, float *C) {
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int i = blockIdx.y * blockDim.y + threadIdx.y;

    float tmp = 0;
    for (int k = 0; k < N; ++k) {
        
        tmp += A[i*N + k] * B[k*N + j];
    }
    C[i*N + j] = tmp;
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled(float *A, float *B, float *C) {
    int bx = blockIdx.x;
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ float local_B[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ float local_C[BLOCK_SIZE][BLOCK_SIZE];

    local_C[tx][ty] = 0;
    for (int kk = 0; kk < N; kk += BLOCK_SIZE) {
        local_A[tx][ty] = A[(kk + tx) * N + by * BLOCK_SIZE + ty];
        local_B[tx][ty] = B[(bx * BLOCK_SIZE + tx) * N + kk + ty];
        __syncthreads();
        for (int k = 0; k < BLOCK_SIZE; ++k) {
            local_C[tx][ty] += local_A[tx][k] * local_B[k][ty];
        }
        __syncthreads();
    }
    C[(bx * BLOCK_SIZE + tx) * N + by * BLOCK_SIZE + ty] = local_C[tx][ty];
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_wrong(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x * COALESCING;
    int ty = threadIdx.y * COALESCING;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                local_A[tx+cx][ty+cy] = A[(bx + tx+cx) * N + kk + ty+cy];
                local_B[tx+cx][ty+cy] = B[(kk + tx+cx) * N + by + ty+cy];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                #pragma unroll
                for (int cy = 0; cy < COALESCING; ++cy) {
                    local_C[cx][cy] += local_A[tx+cx][k] * local_B[k][ty+cy];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cx = 0; cx < COALESCING; ++cx) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            C[(bx + tx+cx) * N + by + ty+cy] = local_C[cx][cy];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x * COALESCING;
    int ty = threadIdx.y * COALESCING;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[ty+cy][tx+cx] = A[(by + ty+cy) * N + kk + tx+cx];
                local_B[ty+cy][tx+cx] = B[(kk + ty+cy) * N + bx + tx+cx];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy][k] * local_B[k][tx+cx];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy) * N + bx + tx+cx] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_strided(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE;
    int by = blockIdx.y * BLOCK_SIZE;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[ty+cy*BLOCK_SIZE][tx+cx*BLOCK_SIZE] = A[(by + ty+cy*BLOCK_SIZE) * N + kk + tx+cx*BLOCK_SIZE];
                local_B[ty+cy*BLOCK_SIZE][tx+cx*BLOCK_SIZE] = B[(kk + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy*BLOCK_SIZE][k] * local_B[k][tx+cx*BLOCK_SIZE];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_strided_transposed(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE;
    int by = blockIdx.y * BLOCK_SIZE;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[tx+cx*BLOCK_SIZE][ty+cy*BLOCK_SIZE] = A[(by + ty+cy*BLOCK_SIZE) * N + kk + tx+cx*BLOCK_SIZE];
                local_B[ty+cy*BLOCK_SIZE][tx+cx*BLOCK_SIZE] = B[(kk + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[k][ty+cy*BLOCK_SIZE] * local_B[k][tx+cx*BLOCK_SIZE];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy*BLOCK_SIZE) * N + bx + tx+cx*BLOCK_SIZE] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_strided_blocked(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE;
    int by = blockIdx.y * BLOCK_SIZE;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE*BLOCK_TILE*COALESCING][BLOCK_SIZE*BLOCK_TILE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*BLOCK_TILE*COALESCING][BLOCK_SIZE*BLOCK_TILE*COALESCING];
    __shared__ float local_C[BLOCK_SIZE*BLOCK_TILE*COALESCING][BLOCK_SIZE*BLOCK_TILE*COALESCING];
    float reg_C[COALESCING][COALESCING] = {0};

    // initialize local_C
    for (int bty = 0; bty < BLOCK_TILE; ++bty) {
        for (int btx = 0; btx < BLOCK_TILE; ++btx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE] = 0;
                }
            }
        }
    }

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        // load into shared memory
        for (int bty = 0; bty < BLOCK_TILE; ++bty) {
            for (int btx = 0; btx < BLOCK_TILE; ++btx) {
                #pragma unroll
                for (int cy = 0; cy < COALESCING; ++cy) {
                    #pragma unroll
                    for (int cx = 0; cx < COALESCING; ++cx) {
                        local_A[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE] = A[(by + ty+(bty*COALESCING+cy)*BLOCK_SIZE) * N + kk + tx+(btx*COALESCING+cx)*BLOCK_SIZE];
                        local_B[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE] = B[(kk + ty+(bty*COALESCING+cy)*BLOCK_SIZE) * N + bx + tx+(btx*COALESCING+cx)*BLOCK_SIZE];
                    }
                }
            }
        }
        __syncthreads();

        for (int bty = 0; bty < BLOCK_TILE; ++bty) {
            for (int btx = 0; btx < BLOCK_TILE; ++btx) {
                #pragma unroll
                for (int cy = 0; cy < COALESCING; ++cy) {
                    #pragma unroll
                    for (int cx = 0; cx < COALESCING; ++cx) {
                        reg_C[cy][cx] = local_C[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE];
                    }
                }
                for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
                    #pragma unroll
                    for (int cy = 0; cy < COALESCING; ++cy) {
                        #pragma unroll
                        for (int cx = 0; cx < COALESCING; ++cx) {
                            reg_C[cy][cx] += local_A[ty+(bty*COALESCING+cy)*BLOCK_SIZE][k] * local_B[k][tx+(btx*COALESCING+cx)*BLOCK_SIZE];
                        }
                    }
                }
                #pragma unroll
                for (int cy = 0; cy < COALESCING; ++cy) {
                    #pragma unroll
                    for (int cx = 0; cx < COALESCING; ++cx) {
                        local_C[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE] = reg_C[cy][cx];
                    }
                }
            }
        }
        __syncthreads();
    }

    for (int bty = 0; bty < BLOCK_TILE; ++bty) {
        for (int btx = 0; btx < BLOCK_TILE; ++btx) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    C[(by + ty+(bty*COALESCING+cy)*BLOCK_SIZE) * N + bx + tx+(btx*COALESCING+cx)*BLOCK_SIZE] = local_C[ty+(bty*COALESCING+cy)*BLOCK_SIZE][tx+(btx*COALESCING+cx)*BLOCK_SIZE];
                }
            }
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void greg(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    __shared__ float local_A[BLOCK_SIZE*COALESCING];
    __shared__ float local_B[BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; ++kk) {
        // first two stripes load the data
        if constexpr (COALESCING*2 <= BLOCK_SIZE) {
            if (ty < COALESCING) {
                local_B[ty*BLOCK_SIZE + tx] = B[kk * N + bx + ty*BLOCK_SIZE + tx];
            } else if (ty < 2*COALESCING) {
                local_A[(ty-COALESCING)*BLOCK_SIZE + tx] = A[(by + (ty-COALESCING)*BLOCK_SIZE + tx) * N + kk];
            }
        } else {
            if (ty == 0) {
                #pragma unroll
                for (int c = 0; c < COALESCING; ++c) {
                    local_B[c*BLOCK_SIZE + tx] = B[kk * N + bx + c*BLOCK_SIZE + tx];
                }
            } else if (ty == 1) {
                #pragma unroll
                for (int c = 0; c < COALESCING; ++c) {
                    local_A[c*BLOCK_SIZE + tx] = A[(by + c*BLOCK_SIZE + tx) * N + kk];
                }
            }
        }
        __syncthreads();

        // accumulate
        #pragma unroll 8
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll 8
            for (int cx = 0; cx < COALESCING; ++cx) {
               local_C[cy][cx] += local_A[cy*BLOCK_SIZE + ty] * local_B[cx*BLOCK_SIZE + tx];
            }
        }

        __syncthreads();

    }
    #pragma unroll 8
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll 8
        for (int cx = 0; cx < COALESCING; ++cx) {
           C[(by + cy*BLOCK_SIZE + ty) * N + bx + cx*BLOCK_SIZE + tx] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_just_A(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x * COALESCING;
    int ty = threadIdx.y * COALESCING;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[ty+cy][tx+cx] = A[(by + ty+cy) * N + kk + tx+cx];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy][k] * B[(kk + k) * N + bx + tx+cx];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy) * N + bx + tx+cx] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced_just_B(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x * COALESCING;
    int ty = threadIdx.y * COALESCING;

    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_B[ty+cy][tx+cx] = B[(kk + ty+cy) * N + bx + tx+cx];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += A[(by + ty+cy) * N + kk + k] * local_B[k][tx+cx];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy) * N + bx + tx+cx] = local_C[cy][cx];
        }
    }
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING>
__global__ void matmul_tiled_coalesced2(float *A, float *B, float *C) {
    int bx = blockIdx.x * BLOCK_SIZE*COALESCING;
    int by = blockIdx.y * BLOCK_SIZE*COALESCING;
    int tx = threadIdx.x * COALESCING;
    int ty = threadIdx.y * COALESCING;

    __shared__ float local_A[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING+1];
    __shared__ float local_B[BLOCK_SIZE*COALESCING][BLOCK_SIZE*COALESCING+1];
    float local_C[COALESCING][COALESCING] = {0};

    for (int kk = 0; kk < N; kk += BLOCK_SIZE*COALESCING) {
        #pragma unroll
        for (int cy = 0; cy < COALESCING; ++cy) {
            #pragma unroll
            for (int cx = 0; cx < COALESCING; ++cx) {
                local_A[ty+cy][tx+cx] = A[(by + ty+cy) * N + kk + tx+cx];
                local_B[ty+cy][tx+cx] = B[(kk + ty+cy) * N + bx + tx+cx];
            }
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE*COALESCING; ++k) {
            #pragma unroll
            for (int cy = 0; cy < COALESCING; ++cy) {
                #pragma unroll
                for (int cx = 0; cx < COALESCING; ++cx) {
                    local_C[cy][cx] += local_A[ty+cy][k] * local_B[k][tx+cx];
                }
            }
        }

        __syncthreads();

    }
    #pragma unroll
    for (int cy = 0; cy < COALESCING; ++cy) {
        #pragma unroll
        for (int cx = 0; cx < COALESCING; ++cx) {
            C[(by + ty+cy) * N + bx + tx+cx] = local_C[cy][cx];
        }
    }
}

template <int N>
void record_cublas(std::string filename, std::string name, float *A, float *B, float *C) {
    float time;
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    const float alpha = 1.0f;
    const float beta = 0.0f;

    cudaEventRecord(start);
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N, &alpha, A, N, B, N, &beta, C, N);
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));
    cudaEventElapsedTime(&time, start, end);

    FILE *file = fopen(filename.c_str(), "a");
    fprintf(file, "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f\n", "cublas", N, 0, 0, 0, 0, 0, 0, 0, 0, 0, time);

    cudaEventRecord(start);
    cublasGemmEx(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N, &alpha, A, CUDA_R_32F, N, B, CUDA_R_32F, N, &beta, C, CUDA_R_32F, N, CUBLAS_COMPUTE_32F_FAST_TF32, CUBLAS_GEMM_DEFAULT);
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));
    cudaEventElapsedTime(&time, start, end);

    cudaEventDestroy(start);
    cudaEventDestroy(end);

    fprintf(file, "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f\n", "cublas_fp32", N, 0, 0, 0, 0, 0, 0, 0, 0, 0, time);

    fclose(file);
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING, typename FUNC_T>
void record_time(std::string filename, std::string name, FUNC_T func, dim3 grid, dim3 block, float *A, float *B, float *C) {
    float time;
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    cudaFuncAttributes attr;
    cudaFuncGetAttributes(&attr, func);

    cudaEventRecord(start);
    func<<<grid, block>>>(A, B, C);
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));

    cudaEventElapsedTime(&time, start, end);

#ifdef CHECK
    cudaMemcpy(C_check, C, N*N*sizeof(float), cudaMemcpyDeviceToHost);

    for (int i = 0; i < N*N; ++i) {
        if (std::abs(C_check[i] - C_true[i]) > C_true[i]/100) {
            printf("ERROR: index %d, %f vs %f (%s<%d, %d, %d, %d>)\n", i, C_check[i], C_true[i], name.c_str(), N, BLOCK_SIZE, BLOCK_TILE, COALESCING);
            exit(-1);
        }
    }
#endif

    cudaEventDestroy(start);
    cudaEventDestroy(end);


    int active_blocks;
    cudaOccupancyMaxActiveBlocksPerMultiprocessor(&active_blocks, (void *) func, block.x * block.y, 0);

    FILE *file = fopen(filename.c_str(), "a");
    fprintf(file, "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f\n", name.c_str(), N, BLOCK_SIZE, BLOCK_TILE, COALESCING, attr.maxThreadsPerBlock, attr.numRegs, attr.sharedSizeBytes, attr.constSizeBytes, attr.localSizeBytes, active_blocks, time);
    fclose(file);
}

template <int N, int BLOCK_SIZE, int BLOCK_TILE, int COALESCING, typename FUNC_T>
void record_time_async(std::string filename, std::string name, FUNC_T func, dim3 grid, dim3 block, float *A, float *B, float *C) {
    float time;
    cudaEvent_t start, end;
    cudaEventCreate(&start);
    cudaEventCreate(&end);

    cudaFuncAttributes attr;
    cudaFuncGetAttributes(&attr, func);

    CUtensorMap tensor_map[2];
    // rank is the number of dimensions of the array.
    constexpr uint32_t rank = 2;
    uint64_t size[rank] = {N, N};
    // The stride is the number of bytes to traverse from the first element of one row to the next.
    // It must be a multiple of 16.
    uint64_t stride[rank - 1] = {N * sizeof(float)};
    // The box_size is the size of the shared memory buffer that is used as the
    // destination of a TMA transfer.
    uint32_t box_size[2][rank] = {{BLOCK_SIZE*COALESCING, BLOCK_SIZE}, {BLOCK_SIZE, BLOCK_SIZE*COALESCING}};
    // The distance between elements in units of sizeof(element). A stride of 2
    // can be used to load only the real component of a complex-valued tensor, for instance.
    uint32_t elem_stride[rank] = {1, 1};

    // Get a function pointer to the cuTensorMapEncodeTiled driver API.
    auto cuTensorMapEncodeTiled = get_cuTensorMapEncodeTiled();

    // Create the tensor descriptor.
    CUresult res = cuTensorMapEncodeTiled(
        &tensor_map[0],             // CUtensorMap *tensorMap,
        CUtensorMapDataType::CU_TENSOR_MAP_DATA_TYPE_FLOAT32,
        rank,                       // cuuint32_t tensorRank,
        A,                          // void *globalAddress,
        size,                       // const cuuint64_t *globalDim,
        stride,                     // const cuuint64_t *globalStrides,
        &box_size[0],                   // const cuuint32_t *boxDim,
        elem_stride,                // const cuuint32_t *elementStrides,
        // Interleave patterns can be used to accelerate loading of values that
        // are less than 4 bytes long.
        CUtensorMapInterleave::CU_TENSOR_MAP_INTERLEAVE_NONE,
        // Swizzling can be used to avoid shared memory bank conflicts.
        CUtensorMapSwizzle::CU_TENSOR_MAP_SWIZZLE_NONE,
        // L2 Promotion can be used to widen the effect of a cache-policy to a wider
        // set of L2 cache lines.
        CUtensorMapL2promotion::CU_TENSOR_MAP_L2_PROMOTION_NONE,
        // Any element that is outside of bounds will be set to zero by the TMA transfer.
        CUtensorMapFloatOOBfill::CU_TENSOR_MAP_FLOAT_OOB_FILL_NONE
    );

    res = cuTensorMapEncodeTiled(
        &tensor_map[1],             // CUtensorMap *tensorMap,
        CUtensorMapDataType::CU_TENSOR_MAP_DATA_TYPE_FLOAT32,
        rank,                       // cuuint32_t tensorRank,
        A,                          // void *globalAddress,
        size,                       // const cuuint64_t *globalDim,
        stride,                     // const cuuint64_t *globalStrides,
        &box_size[1],                   // const cuuint32_t *boxDim,
        elem_stride,                // const cuuint32_t *elementStrides,
        // Interleave patterns can be used to accelerate loading of values that
        // are less than 4 bytes long.
        CUtensorMapInterleave::CU_TENSOR_MAP_INTERLEAVE_NONE,
        // Swizzling can be used to avoid shared memory bank conflicts.
        CUtensorMapSwizzle::CU_TENSOR_MAP_SWIZZLE_NONE,
        // L2 Promotion can be used to widen the effect of a cache-policy to a wider
        // set of L2 cache lines.
        CUtensorMapL2promotion::CU_TENSOR_MAP_L2_PROMOTION_NONE,
        // Any element that is outside of bounds will be set to zero by the TMA transfer.
        CUtensorMapFloatOOBfill::CU_TENSOR_MAP_FLOAT_OOB_FILL_NONE
    );

    cudaEventRecord(start);
    func<<<grid, block>>>(A, B, C);
    gpuErrchk(cudaEventRecord(end));
    gpuErrchk(cudaEventSynchronize(end));

    cudaEventElapsedTime(&time, start, end);

#ifdef CHECK
    cudaMemcpy(C_check, C, N*N*sizeof(float), cudaMemcpyDeviceToHost);

    for (int i = 0; i < N*N; ++i) {
        if (std::abs(C_check[i] - C_true[i]) > C_true[i]/100) {
            printf("ERROR: index %d, %f vs %f (%s<%d, %d, %d, %d>)\n", i, C_check[i], C_true[i], name.c_str(), N, BLOCK_SIZE, BLOCK_TILE, COALESCING);
            exit(-1);
        }
    }
#endif

    cudaEventDestroy(start);
    cudaEventDestroy(end);


    int active_blocks;
    cudaOccupancyMaxActiveBlocksPerMultiprocessor(&active_blocks, (void *) func, block.x * block.y, 0);

    FILE *file = fopen(filename.c_str(), "a");
    fprintf(file, "%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f\n", name.c_str(), N, BLOCK_SIZE, BLOCK_TILE, COALESCING, attr.maxThreadsPerBlock, attr.numRegs, attr.sharedSizeBytes, attr.constSizeBytes, attr.localSizeBytes, active_blocks, time);
    fclose(file);
}

__global__ void tf32_kernel(float *v) {
    int tid = threadIdx.x + blockDim.x * blockIdx.x;

    v[tid] = wmma::__float_to_tf32(v[tid]);
}

int main() {
    cublasCreate(&handle);

    constexpr int N = 1 << 14;

    float *A, *B, *C, *A_TF32, *B_TF32;
    cudaMalloc(&A, N*N*sizeof(float));
    cudaMalloc(&B, N*N*sizeof(float));
    cudaMalloc(&A_TF32, N*N*sizeof(float));
    cudaMalloc(&B_TF32, N*N*sizeof(float));
    cudaMalloc(&C, N*N*sizeof(float));

#ifdef CHECK
    C_check = (float *) malloc(N*N*sizeof(float));
    C_true = (float *) malloc(N*N*sizeof(float));

    for (int i = 0; i < N*N; ++i) {
        C_check[i] = i;
    }
    cudaMemcpy(A, C_check, N*N*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(B, A, N*N*sizeof(float), cudaMemcpyDeviceToDevice);
    cudaMemcpy(A_TF32, A, N*N*sizeof(float), cudaMemcpyDeviceToDevice);
    cudaMemcpy(B_TF32, A, N*N*sizeof(float), cudaMemcpyDeviceToDevice);

    tf32_kernel<<<(N*N)/1024, 1024>>>(A_TF32);
    tf32_kernel<<<(N*N)/1024, 1024>>>(B_TF32);
    cudaDeviceSynchronize();

    // simple_matmul<N, 32, 1><<<dim3(N/32, N/32, 1), dim3(32, 32, 1)>>>(A, B, C);
    const float alpha = 1.0f;
    const float beta = 0.0f;
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N, &alpha, A, N, B, N, &beta, C, N);
    cudaDeviceSynchronize();

    cudaMemcpy(C_true, C, N*N*sizeof(float), cudaMemcpyDeviceToHost);
#endif

    {
        FILE *file = fopen("results.csv", "w");
        fprintf(file, "name,N,BLOCK_SIZE,BLOCK_TILE,COALESCING,attr.maxThreadsPerBlock,attr.numRegs,attr.sharedSizeBytes,attr.constSizeBytes,attr.localSizeBytes,active_blocks,time\n");
        fclose(file);
    }
    std::string file = "results.csv";
    
    record_cublas<N>(file, "cublas", A, B, C);

    #include "tests.h"

    return 0;
}