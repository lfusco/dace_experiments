#include <dace/dace.h>
typedef void * matmul_tilingHandle_t;
extern "C" matmul_tilingHandle_t __dace_init_matmul_tiling(long long N);
extern "C" int __dace_exit_matmul_tiling(matmul_tilingHandle_t handle);
extern "C" void __program_matmul_tiling(matmul_tilingHandle_t handle, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long N);
