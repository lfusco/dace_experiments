#include <cstdlib>
#include "../include/matmul_tiling.h"

int main(int argc, char **argv) {
    matmul_tilingHandle_t handle;
    int err;
    long long N = 42;
    double * __restrict__ A = (double*) calloc((N * N), sizeof(double));
    double * __restrict__ B = (double*) calloc((N * N), sizeof(double));
    double * __restrict__ C = (double*) calloc((N * N), sizeof(double));


    handle = __dace_init_matmul_tiling(N);
    __program_matmul_tiling(handle, A, B, C, N);
    err = __dace_exit_matmul_tiling(handle);

    free(A);
    free(B);
    free(C);


    return err;
}
