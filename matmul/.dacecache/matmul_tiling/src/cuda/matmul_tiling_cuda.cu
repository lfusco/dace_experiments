
#include <cuda_runtime.h>
#include <dace/dace.h>


struct matmul_tiling_state_t {
    dace::cuda::Context *gpu_context;
};



DACE_EXPORTED int __dace_init_cuda(matmul_tiling_state_t *__state, long long N);
DACE_EXPORTED int __dace_exit_cuda(matmul_tiling_state_t *__state);

DACE_DFI void matmul_tiling_23_4_24_8_25_12_27_16_0_0_19(const double&  __tmp_28_38_r, const double* __restrict__ __tmp_31_43_r_in_from_6_0, double*  __tmp_31_60_r_in_from_6_0, double&  __tmp_28_20_w, double&  __tmp_32_20_w) {

    {
        double __tmp_29_20_w;

        {
            double __inp = __tmp_28_38_r;
            double __out;

            ///////////////////
            // Tasklet code (assign_28_20)
            __out = __inp;
            ///////////////////

            __tmp_28_20_w = __out;
        }
        {
            double __out;

            ///////////////////
            // Tasklet code (assign_29_20)
            __out = 0;
            ///////////////////

            __tmp_29_20_w = __out;
        }
        {
            double __inp = __tmp_29_20_w;
            double __out;

            ///////////////////
            // Tasklet code (assign_32_20)
            __out = __inp;
            ///////////////////

            __tmp_32_20_w = __out;
        }

    }
    
}



int __dace_init_cuda(matmul_tiling_state_t *__state, long long N) {
    int count;

    // Check that we are able to run cuda code
    if (cudaGetDeviceCount(&count) != cudaSuccess)
    {
        printf("ERROR: GPU drivers are not configured or cuda-capable device "
               "not found\n");
        return 1;
    }
    if (count == 0)
    {
        printf("ERROR: No cuda-capable devices found\n");
        return 2;
    }

    // Initialize cuda before we run the application
    float *dev_X;
    DACE_GPU_CHECK(cudaMalloc((void **) &dev_X, 1));
    DACE_GPU_CHECK(cudaFree(dev_X));

    

    __state->gpu_context = new dace::cuda::Context(2, 2);

    // Create cuda streams and events
    for(int i = 0; i < 2; ++i) {
        DACE_GPU_CHECK(cudaStreamCreateWithFlags(&__state->gpu_context->internal_streams[i], cudaStreamNonBlocking));
        __state->gpu_context->streams[i] = __state->gpu_context->internal_streams[i]; // Allow for externals to modify streams
    }
    for(int i = 0; i < 2; ++i) {
        DACE_GPU_CHECK(cudaEventCreateWithFlags(&__state->gpu_context->events[i], cudaEventDisableTiming));
    }

    

    return 0;
}

int __dace_exit_cuda(matmul_tiling_state_t *__state) {
    

    // Synchronize and check for CUDA errors
    int __err = static_cast<int>(__state->gpu_context->lasterror);
    if (__err == 0)
        __err = static_cast<int>(cudaDeviceSynchronize());

    // Destroy cuda streams and events
    for(int i = 0; i < 2; ++i) {
        DACE_GPU_CHECK(cudaStreamDestroy(__state->gpu_context->internal_streams[i]));
    }
    for(int i = 0; i < 2; ++i) {
        DACE_GPU_CHECK(cudaEventDestroy(__state->gpu_context->events[i]));
    }

    delete __state->gpu_context;
    return __err;
}

DACE_EXPORTED bool __dace_gpu_set_stream(matmul_tiling_state_t *__state, int streamid, gpuStream_t stream)
{
    if (streamid < 0 || streamid >= 2)
        return false;

    __state->gpu_context->streams[streamid] = stream;

    return true;
}

DACE_EXPORTED void __dace_gpu_set_all_streams(matmul_tiling_state_t *__state, gpuStream_t stream)
{
    for (int i = 0; i < 2; ++i)
        __state->gpu_context->streams[i] = stream;
}

__global__ void __launch_bounds__(1024) matmul_tiling_23_0_0_8(const double * __restrict__ B, double * __restrict__ C, const double * __restrict__ local_A, double * __restrict__ local_B, long long N) {
    {
        {
            int by = blockIdx.x;
            int bx = blockIdx.y;
            {
                {
                    {
                        int ty = threadIdx.x;
                        int tx = threadIdx.y;
                        {
                            {
                                {
                                    for (auto ii = 0; ii < N; ii += 32) {
                                        {
                                            for (auto jj = 0; jj < N; jj += 32) {
                                                matmul_tiling_23_4_24_8_25_12_27_16_0_0_19(B[(((N * ((32 * bx) + tx)) + jj) + ty)], &local_A[(32 * tx)], &local_B[ty], local_B[((32 * tx) + ty)], C[(((N * ((32 * ii) + tx)) + (32 * jj)) + ty)]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


DACE_EXPORTED void __dace_runkernel_matmul_tiling_23_0_0_8(matmul_tiling_state_t *__state, const double * __restrict__ B, double * __restrict__ C, const double * __restrict__ local_A, double * __restrict__ local_B, long long N);
void __dace_runkernel_matmul_tiling_23_0_0_8(matmul_tiling_state_t *__state, const double * __restrict__ B, double * __restrict__ C, const double * __restrict__ local_A, double * __restrict__ local_B, long long N)
{

    if ((int_ceil((N / 32), 1)) == 0 || (int_ceil((N / 32), 1)) == 0) {

        return;
    }

    void  *matmul_tiling_23_0_0_8_args[] = { (void *)&B, (void *)&C, (void *)&local_A, (void *)&local_B, (void *)&N };
    gpuError_t __err = cudaLaunchKernel((void*)matmul_tiling_23_0_0_8, dim3(int_ceil((N / 32), 1), int_ceil((N / 32), 1), 1), dim3(32, 32, 1), matmul_tiling_23_0_0_8_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "matmul_tiling_23_0_0_8", int_ceil((N / 32), 1), int_ceil((N / 32), 1), 1, 32, 32, 1);
}

