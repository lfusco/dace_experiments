/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"

struct matmul_tiling_state_t {
    dace::cuda::Context *gpu_context;
};

DACE_EXPORTED void __dace_runkernel_matmul_tiling_23_0_0_8(matmul_tiling_state_t *__state, const double * __restrict__ B, double * __restrict__ C, const double * __restrict__ local_A, double * __restrict__ local_B, long long N);
void __program_matmul_tiling_internal(matmul_tiling_state_t*__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long N)
{

    {
        double *local_A;
        local_A = new double DACE_ALIGN(64)[1024];
        double *local_B;
        local_B = new double DACE_ALIGN(64)[1024];

        {
            #pragma omp parallel for
            for (auto __i0 = 0; __i0 < 32; __i0 += 1) {
                for (auto __i1 = 0; __i1 < 32; __i1 += 1) {
                    {
                        double __out;

                        ///////////////////
                        // Tasklet code (_numpy_full_)
                        __out = 0.0;
                        ///////////////////

                        local_A[((32 * __i0) + __i1)] = __out;
                    }
                }
            }
        }
        {
            #pragma omp parallel for
            for (auto __i0 = 0; __i0 < 32; __i0 += 1) {
                for (auto __i1 = 0; __i1 < 32; __i1 += 1) {
                    {
                        double __out;

                        ///////////////////
                        // Tasklet code (_numpy_full_)
                        __out = 0.0;
                        ///////////////////

                        local_B[((32 * __i0) + __i1)] = __out;
                    }
                }
            }
        }
        __dace_runkernel_matmul_tiling_23_0_0_8(__state, B, C, local_A, local_B, N);
        DACE_GPU_CHECK(cudaStreamSynchronize(__state->gpu_context->streams[0]));

        delete[] local_A;
        delete[] local_B;

    }
}

DACE_EXPORTED void __program_matmul_tiling(matmul_tiling_state_t *__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long N)
{
    __program_matmul_tiling_internal(__state, A, B, C, N);
}
DACE_EXPORTED int __dace_init_cuda(matmul_tiling_state_t *__state, long long N);
DACE_EXPORTED int __dace_exit_cuda(matmul_tiling_state_t *__state);

DACE_EXPORTED matmul_tiling_state_t *__dace_init_matmul_tiling(long long N)
{
    int __result = 0;
    matmul_tiling_state_t *__state = new matmul_tiling_state_t;


    __result |= __dace_init_cuda(__state, N);

    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED int __dace_exit_matmul_tiling(matmul_tiling_state_t *__state)
{
    int __err = 0;

    int __err_cuda = __dace_exit_cuda(__state);
    if (__err_cuda) {
        __err = __err_cuda;
    }
    delete __state;
    return __err;
}

