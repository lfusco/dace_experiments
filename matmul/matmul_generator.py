from itertools import product
from sys import stderr

N_list = [2**14]
BLOCK_SIZE_list = [8, 16, 32]
COALESCING_list = [1, 2, 4, 8, 16]

template = '        record_time<{0}, {1}, {2}, {3}>(file, "{4}", {4}<{0}, {1}, {2}, {3}>, dim3({0}/({1}*{2}*{3}), {0}/({1}*{2}*{3}), 1), dim3({1}, {1}, 1), A, B, C);'
template_ver = '        record_time<{0}, {1}, {2}>(file, "{3}_ver", {3}<{0}, {1}, {2}>, dim3({0}/{2}, {0}/({1}*{1}*{2}), 1), dim3(1, {1}*{1}, 1), A, B, C);'
template_hor = '        record_time<{0}, {1}, {2}>(file, "{3}_hor", {3}<{0}, {1}, {2}>, dim3({0}/({1}*{1}*{2}), {0}/{2}, 1), dim3({1}*{1}, 1, 1), A, B, C);'
template_wmma = '        record_time<{0}, {1}, 1, {2}>(file, "{3}", {3}<{0}, {1}, 1, {2}>, dim3(2*{0}*{0}/(16*{1}*{1}*{2}*{2}), 1, 1), dim3({1}*{1}, 1, 1), A_TF32, B_TF32, C);'

for N in N_list:
    for BLOCK_SIZE in BLOCK_SIZE_list:
        print(template_wmma.format(N, BLOCK_SIZE, 1, 'wmma_simple'))
        for COALESCING in COALESCING_list:
            print(template_wmma.format(N, BLOCK_SIZE, COALESCING, 'wmma_coalesced'))
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'greg'))
            
            print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_coalesced'))
            print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_coalesced_strided'))
            
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matreduce_A'))
            # print(template_hor.format(N, BLOCK_SIZE, 1, COALESCING, 'matreduce_A'))
            # print(template_ver.format(N, BLOCK_SIZE, 1, COALESCING, 'matreduce_A'))
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matreduce_B'))
            # print(template_hor.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_coalesced'))
            # guards
            # if BLOCK_SIZE == 32 and COALESCING >= 16:
            #     continue
            # if BLOCK_SIZE == 16 and COALESCING >= 16:
            #     continue
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_just_B'))
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_just_A'))
            # if BLOCK_SIZE == 32 and COALESCING >= 4:
            #     continue
            # if BLOCK_SIZE == 16 and COALESCING >= 8:
            #     continue
            # if BLOCK_SIZE == 8 and COALESCING >= 16:
            #     continue

            if (BLOCK_SIZE*BLOCK_SIZE*COALESCING)*2*4 > 220000:
                continue
            
            print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_strided_halved'))

            shmem = ((BLOCK_SIZE*COALESCING)**2)*2*4
            if shmem > 220000:
                continue
            
            print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced'))
            print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_strided'))
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_strided_transposed'))
            
            # for BLOCK_TILE in [1, 2, 4, 8]:
            #     if shmem*(3/2)*BLOCK_TILE*BLOCK_TILE > 167936:
            #         continue
            #     print(template.format(N, BLOCK_SIZE, BLOCK_TILE, COALESCING, 'matmul_tiled_coalesced_strided_blocked'))
            
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced_wrong'))
            # print(template.format(N, BLOCK_SIZE, 1, COALESCING, 'matmul_tiled_coalesced2'))
