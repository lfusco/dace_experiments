import dace
from dace.dtypes import ScheduleType, StorageType
import numpy as np
from dace.sdfg.analysis.schedule_tree.sdfg_to_tree import dealias_sdfg, remove_name_collisions
dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

N = dace.symbol('N', dace.int64)

@dace.program
def matmul(A: dace.float64[N, N], B: dace.float64[N, N], C: dace.float64[N, N]):
    for i in dace.map[0:N] @ ScheduleType.GPU_Device:
        for j in dace.map[0:N] @ ScheduleType.GPU_Device:
            for k in dace.map[0:N] @ ScheduleType.Sequential:
                C[i, j] += A[i, k] * B[k, j]

BLOCK_SIZE = 32

@dace.program
def matmul_tiling(A: dace.float64[N, N], B: dace.float64[N, N], C: dace.float64[N, N]):
    for bx, by in dace.map[0:N/BLOCK_SIZE, 0:N/BLOCK_SIZE] @ ScheduleType.GPU_Device:
        local_A: dace.float64[BLOCK_SIZE, BLOCK_SIZE] @ StorageType.GPU_Shared = np.zeros((BLOCK_SIZE, BLOCK_SIZE))
        local_B: dace.float64[BLOCK_SIZE, BLOCK_SIZE] @ StorageType.GPU_Shared = np.zeros((BLOCK_SIZE, BLOCK_SIZE))
        local_C: dace.float64[BLOCK_SIZE, BLOCK_SIZE] @ StorageType.GPU_Shared = np.zeros((BLOCK_SIZE, BLOCK_SIZE))
        for tx, ty in dace.map[0:BLOCK_SIZE, 0:BLOCK_SIZE] @ ScheduleType.GPU_ThreadBlock:
            for ii in dace.map[0:N:BLOCK_SIZE] @ ScheduleType.Sequential:
                local_A[tx, ty] = A[ii + tx, by * BLOCK_SIZE + ty]
                for jj in dace.map[0:N:BLOCK_SIZE] @ ScheduleType.Sequential:
                    local_B[tx, ty] = B[bx * BLOCK_SIZE + tx, jj + ty]
                    local_C[tx, ty] = 0
                    for k in dace.map[0:BLOCK_SIZE] @ ScheduleType.Sequential:
                        local_C[tx, ty] += local_A[tx, k] * local_B[k, ty]
                    C[ii * BLOCK_SIZE + tx, jj * BLOCK_SIZE + ty] = local_C[tx, ty]

sdfg = matmul.to_sdfg()
dealias_sdfg(sdfg)
remove_name_collisions(sdfg)
sdfg.save('matmul.sdfg')

sdfg = matmul_tiling.to_sdfg(simplify=False)
dealias_sdfg(sdfg)
remove_name_collisions(sdfg)
# sdfg.simplify()
sdfg.save('matmul_tiling.sdfg')

sdfg.compile()