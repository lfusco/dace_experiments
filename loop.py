import dace

N = dace.symbol('N', dace.int64)
K = dace.symbol('K', dace.int64)

@dace.program
def program(flag: dace.bool, arr: dace.float64[N,N]):
    for i in range(N):
        if i == N-1:
            M = 20
        else:
            M = N
        for j in range(M):
            arr[i,j] += 1
    # for i in range(N-1):
    #     for j in range(N):
    #         arr[i][j] += 1
    # for j in range(20):
    #     arr[N-1][j] += 1

sdfg = program.to_sdfg()
sdfg.save('loop.sdfg')