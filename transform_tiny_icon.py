import dace
from dace.transformation.interstate import MapSplit
dace.config.Config().set('compiler', 'cuda', 'backend', value='cuda')

sdfg = dace.sdfg.SDFG.from_file('tiny_icon.sdfg')

state = sdfg.nodes()[0]
map_entry, map_exit, nested_sdfg = list(state.nodes())[:3]

map_split = MapSplit()
map_split.map_entry = map_entry
map_split.nested_sdfg = nested_sdfg
map_split.map_exit = map_exit

map_split.apply(state, sdfg)
map_split.apply(state, sdfg)

# sdfg.simplify()

sdfg.save('transformed.sdfg')

sdfg.compile()