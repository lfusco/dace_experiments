import dace

N = dace.symbol('N', dace.int64)

@dace.program
def program(arr: dace.float64[2*N]):
    for i in dace.map[0:N]:
        if i == N-1:
            arr[i] *= 2
        else:
            arr[i] += 2

sdfg = program.to_sdfg()
sdfg.apply_gpu_transformations()
sdfg.save('map_split.sdfg')