#include <dace/dace.h>
typedef void * programHandle_t;
extern "C" programHandle_t __dace_init_program(long long K, long long N);
extern "C" int __dace_exit_program(programHandle_t handle);
extern "C" void __program_program(programHandle_t handle, long long * __restrict__ coord_arr, double * __restrict__ in_arr, double * __restrict__ out_arr, long long K, long long N, bool flag);
