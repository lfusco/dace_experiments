#include <cstdlib>
#include "../include/program.h"

int main(int argc, char **argv) {
    programHandle_t handle;
    int err;
    long long K = 42;
    long long N = 42;
    bool flag = 42;
    long long * __restrict__ coord_arr = (long long*) calloc((K * N), sizeof(long long));
    double * __restrict__ in_arr = (double*) calloc((K * N), sizeof(double));
    double * __restrict__ out_arr = (double*) calloc((K * N), sizeof(double));


    handle = __dace_init_program(K, N);
    __program_program(handle, coord_arr, in_arr, out_arr, K, N, flag);
    err = __dace_exit_program(handle);

    free(coord_arr);
    free(in_arr);
    free(out_arr);


    return err;
}
