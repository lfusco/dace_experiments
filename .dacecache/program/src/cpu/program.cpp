/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"

struct program_state_t {

};

inline void loop_body_1_1_0(program_state_t *__state, long long* __restrict__ coord_arr, double* __restrict__ in_arr, double* __restrict__ out_arr, long long K, long long i, long long j) {
    long long coord;


    coord = coord_arr[((K * i) + j)];
    {
        double __tmp2;


        dace::CopyND<double, 1, false, 1>::template ConstDst<1>::Copy(
        in_arr + ((K * coord) + coord), &__tmp2, 1);
        {
            double __inp = __tmp2;
            double __out;

            ///////////////////
            // Tasklet code (assign_17_12)
            __out = __inp;
            ///////////////////

            out_arr[((K * i) + j)] = __out;
        }

    }
    
}

inline void loop_body_0_0_0(program_state_t *__state, long long* __restrict__ coord_arr, double* __restrict__ in_arr, double* __restrict__ out_arr, long long K, long long N, long long i) {
    long long M;


    if ((i == (N - 1))) {

        M = K;

    } else {

        M = (K - 1);

    }
    {

        {
            for (auto j = 0; j < K; j += 1) {
                loop_body_1_1_0(__state, &coord_arr[0], &in_arr[0], &out_arr[0], K, i, j);
            }
        }

    }
    
}

void __program_program_internal(program_state_t*__state, long long * __restrict__ coord_arr, double * __restrict__ in_arr, double * __restrict__ out_arr, long long K, long long N, bool flag)
{

    {

        {
            #pragma omp parallel for
            for (auto i = 0; i < N; i += 1) {
                loop_body_0_0_0(__state, &coord_arr[0], &in_arr[0], &out_arr[0], K, N, i);
            }
        }

    }
}

DACE_EXPORTED void __program_program(program_state_t *__state, long long * __restrict__ coord_arr, double * __restrict__ in_arr, double * __restrict__ out_arr, long long K, long long N, bool flag)
{
    __program_program_internal(__state, coord_arr, in_arr, out_arr, K, N, flag);
}

DACE_EXPORTED program_state_t *__dace_init_program(long long * __restrict__ coord_arr, double * __restrict__ in_arr, double * __restrict__ out_arr, long long K, long long N, bool flag)
{
    int __result = 0;
    program_state_t *__state = new program_state_t;



    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED int __dace_exit_program(program_state_t *__state)
{
    int __err = 0;
    delete __state;
    return __err;
}

