
#include <cuda_runtime.h>
#include <dace/dace.h>


struct program_state_t {
    dace::cuda::Context *gpu_context;
};



DACE_EXPORTED int __dace_init_cuda(program_state_t *__state, long long N);
DACE_EXPORTED int __dace_exit_cuda(program_state_t *__state);

DACE_DFI void program_7_4_0_0_2(double * __restrict__ __tmp_9_12_w, long long N, long long i) {


    if ((i == (N - 1))) {
        {

            {
                double __out;

                ///////////////////
                // Tasklet code (assign_9_12)
                __out = 2;
                ///////////////////

                dace::wcr_fixed<dace::ReductionType::Product, double>::reduce_atomic(__tmp_9_12_w, __out);
            }

        }

    } else {
        {

            {
                double __out;

                ///////////////////
                // Tasklet code (assign_11_12)
                __out = 2;
                ///////////////////

                dace::wcr_fixed<dace::ReductionType::Sum, double>::reduce_atomic(__tmp_9_12_w, __out);
            }

        }

    }

    
}



int __dace_init_cuda(program_state_t *__state, long long N) {
    int count;

    // Check that we are able to run cuda code
    if (cudaGetDeviceCount(&count) != cudaSuccess)
    {
        printf("ERROR: GPU drivers are not configured or cuda-capable device "
               "not found\n");
        return 1;
    }
    if (count == 0)
    {
        printf("ERROR: No cuda-capable devices found\n");
        return 2;
    }

    // Initialize cuda before we run the application
    float *dev_X;
    DACE_GPU_CHECK(cudaMalloc((void **) &dev_X, 1));
    DACE_GPU_CHECK(cudaFree(dev_X));

    

    __state->gpu_context = new dace::cuda::Context(1, 1);

    // Create cuda streams and events
    for(int i = 0; i < 1; ++i) {
        DACE_GPU_CHECK(cudaStreamCreateWithFlags(&__state->gpu_context->internal_streams[i], cudaStreamNonBlocking));
        __state->gpu_context->streams[i] = __state->gpu_context->internal_streams[i]; // Allow for externals to modify streams
    }
    for(int i = 0; i < 1; ++i) {
        DACE_GPU_CHECK(cudaEventCreateWithFlags(&__state->gpu_context->events[i], cudaEventDisableTiming));
    }

    

    return 0;
}

int __dace_exit_cuda(program_state_t *__state) {
    

    // Synchronize and check for CUDA errors
    int __err = static_cast<int>(__state->gpu_context->lasterror);
    if (__err == 0)
        __err = static_cast<int>(cudaDeviceSynchronize());

    // Destroy cuda streams and events
    for(int i = 0; i < 1; ++i) {
        DACE_GPU_CHECK(cudaStreamDestroy(__state->gpu_context->internal_streams[i]));
    }
    for(int i = 0; i < 1; ++i) {
        DACE_GPU_CHECK(cudaEventDestroy(__state->gpu_context->events[i]));
    }

    delete __state->gpu_context;
    return __err;
}

DACE_EXPORTED bool __dace_gpu_set_stream(program_state_t *__state, int streamid, gpuStream_t stream)
{
    if (streamid < 0 || streamid >= 1)
        return false;

    __state->gpu_context->streams[streamid] = stream;

    return true;
}

DACE_EXPORTED void __dace_gpu_set_all_streams(program_state_t *__state, gpuStream_t stream)
{
    for (int i = 0; i < 1; ++i)
        __state->gpu_context->streams[i] = stream;
}

__global__ void __launch_bounds__(1) program_7_0_0_8(double * __restrict__ gpu_arr, int N) {
    {
        int tile_tile_i = (32 * blockIdx.x);
        {
            {
                int tile_i = ((128 * threadIdx.x) + tile_tile_i);
                if (tile_i >= tile_tile_i && tile_i < (Min((N - 1), (tile_tile_i + 31)) + 1)) {
                    {
                        for (auto i = tile_i; i < (Min((N - 1), (tile_i + 127)) + 1); i += 1) {
                            program_7_4_0_0_2(&gpu_arr[i], N, i);
                        }
                    }
                }
            }
        }
    }
}


DACE_EXPORTED void __dace_runkernel_program_7_0_0_8(program_state_t *__state, double * __restrict__ gpu_arr, int N);
void __dace_runkernel_program_7_0_0_8(program_state_t *__state, double * __restrict__ gpu_arr, int N)
{

    if ((int_ceil(N, 32)) == 0) {

        return;
    }

    void  *program_7_0_0_8_args[] = { (void *)&gpu_arr, (void *)&N };
    gpuError_t __err = cudaLaunchKernel((void*)program_7_0_0_8, dim3(int_ceil(N, 32), 1, 1), dim3(1, 1, 1), program_7_0_0_8_args, 0, __state->gpu_context->streams[0]);
    DACE_KERNEL_LAUNCH_CHECK(__err, "program_7_0_0_8", int_ceil(N, 32), 1, 1, 1, 1, 1);
}

